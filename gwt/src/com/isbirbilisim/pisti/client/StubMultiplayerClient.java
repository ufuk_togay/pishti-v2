package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.account.OnUserInfoSyncListener;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.multiplayer.ConnectionCallback;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClient;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.scores.Achievement;
import com.isbirbilisim.pisti.scores.Leaderboard;

import java.util.LinkedList;
import java.util.List;

/**
 * Stub implementation of {@link com.isbirbilisim.pisti.multiplayer.MultiplayerClient}.
 */
public class StubMultiplayerClient implements MultiplayerClient {

    private final List<ConnectionCallback> connectionCallbacks =
            new LinkedList<ConnectionCallback>();

    @Override
    public void signIn() {

    }

    @Override
    public void signOut() {

    }

    @Override
    public boolean isSignedIn() {
        return false;
    }

    @Override
    public void registerConnectionCallbacks(final ConnectionCallback callback) {
        connectionCallbacks.add(callback);
    }

    @Override
    public void unregisterConnectionCallbacks(final ConnectionCallback callback) {
        connectionCallbacks.remove(callback);
    }

    @Override
    public void clearConnectionCallbacks() {

    }

    @Override
    public void registerGameManagerDelegate(final GameManager instance) {

    }

    @Override
    public void unregisterGameManagerDelegate() {

    }

    @Override
    public void syncUserInfo(final UserInfo userInfo, final OnUserInfoSyncListener listener) {

    }

    @Override
    public void sendPlayerStateMessage(final PlayerState state) {

    }

    @Override
    public void quickGame(final GameSettings gameSettings, final int gameVariant) {

    }

    @Override
    public void playWithFriends(final GameSettings gameSettings) {

    }

    @Override
    public void inviteFriends() {

    }

    @Override
    public void sentMoveMessage(final CardModel cardModel) {

    }

    @Override
    public void endGame() {

    }

    @Override
    public void unlockAchievement(final Achievement achievement) {

    }

    @Override
    public void showLeaderboard(final Leaderboard leaderboard) {

    }
}