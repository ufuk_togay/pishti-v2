package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.resources.LocaleProvider;

import java.util.Locale;

/**
 * Implementation of LocaleProvider for GWT.
 */
public class GwtLocaleProvider implements LocaleProvider {

    @Override
    public String getLocaleIdentifier() {
        return java.util.Locale.getDefault().toString();
    }

    @Override
    public Locale getLocale() {
        return java.util.Locale.getDefault();
    }
}