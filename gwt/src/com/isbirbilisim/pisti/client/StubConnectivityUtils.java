package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;

/**
 * Stub implementation of ConnectivityUtils.
 */
public class StubConnectivityUtils implements ConnectivityUtils {

    @Override
    public boolean isNetworkAvailable() {
        return false;
    }
}