package com.isbirbilisim.pisti.client;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.screens.ScreenOrientation;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Desktop {@link com.isbirbilisim.pisti.screens.ScreenUtils} implementation.
 */
public class StubScreenUtils implements ScreenUtils {

    @Override
    public void setOrientation(final ScreenOrientation orientation) {
    }

    @Override
    public ScreenOrientation getOrientation() {
        if (Gdx.graphics.getHeight() > Gdx.graphics.getWidth()) {
            return ScreenOrientation.PORTRAIT;
        }
        return ScreenOrientation.LANDSCAPE;
    }

    @Override
    public int getScreenHeight() {
        return Gdx.graphics.getHeight();
    }

    @Override
    public int getScreenWidth() {
        return Gdx.graphics.getWidth();
    }

    @Override
    public float getVirtualWidth() {
        if (getOrientation() == ScreenOrientation.LANDSCAPE) {
            return VIRTUAL_SIDE;
        }

        return VIRTUAL_SIDE / (getScreenHeight() / (float) getScreenWidth());
    }

    @Override
    public float getVirtualHeight() {
        if (getOrientation() == ScreenOrientation.PORTRAIT) {
            return VIRTUAL_SIDE;
        }

        return VIRTUAL_SIDE / (getScreenWidth() / (float) getScreenHeight());
    }

    @Override
    public float toRealWidth(final float virtualWidth) {
        return virtualWidth * (getScreenWidth() / getVirtualWidth());
    }

    @Override
    public float toRealHeight(final float virtualHeight) {
        return virtualHeight * (getScreenHeight() / getVirtualHeight());
    }
}