package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.view.FacebookButton;
import com.isbirbilisim.pisti.view.ViewUtils;

public class StubViewUtils implements ViewUtils {

    private final FacebookButton facebookButton;

    public StubViewUtils() {
        facebookButton = new FacebookButton() {
        };
    }

    @Override
    public FacebookButton getFacebookSignInButton() {
        return facebookButton;
    }
}