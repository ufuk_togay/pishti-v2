package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.ComponentManager;
import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.ad.AdManager;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClient;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

/**
 * {@link com.isbirbilisim.pisti.ComponentManager} implementation for Desktop
 * project.
 */
public class GwtComponentManager implements ComponentManager {

    private final ScreenUtils screenUtils;
    private final MultiplayerClient stubMultiplayerClient;
    private final ViewUtils viewUtils;
    private final StubConnectivityUtils stubConnectivityUtils;
    private final AdManager adManager;
    private final AnalyticsManager analyticsManager;
    private final PlatformInfo platformInfo;

    public GwtComponentManager() {
        screenUtils = new StubScreenUtils();
        stubMultiplayerClient = new StubMultiplayerClient();
        viewUtils = new StubViewUtils();
        stubConnectivityUtils = new StubConnectivityUtils();
        adManager = new StubAdManager();
        analyticsManager = new StubAnalyticsManager();
        platformInfo = new GwtInfo();
    }

    @Override
    public ScreenUtils getScreenUtils() {
        return screenUtils;
    }

    @Override
    public MultiplayerClient getMultiplayerClient() {
        return stubMultiplayerClient;
    }

    @Override
    public ViewUtils getViewUtils() {
        return viewUtils;
    }

    @Override
    public ConnectivityUtils getConnectivityUtils() {
        return stubConnectivityUtils;
    }

    @Override
    public SignatureUtils getSignatureUtils() {
        return null;
    }

    @Override
    public AdManager getAdManager() {
        return adManager;
    }

    @Override
    public AnalyticsManager getAnalyticsManager() {
        return analyticsManager;
    }

    @Override
    public LocaleProvider getLocaleProvider() {
        return null;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return platformInfo;
    }
}