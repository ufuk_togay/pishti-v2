package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.ad.AdManager;

/**
 * Stub implementation of AdManager.
 */
public class StubAdManager implements AdManager {

    @Override
    public void showAds(final boolean show) {
        // Intents to stay empty.
    }

    @Override
    public void showInterstitialAd() {
        // Intents to stay empty.
    }
}