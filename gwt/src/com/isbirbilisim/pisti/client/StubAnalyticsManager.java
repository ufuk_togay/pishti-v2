package com.isbirbilisim.pisti.client;

import com.isbirbilisim.pisti.analytics.AnalyticsManager;

/**
 * Stub analytics manager.
 */
public class StubAnalyticsManager implements AnalyticsManager {

    @Override
    public void sendMenuScreenShownEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendGameScreenShownEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendSingleGameCompletedEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendMultiplayerGameCompletedEvent() {
        // Intents to stay empty.
    }
}