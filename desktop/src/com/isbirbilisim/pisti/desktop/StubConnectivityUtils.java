package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.connectivity.ConnectivityListener;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;

/**
 * Stub implementation of ConnectivityUtils.
 */
public class StubConnectivityUtils implements ConnectivityUtils {

    @Override
    public boolean isNetworkAvailable() {
        return true;
    }

    @Override
    public void addConnectivityListener(final ConnectivityListener listener) {

    }

    @Override
    public void removeConnectivityListener(final ConnectivityListener listener) {

    }
}