package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.PlatformInfo;

/**
 * Desktop info.
 */
public class DesktopInfo implements PlatformInfo {

    @Override
    public boolean isVibrationSupported() {
        return false;
    }
}