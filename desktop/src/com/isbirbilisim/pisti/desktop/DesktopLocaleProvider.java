package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.resources.LocaleProvider;

import java.util.Locale;

public class DesktopLocaleProvider implements LocaleProvider {

    @Override
    public String getLocaleIdentifier() {
        return java.util.Locale.getDefault().toLanguageTag();
    }

    @Override
    public Locale getLocale() {
        return java.util.Locale.getDefault();
    }
}