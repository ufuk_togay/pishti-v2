package com.isbirbilisim.pisti.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.isbirbilisim.pisti.Pisti;

public class DesktopLauncher {
    public static final int WIDTH = 960;
    public static final int HEIGHT = 540;

    public static void main(final String[] arg) {
        final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = WIDTH;
        config.height = HEIGHT;

        new LwjglApplication(new Pisti(new DesktopComponentManager()), config);
    }
}