package com.isbirbilisim.pisti.desktop;


import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookCallbacks;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookProfile;

import java.util.LinkedList;
import java.util.List;

public class DesktopFacebook implements Facebook {

    private final List<FacebookCallbacks> callbacks = new LinkedList<>();

    @Override
    public boolean isSignedIn() {
        return true;
    }

    @Override
    public void signIn() {
        for (final FacebookCallbacks callbacks : this.callbacks) {
            callbacks.onUserSignedIn();
        }
    }

    @Override
    public void signOut() {
        for (final FacebookCallbacks callbacks : this.callbacks) {
            callbacks.onUserSignedOut();
        }
    }

    @Override
    public void addFacebookCallbacks(final FacebookCallbacks callback) {
        callbacks.add(callback);
    }

    @Override
    public void removeFacebookCallbacks(final FacebookCallbacks callback) {
        callbacks.remove(callback);
    }

    @Override
    public void loadUserProfile() {
        final FacebookProfile profile = new FacebookProfile("DesktopGamer", "desktop_gamer_id");
        profile.setAccessToken("stub_access_token");

        for (final FacebookCallbacks callbacks : this.callbacks) {
            callbacks.onUserProfileReceived(profile);
        }
    }
}