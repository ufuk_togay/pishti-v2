package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.ComponentManager;
import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.ad.AdManager;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.backend.IBackendManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.desktop.backend.BackendManager;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

/**
 * {@link com.isbirbilisim.pisti.ComponentManager} implementation for Desktop
 * project.
 */
public class DesktopComponentManager implements ComponentManager {

    private final ScreenUtils screenUtils;
    private final ViewUtils viewUtils;
    private final StubConnectivityUtils stubConnectivityUtils;
    private final LocaleProvider localeProvider;
    private final DesktopAnalyticsManager desktopAnalyticsManager;
    private final PlatformInfo platformInfo;
    private final DesktopFacebook desktopFacebook;

    public DesktopComponentManager() {
        screenUtils = new DesktopScreenUtils();
        viewUtils = new DesktopViewUtils();
        stubConnectivityUtils = new StubConnectivityUtils();
        localeProvider = new DesktopLocaleProvider();
        desktopAnalyticsManager = new DesktopAnalyticsManager();
        platformInfo = new DesktopInfo();
        desktopFacebook = new DesktopFacebook();
    }

    @Override
    public ScreenUtils getScreenUtils() {
        return screenUtils;
    }

    @Override
    public ViewUtils getViewUtils() {
        return viewUtils;
    }

    @Override
    public ConnectivityUtils getConnectivityUtils() {
        return stubConnectivityUtils;
    }

    @Override
    public SignatureUtils getSignatureUtils() {
        return null;
    }

    @Override
    public AdManager getAdManager() {
        return new AdManager() {

            @Override
            public void showAds(final boolean show) {
                // This method intents to stay empty.
            }

            @Override
            public void showInterstitialAd() {
                // This method intents to stay empty.
            }
        };
    }

    @Override
    public AnalyticsManager getAnalyticsManager() {
        return desktopAnalyticsManager;
    }

    @Override
    public LocaleProvider getLocaleProvider() {
        return localeProvider;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return platformInfo;
    }

    @Override
    public Facebook getFacebook() {
        return desktopFacebook;
    }

    @Override
    public IBackendManager getBackendManager() {
        return BackendManager.getInstance();
    }
}