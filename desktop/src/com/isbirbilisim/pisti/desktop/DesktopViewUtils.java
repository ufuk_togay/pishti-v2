package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.view.FacebookButton;
import com.isbirbilisim.pisti.view.ViewUtils;

public class DesktopViewUtils implements ViewUtils {

    private final FacebookButton facebookButton;

    public DesktopViewUtils() {
        facebookButton = new FacebookButton() {
        };
    }

    @Override
    public FacebookButton getFacebookSignInButton() {
        return facebookButton;
    }
}