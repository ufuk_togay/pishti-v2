package com.isbirbilisim.pisti.desktop;

import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;

/**
 * Stub analytics manager.
 */
public class DesktopAnalyticsManager implements AnalyticsManager {

    @Override
    public void sendMenuScreenShownEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendGameScreenShownEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendSingleGameCompletedEvent() {
        // Intents to stay empty.
    }

    @Override
    public void sendMultiplayerGameCompletedEvent() {
        // Intents to stay empty.
    }
}