package com.isbirbilisim.pisti.android;

import android.content.Context;

import com.isbirbilisim.pisti.resources.LocaleProvider;

import java.util.Locale;

/**
 * Android implementation of LocaleProvider.
 */
public class AndroidLocaleProvider implements LocaleProvider {

    private final Context context;

    public AndroidLocaleProvider(final Context context) {
        this.context = context;
    }

    @Override
    public String getLocaleIdentifier() {
        return context.getResources().getConfiguration().locale.toString();
    }

    @Override
    public Locale getLocale() {
        return context.getResources().getConfiguration().locale;
    }
}