package com.isbirbilisim.pisti.android;

import com.isbirbilisim.pisti.PlatformInfo;

/**
 * Android info.
 */
public class AndroidInfo implements PlatformInfo {

    @Override
    public boolean isVibrationSupported() {
        return true;
    }
}