package com.isbirbilisim.pisti.android.activitydelegate;

/**
 * Interface for class, that keeps references to {@link com.isbirbilisim.pisti.android.activitydelegate.ActivityObserver}
 * observers, that need to be notified about {@link android.app.Activity} events.
 */
public interface ActivityObservable {

    /**
     * Adds observer to observer list.
     */
    public void subscribe(final ActivityObserver observer);

    /**
     * Removes observer from observer list.
     */
    public void unscribe(final ActivityObserver observer);
}