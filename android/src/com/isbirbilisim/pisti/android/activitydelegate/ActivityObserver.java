package com.isbirbilisim.pisti.android.activitydelegate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Interface that defines main {@link android.app.Activity} callbacks.
 */
public interface ActivityObserver {

    /**
     * Called when {@link android.app.Activity#onCreate(android.os.Bundle)} of
     * current activity is called.
     *
     * @param activity - current activity.
     */
    void onActivityCreate(final Bundle savedInstanceState,
                          final Activity activity);

    /**
     * Called when {@link android.app.Activity#onStart()} of current activity is
     * called.
     *
     * @param activity - current activity.
     */
    void onActivityStart(final Activity activity);

    /**
     * Called when {@link android.app.Activity#onResume()} of current activity is
     * called.
     *
     * @param activity - current activity.
     */
    void onActivityResume(final Activity activity);

    /**
     * Called when {@link android.app.Activity#onPause()} of current activity is
     * called.
     *
     * @param activity - current activity.
     */
    void onActivityPause(final Activity activity);

    /**
     * Called when {@link android.app.Activity#onStop()} of current activity is
     * called.
     *
     * @param activity - current activity.
     */
    void onActivityStop(final Activity activity);

    /**
     * Called when {@link android.app.Activity#onActivityResult(int, int, android.content.Intent)}
     * of current activity is called.
     *
     * @param activity - current activity.
     * @param request
     * @param response
     * @param data
     */
    void onActivityResult(final Activity activity,
                          final int request,
                          final int response,
                          final Intent data);

    /**
     * Called when {@link android.app.Activity#onDestroy()} of current activity
     * is called.
     */
    void onActivityDestroy(final Activity activity);

    /**
     * Called when {@link android.app.Activity#onSaveInstanceState(android.os.Bundle)}
     * of current activity is called.
     */
    void onActivitySaveInstanceState(final Bundle bundle);
}