package com.isbirbilisim.pisti.android;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;

import com.isbirbilisim.pisti.security.SignatureUtils;

/**
 * Android implementation of SignatureUtils.
 */
public class AndroidSignatureUtils implements SignatureUtils {

    private final Context context;

    public AndroidSignatureUtils(final Context context) {
        this.context = context;
    }

    @Override
    public String getAppSignature() {
        final Signature[] signatures;
        try {
            signatures = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(),
                            PackageManager.GET_SIGNATURES).signatures;
        } catch (final PackageManager.NameNotFoundException exception) {
            Log.e(getClass().getSimpleName(), "Can't retrieve app signature.");
            return null;
        }

        return signatures[0].toCharsString();
    }
}