package com.isbirbilisim.pisti.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.isbirbilisim.pisti.connectivity.ConnectivityListener;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObservable;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObserver;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Android implementation of ConnectivityUtils.
 */
public class AndroidConnectivityUtils implements ConnectivityUtils, ActivityObserver {

    private final Context context;
    private final List<ConnectivityListener> connectivityListeners;

    private final ConnectivityMonitor monitor;

    public AndroidConnectivityUtils(final Context context,
                                    final ActivityObservable observable) {
        this.context = context;

        observable.subscribe(this);

        connectivityListeners = new LinkedList<>();
        monitor = new ConnectivityMonitor(this);
    }

    @Override
    public boolean isNetworkAvailable() {
        final ConnectivityManager manager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void addConnectivityListener(final ConnectivityListener listener) {
        connectivityListeners.add(listener);
    }

    @Override
    public void removeConnectivityListener(final ConnectivityListener listener) {
        connectivityListeners.remove(listener);
    }

    private void notifyListeners(final boolean available) {
        for (final ConnectivityListener listener : connectivityListeners) {
            listener.onConnectivityChanged(available);
        }
    }

    @Override
    public void onActivityCreate(final Bundle savedInstanceState, final Activity activity) {

    }

    @Override
    public void onActivityStart(final Activity activity) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        context.registerReceiver(monitor, intentFilter);
    }

    @Override
    public void onActivityResume(final Activity activity) {

    }

    @Override
    public void onActivityPause(final Activity activity) {

    }

    @Override
    public void onActivityStop(final Activity activity) {
        context.unregisterReceiver(monitor);
    }

    @Override
    public void onActivityResult(final Activity activity, final int request, final int response, final Intent data) {

    }

    @Override
    public void onActivityDestroy(final Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(final Bundle bundle) {

    }

    private static class ConnectivityMonitor extends BroadcastReceiver {

        private final AndroidConnectivityUtils utils;

        private boolean lastState;

        private ConnectivityMonitor(final AndroidConnectivityUtils utils) {
            this.utils = utils;

            lastState = utils.isNetworkAvailable();
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final boolean state = utils.isNetworkAvailable();
            if (lastState != state) {
                utils.notifyListeners(state);
            }

            lastState = state;
        }
    }
}