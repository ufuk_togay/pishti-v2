package com.isbirbilisim.pisti.android;

import android.app.Activity;
import android.content.pm.ActivityInfo;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.screens.ScreenOrientation;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Android {@link ScreenUtils} implementation.
 */
public class AndroidScreenUtils implements ScreenUtils {
    private final Activity activity;

    public AndroidScreenUtils(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void setOrientation(final ScreenOrientation orientation) {
        switch (orientation) {
            case LANDSCAPE:
                setToLandscape();
                break;
            case PORTRAIT:
                setToPortrait();
                break;
        }
    }

    private void setToPortrait() {
        if (getOrientation() == ScreenOrientation.PORTRAIT) {
            return;
        }

        activity.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void setToLandscape() {
        if (getOrientation() == ScreenOrientation.LANDSCAPE) {
            return;
        }

        activity.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public ScreenOrientation getOrientation() {
        final int orientation = activity.getRequestedOrientation();
        switch (orientation) {
            case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                return ScreenOrientation.LANDSCAPE;
            case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                return ScreenOrientation.PORTRAIT;
        }

        return null;
    }

    @Override
    public int getScreenHeight() {
        final int width = Gdx.graphics.getWidth();
        final int height = Gdx.graphics.getHeight();

        if (getOrientation() == ScreenOrientation.PORTRAIT) {
            if (width > height) {
                return width;
            }
            return height;
        }

        if (width < height) {
            return width;
        }
        return height;
    }

    @Override
    public int getScreenWidth() {
        final int width = Gdx.graphics.getWidth();
        final int height = Gdx.graphics.getHeight();

        if (getOrientation() == ScreenOrientation.PORTRAIT) {
            if (width > height) {
                return height;
            }
            return width;
        }

        if (width < height) {
            return height;
        }
        return width;
    }

    @Override
    public float getVirtualWidth() {
        if (getOrientation() == ScreenOrientation.LANDSCAPE) {
            return VIRTUAL_SIDE;
        }

        return VIRTUAL_SIDE / (getScreenHeight() / (float) getScreenWidth());
    }

    @Override
    public float getVirtualHeight() {
        if (getOrientation() == ScreenOrientation.PORTRAIT) {
            return VIRTUAL_SIDE;
        }

        return VIRTUAL_SIDE / (getScreenWidth() / (float) getScreenHeight());
    }

    @Override
    public float toRealWidth(final float virtualWidth) {
        return virtualWidth * (getScreenWidth() / getVirtualWidth());
    }

    @Override
    public float toRealHeight(final float virtualHeight) {
        return virtualHeight * (getScreenHeight() / getVirtualHeight());
    }
}