package com.isbirbilisim.pisti.android;

import android.app.Application;

/**
 * Created by igor on 13.12.14.
 */
public class PistiApp extends Application {

    private static PistiApp instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static PistiApp getInstance() {
        return instance;
    }
}
