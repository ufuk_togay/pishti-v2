package com.isbirbilisim.pisti.android.view;

import android.app.Activity;

import com.isbirbilisim.pisti.android.AndroidActivity;
import com.isbirbilisim.pisti.view.FacebookButton;
import com.isbirbilisim.pisti.view.ViewUtils;

/**
 * Android implementation of ViewUtils.
 */
public class AndroidViewUtils implements ViewUtils {

    private final Activity activity;

    public AndroidViewUtils(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public FacebookButton getFacebookSignInButton() {
        return ((AndroidActivity) activity).getComponentManager().getFacebook()
                .getLoginButton();
    }
}