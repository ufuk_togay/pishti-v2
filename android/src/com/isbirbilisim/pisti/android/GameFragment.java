package com.isbirbilisim.pisti.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.isbirbilisim.pisti.Pisti;

/**
 * Fragment that contains libGDX game screen.
 */
public class GameFragment extends AndroidFragmentApplication {

    private AndroidActivity activity;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        this.activity = (AndroidActivity) activity;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             final ViewGroup container,
                             final Bundle savedInstanceState) {
        return initializeForView(new Pisti(activity.getComponentManager()), getConfiguration());
    }

    private AndroidApplicationConfiguration getConfiguration() {
        final AndroidApplicationConfiguration config =
                new AndroidApplicationConfiguration();
        config.hideStatusBar = true;
        config.useCompass = false;
        config.useAccelerometer = false;
        config.useWakelock = true;

        return config;
    }
}