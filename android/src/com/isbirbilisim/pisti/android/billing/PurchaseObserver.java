package com.isbirbilisim.pisti.android.billing;

import com.isbirbilisim.pisti.purchases.Item;

public interface PurchaseObserver {

    /**
     * Called after processing user purchase.
     *
     * @param item    - purchased item.
     * @param success - true if purchase was successfully verified.
     */
    public void onPurchaseResult(final Item item, final boolean success);
}