package com.isbirbilisim.pisti.android.billing;

import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.background.tasks.Task;
import com.isbirbilisim.pisti.purchases.Item;
import com.isbirbilisim.pisti.purchases.OnAvailableItemsUpdatesListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Task for requesting available for purchase products. Should be executed
 * using {@link com.isbirbilisim.pisti.background.BackgroundTasksManager}.
 */
public class RequestAvailableProductsTask implements Task {

    private final int inAppBillingVersionCode;
    private final String packageName;
    private final String purchaseType;
    private final Bundle querySkus;
    private final IInAppBillingService service;
    private final OnAvailableItemsUpdatesListener listener;

    public RequestAvailableProductsTask(
            final int inAppBillingVersionCode,
            final String packageName,
            final String purchaseType,
            final Bundle querySkus,
            final IInAppBillingService service,
            final OnAvailableItemsUpdatesListener listener) {
        this.inAppBillingVersionCode = inAppBillingVersionCode;
        this.packageName = packageName;
        this.purchaseType = purchaseType;
        this.querySkus = querySkus;
        this.service = service;
        this.listener = listener;
    }

    @Override
    public void execute() {
        Bundle skuDetails;
        try {
            skuDetails = service.getSkuDetails(inAppBillingVersionCode,
                    packageName,
                    purchaseType,
                    querySkus);
        } catch (final RemoteException exception) {
            skuDetails = null;
        }

        final List<Item> items = new ArrayList<Item>();

        if (skuDetails != null && skuDetails.getInt("RESPONSE_CODE") == 0) {
            final ArrayList<String> responseList = skuDetails
                    .getStringArrayList("DETAILS_LIST");

            try {
                for (final String response : responseList) {
                    final JSONObject object = new JSONObject(response);
                    final Item item = new Item(object.getString("productId"),
                            object.getString("type"),
                            object.getString("price"),
                            object.getString("price_amount_micros"),
                            object.getString("price_currency_code"),
                            object.getString("title"),
                            object.getString("description"));
                    items.add(item);
                }
            } catch (final JSONException exception) {
                Log.e("RequestAvailableProductsTask", exception.getMessage());
            }
        }

        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                listener.onAvailableItemsUpdated(items);
            }
        });
    }
}