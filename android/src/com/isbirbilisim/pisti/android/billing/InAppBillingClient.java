package com.isbirbilisim.pisti.android.billing;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObservable;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObserver;
import com.isbirbilisim.pisti.background.BackgroundTasksManager;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;
import com.isbirbilisim.pisti.background.tasks.VerifyPurchaseTask;
import com.isbirbilisim.pisti.purchases.Item;
import com.isbirbilisim.pisti.purchases.ItemPurchase;
import com.isbirbilisim.pisti.purchases.OnAvailableItemsUpdatesListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Client for making in-app purchases.
 */
public class InAppBillingClient implements ActivityObserver {

    public static final int PURCHASE_ITEM_REQUEST = 9012;

    public static final int IN_APP_BILLING_VERSION = 3;
    public static final String PURCHASE_TYPE_IN_APP = "inapp";

    private static final String GOLD_100 = "100Gold";
    private static final String GOLD_500 = "500Gold";
    private static final String GOLD_1000 = "1000Gold";
    private static final String GOLD_5000 = "5000Gold";
    private static final String GOLD_10000 = "10000Gold";
    private static final String GOLD_50000 = "50000Gold";
    private static final String GOLD_100000 = "100000Gold";

    private static final ArrayList<String> PRODUCT_LIST;

    static {
        PRODUCT_LIST = new ArrayList<String>();
        PRODUCT_LIST.add(GOLD_100);
        PRODUCT_LIST.add(GOLD_500);
        PRODUCT_LIST.add(GOLD_1000);
        PRODUCT_LIST.add(GOLD_5000);
        PRODUCT_LIST.add(GOLD_10000);
        PRODUCT_LIST.add(GOLD_50000);
        PRODUCT_LIST.add(GOLD_100000);
    }

    private final List<PurchaseObserver> purchaseResultObservers;
    private final String packageName;

    private ServiceConnection serviceConnection;
    private IInAppBillingService service;
    private Activity activity;

    private UserInfo userInfo;

    public InAppBillingClient(final ActivityObservable observable,
                              final String packageName) {
        this.packageName = packageName;
        this.purchaseResultObservers =
                new ArrayList<PurchaseObserver>();

        observable.subscribe(this);
    }

    @Override
    public void onActivityCreate(final Bundle savedInstanceState,
                                 final Activity activity) {
        this.activity = activity;

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(final ComponentName name,
                                           final IBinder service) {
                InAppBillingClient.this.service =
                        IInAppBillingService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(final ComponentName name) {
                service = null;
            }
        };

        activity.bindService(
                new Intent("com.android.vending.billing.InAppBillingService.BIND"),
                serviceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onActivityStart(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityResume(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityPause(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityStop(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onActivityResult(final Activity activity,
                                 final int request,
                                 final int response,
                                 final Intent data) {
        this.activity = activity;

        if (request == PURCHASE_ITEM_REQUEST) {
            final int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            final String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            final String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (response == Activity.RESULT_OK && responseCode == 0) {
                final ItemPurchase itemPurchase = parseItemPurchase(dataSignature,
                        purchaseData);
                if (itemPurchase == null) {
                    notifyObservers(null, false);
                } else {
                    // Consuming purchase and sending verification request to server.
                    consumePurchase(itemPurchase.getPurchaseToken());
                    sendPurchaseVerificationRequest(itemPurchase, userInfo.getFacebookId());
                }
            } else {
                notifyObservers(null, false);
            }
        }
    }

    private ItemPurchase parseItemPurchase(final String dataSignature,
                                           final String purchaseData) {
        try {
            final JSONObject jsonObject = new JSONObject(purchaseData);
            return new ItemPurchase(
                    dataSignature,
                    jsonObject.getString("orderId"),
                    jsonObject.getString("packageName"),
                    jsonObject.getString("productId"),
                    jsonObject.getString("purchaseTime"),
                    jsonObject.getString("purchaseState"),
                    jsonObject.getString("developerPayload"),
                    jsonObject.getString("purchaseToken")
            );
        } catch (final JSONException exception) {
            return null;
        }
    }

    private void sendPurchaseVerificationRequest(final ItemPurchase purchase,
                                                 final String userId) {
        final Json json = new Json();
        json.setOutputType(JsonWriter.OutputType.json);
        final String purchaseType = json.toJson(purchase, ItemPurchase.class);

        BackgroundTasksManager.getInstance().postTask(new VerifyPurchaseTask(
                purchaseType,
                userId,
                new OnNetworkTaskExecutedListener() {
                    @Override
                    public void onTaskCompleted(final String response) {
                        notifyObservers(null, !response.contains("ERROR"));
                    }

                    @Override
                    public void onTaskFailed() {
                        notifyObservers(null, false);
                    }
                }
        ));
    }

    /*
     * Consumes user's purchase, so user will be able to buy product again.
     */
    private void consumePurchase(final String purchaseToken) {
        if (service != null) {
            BackgroundTasksManager.getInstance().postTask(new ConsumePurchaseTask(
                    IN_APP_BILLING_VERSION,
                    packageName,
                    service,
                    purchaseToken
            ));
        }
    }

    @Override
    public void onActivityDestroy(final Activity activity) {
        this.activity = null;

        if (service != null) {
            activity.unbindService(serviceConnection);
        }
    }

    @Override
    public void onActivitySaveInstanceState(final Bundle bundle) {

    }

    /**
     * Adds observer to observer list.
     */
    public void addPurchaseResultObserver(final PurchaseObserver observer) {
        this.purchaseResultObservers.add(observer);
    }

    /**
     * Removes observer.
     */
    public void removePurchaseResultObserver(final PurchaseObserver observer) {
        this.purchaseResultObservers.remove(observer);
    }

    private void notifyObservers(final Item item, final boolean success) {
        for (final PurchaseObserver observer : purchaseResultObservers) {
            observer.onPurchaseResult(item, success);
        }
    }

    /**
     * Requests list of items, available for purchase.
     *
     * @param listener - result callback.
     */
    public void getAvailableItems(
            final OnAvailableItemsUpdatesListener listener) {
        if (service == null) {
            listener.onServiceUnavailable();
            return;
        }

        final Bundle bundle = new Bundle();
        bundle.putStringArrayList("ITEM_ID_LIST", PRODUCT_LIST);

        BackgroundTasksManager.getInstance().postTask(
                new RequestAvailableProductsTask(IN_APP_BILLING_VERSION,
                        packageName,
                        PURCHASE_TYPE_IN_APP,
                        bundle,
                        service,
                        listener));
    }

    /**
     * Starts purchase flow.
     *
     * @param item     - item that should be purchased.
     * @param listener - result callback.
     */
    public void purchaseItem(final UserInfo userInfo,
                             final Item item,
                             final PurchaseObserver listener) {
        if (service == null || activity == null) {
            this.userInfo = userInfo;

            listener.onPurchaseResult(item, false);
            return;
        }

        final Bundle buyIntentBundle;
        try {
            buyIntentBundle = service.getBuyIntent(
                    IN_APP_BILLING_VERSION,
                    packageName,
                    item.getProductId(),
                    PURCHASE_TYPE_IN_APP,
                    getPayload());
        } catch (final RemoteException exception) {
            listener.onPurchaseResult(item, false);
            return;
        }

        final PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");

        try {
            activity.startIntentSenderForResult(pendingIntent.getIntentSender(),
                    PURCHASE_ITEM_REQUEST,
                    new Intent(),
                    Integer.valueOf(0),
                    Integer.valueOf(0),
                    Integer.valueOf(0));
        } catch (final IntentSender.SendIntentException exception) {
            listener.onPurchaseResult(item, false);
        }
    }

    private String getPayload() {
        // TODO calculate payload at runtime;
        return "";
    }
}