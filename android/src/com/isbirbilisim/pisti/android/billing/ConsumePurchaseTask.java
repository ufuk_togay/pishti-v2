package com.isbirbilisim.pisti.android.billing;

import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;
import com.isbirbilisim.pisti.background.tasks.Task;

/**
 * Consumes user purchase.
 */
public class ConsumePurchaseTask implements Task {

    private final int inAppBillingVersionCode;
    private final String packageName;
    private final IInAppBillingService service;
    private final String purchaseToken;

    public ConsumePurchaseTask(final int inAppBillingVersionCode,
                               final String packageName,
                               final IInAppBillingService service,
                               final String purchaseToken) {
        this.inAppBillingVersionCode = inAppBillingVersionCode;
        this.packageName = packageName;
        this.service = service;
        this.purchaseToken = purchaseToken;
    }

    @Override
    public void execute() {
        try {
            /*
             * Ignoring operation result.
             */
            service.consumePurchase(inAppBillingVersionCode,
                    packageName,
                    purchaseToken);
        } catch (RemoteException exception) {
            exception.printStackTrace();
        }
    }
}