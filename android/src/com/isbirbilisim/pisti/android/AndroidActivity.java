package com.isbirbilisim.pisti.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.isbirbilisim.pisti.ComponentManager;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObservable;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObserver;

import java.util.LinkedList;
import java.util.List;

/**
 * Main activity of Android project.
 */
public class AndroidActivity extends FragmentActivity implements
        AndroidFragmentApplication.Callbacks,
        ActivityObservable {

    private final List<ActivityObserver> observers = new LinkedList<ActivityObserver>();

    private AndroidComponentManager componentManager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        componentManager = new AndroidComponentManager(this, this);

        super.onCreate(savedInstanceState);
        Log.d("Pisti", "onCreate");

        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_place, new GameFragment(), "main").commit();

        for (final ActivityObserver observer : observers) {
            observer.onActivityCreate(savedInstanceState, this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Pisti", "onStart");

        for (final ActivityObserver observer : observers) {
            observer.onActivityStart(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Pisti", "onResume");

        for (final ActivityObserver observer : observers) {
            observer.onActivityResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Pisti", "onPause");

        for (final ActivityObserver observer : observers) {
            observer.onActivityPause(this);
        }
    }

    @Override
    protected void onStop() {
        Log.d("Pisti", "onStop");
        super.onStop();

        for (final ActivityObserver observer : observers) {
            observer.onActivityStop(this);
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("Pisti", "onSaveInstanceState");

        for (final ActivityObserver observer : observers) {
            observer.onActivitySaveInstanceState(outState);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Pisti", "onDestroy");

        for (final ActivityObserver observer : observers) {
            observer.onActivityDestroy(this);
        }

        observers.clear();
    }

    @Override
    protected void onActivityResult(final int request, final int response, final Intent data) {
        Log.d("Pisti", "onActivityResult");

        for (final ActivityObserver observer : observers) {
            observer.onActivityResult(this, request, response, data);
        }
    }

    @Override
    public void subscribe(final ActivityObserver observer) {
        observers.add(observer);
    }

    @Override
    public void unscribe(final ActivityObserver observer) {
        observers.remove(observer);
    }

    public AndroidComponentManager getComponentManager() {
        return componentManager;
    }

    @Override
    public void exit() {

    }
}