package com.isbirbilisim.pisti.android;

import com.isbirbilisim.pisti.ad.AdManager;

/**
 * Temporary stub AdManager for Android.
 */
public class AndroidAdManager implements AdManager {

    @Override
    public void showAds(final boolean show) {

    }

    @Override
    public void showInterstitialAd() {

    }
}