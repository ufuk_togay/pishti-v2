package com.isbirbilisim.pisti.android;

import android.app.Activity;

import com.isbirbilisim.pisti.ComponentManager;
import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.ad.AdManager;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObservable;
import com.isbirbilisim.pisti.android.backend.BackendManager;
import com.isbirbilisim.pisti.android.billing.InAppBillingClient;
import com.isbirbilisim.pisti.android.facebook.FacebookClient;
import com.isbirbilisim.pisti.android.view.AndroidViewUtils;
import com.isbirbilisim.pisti.backend.IBackendManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

/**
 * {@link com.isbirbilisim.pisti.ComponentManager} implementation for android.
 */
public class AndroidComponentManager implements ComponentManager {

    private final AndroidSignatureUtils androidSignatureUtils;
    private final ScreenUtils screenUtils;
    private final FacebookClient facebookClient;
    private final AndroidViewUtils viewUtils;
    private final AndroidConnectivityUtils connectivityUtils;
    private final InAppBillingClient inAppBillingClient;
    private final AnalyticsManager analyticsManager;
    private final LocaleProvider localeProvider;
    private final AdManager adManager;
    private final PlatformInfo platformInfo;

    public AndroidComponentManager(final Activity activity,
                                   final ActivityObservable activityObservable) {
        this.androidSignatureUtils = new AndroidSignatureUtils(activity);
        this.screenUtils = new AndroidScreenUtils(activity);
        this.facebookClient = new FacebookClient(activity, activityObservable);
        this.viewUtils = new AndroidViewUtils(activity);
        this.connectivityUtils = new AndroidConnectivityUtils(activity, activityObservable);
        this.inAppBillingClient = new InAppBillingClient(activityObservable,
                activity.getPackageName());
        this.analyticsManager = new AndroidAnalyticsManager();
        this.localeProvider = new AndroidLocaleProvider(activity);
        this.adManager = new AndroidAdManager();
        this.platformInfo = new AndroidInfo();
    }

    @Override
    public ScreenUtils getScreenUtils() {
        return screenUtils;
    }

    @Override
    public ViewUtils getViewUtils() {
        return viewUtils;
    }

    @Override
    public ConnectivityUtils getConnectivityUtils() {
        return connectivityUtils;
    }

    @Override
    public SignatureUtils getSignatureUtils() {
        return androidSignatureUtils;
    }

    @Override
    public AdManager getAdManager() {
        return adManager;
    }

    @Override
    public AnalyticsManager getAnalyticsManager() {
        return analyticsManager;
    }

    @Override
    public LocaleProvider getLocaleProvider() {
        return localeProvider;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return platformInfo;
    }

    /**
     * @return - {@link com.isbirbilisim.pisti.android.facebook.FacebookClient}
     * instance.
     */
    public FacebookClient getFacebook() {
        return facebookClient;
    }

    @Override
    public IBackendManager getBackendManager() {
        return BackendManager.getInstance();
    }

    /**
     * @return - {@link com.isbirbilisim.pisti.android.billing.InAppBillingClient}
     * instance.
     */
    public InAppBillingClient getInAppBillingClient() {
        return inAppBillingClient;
    }
}