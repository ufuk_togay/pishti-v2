package com.isbirbilisim.pisti.android.facebook;

import android.content.Context;
import android.util.Log;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.LoginButton;
import com.isbirbilisim.pisti.view.FacebookButton;

/**
 * libGDX wrapper for {@link com.facebook.widget.LoginButton} button.
 */
public class GdxFacebookLoginButton extends FacebookButton {

    private final LoginButton button;

    public GdxFacebookLoginButton(final Context context) {
        button = new LoginButton(context);
        setPermissions();

        addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);
                button.performClick();
            }
        });
    }

    private void setPermissions() {
        button.setReadPermissions("user_friends", "public_profile");
    }

    /**
     * Starts sign-in flow.
     */
    public void startSignIn() {
        if (!isOpened()) {
            button.performClick();
        } else {
            Log.w("Facebook client", "Calling starSignIn() when user is already signed in!");
        }
    }

    /**
     * Signs-out user.
     */
    public void signOut() {
        if(isOpened()) {
            button.performClick();
        } else {
            Log.w("Facebook client", "Calling signOut() when user is already signed out!");
        }
    }

    private boolean isOpened() {
        return Session.getActiveSession() != null && Session.getActiveSession().isOpened();
    }

    /**
     * Notifies button about state change.
     */
    public void onStateChanged(final Session session,
                               final SessionState state,
                               final Exception exception) {
        if (state.isOpened()) {
            onStateChanged(State.OPENED);
        } else {
            onStateChanged(State.CLOSED);
        }
    }
}