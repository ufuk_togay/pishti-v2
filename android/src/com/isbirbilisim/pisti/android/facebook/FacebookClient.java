package com.isbirbilisim.pisti.android.facebook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.isbirbilisim.pisti.android.R;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObservable;
import com.isbirbilisim.pisti.android.activitydelegate.ActivityObserver;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookCallbacks;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookProfile;

import java.util.LinkedList;
import java.util.List;

public class FacebookClient implements ActivityObserver, Facebook, Session.StatusCallback {

    private static final String INVITABLE_FRIENDS = "/me/invitable_friends";
    private static final String ME = "/me";

    private final Activity activity;
    private final ActivityObservable activityObservable;
    private final List<FacebookCallbacks> facebookCallbacks;

    private final GdxFacebookLoginButton loginButton;
    private UiLifecycleHelper uiLifecycleHelper;


    private boolean signingIn;

    public FacebookClient(final Activity activity,
                          final ActivityObservable activityObservable) {
        this.activity = activity;
        this.activityObservable = activityObservable;
        activityObservable.subscribe(this);

        loginButton = new GdxFacebookLoginButton(activity);
        uiLifecycleHelper = new UiLifecycleHelper(activity, this);
        facebookCallbacks = new LinkedList<FacebookCallbacks>();

        signingIn = false;
    }

    @Override
    public void onActivityCreate(final Bundle savedInstanceState,
                                 final Activity activity) {
        uiLifecycleHelper.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityStart(final Activity activity) {

    }

    @Override
    public void onActivityResume(final Activity activity) {
        uiLifecycleHelper.onResume();
    }

    @Override
    public void onActivityPause(final Activity activity) {
        uiLifecycleHelper.onPause();
    }

    @Override
    public void onActivityStop(final Activity activity) {
        uiLifecycleHelper.onStop();
    }

    @Override
    public void onActivityResult(final Activity activity,
                                 final int request,
                                 final int response,
                                 final Intent data) {
        uiLifecycleHelper.onActivityResult(request, response, data);
    }

    @Override
    public void onActivityDestroy(final Activity activity) {
        uiLifecycleHelper.onDestroy();
    }

    @Override
    public void onActivitySaveInstanceState(final Bundle bundle) {
        uiLifecycleHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void call(final Session session, final SessionState state, final Exception exception) {
        loginButton.onStateChanged(session, state, exception);

        for (final FacebookCallbacks callback : facebookCallbacks) {
            if (state.isClosed()) {
                callback.onUserSignedOut();
                signingIn = false;
            } else if (state.isOpened()) {
                callback.onUserSignedIn();
                signingIn = false;
            } else {
                // opening
            }
        }
    }

    /**
     * @return - Facebook login button.
     */
    public GdxFacebookLoginButton getLoginButton() {
        return loginButton;
    }

    public void inviteFriends() {
        if (checkIfSigned()) {
            performInvitableFriendsRequest(Session.getActiveSession());
        }
    }

    private boolean checkIfSigned() {
        if (isOpened()) {
            return true;
        } else {
            showFacebookLoginDialog();
            return false;
        }
    }

    private boolean isOpened() {
        return Session.getActiveSession() != null && Session.getActiveSession().isOpened();
    }

    private void showFacebookLoginDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setTitle(R.string.dialog_facebook_signin_title)
                .setMessage(R.string.dialog_facebook_signin_message)
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog,
                                                final int which) {
                                dialog.cancel();
                            }
                        }
                )
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog,
                                                final int which) {
                                dialog.dismiss();
                                loginButton.startSignIn();
                            }
                        }
                );

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                builder.show();
            }
        });
    }

    private void performInvitableFriendsRequest(final Session openedSession) {
        final Request request = new Request(openedSession,
                INVITABLE_FRIENDS,
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    @Override
                    public void onCompleted(final Response response) {
                        processInvitableFriendsResponse(response);
                    }
                }
        );

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                request.executeAsync();
            }
        });
    }

    @Override
    public boolean isSignedIn() {
        return isOpened();
    }

    @Override
    public void signIn() {
        if (signingIn) {
            return;
        }

        signingIn = true;

        loginButton.startSignIn();
    }

    @Override
    public void signOut() {
        if (Session.getActiveSession() != null) {
            Session.getActiveSession().closeAndClearTokenInformation();
        }

        Session.setActiveSession(null);
    }

    @Override
    public void addFacebookCallbacks(final FacebookCallbacks callbacks) {
        facebookCallbacks.add(callbacks);
    }

    @Override
    public void removeFacebookCallbacks(final FacebookCallbacks callbacks) {
        facebookCallbacks.remove(callbacks);
    }

    @Override
    public void loadUserProfile() {
        final Request request = new Request(Session.getActiveSession(),
                ME,
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    @Override
                    public void onCompleted(final Response response) {
                        processUserProfileResponse(response);
                    }
                }
        );

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                request.executeAsync();
            }
        });
    }

    // Callback methods --------------------------------------------------------

    private void processInvitableFriendsResponse(final Response response) {
        final List<InvitableFriend> invitableFriends = InvitableFriend.parseInvitableFriendsList(response.getRawResponse());
    }

    private void processUserProfileResponse(final Response response) {
        final String rawResponse = response.getRawResponse();
        if (rawResponse == null) {
            for (final FacebookCallbacks callbacks : facebookCallbacks) {
                callbacks.onUserProfileRequestFailed();
            }
        } else {
            final FacebookProfile profile = new FacebookProfile(response.getRawResponse());
            profile.setAccessToken(Session.getActiveSession().getAccessToken());

            for (final FacebookCallbacks callbacks : facebookCallbacks) {
                callbacks.onUserProfileReceived(profile);
            }
        }
    }
}