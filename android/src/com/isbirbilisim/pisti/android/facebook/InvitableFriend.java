package com.isbirbilisim.pisti.android.facebook;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Entity that represent single item in invitable_friends request.
 */
public class InvitableFriend implements Json.Serializable {

    private String inviteId;
    private String name;
    private boolean isSilhouette; // if true it means that user has default profile picture.
    private String pictureUrl;

    public String getInviteId() {
        return inviteId;
    }

    public String getName() {
        return name;
    }

    public boolean isSilhouette() {
        return isSilhouette;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    @Override
    public void write(final Json json) {
        // Not needed since we will only received data from Facebook.
    }

    @Override
    public void read(final Json json, final JsonValue jsonData) {
        inviteId = jsonData.getString("id");
        name = jsonData.getString("name");

        final JsonValue pictureData = jsonData.get("picture").get("data");
        isSilhouette = pictureData.getBoolean("is_silhouette");
        pictureUrl = pictureData.getString("url");
    }

    public static List<InvitableFriend> parseInvitableFriendsList(
            final String data) {
        final JsonValue jsonValue = new JsonReader().parse(data);
        final JsonValue jsonList = jsonValue.get("data");

        final List<InvitableFriend> invitableFriends = new ArrayList<InvitableFriend>();

        final Iterator iterator = jsonList.iterator();
        while (iterator.hasNext()) {
            invitableFriends.add(new Json().fromJson(InvitableFriend.class,
                    iterator.next().toString()));
        }

        return invitableFriends;
    }
}