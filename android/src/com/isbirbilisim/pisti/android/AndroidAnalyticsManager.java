package com.isbirbilisim.pisti.android;

import com.isbirbilisim.pisti.analytics.AnalyticsManager;

import com.isbirbilisim.pisti.android.backend.BackendManager;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.game.GameResult;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.scores.Scores;

/**
 * Implementation of AnalyticsManager for Android.
 */
public class AndroidAnalyticsManager implements AnalyticsManager {



    public AndroidAnalyticsManager() {

    }

    @Override
    public void sendMenuScreenShownEvent() {
        // TODO
    }

    @Override
    public void sendGameScreenShownEvent() {
        // TODO
    }

    @Override
    public void sendSingleGameCompletedEvent() {
        // TODO
    }

    @Override
    public void sendMultiplayerGameCompletedEvent() {
        // TODO
    }

}