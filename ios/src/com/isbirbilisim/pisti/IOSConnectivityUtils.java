package com.isbirbilisim.pisti;

import com.isbirbilisim.pisti.connectivity.ConnectivityListener;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;

import org.robovm.apple.corefoundation.CFRunLoop;
import org.robovm.apple.systemconfiguration.SCNetworkReachability;
import org.robovm.apple.systemconfiguration.SCNetworkReachabilityFlags;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;

/**
 * Stub implementation of ConnectivityUtils.
 */
public class IOSConnectivityUtils implements ConnectivityUtils, SCNetworkReachability.ClientCallback {

    private final List<ConnectivityListener> connectivityListeners;

    private boolean lastWasReachable;

    public IOSConnectivityUtils() {
        final SCNetworkReachability reachability = SCNetworkReachability
                .create(new InetSocketAddress("74.125.224.115", 80));
        reachability.setCallback(this);

        final SCNetworkReachabilityFlags reachabilityFlags = reachability.getFlags();
        lastWasReachable = reachabilityFlags.contains(SCNetworkReachabilityFlags.Reachable);

        connectivityListeners = new LinkedList<>();

        reachability.schedule(CFRunLoop.getMain(), CFRunLoop.getCurrent().copyCurrentMode());
    }

    @Override
    public boolean isNetworkAvailable() {
        final SCNetworkReachability reachability = SCNetworkReachability
                .create(new InetSocketAddress("74.125.224.115", 80));
        final SCNetworkReachabilityFlags reachabilityFlags = reachability.getFlags();
        return reachabilityFlags.contains(SCNetworkReachabilityFlags.Reachable);
    }

    @Override
    public void addConnectivityListener(final ConnectivityListener listener) {
        connectivityListeners.add(listener);
    }

    @Override
    public void removeConnectivityListener(final ConnectivityListener listener) {
        connectivityListeners.remove(listener);
    }

    @Override
    public void invoke(final SCNetworkReachability target,
                       final SCNetworkReachabilityFlags flags) {
        final boolean reachable = flags.contains(SCNetworkReachabilityFlags.Reachable);

        if (reachable != lastWasReachable) {
            for (final ConnectivityListener listener : connectivityListeners) {
                listener.onConnectivityChanged(reachable);
            }
            lastWasReachable = reachable;
        }
    }
}