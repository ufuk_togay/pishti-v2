package com.isbirbilisim.pisti;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSPropertyList;
import org.robovm.apple.foundation.NSURL;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.bindings.facebook.manager.FacebookManager;

public class IOSLauncher extends IOSApplication.Delegate {

    private IOSComponentManager iosComponentManager;

    @Override
    protected IOSApplication createApplication() {
        final IOSApplicationConfiguration config = new IOSApplicationConfiguration();

        config.preventScreenDimming = true;
        config.orientationLandscape = true;
        config.orientationPortrait = false;
        config.useAccelerometer = false;
        config.useCompass = false;

        iosComponentManager = new IOSComponentManager();
        final IOSApplication iosApplication = new IOSApplication(new Pisti(iosComponentManager), config);

        ((IOSAdManager) iosComponentManager.getAdManager()).setIosApplication(iosApplication);

        return iosApplication;
    }

    public static void main(final String[] argv) {
        final NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.drain();
    }

    @Override
    public void didBecomeActive(final UIApplication application) {
        FacebookManager.getInstance().handleDidBecomeActive(application);
    }

    @Override
    public boolean openURL(final UIApplication application,
                           final NSURL url,
                           final String sourceApplication,
                           final NSPropertyList annotation) {
        return FacebookManager.getInstance().handleOpenURL(application, url, sourceApplication, annotation);
    }

    @Override
    public void willTerminate(final UIApplication application) {
        FacebookManager.getInstance().handleWillTerminate(application);
    }
}