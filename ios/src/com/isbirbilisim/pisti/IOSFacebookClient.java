package com.isbirbilisim.pisti;


import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookCallbacks;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookProfile;

import org.robovm.bindings.facebook.manager.FacebookError;
import org.robovm.bindings.facebook.manager.FacebookLoginListener;
import org.robovm.bindings.facebook.manager.FacebookManager;
import org.robovm.bindings.facebook.manager.FacebookPermission;
import org.robovm.bindings.facebook.manager.request.GraphUser;
import org.robovm.bindings.facebook.session.FBSession;

import java.util.LinkedList;
import java.util.List;

public class IOSFacebookClient implements Facebook {

    private final FacebookPermission[] permissions = new FacebookPermission[]{
            FacebookPermission.PUBLIC_PROFILE,
            FacebookPermission.USER_FRIENDS,
            FacebookPermission.EMAIL};

    private final List<FacebookCallbacks> callbacks = new LinkedList<>();

    private final FacebookLoginListenerImpl facebookLoginListener;

    private GraphUser lastGraphUser;

    public IOSFacebookClient() {
        facebookLoginListener = new FacebookLoginListenerImpl(this);
    }

    @Override
    public boolean isSignedIn() {
        return FacebookManager.getInstance().isLoggedIn();
    }

    @Override
    public void signIn() {
        FacebookManager.getInstance().login(permissions, true, facebookLoginListener);
    }

    @Override
    public void signOut() {
        FacebookManager.getInstance().logout();

        for (final FacebookCallbacks callbacks : this.callbacks) {
            callbacks.onUserSignedOut();
        }
    }

    @Override
    public void addFacebookCallbacks(final FacebookCallbacks callback) {
        callbacks.add(callback);
    }

    @Override
    public void removeFacebookCallbacks(final FacebookCallbacks callback) {
        callbacks.remove(callback);
    }

    @Override
    public void loadUserProfile() {
        final FacebookProfile facebookProfile = new FacebookProfile(lastGraphUser.getName(), lastGraphUser.getId());
        facebookProfile.setAccessToken(FBSession.getActiveSession().getAccessTokenData().getAccessToken());

        for (final FacebookCallbacks callback : callbacks) {
            callback.onUserProfileReceived(facebookProfile);
        }
    }

    private static class FacebookLoginListenerImpl implements FacebookLoginListener {

        private final IOSFacebookClient iosFacebookClient;

        private FacebookLoginListenerImpl(final IOSFacebookClient iosFacebookClient) {
            this.iosFacebookClient = iosFacebookClient;
        }

        @Override
        public void onSuccess(final GraphUser graphUser) {
            iosFacebookClient.lastGraphUser = graphUser;

            for (final FacebookCallbacks callbacks : iosFacebookClient.callbacks) {
                callbacks.onUserSignedIn();
            }
        }

        @Override
        public void onCancel() {
            for (final FacebookCallbacks callbacks : iosFacebookClient.callbacks) {
                callbacks.onUserSignedOut();
            }
        }

        @Override
        public void onError(final FacebookError facebookError) {
            for (final FacebookCallbacks callbacks : iosFacebookClient.callbacks) {
                callbacks.onUserSignedOut();
            }
        }
    }
}