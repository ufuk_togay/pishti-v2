package com.isbirbilisim.pisti;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.utils.Logger;
import com.isbirbilisim.pisti.ad.AdManager;

import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.coregraphics.CGSize;
import org.robovm.apple.uikit.UIScreen;
import org.robovm.bindings.admob.GADAdSize;
import org.robovm.bindings.admob.GADBannerView;
import org.robovm.bindings.admob.GADBannerViewDelegateAdapter;
import org.robovm.bindings.admob.GADInterstitial;
import org.robovm.bindings.admob.GADInterstitialDelegateAdapter;
import org.robovm.bindings.admob.GADRequest;
import org.robovm.bindings.admob.GADRequestError;

import java.util.ArrayList;
import java.util.List;

/**
 * IOS implementation of AdManager.
 */
public class IOSAdManager implements AdManager {

    private static final Logger log = new Logger(IOSAdManager.class.getName(), Application.LOG_DEBUG);
    private static final boolean USE_TEST_DEVICES = true;
    private boolean adsInitialized = false;

    private IOSApplication iosApplication;

    private GADBannerView adview;
    private GADInterstitial gadInterstitial;

    public void setIosApplication(final IOSApplication iosApplication) {
        this.iosApplication = iosApplication;
    }

    public void initializeAds() {
        if (!adsInitialized) {
            log.debug("Initalizing ads...");

            adsInitialized = true;

            adview = new GADBannerView(GADAdSize.smartBannerLandscape());
            adview.setAdUnitID("ca-app-pub-2361252742276614/5012413885");
            adview.setRootViewController(iosApplication.getUIViewController());
            iosApplication.getUIViewController().getView().addSubview(adview);

            final GADRequest request = GADRequest.create();
            if (USE_TEST_DEVICES) {
                request.setTestDevices(getTestDevices());
                log.debug("Test devices: " + request.getTestDevices());
            }

            adview.setDelegate(new GADBannerViewDelegateAdapter() {
                @Override
                public void didReceiveAd(GADBannerView view) {
                    super.didReceiveAd(view);
                    log.debug("didReceiveAd");
                }

                @Override
                public void didFailToReceiveAd(GADBannerView view,
                                               GADRequestError error) {
                    super.didFailToReceiveAd(view, error);
                    log.debug("didFailToReceiveAd:" + error);
                }
            });

            adview.loadRequest(request);

            log.debug("Initalizing ads complete.");
        }
    }

    private List<String> getTestDevices() {
        final List<String> testDevices = new ArrayList<String>();
        testDevices.add(GADRequest.GAD_SIMULATOR_ID);

        return testDevices;
    }

    @Override
    public void showAds(final boolean show) {
        initializeAds();

        final CGSize screenSize = UIScreen.getMainScreen().getBounds().size();
        double screenWidth = screenSize.width();

        final CGSize adSize = adview.getBounds().size();
        double adWidth = adSize.width();
        double adHeight = adSize.height();

        log.debug(String.format("Hidding ad. size[%s, %s]", adWidth, adHeight));

        float bannerWidth = (float) screenWidth;
        float bannerHeight = (float) (bannerWidth / adWidth * adHeight);

        if (show) {
            adview.setFrame(new CGRect((screenWidth / 2) - adWidth / 2, 0, bannerWidth, bannerHeight));
        } else {
            adview.setFrame(new CGRect(0, -bannerHeight, bannerWidth, bannerHeight));
        }
    }

    @Override
    public void showInterstitialAd() {
        gadInterstitial = new GADInterstitial();
        gadInterstitial.setAdUnitID("ca-app-pub-2361252742276614/6489147089");

        final GADRequest gadRequest = new GADRequest();

        gadRequest.setTestDevices(getTestDevices());

        gadInterstitial.setDelegate(new GADInterstitialDelegateAdapter() {
            @Override
            public void didReceiveAd(final GADInterstitial ad) {
                super.didReceiveAd(ad);

                ad.present(iosApplication.getUIViewController());
            }
        });
        gadInterstitial.loadRequest(gadRequest);
    }
}