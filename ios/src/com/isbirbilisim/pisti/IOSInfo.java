package com.isbirbilisim.pisti;

/**
 * Info of iOS device.
 */
public class IOSInfo implements PlatformInfo {

    @Override
    public boolean isVibrationSupported() {
        return false;
    }
}