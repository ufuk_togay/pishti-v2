package com.isbirbilisim.pisti;

import com.isbirbilisim.pisti.view.FacebookButton;
import com.isbirbilisim.pisti.view.ViewUtils;

public class IOSViewUtils implements ViewUtils {

    private final FacebookButton facebookButton;

    public IOSViewUtils() {
        facebookButton = new FacebookButton() {
        };
    }

    @Override
    public FacebookButton getFacebookSignInButton() {
        return facebookButton;
    }
}
