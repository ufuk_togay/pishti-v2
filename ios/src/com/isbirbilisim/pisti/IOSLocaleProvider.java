package com.isbirbilisim.pisti;

import com.isbirbilisim.pisti.resources.LocaleProvider;

import java.util.Locale;

public class IOSLocaleProvider implements LocaleProvider {

    @Override
    public String getLocaleIdentifier() {
        return java.util.Locale.getDefault().toString();
    }

    @Override
    public Locale getLocale() {
        return java.util.Locale.getDefault();
    }
}