package com.isbirbilisim.pisti;

import org.robovm.apple.foundation.NSNumber;
import org.robovm.bindings.googleanalytics.GAI;
import org.robovm.bindings.googleanalytics.GAIDictionaryBuilder;
import org.robovm.bindings.googleanalytics.GAIFields;

/**
 * Class that provides singleton for AnalyticsManager.
 */
public class AnalyticsManager implements com.isbirbilisim.pisti.analytics.AnalyticsManager {

    private static AnalyticsManager instance;

    private AnalyticsManager() {
        init();
    }

    public static AnalyticsManager getInstance() {
        if (instance == null) {
            instance = new AnalyticsManager();
        }
        return instance;
    }

    private void init() {
        GAITrackerImpl tracker = (GAITrackerImpl) GAI.getSharedInstance().getTracker("UA-43667652-2");
        GAI.getSharedInstance().setDefaultTracker(tracker);
        tracker.set(GAIFields.kGAIScreenName, "Pisti (iOS)");
        tracker.send(GAIDictionaryBuilder.createAppView().build());
    }

    @Override
    public void sendMenuScreenShownEvent() {
        GAI.getSharedInstance().getDefaultTracker().send(GAIDictionaryBuilder.createEvent(
                "ui_action",
                "menu_screen_shown",
                "Menu screen",
                NSNumber.valueOf(0)
        ).build());
    }

    @Override
    public void sendGameScreenShownEvent() {
        GAI.getSharedInstance().getDefaultTracker().send(GAIDictionaryBuilder.createEvent(
                "ui_action",
                "game_screen_shown",
                "Game screen",
                NSNumber.valueOf(0)
        ).build());
    }

    @Override
    public void sendSingleGameCompletedEvent() {
        GAI.getSharedInstance().getDefaultTracker().send(GAIDictionaryBuilder.createEvent(
                "game_event",
                "single_game_completed",
                "Single game completed",
                NSNumber.valueOf(0)
        ).build());
    }

    @Override
    public void sendMultiplayerGameCompletedEvent() {
        GAI.getSharedInstance().getDefaultTracker().send(GAIDictionaryBuilder.createEvent(
                "game_event",
                "multiplayer_game_completed",
                "Multiplayer game completed",
                NSNumber.valueOf(0)
        ).build());
    }
}