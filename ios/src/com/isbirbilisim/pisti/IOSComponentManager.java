package com.isbirbilisim.pisti;

import com.isbirbilisim.pisti.ad.AdManager;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.backend.BackendManager;
import com.isbirbilisim.pisti.backend.IBackendManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

public class IOSComponentManager implements ComponentManager {

    private final ScreenUtils screenUtils;
    private final ViewUtils viewUtils;
    private final IOSConnectivityUtils IOSConnectivityUtils;
    private final AdManager adManager;
    private final LocaleProvider localeProvider;
    private final PlatformInfo platformInfo;
    private final IOSFacebookClient iosFacebookClient;

    public IOSComponentManager() {
        screenUtils = new IOSScreenUtils();
        viewUtils = new IOSViewUtils();
        IOSConnectivityUtils = new IOSConnectivityUtils();
        localeProvider = new IOSLocaleProvider();
        adManager = new IOSAdManager();
        platformInfo = new IOSInfo();
        iosFacebookClient = new IOSFacebookClient();
    }

    @Override
    public ScreenUtils getScreenUtils() {
        return screenUtils;
    }

    @Override
    public ViewUtils getViewUtils() {
        return viewUtils;
    }

    @Override
    public ConnectivityUtils getConnectivityUtils() {
        return IOSConnectivityUtils;
    }

    @Override
    public SignatureUtils getSignatureUtils() {
        return null;
    }

    @Override
    public AdManager getAdManager() {
        return adManager;
    }

    @Override
    public LocaleProvider getLocaleProvider() {
        return localeProvider;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return platformInfo;
    }

    @Override
    public Facebook getFacebook() {
        return iosFacebookClient;
    }

    @Override
    public IBackendManager getBackendManager() {
        return BackendManager.getInstance();
    }

    @Override
    public AnalyticsManager getAnalyticsManager() {
        return com.isbirbilisim.pisti.AnalyticsManager.getInstance();
    }
}