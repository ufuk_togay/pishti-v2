package com.isbirbilisim.pisti.backend;


import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.shephertz.app42.paas.sdk.java.App42API;
import com.shephertz.app42.paas.sdk.java.App42CallBack;
import com.shephertz.app42.paas.sdk.java.App42Exception;
import com.shephertz.app42.paas.sdk.java.storage.Storage;
import com.shephertz.app42.paas.sdk.java.storage.StorageService;

import java.util.ArrayList;

/**
 * Created by igor on 12.12.14.
 */
public class BackendManager implements IBackendManager {

    private static BackendManager instance;

    private StorageService storageService;
    private Json jsonProcessor;

    private GameStorageService gameStorageService;

    private App42StatisticCollector app42StatisticCollector;

    private BackendManager() {
        jsonProcessor = new Json(JsonWriter.OutputType.json);
        initService();
    }

    public static BackendManager getInstance() {
        if (instance == null) {
            return instance = new BackendManager();
        }
        return instance;
    }

    @Override
    public StatisticCollector getStatisticCollector() {
        if (app42StatisticCollector == null) {
            app42StatisticCollector = new App42StatisticCollector();
        }
        return app42StatisticCollector;
    }

    @Override
    public GameStorageService getGameStorageService() {
        if (gameStorageService == null) {
            gameStorageService = new App42GameStorageService();
        }
        return gameStorageService;
    }


    private void initService() {
        App42API.initialize(API_KEY, SECRET_KEY);
        storageService = App42API.buildStorageService();
    }

    private class App42GameStorageService implements GameStorageService {

        @Override
        public void loadByKeyValue(final String collectionName,
                                   final String key,
                                   final String value,
                                   final loadDataCallback callback) {
            storageService.findDocumentByKeyValue(DBName, collectionName, key, value, new App42CallBack() {
                @Override
                public void onSuccess(Object o) {
                    Storage storage = (Storage) o;
                    ArrayList<Storage.JSONDocument> docList = storage.jsonDocList;
                    if (docList.size() > 0) {
                        callback.onDataLoaded(docList.get(0).jsonDoc);
                    } else {
                        callback.onDataLoaded("");
                    }
                }

                @Override
                public void onException(Exception e) {

                    App42Exception exception = (App42Exception) e;
                    int appErrorCode = exception.getAppErrorCode();
                    int httpErrorCode = exception.getHttpErrorCode();

                    if (appErrorCode == STORAGE_SERVICE_DATA_NOT_FOUND) {
                        callback.onDataNotFound();
                    } else {
                        callback.onFailed();
                    }
                }
            });
        }

        @Override
        public void saveDocument(final String collectionName,
                                 final String jsonStr,
                                 final String userId) {
            storageService.saveOrUpdateDocumentByKeyValue(DBName, collectionName, USER_ID_KEY, userId, jsonStr);
        }
    }

    private class App42StatisticCollector implements StatisticCollector {

        @Override
        public void saveSinglePlayerStatistic(final UserStatisticModel stats) {
            String jsonStr = jsonProcessor.toJson(stats);
            getGameStorageService().saveDocument(SINGLE_PLAYER_STATISTIC, jsonStr, stats.getUserID());
        }

        @Override
        public void loadSinglePlayerStatisticData(final String userId, final StatisticLoadCallBack callBack) {
            getGameStorageService().loadByKeyValue(SINGLE_PLAYER_STATISTIC, USER_ID_KEY, userId,
                    new GameStorageService.loadDataCallback() {
                        @Override
                        public void onDataLoaded(String jsonString) {
                            if (jsonString.isEmpty()) {
                                callBack.onNoStatistic();
                            } else {
                                callBack.onStatisticLoaded(jsonProcessor
                                        .fromJson(UserStatisticModel.class, jsonString));
                            }
                        }

                        @Override
                        public void onDataNotFound() {
                            callBack.onNoStatistic();
                        }

                        @Override
                        public void onFailed() {
                            callBack.onFailed();
                        }
                    });
        }

        @Override
        public void saveMultiPlayerStatistic(final UserStatisticModel stats) {
            String jsonStr = jsonProcessor.toJson(stats);
            getGameStorageService().saveDocument(MULTI_PLAYER_STATISTIC, jsonStr, stats.getUserID());
        }

        @Override
        public void loadMultiPlayerStatisticData(final String userId, final StatisticLoadCallBack callBack) {
            getGameStorageService().loadByKeyValue(MULTI_PLAYER_STATISTIC, USER_ID_KEY, userId,
                    new GameStorageService.loadDataCallback() {
                        @Override
                        public void onDataLoaded(String jsonString) {
                            if (jsonString.isEmpty()) {
                                callBack.onNoStatistic();
                            } else {
                                callBack.onStatisticLoaded(jsonProcessor
                                        .fromJson(UserStatisticModel.class, jsonString));
                            }
                        }

                        @Override
                        public void onDataNotFound() {
                            callBack.onNoStatistic();
                        }

                        @Override
                        public void onFailed() {
                            callBack.onFailed();
                        }
                    });
        }
    }
}
