package com.isbirbilisim.pisti.security;

import com.badlogic.gdx.utils.Base64Coder;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Utils for encoding content.
 */
public class EncodingUtils {

    private static final String ALGORITHM = "HmacSHA256";
    private static final String UTF_8_ENCODING = "UTF-8";

    /**
     * Encodes userJsonData using userId and secretKey as key specifications.
     *
     * @param userJsonData - content that should be encoded.
     * @param userId       - userId.
     * @param secretKey    - secretKey.
     * @return - encoded string or null if encoding failed.
     */
    public static String encodeContent(final String userId,
                                       final String userJsonData,
                                       final String date,
                                       final String contentType,
                                       final String secretKey)
            throws EncodingException {
        try {
            return doEncodeContent(userId, userJsonData, date, contentType, secretKey);
        } catch (final NoSuchAlgorithmException exception) {
            throw new EncodingException(exception.getMessage());
        } catch (final InvalidKeyException exception) {
            throw new EncodingException(exception.getMessage());
        } catch (final UnsupportedEncodingException exception) {
            throw new EncodingException(exception.getMessage());
        }
    }

    private static String doEncodeContent(final String userId,
                                          final String userJsonData,
                                          final String date,
                                          final String contentType,
                                          final String secretKey)
            throws NoSuchAlgorithmException,
            InvalidKeyException,
            UnsupportedEncodingException {
        final SecretKeySpec secretKeySpec = new SecretKeySpec(
                createKeySpecBytes(userId, secretKey),
                ALGORITHM);
        final Mac mac = Mac.getInstance(ALGORITHM);
        mac.init(secretKeySpec);

        final StringBuilder resultContent = new StringBuilder();
        resultContent.append(contentType);
        resultContent.append("\n");
        resultContent.append(date);
        resultContent.append("\n");
        resultContent.append(userJsonData);

        final byte[] encodedData = mac.doFinal(resultContent.toString().getBytes());
        final String encodedStringData = new String(Base64Coder.encode(encodedData));

        return encodedStringData;
    }

    private static byte[] createKeySpecBytes(final String userId,
                                             final String secretKey) {
        return (userId + secretKey).getBytes();
    }

    /**
     * Thrown if unable to encode String content.
     */
    public static class EncodingException extends Exception {

        public EncodingException(final String message) {
            super("Exception while encoding content: [ " + message + " ]");
        }
    }
}