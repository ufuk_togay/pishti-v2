package com.isbirbilisim.pisti.security;

/**
 * Utils for getting app signature.
 */
public interface SignatureUtils {

    /**
     * @return - value that uniquely identifies this app.
     */
    String getAppSignature();
}