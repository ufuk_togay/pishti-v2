package com.isbirbilisim.pisti.ad;

/**
 * Interface for advertisement manager.
 */
public interface AdManager {

    /**
     * Shows or hides ad.
     *
     * @param show - show?
     */
    public void showAds(boolean show);

    /**
     * Shows interstitial ad.
     */
    public void showInterstitialAd();
}