package com.isbirbilisim.pisti.ad;

import com.badlogic.gdx.utils.TimeUtils;
import com.isbirbilisim.pisti.preferences.AppPreferences;

/**
 * Utils for showing advertisement.
 */
public class AdUtil {

    private final AdManager adManager;

    public AdUtil(final AdManager adManager) {
        this.adManager = adManager;
    }

    /**
     * Shows interstitial ad if it wasn't shown during last 24 hours.
     */
    public void showInterstitialIfNeeded() {
        if (shouldShowAd()) {
            adManager.showInterstitialAd();
            saveAdShowTime();
        }
    }

    /**
     * Shows or hides ad.
     *
     * @param show - pass true if show, false if should hide.
     */
    public void showAds(final boolean show) {
        adManager.showAds(show);
    }

    private boolean shouldShowAd() {
        final long lastAdShowTime = AppPreferences.getInstance().getLastAdShowTime();
        final long passedTime = TimeUtils.timeSinceMillis(lastAdShowTime);

        final long passedHours = passedTime / 1000 / 60 / 60;

        return passedHours > 24;
    }

    private void saveAdShowTime() {
        final long time = TimeUtils.millis();
        AppPreferences.getInstance().setLastAdShowTime(time);
    }
}