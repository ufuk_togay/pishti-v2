package com.isbirbilisim.pisti.scores;

/**
 * Defines all in-game achievements.
 */
public enum Achievement {
    PLAY_FIRST_GAME("TODO_paste_achievement_id");

    private final String achievementId;

    Achievement(final String id) {
        this.achievementId = id;
    }

    /**
     * @return - achievement Id.
     */
    public String getId() {
        return achievementId;
    }
}