package com.isbirbilisim.pisti.scores;

import com.isbirbilisim.pisti.cards.CardModel;

import java.util.Stack;

public class ScoreCounter {
    public static final int BONUS_SCORE = 3;

    private static final int PISTI_BONUS = 10;
    private static final int SUPER_PISTI_BONUS = 20;

    /**
     * Calculates scores earned from collected cards.
     *
     * @param collectedCards - cards, collected by player.
     * @return - earned scores.
     */
    public static Scores getScore(final Stack<CardModel> collectedCards) {
        final Scores scores = new Scores();

        scores.addCardsCount(collectedCards.size());

        for (final CardModel card : collectedCards) {
            scores.addTotalScores(card.getScoreValue());
        }

        if (collectedCards.size() == 2) {

            final CardModel card1 = collectedCards.get(0);
            final CardModel card2 = collectedCards.get(1);

            if (card1.isSameCard(card2)) {
                if (card1.isJack() && card2.isJack()) {
                    scores.addTotalScores(SUPER_PISTI_BONUS);
                    scores.addSuperPistiScores(SUPER_PISTI_BONUS);
                } else {
                    scores.addTotalScores(PISTI_BONUS);
                    scores.addPistiScores(PISTI_BONUS);
                }
            }
        }

        return scores;
    }
}