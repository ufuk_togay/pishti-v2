package com.isbirbilisim.pisti.scores;

/**
 * Defines all leaderboards.
 */
public enum Leaderboard {
    PISTI_TOP_SCORES("CgkIh5uz_80XEAIQAQ");

    private final String leaderboardId;


    Leaderboard(final String leaderboardId) {
        this.leaderboardId = leaderboardId;
    }

    public String getId() {
        return leaderboardId;
    }
}