package com.isbirbilisim.pisti.scores;

/**
 * Class that contains scores, earned by player.
 */
public class Scores {

    private long totalScore;
    private long pistiScore;
    private long superPistiScore;

    private int cardCount;
    private int multiroundScores;

    public Scores(final long totalScore,
                  final long pistiScore,
                  final long superPistiScore) {
        this.totalScore = totalScore;
        this.pistiScore = pistiScore;
        this.superPistiScore = superPistiScore;
    }

    /**
     * Creates Scores instance with all scores set to 0.
     */
    public Scores() {
    }

    /**
     * Adds scores to Total Score counter.
     *
     * @param scores - scores to add.
     */
    public void addTotalScores(final long scores) {
        this.totalScore += scores;
        this.multiroundScores += scores;
    }

    /**
     * Adds scores to Pisti Score counter.
     *
     * @param scores - scores to add.
     */
    public void addPistiScores(final long scores) {
        this.pistiScore += scores;
    }

    /**
     * Adds scores to Super Pisti Score counter.
     *
     * @param scores - scores to add.
     */
    public void addSuperPistiScores(final long scores) {
        this.superPistiScore += scores;
    }

    /**
     * Adds collected cards to counter;
     *
     * @param cardCount - collected cards;
     */
    public void addCardsCount(final int cardCount) {
        this.cardCount += cardCount;
    }

    /**
     * Sums scores.
     *
     * @param scores - scores to add.
     */
    public void sumScores(final Scores scores) {
        this.totalScore += scores.totalScore;
        this.multiroundScores += scores.totalScore;

        this.pistiScore += scores.pistiScore;
        this.superPistiScore += scores.superPistiScore;

        this.cardCount += scores.cardCount;
    }

    /**
     * Sets total scores to 0.
     */
    public void resetTotalScores() {
        totalScore = 0;
    }

    /**
     * Sets pisti scores to 0.
     */
    public void resetPistiScores() {
        pistiScore = 0;
    }

    /**
     * Sets super pisti scores to 0.
     */
    public void resetSuperPistiScore() {
        superPistiScore = 0;
    }

    /**
     * Sets multiround scores to 0.
     */
    public void resetMultiroundScores() {
        multiroundScores = 0;
    }

    /**
     * Sets cards count to 0.
     */
    public void resetCardsCount() {
        cardCount = 0;
    }

    public long getTotalScore() {
        return totalScore;
    }

    public long getPistiScore() {
        return pistiScore;
    }

    public long getSuperPistiScore() {
        return superPistiScore;
    }

    public int getCardCount() {
        return cardCount;
    }

    public int getMultiroundScores() {
        return multiroundScores;
    }
}