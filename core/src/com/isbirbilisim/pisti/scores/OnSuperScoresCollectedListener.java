package com.isbirbilisim.pisti.scores;

public interface OnSuperScoresCollectedListener {

    /**
     * Called when some player collects pishti scores.
     */
    void onPishtiScoresCollected();

    /**
     * Called when some player collects super pishti scores.
     */
    void onSuperPishtiScoresCollected();
}