package com.isbirbilisim.pisti;

import com.badlogic.gdx.Game;
import com.isbirbilisim.pisti.ad.AdUtil;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.analytics.AnalyticsUtil;
import com.isbirbilisim.pisti.backend.CoinsManager;
import com.isbirbilisim.pisti.backend.StatisticManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.dialogs.DialogManager;
import com.isbirbilisim.pisti.game.MultiplayerGameManager;
import com.isbirbilisim.pisti.game.SingleGameManager;
import com.isbirbilisim.pisti.match.MatchLifecycle;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClient;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClientImpl;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.navigation.CoreScreenNavigator;
import com.isbirbilisim.pisti.navigation.ScreenNavigator;
import com.isbirbilisim.pisti.resources.Assets;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

public class Pisti extends Game {

    private final CoreScreenNavigator screenNavigator;
    private final SingleGameManager singleGameManager;
    private final MultiplayerGameManager multiplayerGameManager;
    private final ComponentManager componentManager;
    private final DialogManager dialogManager;
    private final Assets assets;
    private final AdUtil adUtil;
    private final AnalyticsUtil analyticsUtil;
    private final MultiplayerClient multiplayerClient;
    private final MultimediaManager multimediaManager;

    public Pisti(final ComponentManager componentManager) {
        this.componentManager = componentManager;

        screenNavigator = new CoreScreenNavigator(this);
        multiplayerClient = new MultiplayerClientImpl(this);
        singleGameManager = new SingleGameManager(this);
        multiplayerGameManager = new MultiplayerGameManager(this);
        dialogManager = new DialogManager(this);
        assets = new Assets(this);
        adUtil = new AdUtil(componentManager.getAdManager());
        analyticsUtil = new AnalyticsUtil(componentManager.getAnalyticsManager());
        multimediaManager = new MultimediaManager(getPlatformInfo());
        StatisticManager.createInstance(componentManager.getBackendManager());
        CoinsManager.createInstance(componentManager.getBackendManager());
    }

    @Override
    public void create() {
        assets.loadSplashScreenSkin();

        screenNavigator.showSplashScreen();

        assets.loadLocalizedStrings();
        assets.loadFonts();
        assets.loadDialogAssets();
        assets.loadMainMenuScreenAssets();
        assets.loadGameScreenAssets();
        multimediaManager.loadSounds();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        assets.dispose();
        multimediaManager.disposeSounds();
    }

    /**
     * @return - instance of ScreenNavigator.
     */
    public ScreenNavigator getScreenNavigator() {
        return screenNavigator;
    }

    public MatchLifecycle getMatchLifecycleDelegate() {
        return screenNavigator.getGameScreen();
    }

    /**
     * @return - instance of SingleGameManager.
     */
    public SingleGameManager getSingleGameManager() {
        return singleGameManager;
    }

    /**
     * @return - instance of MultiplayerGameManager.
     */
    public MultiplayerGameManager getMultiplayerGameManager() {
        return multiplayerGameManager;
    }

    /**
     * @return - instance of ScreenUtils.
     */
    public ScreenUtils getScreenUtils() {
        return componentManager.getScreenUtils();
    }

    /**
     * @return - instance of ViewUtils.
     */
    public ViewUtils getViewUtils() {
        return componentManager.getViewUtils();
    }

    /**
     * @return - instance of ConnectivityUtils.
     */
    public ConnectivityUtils getConnectivityUtils() {
        return componentManager.getConnectivityUtils();
    }

    /**
     * @return - {@link com.isbirbilisim.pisti.multiplayer.MultiplayerClient}
     * implementation.
     */
    public MultiplayerClient getMultiplayerClient() {
        return multiplayerClient;
    }

    /**
     * @return - instance of DialogManager.
     */
    public DialogManager getDialogManager() {
        return dialogManager;
    }

    /**
     * @return - instance of Assets.
     */
    public Assets getAssets() {
        return assets;
    }

    /**
     * @return instance of AdUtils.
     */
    public AdUtil getAdUtil() {
        return adUtil;
    }

    /**
     * @return - instance of AnalyticsManager.
     */
    public AnalyticsManager getAnalyticsManager() {
        return componentManager.getAnalyticsManager();
    }

    /**
     * @return instance of LocaleProvider.
     */
    public LocaleProvider getLocaleProvider() {
        return componentManager.getLocaleProvider();
    }

    public AnalyticsUtil getAnalyticsUtil() {
        return analyticsUtil;
    }

    public PlatformInfo getPlatformInfo() {
        return componentManager.getPlatformInfo();
    }

    public MultimediaManager getMultimediaManager() {
        return multimediaManager;
    }

    public Facebook getFacebookClient() {
        return componentManager.getFacebook();
    }
}