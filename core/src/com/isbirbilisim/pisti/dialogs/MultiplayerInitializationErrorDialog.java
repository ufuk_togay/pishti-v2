package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.multiplayer.ConnectionCallback;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

public class MultiplayerInitializationErrorDialog extends GameDialog {

    public MultiplayerInitializationErrorDialog(final ScreenUtils screenUtils,
                                                final Skin skin,
                                                final ConnectionCallback.UserRequestFail cause,
                                                final DialogCallback positiveButtonListener) {
        super(screenUtils,
                skin,
                Res.values.MULTIPLAYER_ERROR_DIALOG_WIDTH,
                Res.values.MULTIPLAYER_ERROR_DIALOG_HEIGHT);

        init(positiveButtonListener, cause);
    }

    private void init(final DialogCallback positiveButtonListener,
                      final ConnectionCallback.UserRequestFail cause) {
        final String text;

        switch (cause) {
            case OTHER_USER_SIGNED:
                text = "AppWarp: This user is already signed in, \nplease sign in with other account";
                break;
            case CONNECTION_ERROR:
                text = "Initialization error, \nplease check your network connection";
                break;
            case STATISTIC_CONNECTION_ERROR:
                text = "Unable to load your statistic, \n" +
                        "please check your network connection";
                break;
            case AUTH_ERROR:
                text = "AppWarp: Auth error";
                break;
            case BAD_REQUEST:
                text = "AppWarp: bad request";
                break;
            case CONNECTION_ERR:
                text = "AppWarp: connection error";
                break;
            case CONNECTION_ERR_RECOVERABLE:
                text = "AppWarp: connection error recoverable";
                break;
            case RESOURCE_MOVED:
                text = "AppWarp: resource mode";
                break;
            case RESOURCE_NOT_FOUND:
                text = "AppWarp: resource not found";
                break;
            case RESULT_SIZE_ERROR:
                text = "AppWarp: result size error";
                break;
            case USER_PAUSED_ERROR:
                text = "AppWarp: user paused error";
                break;
            case SUCCESS:
                text = "AppWarp: success";
                break;
            case SUCCESS_RECOVERED:
                text = "AppWarp: success recovered";
                break;
            default:
                text = "unknown";
        }

        getContentFrame()
                .add(new Label(text, getContentLargeLabelStyle()));

        final ImageButton positiveButton = new ImageButton(getOkButtonStyle());
        positiveButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButtonListener.onButtonClicked();
            }
        });

        addRightBottomButton(positiveButton);
    }
}