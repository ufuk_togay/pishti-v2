package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Custom implementation of dialog for game.
 */
public class GameDialog extends BaseDialog {

    private final ScreenUtils screenUtils;
    private final Skin skin;
    private final Table dialogBackgroundFrame;
    private final Table contentFrame;

    private float contentFramePadding;
    private float backgroundFrameWidth;
    private float backgroundFrameHeight;

    public GameDialog(final ScreenUtils screenUtils,
                      final Skin skin,
                      final float backgroundFrameWidth,
                      final float backgroundFrameHeight) {
        this.screenUtils = screenUtils;
        this.skin = skin;
        this.backgroundFrameWidth = screenUtils.toRealWidth(backgroundFrameWidth);
        this.backgroundFrameHeight = screenUtils.toRealHeight(backgroundFrameHeight);
        this.contentFramePadding = screenUtils.toRealWidth(Res.values.DIALOG_FRAME_PADDING);

        dialogBackgroundFrame = new Table();
        dialogBackgroundFrame.setBackground(skin.getDrawable(Res.textures.DIALOG_BACKGROUND));
        dialogBackgroundFrame.setSize(this.backgroundFrameWidth, this.backgroundFrameHeight);
        dialogBackgroundFrame.setPosition(contentFramePadding, contentFramePadding);

        addActor(dialogBackgroundFrame);

        contentFrame = new Table();
        contentFrame.setSize(this.backgroundFrameWidth - (contentFramePadding * 3),
                this.backgroundFrameHeight - (contentFramePadding * 3));
        dialogBackgroundFrame
                .add(contentFrame)
                .size(this.backgroundFrameWidth - (contentFramePadding * 3),
                        this.backgroundFrameHeight - (contentFramePadding * 3));
    }

    /**
     * @return - width of whole dialog (background width + half of buttons width).
     */
    @Override
    public final float getPrefWidth() {
        return backgroundFrameWidth + (2 * contentFramePadding);
    }

    /**
     * @return - height of whole dialog (background width + half of buttons height).
     */
    @Override
    public final float getPrefHeight() {
        return backgroundFrameHeight + (2 * contentFramePadding);
    }

    /**
     * @return - width of frame that contains dialog background.
     */
    public float getBackgroundFrameWidth() {
        return backgroundFrameWidth;
    }

    /**
     * @return - height of frame that contains dialog background.
     */
    public float getBackgroundFrameHeight() {
        return backgroundFrameHeight;
    }

    /**
     * @return - width of frame that holds dialog content.
     */
    public float getContentFrameWidth() {
        return backgroundFrameWidth - (contentFramePadding * 3);
    }

    /**
     * @return - height of frame that holds dialog content.
     */
    public float getContentFrameHeight() {
        return backgroundFrameHeight - (contentFramePadding * 3);
    }

    protected ScreenUtils getScreenUtils() {
        return screenUtils;
    }

    protected Strings getStrings() {
        return skin.get(Res.properties.STRINGS, Strings.class);
    }

    protected ImageButton.ImageButtonStyle getOkButtonStyle() {
        return skin.get(Res.buttonStyles.DIALOG_OK_BUTTON_STYLE,
                ImageButton.ImageButtonStyle.class);
    }

    protected ImageButton.ImageButtonStyle getBackButtonStyle() {
        return skin.get(Res.buttonStyles.DIALOG_BACK_BUTTON_STYLE,
                ImageButton.ImageButtonStyle.class);
    }

    protected ImageTextButton.ImageTextButtonStyle getRadioButtonStyle() {
        return skin.get(Res.buttonStyles.RADIO_BUTTON_STYLE,
                ImageTextButton.ImageTextButtonStyle.class);
    }

    protected Label.LabelStyle getContentLargeLabelStyle() {
        return skin.get(Res.labelStyles.DIALOG_CONTENT_LARGE_LABEL_STYLE,
                Label.LabelStyle.class);
    }

    protected Label.LabelStyle getContentSmallLabelStyle() {
        return skin.get(Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE,
                Label.LabelStyle.class);
    }

    protected Label.LabelStyle getContentLinkLabelStyle() {
        return skin.get(Res.labelStyles.DIALOG_CONTENT_LINK_LABEL_STYLE,
                Label.LabelStyle.class);
    }

    protected ImageTextButton.ImageTextButtonStyle getCheckboxStyle() {
        return skin.get(Res.buttonStyles.LARGE_CHECKBOX_STYLE,
                ImageTextButton.ImageTextButtonStyle.class);
    }

    protected Skin getSkin() {
        return skin;
    }

    protected Table getContentFrame() {
        return contentFrame;
    }

    protected void addRightBottomButton(final Button button) {
        final float buttonSize = contentFramePadding * 2;

        button.setSize(buttonSize, buttonSize);
        button.setPosition(contentFramePadding / 2 + backgroundFrameWidth - (buttonSize / 2), contentFramePadding / 2);

        addActor(button);
    }

    protected void addRightTopButton(final Button button) {
        final float buttonSize = contentFramePadding * 2;

        button.setSize(buttonSize, buttonSize);
        button.setPosition(contentFramePadding / 2 + backgroundFrameWidth - (buttonSize / 2),
                contentFramePadding / 2 + backgroundFrameHeight - (buttonSize / 2));

        addActor(button);
    }
}