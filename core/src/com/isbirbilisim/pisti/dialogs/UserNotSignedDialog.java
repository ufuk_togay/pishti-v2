package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

public class UserNotSignedDialog extends GameDialog {

    public UserNotSignedDialog(final ScreenUtils screenUtils,
                               final Skin skin,
                               final DialogCallback positiveButton,
                               final DialogCallback negativeButton) {
        super(screenUtils,
                skin,
                Res.values.USER_NOT_SIGNED_DIALOG_WIDTH,
                Res.values.USER_NOT_SIGNED_DIALOG_HEIGHT);
        init(positiveButton, negativeButton);
    }

    private void init(final DialogCallback positiveButton,
                      final DialogCallback negativeButton) {
        final Label label = new Label(getStrings().signInWithFacebook, getContentLargeLabelStyle());
        getContentFrame().add(label);

        final ImageButton ok = new ImageButton(getOkButtonStyle());
        ok.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButton.onButtonClicked();
            }
        });
        addRightBottomButton(ok);

        final ImageButton cancel = new ImageButton(getBackButtonStyle());
        cancel.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                negativeButton.onButtonClicked();
            }
        });
        addRightTopButton(cancel);
    }
}