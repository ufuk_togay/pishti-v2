package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.LevelWrapper;
import com.isbirbilisim.pisti.account.Room;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.RoomMenuWidget;
import com.isbirbilisim.pisti.view.RoomWidget;

import java.util.LinkedList;
import java.util.List;

/**
 * Dialog that shows slide menu for selecting gaming room to play in.
 */
public class GamingRoomDialog extends GameDialog {

    public GamingRoomDialog(final ScreenUtils screenUtils,
                            final Skin skin,
                            final OnLevelSelectedListener listener,
                            final Level maxAvailableLevel,
                            final boolean isMale) {
        super(screenUtils,
                skin,
                Res.values.GAME_ROOM_DIALOG_WIDTH,
                Res.values.GAME_ROOM_DIALOG_HEIGHT);

        init(listener, maxAvailableLevel, isMale);
    }

    private void init(final OnLevelSelectedListener listener,
                      final Level maxAvailableLevel,
                      final boolean isMale) {
        final ImageButton button = new ImageButton(getBackButtonStyle());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                listener.onCancel();
            }
        });
        addRightBottomButton(button);

        getContentFrame().add(
                createRoomWidget(
                        listener,
                        maxAvailableLevel,
                        isMale))
                .expand().fill();
    }

    private RoomMenuWidget createRoomWidget(
            final OnLevelSelectedListener listener,
            final Level maxAvailableLevel,
            final boolean isMale) {
        return new RoomMenuWidget(
                getContentFrameWidth(),
                getContentFrameHeight(),
                createRoomPages(listener, maxAvailableLevel, isMale));
    }

    private LinkedList<RoomWidget> createRoomPages(
            final OnLevelSelectedListener listener,
            final Level maxAvailableLevel,
            final boolean isMale) {
        final LinkedList<RoomWidget> pages = new LinkedList<RoomWidget>();
        final float levelsPadding = getScreenUtils()
                .toRealWidth(Res.values.LEVELS_PADDING);

        final RoomWidget barracks = new RoomWidget(
                createBarracksLevelWrappers(maxAvailableLevel),
                isMale,
                getSkin(),
                Res.textures.DIALOG_BACKGROUND,
                levelsPadding);
        barracks.setOnLevelSelectedListener(listener);

        final RoomWidget generals = new RoomWidget(
                createAssemblyLevelWrappers(maxAvailableLevel),
                isMale,
                getSkin(),
                Res.textures.DIALOG_BACKGROUND,
                levelsPadding);
        generals.setOnLevelSelectedListener(listener);

        final RoomWidget palace = new RoomWidget(
                createPalaceLevelWrappers(maxAvailableLevel),
                isMale,
                getSkin(),
                Res.textures.DIALOG_BACKGROUND,
                levelsPadding);
        palace.setOnLevelSelectedListener(listener);

        pages.add(barracks);
        pages.add(generals);
        pages.add(palace);

        return pages;
    }

    private List<LevelWrapper> createBarracksLevelWrappers(
            final Level maxAvailableLevel) {
        final List<Level> barracksLevels = Level
                .getLevelsFromRoom(Room.BEGINNERS);

        final List<LevelWrapper> levelWrappers = new LinkedList<LevelWrapper>();
        for (final Level level : barracksLevels) {
            levelWrappers.add(new LevelWrapper(level,
                    maxAvailableLevel.getLevelValue() >= level.getLevelValue()));
        }

        return levelWrappers;
    }

    private List<LevelWrapper> createAssemblyLevelWrappers(
            final Level maxAvailableLevel) {
        final List<Level> barracksLevels = Level
                .getLevelsFromRoom(Room.EXPERTS);

        final List<LevelWrapper> levelWrappers = new LinkedList<LevelWrapper>();
        for (final Level level : barracksLevels) {
            levelWrappers.add(new LevelWrapper(level,
                    maxAvailableLevel.getLevelValue() >= level.getLevelValue()));
        }

        return levelWrappers;
    }

    private List<LevelWrapper> createPalaceLevelWrappers(
            final Level maxAvailableLevel) {
        final List<Level> barracksLevels = Level
                .getLevelsFromRoom(Room.PROFESIONALS);

        final List<LevelWrapper> levelWrappers = new LinkedList<LevelWrapper>();
        for (final Level level : barracksLevels) {
            levelWrappers.add(new LevelWrapper(level,
                    maxAvailableLevel.getLevelValue() >= level.getLevelValue()));
        }

        return levelWrappers;
    }

    /**
     * Callback which gets triggered when user selects level from dialog.
     */
    public interface OnLevelSelectedListener {

        /**
         * Called when user selects level from dialog.
         *
         * @param level - selected level.
         */
        void onLevelSelected(final Level level);

        /**
         * Called when user cancels dialog.
         */
        void onCancel();
    }
}