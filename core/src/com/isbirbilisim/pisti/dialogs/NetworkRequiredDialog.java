package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Shows message that network is required.
 */
public class NetworkRequiredDialog extends GameDialog {

    public NetworkRequiredDialog(final ScreenUtils screenUtils,
                                 final Skin skin,
                                 final DialogCallback positiveButton) {
        super(screenUtils,
                skin,
                Res.values.NETWORK_REQUIRED_DIALOG_WIDTH,
                Res.values.NETWORK_REQUIRED_DIALOG_HEIGHT);
        init(positiveButton);
    }

    private void init(final DialogCallback positiveButton) {
        final Label label = new Label(getStrings().networkUnavailable,
                getContentLargeLabelStyle());

        getContentFrame().add(label);

        final ImageButton button = new ImageButton(getOkButtonStyle());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButton.onButtonClicked();
            }
        });
        addRightBottomButton(button);
    }
}