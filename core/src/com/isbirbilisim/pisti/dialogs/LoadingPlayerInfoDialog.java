package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

public class LoadingPlayerInfoDialog extends GameDialog {

    public LoadingPlayerInfoDialog(final ScreenUtils screenUtils,
                                   final Skin skin) {
        super(screenUtils,
                skin,
                Res.values.LOADING_PLAYER_DIALOG_WIDTH,
                Res.values.LOADING_PLAYER_DIALOG_HEIGHT);
        init();
    }

    private void init() {
        final Label label = new Label("Loading player info...", getContentLargeLabelStyle());

        getContentFrame().add(label);
    }
}