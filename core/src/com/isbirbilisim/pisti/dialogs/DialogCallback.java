package com.isbirbilisim.pisti.dialogs;

public interface DialogCallback {

    void onButtonClicked();
}