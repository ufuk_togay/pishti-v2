package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.multiplayer.ConnectionCallback;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClient;
import com.isbirbilisim.pisti.preferences.AppPreferences;
import com.isbirbilisim.pisti.purchases.Item;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.HorizontalImageTextButton;

import java.net.URL;

/**
 * Dialog that shows game settings.
 */
public class SettingsDialog extends GameDialog implements ConnectionCallback {

    private final MultiplayerClient multiplayerClient;
    private final ConnectivityUtils connectivityUtils;
    private final PlatformInfo platformInfo;
    private final DialogCallback positiveButtonListener;
    private final DialogCallback facebookListener;

    private TextButton facebookButton;

    private ClickListener facebookButtonListener = new ClickListener() {
        @Override
        public void clicked(final InputEvent event, final float x, final float y) {
            if (multiplayerClient.isSignedIn()) {
                multiplayerClient.signOut();
            } else {
                multiplayerClient.signIn();
                facebookListener.onButtonClicked();
            }
        }
    };

    public SettingsDialog(final ScreenUtils screenUtils,
                          final ConnectivityUtils connectivityUtils,
                          final MultiplayerClient multiplayerClient,
                          final PlatformInfo platformInfo,
                          final Skin skin,
                          final DialogCallback positiveButtonListener,
                          final DialogCallback facebookListener) {
        super(screenUtils,
                skin,
                Res.values.SETTINGS_DIALOG_WIDTH,
                Res.values.SETTINGS_DIALOG_HEIGHT);

        this.connectivityUtils = connectivityUtils;
        this.platformInfo = platformInfo;
        this.multiplayerClient = multiplayerClient;
        this.positiveButtonListener = positiveButtonListener;
        this.facebookListener = facebookListener;

        init();
        initButtons();
    }

    private void init() {
        final float rowHeight = getCheckboxStyle().font.getCapHeight();

        final Table table = getContentFrame();

        table.add(new Image(getSkin().getDrawable(Res.textures.SOUND_ICON)))
                .maxHeight(rowHeight)
                .maxWidth(rowHeight * 1.3F)
                .padRight(rowHeight)
                .expandY();
        table.add(getSoundButton())
                .height(rowHeight)
                .expandX()
                .fillX();

        table.row();

        if (platformInfo.isVibrationSupported()) {
            table.add(new Image(getSkin().getDrawable(Res.textures.VIBRATION_ICON)))
                    .maxHeight(rowHeight * 2)
                    .maxWidth(rowHeight * 1.25F)
                    .padRight(rowHeight)
                    .expandY();
            table.add(getVibrationButton())
                    .height(rowHeight)
                    .expandX()
                    .fillX();

            table.row();
        }

        final TextButton googlePlus = createTextButton(
                getSkin().get(Res.properties.STRINGS, Strings.class).changeGoogle);
        googlePlus.setDisabled(true);
        table.add(new Image(getSkin().getDrawable(Res.textures.GOOGLE_PLUS_ICON)))
                .maxHeight(rowHeight * 1.2F)
                .maxWidth(rowHeight * 1.2F)
                .padRight(rowHeight)
                .expandY();
        table.add(googlePlus)
                .height(rowHeight)
                .left();

        table.row();

        facebookButton = createTextButton("");
        updateFacebookButton();
        facebookButton.addListener(facebookButtonListener);
        table.add(new Image(getSkin().getDrawable(Res.textures.FACEBOOK_ICON)))
                .maxHeight(rowHeight * 1.2F)
                .maxWidth(rowHeight * 1.2F)
                .padRight(rowHeight)
                .expandY();
        table.add(facebookButton)
                .height(rowHeight)
                .left();
    }

    private CheckBoxButton getVibrationButton() {
        final CheckBoxButton vibrationButton = createCheckbox(getSkin().get(Res.properties.STRINGS, Strings.class).vibration);

        vibrationButton.setChecked(AppPreferences.getInstance().isVibrationEnabled());
        vibrationButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                AppPreferences.getInstance().setVibrationEnabled(vibrationButton.isChecked());
            }
        });

        return vibrationButton;
    }

    private CheckBoxButton getSoundButton() {
        final CheckBoxButton soundButton = createCheckbox(getSkin().get(Res.properties.STRINGS, Strings.class).sound);

        soundButton.setChecked(AppPreferences.getInstance().isSoundEnabled());
        soundButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                AppPreferences.getInstance().setSoundEnabled(soundButton.isChecked());
            }
        });

        return soundButton;
    }

    private void initButtons() {
        final ImageButton positiveButton = new ImageButton(getOkButtonStyle());
        positiveButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                super.clicked(event, x, y);

                positiveButtonListener.onButtonClicked();
            }
        });

        addRightBottomButton(positiveButton);
    }

    private CheckBoxButton createCheckbox(final String text) {
        final float padRight = getCheckboxStyle().font.getCapHeight()
                / 2;
        return new CheckBoxButton(text, getCheckboxStyle(), padRight);
    }

    private TextButton createTextButton(final String text) {
        final TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = getCheckboxStyle().font;
        style.fontColor = getCheckboxStyle().fontColor;
        style.disabledFontColor = getCheckboxStyle().disabledFontColor;

        return new TextButton(text, style);
    }

    @Override
    public BaseDialog show(final Stage stage) {
        multiplayerClient.registerConnectionCallbacks(this);

        return super.show(stage);
    }

    @Override
    public void hide() {
        multiplayerClient.unregisterConnectionCallbacks(this);

        super.hide();
    }


    @Override
    public void onUserInfoUpdated(final UserInfo userInfo) {
        updateFacebookButton();
    }

    @Override
    public void onUserPurchaseResult(final Item item, final boolean success) {
        // Should stay empty
    }

    @Override
    public void onUserInfoRequestFailed(final UserRequestFail cause) {
        updateFacebookButton();
    }

    @Override
    public void onDisconnected() {
        updateFacebookButton();
    }

    @Override
    public void onJoinRoomError() {
        // Should stay empty
    }

    @Override
    public void onGetRoomInfoError() {
        // Should stay empty
    }

    @Override
    public void onWaitingForPlayers() {
        // Should stay empty
    }

    @Override
    public void onPlayerConnected(final String userId, final String playerName, final Level level, final URL photoUrl, final boolean male) {
        // Should stay empty
    }

    @Override
    public void onPlayerDisconnected(final String userId) {
        // Should stay empty
    }

    @Override
    public void onPlayersReady() {
        // Should stay empty
    }

    private void updateFacebookButton() {
        final String facebookText = multiplayerClient.isSignedIn()
                ? getSkin().get(Res.properties.STRINGS, Strings.class).logOutFacebook
                : getSkin().get(Res.properties.STRINGS, Strings.class).loginToFacebook;
        facebookButton.setText(facebookText);
        facebookButton.setDisabled(!connectivityUtils.isNetworkAvailable());
        facebookButton.setTouchable(connectivityUtils.isNetworkAvailable()
                ? Touchable.enabled
                : Touchable.disabled);
    }

    private static class CheckBoxButton extends HorizontalImageTextButton {

        public CheckBoxButton(final String text,
                              final ImageTextButton.ImageTextButtonStyle style,
                              final float padRight) {
            super(text, style);

            getImageCell().padRight(padRight / 2);
            getLabelCell().expandX().left();
        }
    }
}