package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Created by igor on 27.12.14.
 */
public class SinglePlayerStatisticDialog extends GameDialog {

    private UserStatisticModel statistic;

    public SinglePlayerStatisticDialog(final Skin skin,
                                       final ScreenUtils screenUtils,
                                       final DialogCallback positiveButton,
                                       final UserStatisticModel statistic) {
        super(screenUtils,
                skin,
                Res.values.USER_STATS_DIALOG_WIDTH,
                Res.values.USER_STATS_DIALOG_HEIGHT);
        this.statistic = statistic;
        init(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButton.onButtonClicked();
            }
        });
    }

    private void init(final ClickListener positiveButton) {
        final float pad = getContentFrameWidth() / 15;

        Table table = getContentFrame();
        table.add(createTextTitle()).center().colspan(2).padBottom(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgCompletedGamesCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getCompletedGamesCount())).padRight(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgWinGamesCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getWinGamesCount())).padRight(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgLostGameCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getLostGameCount())).padRight(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgPistiScoresCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getPistiScoresCount())).padRight(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgSuperPistiScoresCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getSuperPistiScoresCount())).padRight(pad);
        table.row();
        table.add(createText(getStrings().singlePlayerDlgTotalScoresCount)).left().padLeft(pad);
        table.add(getTextFromLong(statistic.getTotalScoresCount())).padRight(pad);

        final ImageButton button = new ImageButton(getOkButtonStyle());
        button.addListener(positiveButton);
        addRightBottomButton(button);
    }

    private Label createTextTitle() {
        return new Label(getStrings().singlePlayerDlgTableTitle, getContentLargeLabelStyle());
    }

    private Label createText(String text) {
        return new Label(text, getContentSmallLabelStyle());
    }

    private Label getTextFromLong(long digit) {
        String text = String.valueOf(digit);
        return createText(text);
    }
}
