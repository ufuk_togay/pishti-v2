package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.WaitingRoomSlot;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that shows window with waiting room.
 */
public class WaitingRoomDialog extends GameDialog {

    private final UserInfo userInfo;
    private final int players;
    private final List<WaitingRoomSlot> slots;

    private final ImageButton cancel;

    public WaitingRoomDialog(final ScreenUtils screenUtils,
                             final Skin skin,
                             final UserInfo userInfo,
                             final int players,
                             final DialogCallback dialogCallback) {
        super(screenUtils,
                skin,
                Res.values.WAITING_ROOM_WIDTH,
                players == 4 ? Res.values.WAITING_ROOM_HEIGHT_4 : Res.values.WAITING_ROOM_HEIGHT_2);

        this.userInfo = userInfo;
        this.players = players;
        this.slots = new ArrayList<>();

        init();

        cancel = new ImageButton(getBackButtonStyle());
        cancel.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                dialogCallback.onButtonClicked();
            }
        });
        addRightTopButton(cancel);
    }

    private void init() {
        final float height = getContentFrameHeight() / players;
        final Table table = getContentFrame();

        for (int i = 0; i < players; i++) {
            final WaitingRoomSlot slot = new WaitingRoomSlot(getSkin(),
                    getContentFrameWidth(),
                    height);
            slots.add(slot);

            table.add(slot).size(getContentFrameWidth(), height);
            table.row();
        }

        slots.get(0).setUserId(userInfo.getWarpId());
        slots.get(0).setPlayerName(userInfo.getName());
        slots.get(0).setPlayerLevel(userInfo.getLevel(), userInfo.isMale());
        slots.get(0).setPlayerPhotoUrl(userInfo.getPlayerPhotoURL());
    }

    public void connectPlayer(final String userId,
                              final String playerName,
                              final Level level,
                              final URL photoUrl,
                              final boolean male) {
        for (final WaitingRoomSlot slot : slots) {
            if (slot.isPlayerConnected()
                    && slot.getUserId().equals(userId)) {
                break;
            }

            if (!slot.isPlayerConnected()) {
                slot.setUserId(userId);
                slot.setPlayerName(playerName);
                slot.setPlayerLevel(level, male);
                slot.setPlayerPhotoUrl(photoUrl);
                break;
            }
        }

        if (allConnected()) {
            cancel.setTouchable(Touchable.disabled);
            cancel.setDisabled(true);
        }
    }

    private boolean allConnected() {
        for (final WaitingRoomSlot slot : slots) {
            if (!slot.isPlayerConnected()) {
                return false;
            }
        }

        return true;
    }

    public void disconnectPlayer(final String userId) {
        for (final WaitingRoomSlot slot : slots) {
            if (slot.isPlayerConnected() && slot.getUserId().equals(userId)) {
                slot.disconnect();
                break;
            }
        }

        cancel.setTouchable(Touchable.enabled);
        cancel.setDisabled(false);
    }

    public void setSearchingGameMessage() {

    }

    public void setJoinRoomErrorMessage() {

    }

    public void setWaitingForPlayersMessage() {

    }

    public void setPlayersFoundMessage() {

    }
}