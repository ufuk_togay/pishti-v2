package com.isbirbilisim.pisti.dialogs;

import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.game.GameResultManager;
import com.isbirbilisim.pisti.game.GameplayType;
import com.isbirbilisim.pisti.multiplayer.ConnectionCallback;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.view.PlayerWidget;

import java.util.List;

/**
 * Instantiates game dialogs.
 */
public class DialogManager {
    private final Pisti pisti;

    public DialogManager(final Pisti pisti) {
        this.pisti = pisti;
    }

    /**
     * Creates dialog with match final score.
     *
     * @param players - list of players.
     * @return - Dialog instance.
     */
    public GameDialog getFinalScoresDialog(final List<PlayerWidget> players,
                                           final GameplayType gameplayType,
                                           final GameResultManager gameResultManager,
                                           final DialogCallback returnButton,
                                           final DialogCallback playAgainButton) {
        return new FinalScoresDialog(
                pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                gameplayType == GameplayType.MULTIPLAYER,
                players,
                gameResultManager,
                returnButton,
                playAgainButton
        );
    }

    /**
     * Creates dialog with game abandoning message.
     *
     * @param positiveButton - positive button click listener.
     * @param negativeButton - negative button click listener.
     * @return - Dialog instance.
     */
    public LeaveGameDialog getLeaveGameDialog(final DialogCallback positiveButton,
                                              final DialogCallback negativeButton) {
        return new LeaveGameDialog(
                pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                positiveButton,
                negativeButton);
    }

    /**
     * Creates dialog for selecting number of players.
     *
     * @param listener - listener that called when user selects number of
     *                 game participants or cancels dialog.
     * @return - Dialog instance.
     */
    public GameDialog getSetGameOptionsDialog(
            final GameOptionsDialog.OnGameOptionsSetListener listener) {
        return new GameOptionsDialog(
                pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                listener);
    }

    /**
     * Creates dialog for selecting gaming room.
     *
     * @return - Dialog instance.
     */
    public GameDialog getSelectGamingRoomDialog(
            final GamingRoomDialog.OnLevelSelectedListener listener,
            final Level maxAvailableLevel,
            final boolean isMale) {
        return new GamingRoomDialog(
                pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                listener,
                maxAvailableLevel,
                isMale);
    }

    /**
     * Creates dialog that shows "Coming soon!" message.
     */
    public GameDialog getComingSoonDialog(final DialogCallback positiveButton) {
        return new ComingSoonDialog(pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                positiveButton);
    }

    /**
     * Creates dialog that shows "About the game" message.
     */
    public GameDialog getAboutDialog(final DialogCallback positiveButton) {
        return new AboutDialog(pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                positiveButton);
    }

    /**
     * Creates dialog that shows "Single player statistic dialog" message.
     */
    public GameDialog getSinglePlayerStatisticDialog(final DialogCallback positiveButton,
                                                     UserStatisticModel statistic) {
        return new SinglePlayerStatisticDialog(pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils(),
                positiveButton, statistic);
    }

    /**
     * Creates dialog that shows game settings.
     */
    public GameDialog getSettingsDialog(final DialogCallback positiveButton,
                                        final DialogCallback facebookListener) {
        return new SettingsDialog(
                pisti.getScreenUtils(),
                pisti.getConnectivityUtils(),
                pisti.getMultiplayerClient(),
                pisti.getPlatformInfo(),
                pisti.getAssets().getDialogSkin(),
                positiveButton,
                facebookListener);
    }

    public GameDialog getMultiplayerErrorDialog(final DialogCallback clickListener,
                                                final ConnectionCallback.UserRequestFail cause) {
        return new MultiplayerInitializationErrorDialog(
                pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                cause,
                clickListener);
    }

    public GameDialog getLoadingPlayerDialog() {
        return new LoadingPlayerInfoDialog(pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin());
    }

    public BaseDialog getLoadingDialog() {
        return new LoadingDialog(pisti.getAssets().getDialogSkin(),
                pisti.getScreenUtils());
    }

    public GameDialog getGameSearchDialog(final DialogCallback clickListener) {
        return new GameSearchDialog(pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                clickListener);
    }

    public GameDialog getNetworkUnavailableDialog(final DialogCallback listener) {
        return new NetworkRequiredDialog(pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                listener);
    }

    public GameDialog getUserNotSignedDialog(final DialogCallback positive,
                                             final DialogCallback negative) {
        return new UserNotSignedDialog(pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                positive,
                negative);
    }

    public WaitingRoomDialog getWaitingRoomDialog(final UserInfo user,
                                                  final int players,
                                                  final DialogCallback negative) {
        return new WaitingRoomDialog(pisti.getScreenUtils(),
                pisti.getAssets().getDialogSkin(),
                user,
                players,
                negative);
    }
}