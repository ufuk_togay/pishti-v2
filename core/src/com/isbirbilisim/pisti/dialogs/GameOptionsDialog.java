package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.HorizontalImageTextButton;

/**
 * Dialog that allows user to choose number of game participants.
 */
public class GameOptionsDialog extends GameDialog {

    private final OnGameOptionsSetListener listener;

    private final WidgetGroup rootGroup;

    private final GameSettings gameSettings;

    public GameOptionsDialog(
            final Skin skin,
            final ScreenUtils screenUtils,
            final OnGameOptionsSetListener listener) {
        super(screenUtils,
                skin,
                Res.values.GAME_SETTINGS_DIALOG_WIDTH,
                Res.values.GAME_SETTINGS_DIALOG_HEIGHT);
        this.listener = listener;

        gameSettings = new GameSettings();

        rootGroup = new WidgetGroup();
        rootGroup.setSize(getContentFrameWidth(), getContentFrameHeight());

        getContentFrame().add(rootGroup).size(getContentFrameWidth(), getContentFrameHeight());

        init();
    }

    private void init() {
        final Table gameLengthTable = new Table();
        gameLengthTable.setSize(getContentFrameWidth() / 2.5F, getContentFrameHeight());
        gameLengthTable.setPosition(getContentFrameWidth() / 4 * 3 - (getContentFrameWidth() / 4), 0);
        initGameLengthTable(gameLengthTable);
        rootGroup.addActor(gameLengthTable);

        final Table playersNumberTable = new Table();
        playersNumberTable.setSize(getContentFrameWidth() / 2.5F, getContentFrameHeight());
        playersNumberTable.setPosition(getContentFrameWidth() / 4 - (getContentFrameWidth() / 4), 0);
        initPlayersNumberTable(playersNumberTable);
        rootGroup.addActor(playersNumberTable);

        final ImageButton negativeButton = new ImageButton(getBackButtonStyle());
        negativeButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);
                listener.onDialogCanceled();
            }
        });
        addRightTopButton(negativeButton);

        final ImageButton positiveButton = new ImageButton(getOkButtonStyle());
        positiveButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);
                listener.onGameOptionsSet(gameSettings);
            }
        });
        addRightBottomButton(positiveButton);
    }

    private void initPlayersNumberTable(final Table table) {
        final ClickListener clickListener = new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);
                final ChoiceItem button = (ChoiceItem) event.getListenerActor();
                final String text = button.getText().toString();

                for (final GameSettings.PlayersNumber value : GameSettings.PlayersNumber.values()) {
                    if (getString(value).equals(text)) {
                        gameSettings.numberOfPlayers = value;
                    }
                }
            }
        };

        final ButtonGroup group = new ButtonGroup();
        for (final GameSettings.PlayersNumber item : GameSettings.PlayersNumber.values()) {
            group.add(createChoiceItem(getString(item)));
        }
        group.setChecked(getString(GameSettings.PlayersNumber.values()[0]));

        for (final Button button : group.getButtons()) {
            button.addListener(clickListener);
            table.add(button)
                    .expandX()
                    .fillX()
                    .left()
                    .padTop(getScreenUtils().toRealHeight(Res.values.RADIO_BUTTON_SIZE) / 2)
                    .padBottom(getScreenUtils().toRealHeight(Res.values.RADIO_BUTTON_SIZE) / 4)
                    .row();
        }
    }

    private String getString(final GameSettings.PlayersNumber playersNumber) {
        if (playersNumber == GameSettings.PlayersNumber.TWO_PLAYERS) {
            return getStrings().players2;
        }
        return getStrings().players4;
    }

    private String getString(final GameSettings.GameLength gameLength) {
        switch (gameLength) {
            case ONE_ROUND:
                return getStrings().oneRound;
            case SHORT_GAME:
                return getStrings().shortGame;
            case LONG_GAME:
                return getStrings().longGame;
            default:
                return "";
        }
    }

    private void initGameLengthTable(
            final Table table) {
        final ClickListener clickListener = new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);
                final ChoiceItem button = (ChoiceItem) event.getListenerActor();
                final String text = button.getText().toString();

                for (final GameSettings.GameLength value : GameSettings.GameLength.values()) {
                    if (getString(value).equals(text)) {
                        gameSettings.gameLength = value;
                    }
                }
            }
        };

        final ButtonGroup group = new ButtonGroup();
        for (final GameSettings.GameLength item : GameSettings.GameLength.values()) {
            group.add(createChoiceItem(getString(item)));
        }
        group.setMaxCheckCount(1);
        group.setMinCheckCount(1);
        group.setChecked(getString(GameSettings.GameLength.values()[0]));

        for (final Button button : group.getButtons()) {
            button.addListener(clickListener);
            table.add(button)
                    .expandX()
                    .fillX()
                    .left()
                    .padTop(getScreenUtils().toRealHeight(Res.values.RADIO_BUTTON_SIZE) / 4)
                    .padBottom(getScreenUtils().toRealHeight(Res.values.RADIO_BUTTON_SIZE) / 4)
                    .row();
        }
    }

    private ChoiceItem createChoiceItem(final String value) {
        return new ChoiceItem(value,
                getRadioButtonStyle(),
                getContentFrameWidth() / 2,
                getScreenUtils().toRealHeight(Res.values.RADIO_BUTTON_SIZE));
    }

    /**
     * Base class for notifying app about user choice.
     */
    public static abstract class OnGameOptionsSetListener {

        /**
         * Called when user sets game settings.
         *
         * @param gameSettings - game settings.
         */
        public void onGameOptionsSet(
                final GameSettings gameSettings) {
            // Default implementation does nothing.
        }

        /**
         * Called when user cancels dialog.
         */
        public void onDialogCanceled() {
            // Default implementation does nothing.
        }
    }

    private static class ChoiceItem extends HorizontalImageTextButton {

        private ChoiceItem(final String text,
                           final ImageTextButton.ImageTextButtonStyle style,
                           final float width,
                           final float height) {
            super(text, style);

            setSize(width, height);

            getImageCell().size(height, height).fill().padLeft(height);
            getLabelCell().expandX().fillX();
        }
    }
}