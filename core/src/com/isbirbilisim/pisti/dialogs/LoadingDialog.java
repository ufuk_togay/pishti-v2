package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.Spinner;

/**
 * Loading spinner.
 */
public class LoadingDialog extends BaseDialog {

    private final Skin skin;
    private final ScreenUtils screenUtils;

    private Spinner spinner;

    public LoadingDialog(final Skin skin, final ScreenUtils screenUtils) {
        this.skin = skin;
        this.screenUtils = screenUtils;

        init();
    }

    private void init() {
        spinner = new Spinner(skin, getPrefWidth());
        addActor(spinner);
    }

    @Override
    public float getPrefHeight() {
        return screenUtils.toRealWidth(Res.values.SPINNER_SIZE);
    }

    @Override
    public float getPrefWidth() {
        return screenUtils.toRealWidth(Res.values.SPINNER_SIZE);
    }
}