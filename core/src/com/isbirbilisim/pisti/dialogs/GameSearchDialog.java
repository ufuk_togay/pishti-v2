package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Dialog which shows game search status
 */
public class GameSearchDialog extends GameDialog {

    private Label statusLabel;

    public GameSearchDialog(final ScreenUtils screenUtils,
                            final Skin skin,
                            final DialogCallback cancel) {
        super(screenUtils, skin, Res.values.GAME_SEARCH_DIALOG_WIDTH, Res.values.GAME_SEARCH_DIALOG_HEIGHT);

        statusLabel = new Label("", getContentLargeLabelStyle());

        final Button button = new ImageButton(getBackButtonStyle());
        button.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                cancel.onButtonClicked();
            }
        });
        addRightTopButton(button);
        getContentFrame().add(statusLabel);
    }

    public void setSearchingGameMessage() {
        statusLabel.setText(getStrings().searchingGame);
    }

    public void setJoinRoomErrorMessage() {
        statusLabel.setText(getStrings().cantJoinRoom);
    }

    public void setWaitingForPlayersMessage() {
        statusLabel.setText(getStrings().waitingForOtherPlayers);
    }

    public void setPlayersFoundMessage() {
        statusLabel.setText(getStrings().playersFound);
    }
}
