package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.ScreenUtils;

public class ComingSoonDialog extends GameDialog {

    public ComingSoonDialog(final Skin skin,
                            final ScreenUtils screenUtils,
                            final DialogCallback positiveButton) {
        super(screenUtils,
                skin,
                Res.values.COMING_SOON_DIALOG_WIDTH,
                Res.values.COMING_SOON_DIALOG_HEIGHT);
        init(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButton.onButtonClicked();
            }
        });
    }

    private void init(final ClickListener positiveButton) {
        getContentFrame().add(createTextMessage());

        final ImageButton button = new ImageButton(getOkButtonStyle());
        button.addListener(positiveButton);
        addRightBottomButton(button);
    }

    private Label createTextMessage() {
        return new Label(getStrings().comingSoonMessage, getContentLargeLabelStyle());
    }
}