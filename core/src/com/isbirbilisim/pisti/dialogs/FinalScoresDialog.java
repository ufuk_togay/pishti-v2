package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.game.GameResult;
import com.isbirbilisim.pisti.game.GameResultManager;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.PlayerWidget;

import java.util.ArrayList;
import java.util.List;

/**
 * Dialog that shows table with game final scores.
 */
public class FinalScoresDialog extends GameDialog {
    private final List<PlayerWidget> players;
    private final GameResultManager gameResultManager;

    private final float scoresRowWidth;
    private float photoSize;

    public FinalScoresDialog(final Skin skin,
                             final ScreenUtils screenUtils,
                             final boolean isMultiplayer,
                             final List<PlayerWidget> players,
                             final GameResultManager gameResultManager,
                             final DialogCallback returnButton,
                             final DialogCallback playAgainButton) {
        super(screenUtils,
                skin,
                Res.values.FINAL_SCORES_DIALOG_WIDTH,
                players.size() == 2
                        ? Res.values.FINAL_SCORES_2_DIALOG_HEIGHT
                        : Res.values.FINAL_SCORES_4_DIALOG_HEIGHT);
        this.players = players;
        this.gameResultManager = gameResultManager;

        scoresRowWidth = getPrefWidth() / 5;
        photoSize = screenUtils.toRealHeight(players.size() == 2
                ? Res.values.FINAL_SCORES_2_PHOTO_SIZE
                : Res.values.FINAL_SCORES_4_PHOTO_SIZE);

        getContentFrame().add(createResultsTableHeader()).expandX().fillX();
        getContentFrame().row();
        getContentFrame().add(createPlayersResultTable()).expandX().fillX();
        getContentFrame().row();

        if (!isMultiplayer) {
            getContentFrame().add(createPlayAgainMessage())
                    .padTop(screenUtils.toRealHeight(Res.values.DIALOG_MESSAGE_PAD))
                    .padBottom(screenUtils.toRealHeight(Res.values.DIALOG_MESSAGE_PAD) / 1.5F);
        }

        final ImageButton positiveButton = new ImageButton(getOkButtonStyle());
        positiveButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                hide();
                playAgainButton.onButtonClicked();
            }
        });

        final ImageButton negativeButton = new ImageButton(getBackButtonStyle());
        negativeButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                hide();
                returnButton.onButtonClicked();
            }
        });

        addRightTopButton(negativeButton);
        if (!isMultiplayer) {
            addRightBottomButton(positiveButton);
        }
    }

    private Table createResultsTableHeader() {
        final Table table = new Table();

        final Label collectedCards =
                new Label(getStrings().columnCollectedCards, getContentSmallLabelStyle());
        collectedCards.setAlignment(Align.center);

        final Label totalScores =
                new Label(getStrings().columnTotalScores, getContentSmallLabelStyle());
        totalScores.setAlignment(Align.center);

        table.add()
                .size(photoSize, photoSize);
        table.add()
                .expandX()
                .padLeft(photoSize / 3)
                .left();
        table.add(collectedCards)
                .width(scoresRowWidth);
        table.add(totalScores)
                .width(scoresRowWidth);
        table.add()
                .width(scoresRowWidth / 2);

        return table;
    }

    private Table createPlayersResultTable() {
        final Table table = new Table();
        for (final PlayerWidget playerWidget : players) {
            table.add(createPlayerRow(playerWidget)).expandX().fillX().padTop(10);
            table.row();
        }

        return table;
    }

    private Table createPlayerRow(final PlayerWidget player) {
        final Table table = new Table();

        final Actor playerPhoto = createPlayerImage(player, photoSize);

        final Label name;
        if (player.getPlayer().getUserInfo() == null) {
            name = new Label(getStrings().player, getContentSmallLabelStyle());
        } else {
            name = new Label(player.getPlayer().getUserInfo().getName(), getContentSmallLabelStyle());
        }

        final Label collectedCards = new Label(String.valueOf(player.getPlayer()
                .collectedCardsCount()), getCountsLabelStyle());
        collectedCards.setAlignment(Align.center);

        final Label totalScores = new Label(String.valueOf(player.getPlayer().getScores()
                .getMultiroundScores()), getCountsLabelStyle());
        totalScores.setAlignment(Align.center);

        final String gameResult;

        final List<Player> players = new ArrayList<>();
        for (final PlayerWidget playerWidget : this.players) {
            players.add(playerWidget.getPlayer());
        }
        final MatchResult matchResult = gameResultManager.calculateMatchResult(new ArrayList<Player>(players), player.getPlayer());
        if (matchResult.getGameResult() == GameResult.WIN1) {
            gameResult = getStrings().winner;
        } else {
            gameResult = "";
        }

        final Label place = new Label(gameResult, getCountsLabelStyle());
        totalScores.setAlignment(Align.center);

        table.add(playerPhoto)
                .size(photoSize, photoSize);
        table.add(name)
                .expandX()
                .padLeft(photoSize / 3)
                .left();
        table.add(collectedCards)
                .width(scoresRowWidth);
        table.add(totalScores)
                .width(scoresRowWidth);
        table.add(place)
                .width(scoresRowWidth / 2);

        return table;
    }

    private Label.LabelStyle getCountsLabelStyle() {
        final Label.LabelStyle style = new Label.LabelStyle();
        style.font = getContentLargeLabelStyle().font;
        style.fontColor = Res.colors.FINAL_SCORES_COUNTERS_COLOR;

        return style;
    }

    private Actor createPlayerImage(final PlayerWidget player,
                                    final float size) {
        final float photoFrameSize = size;
        final float frameThickness = getScreenUtils().toRealWidth(Res.values.PHOTO_FRAME_THICKNESS);
        final float photoSize = photoFrameSize - frameThickness;

        final WidgetGroup photoPlace = new WidgetGroup();
        photoPlace.setSize(photoFrameSize, photoFrameSize);

        final Image photo = new Image(player.getUserPicture());
        photo.setSize(photoSize, photoSize);
        photo.setPosition(frameThickness / 2, frameThickness / 2);

        final Image frame = new Image(getSkin().getDrawable(Res.textures.PLAYER_PHOTO_FRAME));
        frame.setSize(photoFrameSize, photoFrameSize);

        photoPlace.addActor(photo);
        photoPlace.addActor(frame);

        return photoPlace;
    }

    private Label createPlayAgainMessage() {
        return new Label(
                getStrings().playAgainMessage,
                getContentLargeLabelStyle());
    }
}