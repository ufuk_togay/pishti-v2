package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Dialog that shows message about game abandoning.
 */
public class LeaveGameDialog extends GameDialog {

    public LeaveGameDialog(final Skin skin,
                           final ScreenUtils screenUtils,
                           final DialogCallback positiveButton,
                           final DialogCallback negativeButton) {
        super(screenUtils,
                skin,
                Res.values.LEAVE_GAME_DIALOG_WIDTH,
                Res.values.LEAVE_GAME_DIALOG_HEIGHT);

        init(positiveButton, negativeButton);
    }

    private void init(final DialogCallback positiveButtonListener,
                      final DialogCallback negativeButtonListener) {
        final float iconHeight = getPrefHeight() / 4;
        final float iconWidth = iconHeight / 2;
        getContentFrame()
                .add(new Image(getSkin().getDrawable(Res.textures.LEAVE_GAME_ICON)))
                .size(iconWidth, iconHeight)
                .padRight(iconWidth);

        getContentFrame()
                .add(createTextMessage());

        final ImageButton positiveButton = new ImageButton(getOkButtonStyle());
        positiveButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButtonListener.onButtonClicked();
            }
        });

        addRightBottomButton(positiveButton);

        final ImageButton negativeButton = new ImageButton(getBackButtonStyle());
        negativeButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                negativeButtonListener.onButtonClicked();
            }
        });

        addRightTopButton(negativeButton);
    }

    private Label createTextMessage() {
        return new Label(
                getStrings().leaveGameDialogMessage,
                getContentLargeLabelStyle());
    }
}