package com.isbirbilisim.pisti.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.ScreenUtils;


/**
 * Created by igor on 11.11.14.
 */
public class AboutDialog extends GameDialog {

    public static final String PICAREX_URL = "http://picarex.com/";
    public static final float LOGO_HEIGHT = 180f;
    public static final float LOGO_WIDTH = 150f;

    public AboutDialog(final Skin skin,
                       final ScreenUtils screenUtils,
                       final DialogCallback positiveButton) {
        super(screenUtils,
                skin,
                Res.values.ABOUT_DIALOG_WIDTH,
                Res.values.ABOUT_DIALOG_HEIGHT);
        init(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                positiveButton.onButtonClicked();
            }
        });
    }

    private void init(final ClickListener positiveButton) {
        Table table = getContentFrame();

        final float logoWidth = getScreenUtils().toRealWidth(LOGO_WIDTH);
        final float logoHeight = getScreenUtils().toRealHeight(LOGO_HEIGHT);

        table.add(createLogo())
                .center()
                .height(logoHeight) // Height of Cell
                .width(logoWidth) // Width of Cell
                .top()
                .fill(); // Makes object inside Cell fill it.
        table.row();
        table.add(createTextMessage()).center();
        table.row();
        table.add(createTextMessageForCopyright()).center().padBottom(10f);
        table.row();
        table.add(createTextMessageForLink()).center().padBottom(50);

        final ImageButton button = new ImageButton(getOkButtonStyle());
        button.addListener(positiveButton);
        addRightBottomButton(button);
    }

    private Label createTextMessage() {
        return new Label(getStrings().aboutDialogMessage, getContentLargeLabelStyle());
    }

    private Label createTextMessageForLink() {
        Label label = new Label(getStrings().aboutDialogLink, getContentLinkLabelStyle());
        label.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                if (((InputEvent) event).getType().equals(InputEvent.Type.touchDown)) {
                    Gdx.net.openURI(PICAREX_URL);
                    return true;
                }
                return false;
            }
        });
        return label;
    }

    private Label createTextMessageForCopyright() {
        return new Label(getStrings().aboutDialogCopyright, getContentSmallLabelStyle());
    }

    private Image createLogo() {
        Image image = new Image(getSkin().getDrawable(Res.textures.STUDIO_ICON));
        image.layout();
        return image;
    }
}
