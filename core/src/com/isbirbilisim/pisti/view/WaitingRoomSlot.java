package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.resources.AvatarPicker;
import com.isbirbilisim.pisti.resources.Res;

import java.net.URL;

public class WaitingRoomSlot extends Table {

    private final Skin skin;
    private final LoadingName loadingName;
    private final Label levelLabel;
    private final Image photo;

    private String userId;

    private boolean playerConnected;

    public WaitingRoomSlot(final Skin skin,
                           final float width,
                           final float height) {
        this.skin = skin;

        setSize(width, height);

        loadingName = new LoadingName(skin, width / 3, height);
        add(loadingName).width(width / 3).left().bottom();

        levelLabel = new Label("", getLabelStyle());
        levelLabel.setWidth(width / 3);
        levelLabel.setAlignment(Align.center);
        add(levelLabel).width(width / 3);

        photo = new Image();
        photo.setSize(height, height);
        add(photo).size(height, height).right();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public void setPlayerName(final String name) {
        loadingName.setPlayerName(name);

        playerConnected = true;
    }

    public void setPlayerLevel(final Level level,
                               final boolean male) {
        levelLabel.setText(level.getName(male));
    }

    public void setPlayerPhotoUrl(final URL url) {
        AvatarPicker.setImageAsync(photo, url);
    }

    private Label.LabelStyle getLabelStyle() {
        return skin.get(Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE, Label.LabelStyle.class);
    }

    public boolean isPlayerConnected() {
        return playerConnected;
    }

    public void disconnect() {
        playerConnected = false;
        levelLabel.setText("");
        photo.setVisible(false);
        loadingName.setLoading();
    }

    private static class LoadingName extends WidgetGroup {

        private final Spinner spinner;

        private Label name;

        private LoadingName(final Skin skin,
                            final float width,
                            final float height) {
            setSize(width, height);

            spinner = new Spinner(skin, height * 0.8F);
            spinner.setPosition(width / 2 - (height * 0.8F / 2),
                    height / 2 - (height * 0.8F / 2));

            name = new Label("", skin.get(Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE, Label.LabelStyle.class));
            name.setVisible(false);
            name.setSize(width, height);
            name.setPosition(0, 0);
            name.setAlignment(Align.center);

            addActor(spinner);
            addActor(name);
        }

        private void setPlayerName(final String name) {
            this.name.setText(name);
            this.name.setVisible(true);

            this.spinner.setVisible(false);
        }

        private void setLoading() {
            this.name.setVisible(false);
            this.spinner.setVisible(true);
        }
    }
}