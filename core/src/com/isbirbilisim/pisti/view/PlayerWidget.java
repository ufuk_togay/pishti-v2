package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.Role;
import com.isbirbilisim.pisti.resources.AvatarPicker;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.scores.Scores;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Shows info about player.
 */
public class PlayerWidget extends Table {
    private final ScreenUtils screenUtils;
    private final float width;
    private final float height;
    private final Player player;
    private final Skin skin;
    private final boolean multiround;
    private final boolean networkAvailable;

    private Label level;
    private Label name;

    private Image photo;
    private WidgetGroup photoPlace;

    private ScoreView totalScore;
    private ScoreView cardCount;
    private ScoreView multiroundScore;

    private Image photoTimer;
    private final Image photoTurn;
    private final Timer timer;

    private ChatOutput chatOutput;

    public PlayerWidget(final ScreenUtils screenUtils,
                        final float width,
                        final float height,
                        final Player player,
                        final Skin skin,
                        final boolean multiround,
                        final boolean networkAvailable,
                        final Image photoTurn) {
        this.screenUtils = screenUtils;
        this.width = width;
        this.height = height;
        this.player = player;
        this.skin = skin;
        this.multiround = multiround;
        this.networkAvailable = networkAvailable;
        this.photoTurn = photoTurn;

        timer = new Timer();

        init();
    }

    private void init() {
        setSize(width, height);

        final String stringName;
        if (player.getRole() == Role.PLAYER) {
            stringName = skin.get(Res.properties.STRINGS, Strings.class).player;
        } else {
            stringName = player.getUserInfo().getName() == null
                    ? ""
                    : player.getUserInfo().getName();
        }

        final String levelString;
        if (player.getUserInfo() == null) {
            levelString = Level.LEVEL1.getName(true);
        } else {
            levelString = player.getUserInfo().getLevel().getName(player.getUserInfo().isMale());
        }

        final Label.LabelStyle labelStyle = pickStyle();
        level = new Label(levelString, labelStyle);
        level.setFontScale(0.75F);

        name = new Label(stringName, labelStyle);
        name.setFontScale(0.75F);

        final float photoFrameSize = height / 3 * 2;
        final float frameThickness = screenUtils.toRealWidth(Res.values.PHOTO_FRAME_THICKNESS);
        final float photoSize = photoFrameSize - frameThickness;

        photoPlace = new WidgetGroup();

        photo = new Image();
        AvatarPicker.setAvatar(photo, player, networkAvailable);
        photo.setSize(photoSize, photoSize);
        photo.setPosition(frameThickness / 2, frameThickness / 2);

        final Drawable timerDrawable = skin.getDrawable(Res.textures.PHOTO_TIMER);
        photoTimer = new Image(timerDrawable);
        photoTimer.setVisible(false);
        photoTimer.setSize(photoSize, photoSize);
        photoTimer.setPosition(frameThickness / 2, frameThickness / 2);

        final Image frame = new Image(skin.getDrawable(Res.textures.PLAYER_PHOTO_FRAME));
        frame.setSize(photoFrameSize, photoFrameSize);

        photoPlace.addActor(photo);
        photoPlace.addActor(photoTimer);
        photoPlace.addActor(frame);

        add();
        add(level).expandX().center();
        add();
        row();

        add(getScoresTable(width - photoFrameSize, photoFrameSize))
                .width(width - photoFrameSize)
                .height(photoFrameSize);
        add(photoPlace).size(photoFrameSize, photoFrameSize);
        row();
        add();
        add(name).expandX().center();
        add();
    }

    public void showChatMessage(final String message) {
        if (chatOutput == null) {
            chatOutput = new ChatOutput(screenUtils, skin);
            chatOutput.setPosition(width / 3 * 2, height / 3 * 2);
            addActor(chatOutput);
        }

        chatOutput.showMessage(message);
    }

    private WidgetGroup getScoresTable(final float width, final float height) {
        final Label.LabelStyle style = pickStyle();
        final BitmapFont.TextBounds scoresSize = getScoresSize(style);
        final float scoreWidth = scoresSize.width;
        final float scoreHeight = scoresSize.height;

        final float scoreX = width / 2 - (scoreWidth / 2);
        final float totalScoreY;
        final float cardCountY;
        final float multiroundScoreY;

        final float spaceY;
        if (multiround) {
            spaceY = (height - (scoreHeight * 3)) / 4;
        } else {
            spaceY = (height - (scoreHeight * 2)) / 3;
        }

        totalScoreY = height - spaceY - scoreHeight;
        cardCountY = totalScoreY - spaceY - scoreHeight;
        multiroundScoreY = cardCountY - spaceY - scoreHeight;

        totalScore = new ScoreView(style, scoreWidth, scoreHeight, Res.colors.TOTAL_SCORE_COLOR);
        totalScore.setPosition(scoreX, totalScoreY);
        cardCount = new ScoreView(style, scoreWidth, scoreHeight, Res.colors.CARD_COUNT_COLOR);
        cardCount.setPosition(scoreX, cardCountY);

        if (multiround) {
            multiroundScore = new ScoreView(style, scoreWidth, scoreHeight, Res.colors.MULTIROUND_COLOR);
            multiroundScore.setPosition(scoreX, multiroundScoreY);
        }

        final WidgetGroup scoresTable = new WidgetGroup();
        scoresTable.addActor(totalScore);
        scoresTable.addActor(cardCount);

        if (multiround) {
            scoresTable.addActor(multiroundScore);
        }

        return scoresTable;
    }

    private BitmapFont.TextBounds getScoresSize(final Label.LabelStyle style) {
        return style.font.getBounds("666");
    }

    private Label.LabelStyle pickStyle() {
        return skin.get(player.getRole() == Role.PLAYER
                        ? Res.labelStyles.GAME_SCREEN_LARGE_LABEL_STYLE
                        : Res.labelStyles.GAME_SCREEN_SMALL_LABEL_STYLE,
                Label.LabelStyle.class);
    }

    /**
     * Updates view with player's scores.
     *
     * @param scores - updated player's scores.
     */
    public void updateScores(final Scores scores) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                addAction(Actions.sequence(
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                totalScore.updateScoresIfNeeded((int) scores.getTotalScore());
                            }
                        }),
                        Actions.delay(0.15F),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                cardCount.updateScoresIfNeeded(scores.getCardCount());
                            }
                        }),
                        Actions.delay(0.15F),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                if (multiround) {
                                    multiroundScore.updateScoresIfNeeded(scores.getMultiroundScores());
                                }
                            }
                        })
                ));
            }
        });
    }

    /**
     * Starts move progress animation.
     *
     * @param time - count down initial time.
     */

    public void startCountDown(final long time,
                               final Timer.Task executionFinishedTask) {
        photoTimer.setVisible(true);

        timer.clear();
        timer.stop();

        final int ticks = 10;
        for (int i = 0; i < ticks; i++) {
            scheduleScaleTask(i, ticks, time);
        }

        if (executionFinishedTask != null) {
            timer.scheduleTask(executionFinishedTask, time);
        }

        timer.start();
    }

    private void scheduleScaleTask(final int tick,
                                   final int ticks,
                                   final long time) {
        timer.scheduleTask(
                new Timer.Task() {
                    @Override
                    public void run() {
                        final float yScale = 1 - (tick / (float) ticks);
                        photoTimer.setScale(1, yScale);
                    }
                },
                time / ticks * tick);
    }

    /**
     * Stops move progress count down.
     */
    public void stopCountDown() {
        timer.clear();
        timer.stop();

        photoTimer.setScale(1);
        photoTimer.setVisible(false);
    }

    public Player getPlayer() {
        return player;
    }

    public void showTurnToMove() {
        photoTurn.addAction(Actions.fadeIn(0.4F));
    }

    public void hideTurnToMove() {
        photoTurn.addAction(Actions.fadeOut(0.4F));
    }

    public Drawable getUserPicture() {
        return photo.getDrawable();
    }

    /**
     * Contains settings of zone with player info.
     */
    public static class PlayerWidgetSettings {
        private final Color color;
        private final float width;
        private final float height;
        private final float x;
        private final float y;

        public PlayerWidgetSettings(final float width,
                                    final float height,
                                    final float x,
                                    final float y,
                                    final Color color) {
            this.width = width;
            this.height = height;
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public float getWidth() {
            return width;
        }

        public float getHeight() {
            return height;
        }

        public float getX() {
            return x;
        }

        public float getY() {
            return y;
        }

        public Color getColor() {
            return color;
        }
    }

    private static class ScoreView extends WidgetGroup {

        private final Label.LabelStyle style;
        private final float width;
        private final float height;
        private final Color color;

        private Label scoreLabel;
        private Label scoreChangeLabel;

        private static final Pool<Action> scaleOutPool = new Pool<Action>() {
            @Override
            protected Action newObject() {
                return Actions.scaleTo(0, 2F, 0.15F);
            }
        };

        private static final Pool<Action> scaleInPool = new Pool<Action>() {
            @Override
            protected Action newObject() {
                return Actions.sequence(
                        Actions.scaleTo(3F, 3F, 0.20F, Interpolation.circleIn),
                        Actions.scaleTo(1F, 1F, 0.45F, Interpolation.circleOut));
            }
        };

        private ScoreView(final Label.LabelStyle style,
                          final float width,
                          final float height,
                          final Color color) {
            this.style = style;
            this.width = width;
            this.height = height;
            this.color = color;

            init();
        }

        private void init() {
            setSize(width, height);

            scoreLabel = new Label("0", style);
            scoreLabel.setFontScale(0.75F);
            scoreLabel.setAlignment(Align.center);
            scoreLabel.setSize(width, height);
            scoreLabel.setPosition(0, 0);

            scoreChangeLabel = new Label("0", getChangeScoreStyle());
            scoreChangeLabel.setFontScale(0.75F);
            scoreChangeLabel.setAlignment(Align.center);
            scoreChangeLabel.setSize(width, height);
            scoreChangeLabel.setPosition(0, 0);

            addActor(scoreChangeLabel);
            addActor(scoreLabel);

            scoreChangeLabel.setVisible(false);

            setOrigin(width / 2, height / 2);
        }

        private Label.LabelStyle getChangeScoreStyle() {
            final Label.LabelStyle changeScoreStyle = new Label.LabelStyle();
            changeScoreStyle.font = style.font;
            changeScoreStyle.fontColor = color;

            return changeScoreStyle;
        }

        private void updateScoresIfNeeded(final int newScore) {
            if (areScoresUpdated(newScore)) {
                updateScore(newScore);
            }
        }

        private boolean areScoresUpdated(final int newScore) {
            final int currentScore = Integer.valueOf(scoreLabel.getText().toString());

            return currentScore != newScore;
        }

        private void updateScore(final int newScore) {
            scoreLabel.setVisible(false);
            scoreLabel.setText(String.valueOf(newScore));

            scoreChangeLabel.setVisible(true);

            setTransform(true);

            addAction(getUpdateScoresAction(String.valueOf(newScore)));
        }

        private Action getUpdateScoresAction(final String text) {
            return Actions.sequence(scaleOutPool.obtain(),
                    Actions.run(new Runnable() {

                        @Override
                        public void run() {
                            scoreChangeLabel.setText(text);
                        }
                    }),
                    scaleInPool.obtain(),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            scoreChangeLabel.setVisible(false);
                            scoreLabel.setVisible(true);
                        }
                    }));
        }
    }
}