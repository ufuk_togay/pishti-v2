package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.preferences.AppPreferences;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * Menu that shows game options in GameScreen.
 */
public class GameScreenMenu extends Table {

    private final ScreenUtils screenUtils;
    private final PlatformInfo platformInfo;
    private final Skin skin;
    private final Strings strings;

    private final WidgetGroup rootGroup;
    private final Table mainTable;

    private final float height;

    private final ClickListener leaveGameButtonListener;

    public GameScreenMenu(final ScreenUtils screenUtils,
                          final PlatformInfo platformInfo,
                          final Skin skin,
                          final ClickListener hideMenuButtonListener,
                          final ClickListener leaveGameButtonListener,
                          final float height) {
        this.screenUtils = screenUtils;
        this.platformInfo = platformInfo;
        this.skin = skin;
        this.height = height;
        this.leaveGameButtonListener = leaveGameButtonListener;

        strings = skin.get(Res.properties.STRINGS, Strings.class);

        setSize(getPrefWidth(), height);

        rootGroup = new WidgetGroup();
        add(rootGroup).expand().fill();

        final float mainTableWidth = getPrefWidth()
                - screenUtils.toRealWidth(Res.values.HIDE_MENU_BUTTON_SIZE)
                + ((screenUtils.toRealWidth(Res.values.LINE_THICKNESS)) / 2);

        mainTable = new Table();
        mainTable.setSize(mainTableWidth, height);
        mainTable.setPosition(0, 0);
        mainTable.setBackground(skin.getDrawable(Res.textures.GAME_MENU_BACKGROUND));
        rootGroup.addActor(mainTable);

        final Image rightBorder = new Image(skin.getDrawable(Res.textures.BIG_VERTICAL_LINE));
        rightBorder.setSize(screenUtils.toRealWidth(Res.values.LINE_THICKNESS), height);
        rightBorder.setPosition(mainTableWidth - (screenUtils.toRealWidth(Res.values.LINE_THICKNESS) / 2), 0);
        rootGroup.addActor(rightBorder);

        final ImageButton button = createBackImage();
        button.addListener(hideMenuButtonListener);
        rootGroup.addActor(button);

        initMainTable();
    }

    private ImageButton createBackImage() {
        final ImageButton.ImageButtonStyle style =
                skin.get(Res.buttonStyles.DIALOG_BACK_BUTTON_STYLE,
                        ImageButton.ImageButtonStyle.class);
        final ImageButton button = new ImageButton(style);

        final float buttonSize = screenUtils.toRealWidth(Res.values.HIDE_MENU_BUTTON_SIZE);
        button.setSize(buttonSize, buttonSize);

        final float padTop = screenUtils.toRealHeight(Res.settings.showingAd
                ? Res.values.GAME_SCREEN_SETTINGS_ICON_PAD_WITH_AD
                : Res.values.GAME_SCREEN_SETTINGS_ICON_PAD_NO_AD);
        button.setPosition(getPrefWidth() - (buttonSize * 1.5F), height - padTop);

        return button;
    }

    private void initMainTable() {
        final float optionsTablePadTop = screenUtils.toRealHeight(Res.settings.showingAd
                ? Res.values.GAME_SCREEN_MENU_OPTIONS_PAD_TOP_WITH_AD
                : Res.values.GAME_SCREEN_MENU_OPTIONS_PAD_TOP_NO_AD);

        final float rowHeight = getCheckboxStyle().font.getCapHeight();
        final float pad = rowHeight;

        mainTable.add(new Image(skin.getDrawable(Res.textures.SOUND_ICON)))
                .maxHeight(rowHeight)
                .maxWidth(rowHeight * 1.3F)
                .padTop(optionsTablePadTop)
                .padLeft(pad)
                .padRight(pad)
                .expandY();
        mainTable.add(getSoundButton())
                .height(rowHeight)
                .padTop(optionsTablePadTop)
                .padRight(pad)
                .expandX()
                .fillX()
                .left()
                .expandY();

        mainTable.row();

        if (platformInfo.isVibrationSupported()) {
            mainTable.add(new Image(skin.getDrawable(Res.textures.VIBRATION_ICON)))
                    .maxHeight(rowHeight * 2)
                    .maxWidth(rowHeight * 1.25F)
                    .padLeft(pad)
                    .padRight(pad)
                    .expandY();
            mainTable.add(getVibrationButton())
                    .height(rowHeight)
                    .padRight(pad)
                    .expandX()
                    .fillX()
                    .left()
                    .expandY();

            mainTable.row();
        }

        mainTable.add(new Image(skin.getDrawable(Res.textures.MUTE_DEALER_BUTTON_PRESSED)))
                .maxHeight(rowHeight * 1.25F)
                .maxWidth(rowHeight * 1.25F)
                .padLeft(pad)
                .padRight(pad)
                .expandY();

        final CheckBoxButton muteDealer = createCheckbox(strings.muteDealer);
        muteDealer.setDisabled(true);
        mainTable.add(muteDealer)
                .height(rowHeight)
                .padRight(pad)
                .expandX()
                .fillX()
                .left()
                .expandY();

        mainTable.row();

        mainTable.add(new Image(skin.getDrawable(Res.textures.MUTE_PLAYERS_ICON)))
                .maxHeight(rowHeight * 1.25F)
                .maxWidth(rowHeight * 1.25F)
                .padLeft(pad)
                .padRight(pad)
                .expandY();

        final CheckBoxButton mutePlayers = createCheckbox(strings.mutePlayers);
        mutePlayers.setDisabled(true);
        mainTable.add(mutePlayers)
                .height(rowHeight)
                .padRight(pad)
                .expandX()
                .fillX()
                .left()
                .expandY();

        mainTable.row();

        final TextButton leaveGame = createTextButton(strings.leaveGameOption);
        leaveGame.getLabel().setAlignment(Align.left);
        leaveGame.addListener(leaveGameButtonListener);
        mainTable.add(new Image(skin.getDrawable(Res.textures.LEAVE_GAME_ICON)))
                .maxHeight(rowHeight * 2F)
                .maxWidth(rowHeight * 1.2F)
                .padTop(rowHeight * 1F)
                .expandY()
                .padLeft(pad)
                .padRight(pad);
        mainTable.add(leaveGame)
                .expandX()
                .fillX()
                .expandY()
                .fillY()
                .padTop(rowHeight * 1F)
                .left();
    }

    private CheckBoxButton getVibrationButton() {
        final CheckBoxButton vibrationButton = createCheckbox(skin.get(Res.properties.STRINGS, Strings.class).vibration);

        vibrationButton.setChecked(AppPreferences.getInstance().isVibrationEnabled());
        vibrationButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                AppPreferences.getInstance().setVibrationEnabled(vibrationButton.isChecked());
            }
        });

        return vibrationButton;
    }

    private CheckBoxButton getSoundButton() {
        final CheckBoxButton soundButton = createCheckbox(skin.get(Res.properties.STRINGS, Strings.class).sound);

        soundButton.setChecked(AppPreferences.getInstance().isSoundEnabled());
        soundButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                AppPreferences.getInstance().setSoundEnabled(soundButton.isChecked());
            }
        });

        return soundButton;
    }

    private CheckBoxButton createCheckbox(final String text) {
        final float padRight = getCheckboxStyle().font.getCapHeight()
                / 2;
        return new CheckBoxButton(text, getCheckboxStyle(), padRight);
    }

    private TextButton createTextButton(final String text) {
        final TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
        style.font = getCheckboxStyle().font;
        style.fontColor = getCheckboxStyle().fontColor;
        style.disabledFontColor = getCheckboxStyle().disabledFontColor;

        return new TextButton(text, style);
    }

    private ImageTextButton.ImageTextButtonStyle getCheckboxStyle() {
        return skin.get(Res.buttonStyles.SMALL_CHECKBOX_STYLE,
                ImageTextButton.ImageTextButtonStyle.class);
    }

    @Override
    public float getPrefWidth() {
        final BitmapFont font = getCheckboxStyle().font;
        final float width = screenUtils.toRealWidth(Res.values.GAME_SCREEN_MENU_WIDTH);

        float maxTextWidth = 0;

        if (font.getBounds(strings.sound).width > maxTextWidth) {
            maxTextWidth = font.getBounds(strings.sound).width;
        }

        if (font.getBounds(strings.vibration).width > maxTextWidth) {
            maxTextWidth = font.getBounds(strings.vibration).width;
        }

        if (font.getBounds(strings.muteDealer).width > maxTextWidth) {
            maxTextWidth = font.getBounds(strings.muteDealer).width;
        }

        if (font.getBounds(strings.mutePlayers).width > maxTextWidth) {
            maxTextWidth = font.getBounds(strings.mutePlayers).width;
        }

        return maxTextWidth * 2 > width ? maxTextWidth * 2 : width;
    }

    private static class CheckBoxButton extends HorizontalImageTextButton {

        public CheckBoxButton(final String text,
                              final ImageTextButton.ImageTextButtonStyle style,
                              final float padRight) {
            super(text, style);

            getImageCell().padRight(padRight / 2);
            getLabelCell().expandX().left();
        }
    }
}