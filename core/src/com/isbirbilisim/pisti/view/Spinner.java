package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.resources.Res;

/**
 * Rotating spinner.
 */
public class Spinner extends Image {

    public Spinner(final Skin skin,
                   final float size) {
        setDrawable(skin, Res.textures.SPINNER);

        setSize(size, size);
        setOrigin(size / 2, size / 2);
        addAction(Actions.forever(Actions.rotateBy(-180, 1F)));
    }
}