package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

public class HorizontalMenuItem extends VerticalImageTextButton {

    private static final float IMAGE_SIZE = 0.4F;

    private final MenuEntry menuEntry;

    public HorizontalMenuItem(final MenuEntry menuEntry,
                              final Skin skin,
                              final String drawableName,
                              final float width,
                              final float height) {
        super(menuEntry.getName(skin.get(Res.properties.STRINGS, Strings.class)),
                getStyle(drawableName, skin));
        this.menuEntry = menuEntry;

        setSize(width, height);

        final float imageCellSize = height * IMAGE_SIZE;
        getImageCell().size(imageCellSize).center();
    }

    private static ImageTextButton.ImageTextButtonStyle getStyle(
            final String drawableName,
            final Skin skin) {
        final ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle(
                skin.get(Res.buttonStyles.MENU_HORIZONTAL_ITEM_STYLE, ImageTextButton.ImageTextButtonStyle.class));

        style.imageUp = skin.getDrawable(drawableName);
        style.imageDisabled = skin.newDrawable(drawableName, Res.colors.MENU_HORIZONTAL_ITEM_LABEL_COLOR_DISABLED);
        style.imageDown = skin.newDrawable(drawableName, Res.colors.MENU_HORIZONTAL_ITEM_LABEL_COLOR_PRESSED);

        return style;
    }

    public MenuEntry getMenuEntry() {
        return menuEntry;
    }
}