package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Pool;

/**
 * Button that has to states: signed in or signed out.
 */
public class SignInButton extends WidgetGroup {

    private static final float TOUCH_SCALE_ACTION_DURATION = 0.1F;
    private static final float SCALE = 0.2F;

    private final Pool<ScaleByAction> touchDownActionPool =
            new Pool<ScaleByAction>() {
                @Override
                protected ScaleByAction newObject() {
                    return Actions.scaleBy(SCALE,
                            SCALE,
                            TOUCH_SCALE_ACTION_DURATION);
                }
            };

    private final Pool<ScaleByAction> touchUpActionPool =
            new Pool<ScaleByAction>() {
                @Override
                protected ScaleByAction newObject() {
                    return Actions.scaleBy(-SCALE,
                            -SCALE,
                            TOUCH_SCALE_ACTION_DURATION);
                }
            };

    private Image signIn;
    private Image signOut;

    protected void initImages(final Image signIn,
                              final Image signOut,
                              final float width,
                              final float height,
                              final boolean signed) {
        this.signIn = signIn;
        this.signOut = signOut;

        setSigned(signed);
        setButtonSize(width, height);
        initTouchListeners();

        addActor(this.signIn);
        addActor(this.signOut);
    }

    private void setButtonSize(final float width, final float height) {
        setSize(width, height);
        setOrigin(width / 2, height / 2);

        this.signIn.setSize(width, height);
        this.signOut.setSize(width, height);
        this.signIn.setOrigin(width / 2, height / 2);
        this.signOut.setOrigin(width / 2, height / 2);
    }

    /**
     * Sets button state to signed-in/signed-out.
     */
    public void setSigned(final boolean signed) {
        if(signIn == null || signOut == null) {
            return;
        }

        if (signed) {
            signIn.setVisible(false);
            signOut.setVisible(true);
        } else {
            signIn.setVisible(true);
            signOut.setVisible(false);
        }
    }

    private void initTouchListeners() {
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(final InputEvent event,
                                     final float x,
                                     final float y,
                                     final int pointer,
                                     final int button) {
                addAction(touchDownActionPool.obtain());

                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(final InputEvent event,
                                final float x,
                                final float y,
                                final int pointer,
                                final int button) {
                addAction(touchUpActionPool.obtain());

                super.touchUp(event, x, y, pointer, button);
            }
        });
    }
}