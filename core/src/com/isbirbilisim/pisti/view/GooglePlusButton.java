package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Button that has to states: signed in or signed out.
 */
public class GooglePlusButton extends SignInButton {

    private ClickListener clickListener;

    public GooglePlusButton(final Drawable signIn,
                            final Drawable signOut,
                            final float width,
                            final float height,
                            final boolean signed) {
        initImages(new Image(signIn), new Image(signOut), width, height, signed);
        initTouchListeners();
    }

    /**
     * Adds button click listener.
     *
     * @param clickListener - called when user clicks on button.
     */
    public void setClickListener(final ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    private void initTouchListeners() {
        addListener(new ClickListener() {

            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);

                if (clickListener != null) {
                    clickListener.clicked(event, x, y);
                }
            }
        });
    }
}