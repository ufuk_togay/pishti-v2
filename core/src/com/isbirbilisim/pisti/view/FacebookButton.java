package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Facebook sign-in button.
 */
public abstract class FacebookButton extends SignInButton {

    public void setImages(final Drawable signIn,
                          final Drawable signOut,
                          final float width,
                          final float height,
                          final boolean signed) {
        initImages(new Image(signIn), new Image(signOut), width, height, signed);
        setSigned(signed);
    }

    public void onStateChanged(final State state) {
        setSigned(state == State.OPENED);
    }

    public enum State {
        OPENED,
        CLOSED
    }
}