package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

public class VerticalMenuItem extends ImageTextButton {

    private static final float IMAGE_SIZE = 0.8F;

    private final MenuEntry menuEntry;

    public VerticalMenuItem(final MenuEntry menuEntry,
                            final Skin skin,
                            final String drawableName,
                            final float width,
                            final float height) {
        super(menuEntry.getName(skin.get(Res.properties.STRINGS, Strings.class)),
                getStyle(drawableName, skin));
        this.menuEntry = menuEntry;

        setSize(width, height);

        final float imageCellSize = height * IMAGE_SIZE;
        getImageCell().size(imageCellSize).padLeft(imageCellSize * 0.6F);
        getLabelCell().size(width - (imageCellSize + (imageCellSize * 0.6F * 2)), height).fill().padLeft(imageCellSize * 0.6F);
        getLabel().setAlignment(Align.left);
    }

    private static ImageTextButtonStyle getStyle(
            final String drawableName,
            final Skin skin) {
        final ImageTextButtonStyle style = new ImageTextButtonStyle(
                skin.get(Res.buttonStyles.MENU_VERTICAL_ITEM_STYLE, ImageTextButtonStyle.class));

        style.imageUp = skin.getDrawable(drawableName);
        style.imageDisabled = skin.newDrawable(drawableName, Res.colors.MENU_VERTICAL_ITEM_LABEL_COLOR_DISABLED);
        style.imageDown = skin.newDrawable(drawableName, Res.colors.MENU_VERTICAL_ITEM_LABEL_COLOR_PRESSED);

        return style;
    }

    public MenuEntry getMenuEntry() {
        return menuEntry;
    }
}