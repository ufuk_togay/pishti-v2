package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * View that consist of MenuView and loading screen.
 */
public class SlideScreenView extends Table {

    private final float frameWidth;
    private final float frameHeight;

    private final ScreenUtils screenUtils;

    private final WidgetGroup rootGroup;

    private Skin skin;

    private LoadingGameView loadingGameView;
    private VerticalScrollMenu verticalMenu;
    private HorizontalMenu horizontalMenu;


    public SlideScreenView(final float frameWidth,
                           final float frameHeight,
                           final ScreenUtils screenUtils) {
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.screenUtils = screenUtils;

        this.rootGroup = new WidgetGroup();

        setSize(frameWidth * 1.5F, frameHeight);
        add(rootGroup).expand().fill();
    }

    /**
     * Sets skin with resources.
     */
    public void setSkin(final Skin skin) {
        this.skin = skin;
        setBackground(skin.getDrawable(Res.textures.MENU_BACKGROUND));
    }

    /**
     * Sets views with menu and loading frame.
     */
    public void setViews(final VerticalScrollMenu verticalMenu,
                         final HorizontalMenu horizontalMenu,
                         final LoadingGameView loadingGameView) {
        this.verticalMenu = verticalMenu;
        this.horizontalMenu = horizontalMenu;
        this.loadingGameView = loadingGameView;

        horizontalMenu.setPosition(0, 0);
        rootGroup.addActor(horizontalMenu);

        verticalMenu.setPosition(0, frameHeight / 4F);
        rootGroup.addActor(verticalMenu);

        loadingGameView.setPosition(verticalMenu.getWidth(), 0);
        rootGroup.addActor(loadingGameView);

        final float lineThickness = screenUtils.toRealWidth(Res.values.LINE_THICKNESS);

        final Image bigVerticalLine = new Image(skin.getDrawable(Res.textures.BIG_VERTICAL_LINE));
        bigVerticalLine.setWidth(lineThickness);
        bigVerticalLine.setHeight(frameHeight);
        bigVerticalLine.setPosition(-bigVerticalLine.getWidth() / 2F, 0);
        addActor(bigVerticalLine);

        final Image horizontalLine = new Image(skin.getDrawable(Res.textures.HORIZONTAL_LINE));
        horizontalLine.setWidth(frameWidth / 2);
        horizontalLine.setHeight(lineThickness);
        horizontalLine.setPosition(0, frameHeight / 4F);
        addActor(horizontalLine);

        final Image verticalLine1 = new Image(skin.getDrawable(Res.textures.VERTICAL_LINE_1));
        verticalLine1.setWidth(lineThickness);
        verticalLine1.setHeight(frameHeight / 4F);
        verticalLine1.setPosition(frameWidth / 2 - (frameWidth / 6), 0);
        addActor(verticalLine1);

        final Image verticalLine2 = new Image(skin.getDrawable(Res.textures.VERTICAL_LINE_2));
        verticalLine2.setWidth(lineThickness);
        verticalLine2.setHeight(frameHeight / 4F);
        verticalLine2.setPosition(frameWidth / 2 - (frameWidth / 3), 0);
        addActor(verticalLine2);
    }

    /**
     * Enables/disables menu items that represent action which are connection
     * dependent.
     *
     * @param enabled - true if item should be enabled, false otherwise.
     */
    public void enableConnectionDependentItems(final boolean enabled) {
        verticalMenu.enableConnectionDependentItems(enabled);
        horizontalMenu.enableConnectionDependentItems(enabled);
    }
}