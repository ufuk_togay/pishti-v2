package com.isbirbilisim.pisti.view;

/**
 * Contains info about Card position on table and its size.
 */
public class CardViewSettings {
    public float width;
    public float height;
    public float x;
    public float y;
    public float rotation;

    public CardViewSettings(final float width,
                            final float height,
                            final float x,
                            final float y,
                            final float rotation) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.rotation = rotation;
    }
}