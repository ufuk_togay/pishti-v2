package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.cards.CardModel;

import java.util.List;
import java.util.Stack;

/**
 * Provides functionality for creating new CardViews from CardModel.
 */
public class CardViewFactory {
    private final Skin skin;

    public CardViewFactory(final Skin skin) {
        this.skin = skin;
    }

    /**
     * Creates list of CardViews from List of CardModels.
     *
     * @param cards - list of {@link com.isbirbilisim.pisti.cards.CardModel}
     * @return - list of {@link com.isbirbilisim.pisti.view.CardView}
     */
    public Stack<CardView> createCardViewList(final List<CardModel> cards) {
        final Stack<CardView> cardViews = new Stack<CardView>();

        for (final CardModel cardModel : cards) {
            final Image visible = getImage(cardModel.getVisibleDrawableName());
            final Image hidden = getImage(cardModel.getHiddenDrawableName());

            cardViews.add(new CardView(cardModel, visible, hidden));
        }

        return cardViews;
    }

    private Image getImage(final String name) {
        return new Image(skin.newDrawable(name));
    }
}