package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Pool;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

import java.util.LinkedList;
import java.util.List;

public class VerticalScrollMenu extends Table {

    private static final float BALANCE_ANIMATION_DURATION = 0.2F;

    private final LinkedList<Container<VerticalMenuItem>> containers;

    private final float width;
    private final float height;
    private final float topPad;

    private final ScreenUtils screenUtils;
    private final MultimediaManager multimediaManager;

    private final Pool<AlphaAction> alphaActionPool = new Pool<AlphaAction>() {
        @Override
        protected AlphaAction newObject() {
            return new AlphaAction();
        }
    };

    private OnMenuItemSelectedListener listener;

    private boolean wasPanning;

    public VerticalScrollMenu(final float width,
                              final float height,
                              final List<VerticalMenuItem> items,
                              final ScreenUtils screenUtils,
                              final MultimediaManager multimediaManager) {
        this.width = width;
        this.height = height;
        this.screenUtils = screenUtils;
        this.multimediaManager = multimediaManager;
        this.containers = getItemContainers(items);

        topPad = getTopPad();
        setSize(width, height);

        initItemsPositions();
        setInputEventsCallbacks();

        for (final Container container : containers) {
            addActor(container);
        }
    }

    private float getTopPad() {
        if (Res.settings.showingAd) {
            return screenUtils.toRealHeight(Res.values.AD_TOP_PAD);
        } else {
            return (height - (containers.size() * containers.get(1).getHeight())) / 2;
        }
    }

    private LinkedList<Container<VerticalMenuItem>> getItemContainers(
            final List<VerticalMenuItem> items) {
        final LinkedList<Container<VerticalMenuItem>> containers =
                new LinkedList<Container<VerticalMenuItem>>();

        for (final VerticalMenuItem item : items) {
            final Container<VerticalMenuItem> container = new Container<VerticalMenuItem>();

            container.left();
            container.bottom();
            container.setTransform(true);
            container.setSize(item.getWidth(), item.getHeight());
            container.setOriginX(item.getWidth() / 2);
            container.setOriginY(item.getHeight() / 2);

            item.setFillParent(true);
            container.setActor(item);
            containers.add(container);
        }

        return containers;
    }

    private void initItemsPositions() {
        float yShift = topPad;

        for (final Container<VerticalMenuItem> container : containers) {
            container.setPosition(getCenterX() - (container.getWidth() / 2),
                    height - (container.getHeight()) - yShift);
            yShift += container.getHeight();

            if (container.getY() < 0) {
                container.setTouchable(Touchable.disabled);

                final AlphaAction alphaAction = alphaActionPool.obtain();
                alphaAction.setAlpha(0);
                container.addAction(alphaAction);
            } else if (container.getY() < container.getHeight()) {
                final AlphaAction alphaAction = alphaActionPool.obtain();
                alphaAction.setAlpha((container.getY() / container.getHeight()));
                container.addAction(alphaAction);
            }
        }
    }

    private void setInputEventsCallbacks() {
        addCaptureListener(new ActorGestureListener() {

            @Override
            public void touchDown(final InputEvent event,
                                  final float x,
                                  final float y,
                                  final int pointer,
                                  final int button) {
                super.touchDown(event, x, y, pointer, button);

                wasPanning = false;
            }

            @Override
            public void pan(final InputEvent event,
                            final float x,
                            final float y,
                            final float deltaX,
                            final float deltaY) {
                super.pan(event, x, y, deltaX, deltaY);

                dragItems(deltaY);

                wasPanning = true;
            }

            @Override
            public void touchUp(final InputEvent event,
                                final float x,
                                final float y,
                                final int pointer,
                                final int button) {
//                centerToClosestItems();
            }
        });

        for (final Container<VerticalMenuItem> item : containers) {
            item.getActor().addCaptureListener(new ClickListener() {
                @Override
                public void clicked(final InputEvent event,
                                    final float x,
                                    final float y) {
                    if (wasPanning) {
                        return;
                    }

                    multimediaManager.playClickSound();

                    if (listener != null) {
                        listener.onMenuItemSelected(item.getActor().getMenuEntry());
                    }
                }
            });
        }
    }

    private void dragItems(final float y) {
        final float itemHeight = containers.getFirst().getHeight();

        if (y > 0 && getItemCenterPosition(containers.getLast()).y > itemHeight) {
            return;
        } else if (y < 0 && getItemCenterPosition(containers.getFirst()).y < height - itemHeight) {
            return;
        }

        if (y < 0) {
            if (height - topPad > containers.getFirst().getY() + itemHeight - y) {
                return;
            }
        }

        final float shift = y * 0.5F;

        for (final Container<VerticalMenuItem> item : containers) {
            final AlphaAction alphaAction = alphaActionPool.obtain();
            final float resultPos = item.getY() + shift;

            if (resultPos < 0) {
                item.setTouchable(Touchable.disabled);
                alphaAction.setAlpha(0);
            } else if (resultPos < itemHeight) {
                item.setTouchable(Touchable.enabled);
                alphaAction.setAlpha((resultPos / itemHeight));
            } else {
                item.setTouchable(Touchable.enabled);
                alphaAction.setAlpha(1);
            }

            item.addAction(alphaAction);

            final MoveByAction moveByAction = createMoveByAction(0, shift);
            item.addAction(moveByAction);
        }
    }

    private Vector2 getItemCenterPosition(final Container<VerticalMenuItem> item) {
        return new Vector2(item.getX() + (item.getWidth() / 2),
                item.getY() + (item.getHeight() / 2));
    }

    private void centerToClosestItems() {
        float balanceShift = height;

        for (final Container<VerticalMenuItem> item : containers) {
            final Vector2 itemCenter = getItemCenterPosition(item);
            if (Math.abs(getCenterY() - itemCenter.y) < Math.abs(balanceShift)) {
                balanceShift = (getCenterY() - itemCenter.y);
            }
        }

        moveItems(balanceShift);
    }

    private float getCenterY() {
        return height / 2 - topPad;
    }

    private float getCenterX() {
        return getWidth() / 2;
    }

    private void moveItems(final float y) {
        for (final Container<VerticalMenuItem> item : containers) {
            final MoveByAction moveByAction = createMoveByAction(0, y);

            moveByAction.setDuration(BALANCE_ANIMATION_DURATION);
            moveByAction.setInterpolation(Interpolation.circleOut);

            item.addAction(moveByAction);
        }
    }

    private MoveByAction createMoveByAction(final float x,
                                            final float y) {
        return Actions.moveBy(x, y);
    }

    /**
     * Enables/disables menu items that represent action which are connection
     * dependent.
     *
     * @param enabled - true if item should be enabled, false otherwise.
     */
    public void enableConnectionDependentItems(final boolean enabled) {
        for (final Container<VerticalMenuItem> item : containers) {
            final MenuEntry menuEntry = item.getActor().getMenuEntry();
            if (menuEntry.isConnectionRequired()) {
                item.getActor().setDisabled(!enabled);
            }
        }
    }

    /**
     * Sets listener that will be triggered when user selects menu item.
     *
     * @param listener - callback instance.
     */
    public void setOnMenuItemSelectedListener(
            final OnMenuItemSelectedListener listener) {
        this.listener = listener;
    }

    /**
     * Callback that informs about menu item click event.
     */
    public interface OnMenuItemSelectedListener {
        /**
         * Called when some of menu item was selected by user.
         *
         * @param menuEntry - selected item.
         */
        void onMenuItemSelected(final MenuEntry menuEntry);
    }
}