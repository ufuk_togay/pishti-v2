package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

import java.util.LinkedList;

public class HorizontalMenuCreator {

    /**
     * Creates VerticalScrollMenu with given params.
     *
     * @param skin   - skin.
     * @param width  - menu with.
     * @param height - menu height.
     * @return - menu instance.
     */
    public static HorizontalMenu create(
            final MultimediaManager multimediaManager,
            final HorizontalMenu.OnMenuItemSelectedListener listener,
            final Skin skin,
            final float width,
            final float height) {
        final LinkedList<HorizontalMenuItem> items = new LinkedList<HorizontalMenuItem>();

        final HorizontalMenuItem singleGame = new HorizontalMenuItem(MenuEntry.LEADERBOARD,
                skin,
                Res.textures.ICON_LEADERBOARD,
                width / 3,
                height);
        items.add(singleGame);

        final HorizontalMenuItem quickGame = new HorizontalMenuItem(MenuEntry.BUY_COINS,
                skin,
                Res.textures.ICON_BUY_COINS,
                width / 3,
                height);
        items.add(quickGame);

        final HorizontalMenuItem playWithFriends = new HorizontalMenuItem(MenuEntry.SETTINGS,
                skin,
                Res.textures.ICON_SETTINGS,
                width / 3,
                height);
        items.add(playWithFriends);

        final HorizontalMenu menu = new HorizontalMenu(items, width, height, multimediaManager);
        menu.setOnMenuItemSelectedListener(listener);
        return menu;
    }
}