package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

import org.apache.commons.lang3.StringUtils;

/**
 * View that shows text input for sending text messages to other players.
 */
public class ChatInput extends WidgetGroup {

    private final Skin skin;
    private final ScreenUtils screenUtils;
    private final OnSendMessageListener sendMessageListener;
    private TextField input;

    public ChatInput(final Skin skin,
                     final ScreenUtils screenUtils,
                     final OnSendMessageListener sendMessageListener) {
        this.sendMessageListener = sendMessageListener;
        this.skin = skin;
        this.screenUtils = screenUtils;

        init();
    }

    private void init() {
        final float inputWidth = screenUtils.toRealWidth(Res.values.CHAT_INPUT_WIDTH);
        final float inputHeight = screenUtils.toRealHeight(Res.values.CHAT_INPUT_HEIGHT);

        final float frameThickness = screenUtils.toRealWidth(4);

        final Image background = new Image(skin.getDrawable(Res.textures.CHAT_WINDOW_BACKGROUND));
        background.setSize(inputWidth - frameThickness, inputHeight - frameThickness);
        background.setPosition(frameThickness / 2, frameThickness / 2);

        final Image frame = new Image(skin.getDrawable(Res.textures.CHAT_WINDOW_FRAME));
        frame.setSize(inputWidth, inputHeight);
        frame.setPosition(0, 0);

        addActor(background);
        addActor(frame);

        final TextField.TextFieldStyle style = skin.get(Res.labelStyles.CHAT_INPUT_STYLE,
                TextField.TextFieldStyle.class);
        style.cursor.setMinWidth(screenUtils.toRealWidth(2F));

        final float textMargin = screenUtils.toRealWidth(Res.values.CHAT_TEXT_MARGIN);
        input = new TextField("", style);
        input.setSize(inputWidth - (textMargin * 2), inputHeight - (frameThickness * 5));
        input.setPosition((inputWidth - input.getWidth()) / 2, (inputHeight - input.getHeight()) / 2);

        addActor(input);

        final ImageButton.ImageButtonStyle cancelStyle = new ImageButton.ImageButtonStyle();
        cancelStyle.imageUp = skin.getDrawable(Res.textures.CLOSE_CHAT_BUTTON);
        cancelStyle.imageDown = skin.getDrawable(Res.textures.CLOSE_CHAT_BUTTON_PRESSED);

        final float cancelSize = screenUtils.toRealWidth(Res.values.CHAT_CANCEL_SIZE);
        final WidgetGroup widgetGroup = new WidgetGroup();
        widgetGroup.setSize(cancelSize * 2, cancelSize * 2);
        widgetGroup.setPosition(0, -(cancelSize * 2));
        widgetGroup.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                hideInput();
            }
        });
        widgetGroup.setTouchable(Touchable.enabled);

        final ImageButton cancel = new ImageButton(cancelStyle);
        cancel.setSize(cancelSize, cancelSize);
        cancel.setPosition(0, cancelSize);

        widgetGroup.addActor(cancel);
        addActor(widgetGroup);
    }

    public void showInput(final Stage stage) {
        setVisible(true);
        Gdx.input.setOnscreenKeyboardVisible(true);
        stage.setKeyboardFocus(input);
    }

    public void hideInput() {
        setVisible(false);
        Gdx.input.setOnscreenKeyboardVisible(false);
    }

    public void enterPressed() {
        final String message = input.getText();

        if (isVisible() && StringUtils.isNotEmpty(message)) {
            input.setText("");
            hideInput();

            sendMessageListener.onSendMessage(message);
        }
    }

    /**
     * Interface for receiving callback when user tries to send message.
     */
    public static interface OnSendMessageListener {

        /**
         * @param message - message to be sent.
         */
        void onSendMessage(final String message);
    }
}