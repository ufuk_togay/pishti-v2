package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.isbirbilisim.pisti.cards.animations.OnAnimationFinishedListener;
import com.isbirbilisim.pisti.resources.Res;

/**
 * View that shows game logo on screen.
 */
public class GameLogoView extends WidgetGroup {

    private static final float FLIP_SUIT_DURATION = 0.5F;
    private static final float SHOW_LABEL_DURATION = 0.7F;

    private final Skin skin;
    private final float width;
    private final float height;

    private Image logoMain;
    private PistiLabel pistiLabel;
    private SuitView leftSuit;
    private SuitView rightSuit;
    private SuitView topSuit;
    private SuitView bottomSuit;

    private Image superWord;

    public GameLogoView(final Skin skin,
                        final float width,
                        final float height) {
        this.skin = skin;
        this.width = width;
        this.height = height;

        setSize(width, height);
        init();
    }

    private void init() {
        logoMain = new Image(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_MAIN));
        logoMain.setPosition(0, 0);
        logoMain.setSize(width, height);
        addActor(logoMain);

        final float pistiLabelWidth = width / 2;
        final float pistiLabelHeight = height / 3.5F;
        pistiLabel = new PistiLabel(skin);
        pistiLabel.setSize(pistiLabelWidth, pistiLabelHeight);
        pistiLabel.setOrigin(pistiLabelWidth / 2, pistiLabelHeight / 2);
        pistiLabel.setPosition(width / 2 - (pistiLabelWidth / 2),
                height / 2 - (pistiLabelHeight / 2));
        addActor(pistiLabel);

        final float superLabelWidth = width / 1.7F;
        final float superLabelHeight = height / 3.5F;
        superWord = new Image(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_SUPER));
        superWord.setSize(superLabelWidth, superLabelHeight);
        superWord.setOrigin(superLabelWidth / 2, superLabelHeight / 2);
        superWord.setPosition(width / 2 - superLabelWidth,
                height / 2 - (superLabelHeight / 2));
        superWord.setVisible(false);
        addActor(superWord);

        leftSuit = new SuitView(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_LEFT),
                skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_LEFT_BACK));
        rightSuit = new SuitView(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_RIGHT),
                skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK));
        topSuit = new SuitView(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_TOP),
                skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_TOP_BACK));
        bottomSuit = new SuitView(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_BOTTOM),
                skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK));

        final float topSuitHeight = height / 6;
        final float leftSuitHeight = height / 5;
        final float rightSuitHeight = height / 5;
        final float bottomSuitHeight = height / 5;

        final float leftSuitWidth = leftSuitHeight;
        final float rightSuitWidth = rightSuitHeight;
        final float topSuitWidth = topSuitHeight * 1.2F;
        final float bottomSuitWidth = bottomSuitHeight * 0.9F;

        leftSuit.setSize(leftSuitWidth, leftSuitHeight);
        rightSuit.setSize(rightSuitWidth, rightSuitHeight);
        topSuit.setSize(topSuitWidth, topSuitHeight);
        bottomSuit.setSize(bottomSuitWidth, bottomSuitHeight);

        leftSuit.setOrigin(leftSuitWidth / 2, leftSuitHeight / 2);
        rightSuit.setOrigin(rightSuitWidth / 2, rightSuitHeight / 2);
        topSuit.setOrigin(topSuitWidth / 2, topSuitHeight / 2);
        bottomSuit.setOrigin(bottomSuitWidth / 2, bottomSuitHeight / 2);

        leftSuit.setPosition(width / 9 - (leftSuitWidth / 2), height / 2 - (leftSuitHeight / 2));
        rightSuit.setPosition(width / 9 * 8 - (rightSuitWidth / 2), height / 2 - (rightSuitHeight / 2));
        topSuit.setPosition(width / 2 - (topSuitWidth / 2), height / 5 * 4 - (topSuitHeight / 2));
        bottomSuit.setPosition(width / 2 - (bottomSuitWidth / 2), height / 5 - (bottomSuitHeight / 2));

        addActor(leftSuit);
        addActor(rightSuit);
        addActor(topSuit);
        addActor(bottomSuit);
    }

    public void showPistiAnimation(final OnAnimationFinishedListener listener) {
        pistiLabel.addAction(getPistiLabelAnimation());
        leftSuit.addAction(getFlipAction(leftSuit, -1, 0, false));
        rightSuit.addAction(getFlipAction(rightSuit, 1, 0, false));
        topSuit.addAction(getFlipAction(topSuit, 0, 1, false));
        bottomSuit.addAction(Actions.sequence(getFlipAction(bottomSuit, 0, -1, false),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        listener.onAnimationFinished();
                    }
                })));
    }

    public void showSuperPistiAnimation(final OnAnimationFinishedListener listener) {
        pistiLabel.addAction(getPistiWordAnimation());
        superWord.addAction(getSuperWordAnimation());

        leftSuit.addAction(getFlipAction(leftSuit, -1, 0, true));
        rightSuit.addAction(getFlipAction(rightSuit, 1, 0, true));
        topSuit.addAction(getFlipAction(topSuit, 0, 1, true));
        bottomSuit.addAction(Actions.sequence(getFlipAction(bottomSuit, 0, -1, true),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        listener.onAnimationFinished();
                    }
                })));
    }

    public Action getPistiWordAnimation() {
        final float scaleAmount = 0.9F;

        pistiLabel.setOrigin(0, pistiLabel.getHeight() / 2);
        final Vector2 startPosition = new Vector2(pistiLabel.getX(), pistiLabel.getY());

        final MoveToAction shiftAction = Actions.moveTo(width / 2 + (pistiLabel.getWidth() / 4), startPosition.y, 0.1F);
        final ScaleByAction scaleOutAction = Actions.scaleBy(scaleAmount,
                scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticOut);
        final ScaleByAction scaleInAction = Actions.scaleBy(-scaleAmount,
                -scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticIn);
        final MoveToAction toStartPosition = Actions.moveTo(startPosition.x, startPosition.y);

        return Actions.sequence(
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        pistiLabel.showLight();
                    }
                }),
                shiftAction,
                scaleOutAction,
                scaleInAction,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        pistiLabel.showDark();

                        pistiLabel.setOrigin(pistiLabel.getWidth() / 2, pistiLabel.getHeight() / 2);
                    }
                }),
                toStartPosition);
    }

    public Action getSuperWordAnimation() {
        final float scaleAmount = 0.9F;

        superWord.setOrigin(superWord.getWidth(), superWord.getHeight() / 2);

        final ScaleByAction scaleOutAction = Actions.scaleBy(scaleAmount,
                scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticOut);
        final ScaleByAction scaleInAction = Actions.scaleBy(-scaleAmount,
                -scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticIn);

        return Actions.sequence(
                Actions.delay(0.1F),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        superWord.setVisible(true);
                    }
                }),
                scaleOutAction,
                scaleInAction,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        superWord.setOrigin(superWord.getWidth() / 2, superWord.getHeight() / 2);
                        superWord.setVisible(false);
                    }
                }));
    }

    private Action getFlipAction(final SuitView suitView, final int xMul, final int yMul, final boolean superAnimation) {
        final float addX = superAnimation ? superWord.getWidth() * 0.8F : 0;

        final float moveByX = xMul * (pistiLabel.getWidth() / 2 * 1.5F + addX);
        final float moveByY = yMul * (pistiLabel.getHeight() / 2 * 1.5F);
        final MoveByAction moveOutAction = Actions.moveBy(moveByX, moveByY, FLIP_SUIT_DURATION / 2);
        final MoveByAction moveInAction = Actions.moveBy(-moveByX, -moveByY, FLIP_SUIT_DURATION / 2);

        final ScaleToAction scaleIn1 = Actions.scaleTo(0, 1, FLIP_SUIT_DURATION / 2, Interpolation.pow2Out);
        final ScaleToAction scaleOut1 = Actions.scaleTo(1, 1, FLIP_SUIT_DURATION / 2, Interpolation.pow2In);

        final AlphaAction setAlphaAction = Actions.alpha(0);
        final AlphaAction fadeInAction1 = Actions.fadeIn(FLIP_SUIT_DURATION / 3);
        final AlphaAction fadeInAction2 = Actions.fadeIn(FLIP_SUIT_DURATION / 3);
        final AlphaAction fadeOutAction = Actions.fadeOut(FLIP_SUIT_DURATION / 3);

        final ScaleToAction scaleIn2 = Actions.scaleTo(0, 1, FLIP_SUIT_DURATION / 2, Interpolation.pow2Out);
        final ScaleToAction scaleOut2 = Actions.scaleTo(1, 1, FLIP_SUIT_DURATION / 2, Interpolation.pow2In);

        final Action flipAction =
                Actions.sequence(
                        moveOutAction,
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                suitView.showBack();
                            }
                        }),
                        scaleIn1,
                        setAlphaAction,
                        Actions.parallel(fadeInAction1, scaleOut1),
                        Actions.parallel(fadeOutAction, scaleIn2),
                        Actions.parallel(fadeInAction2, scaleOut2),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                suitView.showFront();
                            }
                        }),
                        moveInAction);

        return flipAction;
    }

    private Action getPistiLabelAnimation() {
        final float scaleAmount = 1.3F;

        final ScaleByAction scaleOutAction = Actions.scaleBy(scaleAmount,
                scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticOut);
        final ScaleByAction scaleInAction = Actions.scaleBy(-scaleAmount,
                -scaleAmount, SHOW_LABEL_DURATION, Interpolation.elasticIn);

        return Actions.sequence(Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        pistiLabel.showLight();
                    }
                }),
                scaleOutAction,
                scaleInAction,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        pistiLabel.showDark();
                    }
                }));
    }

    private static class SuitView extends WidgetGroup {

        private final Image frontImage;
        private final Image backImage;

        private SuitView(final Drawable frontDrawable,
                         final Drawable backDrawable) {
            this.frontImage = new Image(frontDrawable);
            this.backImage = new Image(backDrawable);

            frontImage.setFillParent(true);
            backImage.setFillParent(true);

            addActor(backImage);
            addActor(frontImage);

            frontImage.setVisible(true);
            backImage.setVisible(false);
        }

        private void showFront() {
            frontImage.setVisible(true);
            backImage.setVisible(false);
        }

        private void showBack() {
            frontImage.setVisible(false);
            backImage.setVisible(true);
        }
    }

    private static class PistiLabel extends WidgetGroup {
        private final Image dark;
        private final Image light;

        private PistiLabel(final Skin skin) {
            this.dark = new Image(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_PISTI));
            this.light = new Image(skin.getDrawable(Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT));

            dark.setFillParent(true);
            light.setFillParent(true);

            light.setVisible(false);
            dark.setVisible(true);

            addActor(dark);
            addActor(light);
        }

        private void showLight() {
            dark.setVisible(false);
            light.setVisible(true);
        }

        private void showDark() {
            dark.setVisible(true);
            light.setVisible(false);
        }
    }
}