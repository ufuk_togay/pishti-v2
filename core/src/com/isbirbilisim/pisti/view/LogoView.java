package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.resources.Res;


/**
 * Shows logo window and buttons for login.
 */
public class LogoView extends Table {

    private static final float LOGO_RATIO = 1.34F;

    private final Pisti pisti;
    private final Skin splashScreenSkin;
    private final float width;
    private final float height;

    private Skin mainMenuSkin;

    private Image logo;
    private GooglePlusButton googlePlusButton;
    private FacebookButton facebookButton;
    private Image coinIcon;
    private Label currentGoldLabel;

    public LogoView(final Pisti pisti,
                    final Skin splashScreenSkin,
                    final float width,
                    final float height) {
        this.pisti = pisti;
        this.splashScreenSkin = splashScreenSkin;
        this.width = width;
        this.height = height;

        setBackground(splashScreenSkin.getDrawable(Res.textures.SPLASH_BACKGROUND));
        setSize(width, height);
        initLogo();
    }

    private void initLogo() {
        final WidgetGroup rootGroup = new WidgetGroup();
        rootGroup.setFillParent(true);
        add(rootGroup).expand().fill();

        final Drawable drawable = splashScreenSkin.getDrawable(Res.textures.GAME_LOGO);
        logo = new Image(drawable);
        final float logoWidth = width / 3;
        final float logoHeight = width / (3 * LOGO_RATIO);
        logo.setSize(logoWidth, logoHeight);
        logo.setPosition(getCenterX() - (logoWidth / 2),
                getCenterY() - (logoHeight / 2));
        rootGroup.addActor(logo);
    }

    /**
     * Sets skin for main menu.
     */
    public void setMainMenuSkin(final Skin skin) {
        this.mainMenuSkin = skin;

        initMenuViews();
    }

    private void initMenuViews() {
        facebookButton = createFacebookButton();
        googlePlusButton = createSignInButton();

        facebookButton.setVisible(false);
        googlePlusButton.setVisible(false);

        facebookButton.setPosition(getCenterX() - (facebookButton.getWidth() * 1.5F),
                logo.getY() / 2F - (facebookButton.getHeight() / 2F));
        googlePlusButton.setPosition(getCenterX() + (googlePlusButton.getWidth() * 0.5F),
                logo.getY() / 2F - (googlePlusButton.getHeight() / 2F));
        addActor(facebookButton);
        addActor(googlePlusButton);

        coinIcon = new Image(mainMenuSkin.getDrawable(Res.textures.ICON_COIN));
        final float coinSize = pisti.getScreenUtils().toRealWidth(Res.values.COIN_SIZE);
        coinIcon.setSize(coinSize, coinSize);
        coinIcon.setOrigin(coinSize / 2, coinSize / 2);

        coinIcon.setPosition(getCenterX() - (coinSize * 1.5F),
                height - ((height - (logo.getY() + logo.getHeight())) / 2F) - (coinSize / 2F));
        coinIcon.setVisible(false);
        addActor(coinIcon);

        currentGoldLabel = new Label("", mainMenuSkin.get(Res.labelStyles.GOLD_AMOUNT_LABEL_STYLE,
                Label.LabelStyle.class));
        currentGoldLabel.setPosition(coinIcon.getX() + (coinSize * 1.5F),
                coinIcon.getY() + getCenterY(coinIcon) - (currentGoldLabel.getHeight() / 2));
        currentGoldLabel.setVisible(false);
        addActor(currentGoldLabel);
    }

    private float getCenterY() {
        return getHeight() / 2;
    }

    private float getCenterX() {
        return getWidth() / 2;
    }

    private float getCenterY(final Actor actor) {
        return actor.getHeight() / 2;
    }

    /**
     * Shows views.
     */
    public void showViews() {
        /**
         * temporary unavailable.
         */
//        facebookButton.setScale(0.1F);
//        googlePlusButton.setScale(0.1F);
//
//        facebookButton.setVisible(true);
//        googlePlusButton.setVisible(true);
//
//        facebookButton.addAction(Actions.scaleTo(1, 1, 0.2F));
//        googlePlusButton.addAction(Actions.scaleTo(1, 1, 0.2F));
    }

    /**
     * Updates gold label and performs animations.
     */
    public void updateGoldAmount(final long goldAmount) {
        coinIcon.setVisible(true);
        coinIcon.setScale(0.1F, 0.1F);
        coinIcon.addAction(Actions.scaleTo(1, 1, 0.2F));

        currentGoldLabel.setVisible(true);
        currentGoldLabel.setScale(0.1F, 0.1F);
        currentGoldLabel.setText(String.valueOf(goldAmount));
        currentGoldLabel.addAction(Actions.scaleTo(1, 1, 0.2F));
    }

    /**
     * Hides gold label and coin icon.
     */
    public void hideGoldLabelAndIcon() {
        currentGoldLabel.addAction(Actions.sequence(
                Actions.scaleTo(0.1F, 0.1F, 0.2F),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        currentGoldLabel.setVisible(false);
                        currentGoldLabel.setScale(1, 1);
                    }
                })
        ));

        coinIcon.addAction(Actions.sequence(
                Actions.scaleTo(0.1F, 0.1F, 0.2F),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        coinIcon.setVisible(false);
                        coinIcon.setScale(1, 1);
                    }
                })
        ));
    }

    /**
     * @return - instance of GooglePlusButton.
     */
    public GooglePlusButton getGooglePlusButton() {
        return googlePlusButton;
    }

    /**
     * @return - instance of FacebookButton.
     */
    public FacebookButton getFacebookButton() {
        return facebookButton;
    }

    private GooglePlusButton createSignInButton() {
        final GooglePlusButton button = new GooglePlusButton(
                mainMenuSkin.getDrawable(Res.textures.GOOGLE_PLUS_ICON),
                mainMenuSkin.getDrawable(Res.textures.GOOGLE_PLUS_ICON),
                pisti.getScreenUtils().toRealWidth(Res.values.SOCIAL_BUTTON_SIZE),
                pisti.getScreenUtils().toRealHeight(Res.values.SOCIAL_BUTTON_SIZE),
                pisti.getMultiplayerClient().isSignedIn()
        );

        button.setClickListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                if (pisti.getMultiplayerClient().isSignedIn()) {
                    pisti.getMultiplayerClient().signOut();
                    button.setSigned(false);
                } else {
                    pisti.getMultiplayerClient().signIn();
                }
            }
        });

        return button;
    }

    private FacebookButton createFacebookButton() {
        final FacebookButton facebookSignInButton = pisti.getViewUtils()
                .getFacebookSignInButton();
        facebookSignInButton.setImages(
                mainMenuSkin.getDrawable(Res.textures.FACEBOOK_ICON),
                mainMenuSkin.getDrawable(Res.textures.FACEBOOK_ICON),
                pisti.getScreenUtils().toRealWidth(Res.values.SOCIAL_BUTTON_SIZE),
                pisti.getScreenUtils().toRealHeight(Res.values.SOCIAL_BUTTON_SIZE),
                false);

        return facebookSignInButton;
    }
}