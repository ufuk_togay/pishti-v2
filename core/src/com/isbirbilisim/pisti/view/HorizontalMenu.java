package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

import java.util.LinkedList;

public class HorizontalMenu extends Table {

    private final LinkedList<HorizontalMenuItem> items;
    private final WidgetGroup rootGroup;

    private final float width;
    private final float height;

    private final MultimediaManager multimediaManager;

    private OnMenuItemSelectedListener listener;

    public HorizontalMenu(final LinkedList<HorizontalMenuItem> items,
                          final float width,
                          final float height,
                          final MultimediaManager multimediaManager) {
        this.items = items;
        this.width = width;
        this.height = height;
        this.multimediaManager = multimediaManager;

        rootGroup = new WidgetGroup();
        add(rootGroup).expand().fill();

        initItems();
    }

    private void initItems() {
        float xShift = 0;
        for (final HorizontalMenuItem item : items) {
            item.setPosition(xShift, 0);
            xShift += item.getWidth();

            rootGroup.addActor(item);

            item.addCaptureListener(new ClickListener() {
                @Override
                public void clicked(final InputEvent event,
                                    final float x,
                                    final float y) {

                    multimediaManager.playClickSound();

                    if (listener != null) {
                        listener.onMenuItemSelected(item.getMenuEntry());
                    }
                }
            });
        }
    }

    /**
     * Enables/disables menu items that represent action which are connection
     * dependent.
     *
     * @param enabled - true if item should be enabled, false otherwise.
     */
    public void enableConnectionDependentItems(final boolean enabled) {
        for (final HorizontalMenuItem item : items) {
            final MenuEntry menuEntry = item.getMenuEntry();
            if (menuEntry.isConnectionRequired()) {
                item.setDisabled(!enabled);
            }
        }
    }

    /**
     * Sets menu item selected callback.
     */
    public void setOnMenuItemSelectedListener(final OnMenuItemSelectedListener listener) {
        this.listener = listener;
    }

    /**
     * Callback that informs about menu item click event.
     */
    public interface OnMenuItemSelectedListener {
        /**
         * Called when some of menu item was selected by user.
         *
         * @param menuEntry - selected item.
         */
        void onMenuItemSelected(final MenuEntry menuEntry);
    }
}