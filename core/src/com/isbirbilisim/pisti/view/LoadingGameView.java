package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * View that shows game loading progress.
 */
public class LoadingGameView extends Table {

    public LoadingGameView(final ScreenUtils screenUtils,
                           final Skin skin,
                           final float width,
                           final float height) {
        setSize(width, height);

        final float logoWidth = screenUtils.toRealWidth(Res.values.GAME_SCREEN_LOGO_WIDTH);
        final float logoHeight = screenUtils.toRealHeight(Res.values.GAME_SCREEN_LOGO_HEIGHT);

        add(new GameLogoView(skin, logoWidth, logoHeight))
                .center()
                .size(logoWidth, logoHeight);
    }
}