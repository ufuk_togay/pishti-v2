package com.isbirbilisim.pisti.view;

/**
 * Returns instances of views implementation which depends on specific platform.
 */
public interface ViewUtils {

    /**
     * @return - button that handles facebook sing-in requests and maintains
     * current sing in state.
     */
    FacebookButton getFacebookSignInButton();
}