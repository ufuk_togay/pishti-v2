package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;

/**
 * View that shows incoming chat message.
 */
public class ChatOutput extends WidgetGroup {

    private static final float DELAY = 2F;
    private static final float FADE_OUT_DURATION = 0.2F;

    private static final float TEXT_PAD = 30;
    private static final float MAX_WIDTH = 250;

    private final ScreenUtils screenUtils;
    private final Skin skin;
    private final Label messageLabel;

    public ChatOutput(final ScreenUtils screenUtils,
                      final Skin skin) {
        this.screenUtils = screenUtils;
        this.skin = skin;

        final Texture back = skin.get(Res.textures.CHAT_OUTPUT_BACKGROUND, Texture.class);
        final NinePatch ninePatch = new NinePatch(new TextureRegion(back), 20, 20, 15, 15);
        final Image image = new Image(ninePatch);
        image.setFillParent(true);
        addActor(image);

        final Label.LabelStyle labelStyle = skin.get(Res.labelStyles.CHAT_OUTPUT_STYLE,
                Label.LabelStyle.class);

        messageLabel = new Label("", labelStyle);
        messageLabel.setFillParent(true);
        messageLabel.setAlignment(Align.center);
        addActor(messageLabel);

        setVisible(false);
    }

    public void showMessage(final String message) {
        final float textWidth = messageLabel.getStyle().font.getBounds(message).width;
        final float textHeight = messageLabel.getStyle().font.getBounds(message).height;
        final float width;
        if (textWidth > screenUtils.toRealWidth(MAX_WIDTH)) {
            width = MAX_WIDTH + (screenUtils.toRealWidth(TEXT_PAD) * 2);
        } else {
            width = textWidth + (screenUtils.toRealWidth(TEXT_PAD) * 2);
        }
        setWidth(width);
        setHeight(textHeight * 4);

        messageLabel.setText(message);

        setVisible(true);
        setScale(0);

        addAction(Actions.sequence(
                Actions.alpha(1),
                Actions.scaleTo(1, 1, FADE_OUT_DURATION, Interpolation.circle),
                Actions.delay(DELAY),
                Actions.alpha(0, FADE_OUT_DURATION * 2, Interpolation.circle)));
    }
}