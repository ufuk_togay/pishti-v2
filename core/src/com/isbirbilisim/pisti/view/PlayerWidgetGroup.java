package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.utils.Timer;
import com.isbirbilisim.pisti.players.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Logical group of {@link com.isbirbilisim.pisti.view.PlayerWidget} items.
 */
public class PlayerWidgetGroup {
    private final static long INITIAL_COUNT_DOWN = 10;

    private final List<PlayerWidget> items;

    public PlayerWidgetGroup() {
        items = new ArrayList<PlayerWidget>();
    }

    /**
     * Adds {@link com.isbirbilisim.pisti.view.PlayerWidget} item to logical
     * group if items.
     *
     * @param item - item to add.
     */
    public void addWidget(final PlayerWidget item) {
        items.add(item);
    }

    /**
     * Shows turn to move animation.
     *
     * @param player - player that takes turn to move.
     */
    public void showPlayerTurnTimer(final Player player,
                                    final Timer.Task task) {
        cancelCountDown();

        getPlayerWidget(player).startCountDown(INITIAL_COUNT_DOWN, task);
    }

    private PlayerWidget getPlayerWidget(final Player player) {
        for (final PlayerWidget widget : items) {
            if (widget.getPlayer().getParticipantId().
                    equals(player.getParticipantId())) {
                return widget;
            }
        }

        throw new IllegalArgumentException("Unknown player");
    }

    /**
     * Shows turn to move layer over player's photo.
     */
    public void showPlayerTurnToMove(final Player player) {
        getPlayerWidget(player).showTurnToMove();
    }

    /**
     * Hides turn to move layer from photo.
     */
    public void hidePlayerTurnToMove(final Player player) {
        getPlayerWidget(player).hideTurnToMove();
    }

    /**
     * Stops all countdowns.
     */
    public void cancelCountDown() {
        for (final PlayerWidget widget : items) {
            widget.stopCountDown();
        }
    }

    public void refreshScores(final Player player) {
        getPlayerWidget(player).updateScores(player.getScores());
    }

    public void resetRoundEnd() {
        for(final PlayerWidget playerWidget : items) {
            final Player player = playerWidget.getPlayer();
            player.resetCards();
            player.resetCollectedCards();

            player.resetRoundScores();

            refreshScores(player);
        }
    }

    public void resetGameEnd() {
        for(final PlayerWidget playerWidget : items) {
            final Player player = playerWidget.getPlayer();
            player.resetCards();
            player.resetCollectedCards();

            player.resetGameScores();

            refreshScores(player);
        }
    }

    public List<PlayerWidget> getPlayerWidgets() {
        return items;
    }
}