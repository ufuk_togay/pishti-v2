package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.isbirbilisim.pisti.account.LevelWrapper;
import com.isbirbilisim.pisti.dialogs.GamingRoomDialog;

import java.util.List;

/**
 * View that shows info about gaming room.
 */
public class RoomWidget extends Table {

    private final List<LevelWrapper> levelWrappers;
    private final boolean isMale;
    private final Skin skin;

    private GamingRoomDialog.OnLevelSelectedListener listener;

    private boolean processClick;

    public RoomWidget(final List<LevelWrapper> levelWrappers,
                      final boolean isMale,
                      final Skin skin,
                      final String backgroundName,
                      final float levelsPadding) {
        this.levelWrappers = levelWrappers;
        this.isMale = isMale;
        this.skin = skin;

        processClick = true;

        initBackground(backgroundName);
        initLevels(levelsPadding);
    }

    private void initBackground(final String backgroundName) {
        setBackground(skin.getDrawable(backgroundName));
    }

    private void initLevels(final float levelsPadding) {
        for (final LevelWrapper levelWrapper : levelWrappers) {
            add(createLevelWidget(levelWrapper, isMale, levelsPadding)).center().expandX()
                    .fillX().pad(levelsPadding);
            row();
        }
    }

    private LevelWidget createLevelWidget(final LevelWrapper wrapper,
                                          final boolean isMale,
                                          final float padding) {
        final LevelWidget levelWidget = new LevelWidget(wrapper,
                isMale,
                skin,
                padding);
        levelWidget.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);

                if (listener != null && processClick) {
                    listener.onLevelSelected(wrapper.getLevel());
                }
            }
        });

        return levelWidget;
    }

    public void setOnLevelSelectedListener(
            final GamingRoomDialog.OnLevelSelectedListener listener) {
        this.listener = listener;
    }

    public void setProcessClick(final boolean processClick) {
        this.processClick = processClick;
    }
}