package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Pool;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Widget group that contains menu items. Items can be swiped using mouse or touchpad.
 */
public class MenuWidget extends WidgetGroup {
    /**
     * Callback that informs about menu item click event.
     */
    public interface OnMenuItemSelectedListener {
        /**
         * Called when some of menu item was selected by user.
         *
         * @param menuEntry - selected item.
         */
        void onMenuItemSelected(final MenuEntry menuEntry);
    }

    private static final float VISIBLE_REGION_ANGLE = 100;
    private static final float DRAG_ACTION_DURATION = 0.4F;
    private static final float SWAP_ANIMATION_X = 58;
    private static final float SWAP_ANIMATION_Y = 17;
    private static final float SWAP_ACTION_DURATION = 0.15F;
    private static final float ITEM_SCALE_ANIMATION_DURATION = 0.1F;
    private static final float MIN_DRAG_DISTANCE = 20;
    private static final float CARD_RATIO = 1.36F;
    private static final int VISIBLE_ITEMS = 5;

    private final ScreenUtils screenUtils;

    private final float screenWidth;
    private final float menuInnerCircleRadius;
    private final float menuOuterCircleRadius;
    private final Vector2 circleCenter;

    private float dragPoint;
    private boolean isDragging;
    private LinkedList<Item> items;
    private OnMenuItemSelectedListener listener;

    private final Pool<AlphaAction> alphaActionPool = new Pool<AlphaAction>() {
        @Override
        protected AlphaAction newObject() {
            final AlphaAction action = new AlphaAction();
            action.setDuration(DRAG_ACTION_DURATION);
            action.setInterpolation(Interpolation.circleOut);

            return action;
        }
    };

    private final Pool<RotateToAction> rotateToActionPool = new Pool<RotateToAction>() {
        @Override
        protected RotateToAction newObject() {
            final RotateToAction action = new RotateToAction();
            action.setDuration(DRAG_ACTION_DURATION);
            action.setInterpolation(Interpolation.circleOut);

            return action;
        }
    };

    private final Pool<MoveToAction> moveToActionPool = new Pool<MoveToAction>() {
        @Override
        protected MoveToAction newObject() {
            final MoveToAction action = new MoveToAction();
            action.setDuration(DRAG_ACTION_DURATION);
            action.setInterpolation(Interpolation.circleOut);

            return action;
        }
    };

    public MenuWidget(final ScreenUtils screenUtils,
                      final LinkedList<MenuItem> menuItems) {
        this.screenUtils = screenUtils;
        this.screenWidth = screenUtils.getScreenWidth();

        menuOuterCircleRadius = getMenuOuterCircleRadius();
        menuInnerCircleRadius = menuOuterCircleRadius / 2;
        circleCenter = getCircleCenter();

        initMenuItems(menuItems);
        addItems(sortContainersByAngle(getContainerList()));
        setInputEventsCallbacks();

        setClickCallbacks();
    }

    /**
     * Enables/disables menu items that represent action which are connection
     * dependent.
     *
     * @param enabled - true if item should be enabled, false otherwise.
     */
    public void enabledConnectionDependentItems(final boolean enabled) {
        for (final Item item : items) {
            final MenuItem menuItem = item.container.getActor();
            if (menuItem.getMenuEntry().isConnectionRequired()) {
                menuItem.setDisabled(!enabled);
            }
        }
    }

    /**
     * Sets {@link OnMenuItemSelectedListener} listener instance.
     *
     * @param listener - listener instance.
     */
    public void setOnMenuSelectedItemListener(
            final OnMenuItemSelectedListener listener) {
        this.listener = listener;
    }

    /**
     * Swipes all menu items to right by single position.
     */
    public void swipeToRight() {
        if (!isDragging) {
            dragPoint = 0;
            dragItems(1);
        }
    }

    /**
     * Swipes all menu items to left by single position.
     */
    public void swipeToLeft() {
        if (!isDragging) {
            dragPoint = 1;
            dragItems(0);
        }
    }

    /**
     * @return - Name of menu item that currently is on top of other items.
     */
    public String getCurrentTopItemName() {
        for (final Item item : items) {
            if (item.angle < VISIBLE_REGION_ANGLE / VISIBLE_ITEMS
                    && item.angle > -VISIBLE_REGION_ANGLE / VISIBLE_ITEMS) {
                return item.container.getActor().getName();
            }
        }

        return null;
    }

    private void initMenuItems(final LinkedList<MenuItem> menuItems) {
        items = new LinkedList<Item>();
        final float[] angles = createInitialAngles(menuItems.size());
        final Vector2 vector2 = calculateActorSize();

        for (int i = 0; i < menuItems.size(); i++) {
            menuItems.get(i).setSize(vector2.x, vector2.y);

            final Container<MenuItem> wrapper =
                    createContainer(menuItems.get(i), angles[i]);
            final Item item = new Item(wrapper, angles[i]);
            items.add(item);
        }
    }

    private float[] createInitialAngles(final int size) {
        final float[] angles = new float[size];

        angles[0] = VISIBLE_REGION_ANGLE / VISIBLE_ITEMS * 2;
        for (int i = 1; i < size; i++) {
            angles[i] = (i - 2) * (-VISIBLE_REGION_ANGLE / VISIBLE_ITEMS);
        }

        return angles;
    }

    private Container<MenuItem> createContainer(final MenuItem menuItem,
                                                final float angle) {
        menuItem.setFillParent(true);

        final Container<MenuItem> wrapper = new Container<MenuItem>();
        wrapper.left();
        wrapper.bottom();

        wrapper.setTransform(true);
        wrapper.getColor().a = calculateAlpha(angle);

        final Vector2 position = calculatePosition(menuItem.getWidth(),
                angle);

        wrapper.setSize(menuItem.getWidth(), menuItem.getHeight());
        wrapper.setOriginX(wrapper.getWidth() / 2);
        wrapper.setOriginY(wrapper.getHeight() / 2);

        wrapper.setRotation(angle);
        wrapper.setPosition(position.x, position.y);

        wrapper.setActor(menuItem);
        wrapper.setName(menuItem.getName());

        return wrapper;
    }

    private Vector2 getCircleCenter() {
        final float x = screenWidth / 2;
        final float y = -menuOuterCircleRadius
                * MathUtils.cosDeg(VISIBLE_REGION_ANGLE / 2);

        return new Vector2(x, y);
    }

    private Vector2 calculateActorSize() {
        final float width = getActLength(menuOuterCircleRadius, VISIBLE_REGION_ANGLE)
                / VISIBLE_ITEMS * 0.8F;
        final float height = width * CARD_RATIO;
        return new Vector2(width, height);
    }

    private float getActLength(final float radius, final float angle) {
        return MathUtils.PI * radius * (angle / 180);
    }

    private float getMenuOuterCircleRadius() {
        return screenWidth * 0.9F;
    }

    private Vector2 calculatePosition(final float itemWidth,
                                      final float angle) {
        final Vector2 circlePoint = getCirclePoint(menuInnerCircleRadius,
                angle, circleCenter);

        return calculateFixedPosition(circlePoint, itemWidth);
    }

    private Vector2 calculateFixedPosition(final Vector2 position,
                                           final float itemWidth) {
        final Vector2 vector2 = new Vector2();
        vector2.x = position.x - (itemWidth / 2);
        vector2.y = position.y;

        return vector2;
    }

    private void addItems(final List<Actor> items) {
        for (final Actor menuItem : items) {
            addActor(menuItem);
        }
    }

    private float calculateAlpha(final float angle) {
        return 1F - (Math.abs(angle) / (90) / 2F);
    }

    private Vector2 getCirclePoint(final float radius,
                                   final float angle,
                                   final Vector2 circleCenter) {
        final float x;
        final float y;

        if (angle > 0) {
            x = circleCenter.x
                    - radius * MathUtils.cosDeg(-angle + 90);
            y = circleCenter.y
                    + radius * MathUtils.sinDeg(-angle + 90);
        } else {
            x = circleCenter.x
                    + radius * MathUtils.cosDeg(angle + 90);
            y = circleCenter.y
                    + radius * MathUtils.sinDeg(-angle + 90);
        }

        return new Vector2(x, y);
    }

    private void setInputEventsCallbacks() {
        addCaptureListener(new DragListener() {
            @Override
            public boolean touchDown(final InputEvent event,
                                     final float x,
                                     final float y,
                                     final int pointer,
                                     final int button) {
                dragPoint = x;
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchDragged(final InputEvent event,
                                     final float x,
                                     final float y,
                                     final int pointer) {
                super.touchDragged(event, x, y, pointer);

                if (!isDragging && shouldDrag(x)) {
                    dragItems(x);
                }
                dragPoint = x;
            }

            private boolean shouldDrag(final float x) {
                return Math.abs(x - dragPoint) > MIN_DRAG_DISTANCE;
            }
        });
    }

    private void setClickCallbacks() {
        final ClickListener clickListener = new ClickListener() {
            @Override
            public boolean touchDown(final InputEvent event,
                                     final float x,
                                     final float y,
                                     final int pointer,
                                     final int button) {
                event.getListenerActor().addAction(Actions.scaleBy(
                        0.2F, 0.2F, ITEM_SCALE_ANIMATION_DURATION
                ));

                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(final InputEvent event,
                                final float x,
                                final float y,
                                final int pointer,
                                final int button) {
                super.touchUp(event, x, y, pointer, button);

                event.getListenerActor().addAction(Actions.scaleBy(
                        -0.2F, -0.2F, ITEM_SCALE_ANIMATION_DURATION
                ));
            }

            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) {
                super.clicked(event, x, y);

                if (isDragging) {
                    return;
                }

                final MenuItem menuItem = ((Container<MenuItem>) event
                        .getListenerActor()).getActor();
                if (!menuItem.isDisabled() && listener != null) {
                    listener.onMenuItemSelected(
                            menuItem.getMenuEntry());
                }
            }
        };

        for (final Item item : items) {
            item.container.addListener(clickListener);
        }
    }

    private void dragItems(final float x) {
        isDragging = true;

        final boolean movingRight;
        if (x - dragPoint > 0) {
            movingRight = true;

            if (isItemToLeftNeeded()) {
                swapItemToLeft();
            }

            updateAngles(-VISIBLE_REGION_ANGLE / VISIBLE_ITEMS);
        } else {
            movingRight = false;

            if (isItemToRightNeeded()) {
                swapItemToRight();
            }

            updateAngles(VISIBLE_REGION_ANGLE / VISIBLE_ITEMS);
        }
        for (final Item item : items) {
            if (shouldSwap(item.container, movingRight)) {
                swapItem(item.container, item.angle);
            } else {
                dragItem(item.container, item.angle);
            }
        }
    }

    private boolean shouldSwap(final Actor item, final boolean isMovingRight) {
        final float angle = VISIBLE_REGION_ANGLE / VISIBLE_ITEMS;

        if (isMovingRight) {
            return item.getRotation() < angle + 5 && item.getRotation() > angle - 5;
        }

        return item.getRotation() > -angle - 5 && item.getRotation() < -angle + 5;
    }

    private void updateAngles(final float angle) {
        for (final Item item : items) {
            item.angle += angle;
        }
    }

    private void dragItem(final Actor item, final float angle) {
        final Action action = Actions.parallel(
                createMoveToAction(item.getWidth(), angle),
                createRotateToAction(angle),
                createAlphaAction(angle));
        item.addAction(action);
    }

    private void swapItem(final Actor actor, final float angle) {
        final boolean movingRight = actor.getRotation() > 0;

        final RunnableAction updateOrderAction = new RunnableAction();
        updateOrderAction.setRunnable(new Runnable() {
            @Override
            public void run() {
                removeContainers();
                addItems(sortContainersByAngle(getContainerList()));
            }
        });

        final RunnableAction dragEndAction = new RunnableAction();
        dragEndAction.setRunnable(new Runnable() {
            @Override
            public void run() {
                isDragging = false;
            }
        });

        final Action action = Actions.sequence(createSwapAnimation(movingRight),
                updateOrderAction,
                Actions.parallel(
                        createMoveToAction(actor.getWidth(), angle),
                        createRotateToAction(angle),
                        createAlphaAction(angle)),
                dragEndAction
        );
        actor.addAction(action);
    }

    private void removeContainers() {
        for (final Item item : items) {
            removeActor(item.container);
        }
    }

    private Action createSwapAnimation(final boolean moveRight) {
        final float moveByXAmount;
        final float rotateByAmount;

        if (moveRight) {
            moveByXAmount = -screenUtils.toRealWidth(SWAP_ANIMATION_X);
            rotateByAmount = -VISIBLE_REGION_ANGLE / VISIBLE_ITEMS / 2;
        } else {
            moveByXAmount = screenUtils.toRealWidth(SWAP_ANIMATION_X);
            rotateByAmount = VISIBLE_REGION_ANGLE / VISIBLE_ITEMS / 2;
        }

        final MoveByAction moveByAction = new MoveByAction();

        moveByAction.setAmount(moveByXAmount,
                screenUtils.toRealHeight(SWAP_ANIMATION_Y));
        moveByAction.setDuration(SWAP_ACTION_DURATION);
        moveByAction.setInterpolation(Interpolation.circleOut);

        final RotateByAction rotateByAction = new RotateByAction();

        rotateByAction.setAmount(rotateByAmount);
        rotateByAction.setDuration(SWAP_ACTION_DURATION);
        rotateByAction.setInterpolation(Interpolation.circleOut);

        return Actions.parallel(moveByAction, rotateByAction);
    }

    private boolean isItemToLeftNeeded() {
        final Container leftItem = items.getFirst().container;
        return leftItem.getRotation() < (VISIBLE_REGION_ANGLE / VISIBLE_ITEMS) * 2 + 5;
    }

    private boolean isItemToRightNeeded() {
        final Container rightItem = items.getLast().container;
        return rightItem.getRotation() > -(VISIBLE_REGION_ANGLE / VISIBLE_ITEMS) * 2 - 5;
    }

    private void swapItemToLeft() {
        final Item item = items.pollLast();

        final float angle = items.getFirst().angle
                + VISIBLE_REGION_ANGLE / VISIBLE_ITEMS;
        final Vector2 position = calculatePosition(item.container.getWidth(),
                angle
        );

        item.container.setRotation(angle);
        item.angle = angle;
        item.container.setPosition(position.x, position.y);

        items.addFirst(item);
    }

    private void swapItemToRight() {
        final Item item = items.pollFirst();

        final float angle = items.getLast().angle
                - VISIBLE_REGION_ANGLE / VISIBLE_ITEMS;
        final Vector2 position = calculatePosition(item.container.getWidth(),
                angle
        );

        item.container.setRotation(angle);
        item.angle = angle;
        item.container.setPosition(position.x, position.y);

        items.addLast(item);
    }

    private LinkedList<Actor> getContainerList() {
        final LinkedList<Actor> containers = new LinkedList<Actor>();
        for (final Item item : items) {
            containers.add(item.container);
        }

        return containers;
    }

    private List<Actor> sortContainersByAngle(final LinkedList<Actor> list) {
        Collections.sort(list, new Comparator<Actor>() {
            @Override
            public int compare(final Actor o1, final Actor o2) {
                final float angle1 = Math.abs(o1.getRotation());
                final float angle2 = Math.abs(o2.getRotation());
                if (angle1 > angle2) return -1;
                return 1;
            }
        });

        return list;
    }

    private MoveToAction createMoveToAction(final float itemWidth,
                                            final float angle) {
        final MoveToAction action = moveToActionPool.obtain();

        final Vector2 position = calculatePosition(itemWidth, angle);

        action.setPosition(position.x, position.y);

        return action;
    }

    private RotateToAction createRotateToAction(final float angle) {
        final RotateToAction rotateToAction = rotateToActionPool.obtain();
        rotateToAction.setRotation(angle);

        return rotateToAction;
    }

    private AlphaAction createAlphaAction(final float angle) {
        final AlphaAction alphaAction = alphaActionPool.obtain();
        alphaAction.setAlpha(calculateAlpha(angle));

        return alphaAction;
    }

    private static class Item {
        private final Container<MenuItem> container;
        private float angle;

        private Item(final Container<MenuItem> container,
                     final float angle) {
            this.container = container;
            this.angle = angle;
        }
    }
}