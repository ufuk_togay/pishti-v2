package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.isbirbilisim.pisti.resources.Res;

/**
 * View that contains coin image and text with player's amount of gold.
 */
public class GoldView extends Table {

    private final Skin skin;

    private Label goldAmount;

    public GoldView(final Skin skin, final float coinSize) {
        this.skin = skin;

        init(coinSize);
    }

    private void init(final float coinSize) {
        final Image coin = new Image(skin.getDrawable(Res.textures.MAIN_MENU_COIN));
        add(coin).prefSize(coinSize);

       // goldAmount = new Label("", skin, Res.labelStyles.MAIN_MENU_GOLD_LABEL_STYLE);
        add(goldAmount).padLeft(coinSize / 3);
    }

    /**
     * Shows label with gold amount.
     */
    public void showGoldAmountLabel() {
        goldAmount.setVisible(true);
    }

    /**
     * Hides label with gold amount.
     */
    public void hideGoldAmountLabel() {
        goldAmount.setVisible(false);
    }

    /**
     * Sets amount of gold
     *
     * @param gold - amount of gold.
     */
    public void setAmountOfGold(final long gold) {
        goldAmount.setText(String.valueOf(gold));
    }
}