package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.isbirbilisim.pisti.account.LevelWrapper;
import com.isbirbilisim.pisti.resources.Res;

/**
 * Widget which shows info about gaming level.
 */
public class LevelWidget extends Table {

    private final LevelWrapper levelWrapper;
    private final boolean isMale;
    private final Skin skin;

    public LevelWidget(final LevelWrapper levelWrapper,
                       final boolean isMale,
                       final Skin skin,
                       final float padding) {
        this.levelWrapper = levelWrapper;
        this.isMale = isMale;
        this.skin = skin;

        init(padding);
    }

    private void init(final float padding) {
        add(getLevelNameLabel()).expand().left().padLeft(padding);
        add(getBidValueLabel()).expand().right().padRight(padding);
    }

    private Label getLevelNameLabel() {
        return new Label(levelWrapper.getLevel().getName(isMale),
                skin,
                Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE);
    }

    private Label getBidValueLabel() {
        return new Label(
                String.valueOf(levelWrapper.getLevel().getBidAmount()),
                skin,
                Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE);
    }
}