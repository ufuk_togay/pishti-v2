package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.Scaling;

public class VerticalImageTextButton extends Button {
    private final Image image;
    private final Label label;
    private ImageTextButton.ImageTextButtonStyle style;

    public VerticalImageTextButton(String text, Skin skin) {
        this(text, skin.get(ImageTextButton.ImageTextButtonStyle.class));
        setSkin(skin);
    }

    public VerticalImageTextButton(String text, Skin skin, String styleName) {
        this(text, skin.get(styleName, ImageTextButton.ImageTextButtonStyle.class));
        setSkin(skin);
    }

    public VerticalImageTextButton(String text, ImageTextButton.ImageTextButtonStyle style) {
        super(style);
        this.style = style;

        defaults().space(3);

        image = new Image();
        image.setScaling(Scaling.fit);
        add(image).row();

        label = new Label(text, new Label.LabelStyle(style.font, style.fontColor));
        label.setAlignment(Align.center);
        add(label);

        setStyle(style);

        setSize(getPrefWidth(), getPrefHeight());
    }

    public void setStyle(ButtonStyle style) {
        if (!(style instanceof ImageTextButton.ImageTextButtonStyle))
            throw new IllegalArgumentException("style must be a ImageTextButtonStyle.");
        super.setStyle(style);
        this.style = (ImageTextButton.ImageTextButtonStyle) style;
        if (image != null) updateImage();
        if (label != null) {
            ImageTextButton.ImageTextButtonStyle textButtonStyle = (ImageTextButton.ImageTextButtonStyle) style;
            Label.LabelStyle labelStyle = label.getStyle();
            labelStyle.font = textButtonStyle.font;
            labelStyle.fontColor = textButtonStyle.fontColor;
            label.setStyle(labelStyle);
        }
    }

    public ImageTextButton.ImageTextButtonStyle getStyle() {
        return style;
    }

    private void updateImage() {
        boolean isPressed = isPressed();
        if (isDisabled() && style.imageDisabled != null)
            image.setDrawable(style.imageDisabled);
        else if (isPressed && style.imageDown != null)
            image.setDrawable(style.imageDown);
        else if (isChecked() && style.imageChecked != null)
            image.setDrawable((style.imageCheckedOver != null && isOver()) ? style.imageCheckedOver : style.imageChecked);
        else if (isOver() && style.imageOver != null)
            image.setDrawable(style.imageOver);
        else if (style.imageUp != null) //
            image.setDrawable(style.imageUp);
    }

    public void draw(Batch batch, float parentAlpha) {
        updateImage();
        Color fontColor;
        if (isDisabled() && style.disabledFontColor != null)
            fontColor = style.disabledFontColor;
        else if (isPressed() && style.downFontColor != null)
            fontColor = style.downFontColor;
        else if (isChecked() && style.checkedFontColor != null)
            fontColor = (isOver() && style.checkedOverFontColor != null) ? style.checkedOverFontColor : style.checkedFontColor;
        else if (isOver() && style.overFontColor != null)
            fontColor = style.overFontColor;
        else
            fontColor = style.fontColor;
        if (fontColor != null) label.getStyle().fontColor = fontColor;
        super.draw(batch, parentAlpha);
    }

    public Image getImage() {
        return image;
    }

    public Cell getImageCell() {
        return getCell(image);
    }

    public Label getLabel() {
        return label;
    }

    public Cell getLabelCell() {
        return getCell(label);
    }

    public void setText(CharSequence text) {
        label.setText(text);
    }

    public CharSequence getText() {
        return label.getText();
    }
}