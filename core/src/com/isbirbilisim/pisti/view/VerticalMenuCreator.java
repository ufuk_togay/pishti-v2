package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Utils for creating vertical menu.
 */
public class VerticalMenuCreator {

    /**
     * Creates VerticalScrollMenu with given params.
     *
     * @param skin   - skin.
     * @param width  - menu with.
     * @param height - menu height.
     * @return - menu instance.
     */
    public static VerticalScrollMenu create(
            final VerticalScrollMenu.OnMenuItemSelectedListener listener,
            final Skin skin,
            final ScreenUtils screenUtils,
            final MultimediaManager multimediaManager,
            final float width,
            final float height) {
        final List<VerticalMenuItem> items = new ArrayList<VerticalMenuItem>();

        final VerticalMenuItem singleGame = new VerticalMenuItem(MenuEntry.SINGLE_PLAYER,
                skin,
                Res.textures.ICON_SINGLE_GAME,
                width,
                height / 6F);
        items.add(singleGame);

        final VerticalMenuItem playWithFriends = new VerticalMenuItem(MenuEntry.PLAY_WITH_FRIENDS,
                skin,
                Res.textures.ICON_PLAY_WITH_FRIENDS,
                width,
                height / 6F);
        items.add(playWithFriends);

        final VerticalMenuItem quickGame = new VerticalMenuItem(MenuEntry.QUICK_GAME,
                skin,
                Res.textures.ICON_QUICK_GAME,
                width,
                height / 6F);
        items.add(quickGame);

        final VerticalMenuItem inviteFriends = new VerticalMenuItem(MenuEntry.INVITE_FRIENDS,
                skin,
                Res.textures.ICON_INVITE_FRIENDS,
                width,
                height / 6F);
        items.add(inviteFriends);

        final VerticalMenuItem singlePlayerStatistic = new VerticalMenuItem(MenuEntry.SINGLE_PLAYER_STATISTIC,
                skin,
                Res.textures.ICON_ABOUT,
                width,
                height / 6F);
        items.add(singlePlayerStatistic);

        final VerticalMenuItem about = new VerticalMenuItem(MenuEntry.ABOUT,
                skin,
                Res.textures.ICON_ABOUT,
                width,
                height / 6F);

        items.add(about);

        final VerticalScrollMenu menu = new VerticalScrollMenu(width,
                height,
                items,
                screenUtils,
                multimediaManager);
        menu.setOnMenuItemSelectedListener(listener);
        return menu;
    }
}