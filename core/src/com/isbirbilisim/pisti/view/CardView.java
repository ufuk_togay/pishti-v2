package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.isbirbilisim.pisti.cards.CardModel;

/**
 * Widget that contains card image.
 */
public class CardView extends WidgetGroup {
    private static final float FLIP_ROTATION = 0;

    private final CardModel cardModel;
    private final Image visibleCard;
    private final Image hiddenCard;

    public CardView(final CardModel cardModel,
                    final Image visibleCard,
                    final Image hiddenCard) {
        this.cardModel = cardModel;
        this.visibleCard = visibleCard;
        this.hiddenCard = hiddenCard;

        init();
    }

    private void init() {
        addActor(visibleCard);
        visibleCard.setVisible(false);

        addActor(hiddenCard);
    }

    /**
     * Sets card size.
     *
     * @param width  - card width.
     * @param height - card height.
     */
    public void setSize(final float width, final float height) {
        super.setSize(width, height);
        setOrigin(width / 2, height / 2);

        visibleCard.setSize(width, height);
        visibleCard.setOrigin(width / 2, height / 2);
        hiddenCard.setSize(width, height);
        hiddenCard.setOrigin(width / 2, height / 2);
    }

    public CardModel getCard() {
        return cardModel;
    }

    public boolean isHidden() {
        return !visibleCard.isVisible();
    }

    /**
     * Flips card to opposite side.
     *
     * @param duration - flip animation duration.
     */
    public void flipCard(final float duration) {
        if (visibleCard.isVisible()) {
            visibleCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            visibleCard.setVisible(false);
                            visibleCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));

            hiddenCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            hiddenCard.setVisible(true);
                            hiddenCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));
        } else {
            visibleCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            visibleCard.setVisible(true);
                            visibleCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));

            hiddenCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            hiddenCard.setVisible(false);
                            hiddenCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));
        }
    }

    /**
     * Flips card to opposite side and performs other given action at the
     * same time.
     *
     * @param duration - flip animation duration.
     * @param parallel - animation thar will be performed at the same time.
     */
    public void flipCard(final float duration,
                         final Action parallel) {
        addAction(parallel);

        if (visibleCard.isVisible()) {
            visibleCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            visibleCard.setVisible(false);
                            visibleCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));

            hiddenCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            hiddenCard.setVisible(true);
                            hiddenCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));
        } else {
            visibleCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            visibleCard.setVisible(true);
                            visibleCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));

            hiddenCard.addAction(Actions.sequence(
                    getFlipInAction(duration / 4),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            hiddenCard.setVisible(false);
                            hiddenCard.addAction(
                                    getFlipOutAction(duration / 4));
                        }
                    })
            ));
        }
    }

    private Action getFlipInAction(final float duration) {
        final RotateByAction rotateByAction = new RotateByAction();
        rotateByAction.setDuration(duration);
        rotateByAction.setAmount(FLIP_ROTATION);

        return Actions.parallel(Actions.scaleTo(0, 1, duration),
                rotateByAction);
    }

    private Action getFlipOutAction(final float duration) {
        final RotateByAction rotateByAction = new RotateByAction();
        rotateByAction.setDuration(duration);
        rotateByAction.setAmount(-FLIP_ROTATION);

        return Actions.parallel(Actions.scaleTo(1, 1, duration),
                rotateByAction);
    }
}