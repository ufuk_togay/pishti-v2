package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.LinkedList;

/**
 * Widget that shows sliding pages.
 */
public class RoomMenuWidget extends WidgetGroup {

    private static final float PAGE_SIDES_RATIO = 1.4F;
    private static final float BALANCE_ANIMATION_DURATION = 0.2F;

    private final float width;
    private final float height;

    private final LinkedList<Container<RoomWidget>> containers;

    private boolean wasPanning;

    public RoomMenuWidget(final float width,
                          final float height,
                          final LinkedList<RoomWidget> pages) {
        this.width = width;
        this.height = height;
        this.containers = getPagesContainers(pages);

        setSize(width, height);
        initPagesSize();
        initPagesPosition();
        initPageScale();
        setInputEventsCallbacks();

        for (final Container<RoomWidget> container : containers) {
            addActor(container);
        }
    }

    private LinkedList<Container<RoomWidget>> getPagesContainers(
            final LinkedList<RoomWidget> pages) {
        final LinkedList<Container<RoomWidget>> containers =
                new LinkedList<Container<RoomWidget>>();
        for (final RoomWidget roomWidget : pages) {
            final Container<RoomWidget> container = new Container<RoomWidget>();

            container.left();
            container.bottom();
            container.setTransform(true);
            container.setSize(roomWidget.getWidth(), roomWidget.getHeight());
            container.setOriginX(roomWidget.getWidth() / 2);
            container.setOriginY(roomWidget.getHeight() / 2);

            roomWidget.setFillParent(true);
            container.setActor(roomWidget);

            containers.add(container);
        }

        return containers;
    }

    private void initPagesSize() {
        for (final Container<RoomWidget> page : containers) {
            page.setSize(height * PAGE_SIDES_RATIO, height);
            page.setOrigin(page.getWidth() / 2,
                    page.getHeight() / 2);
        }
    }

    private void initPagesPosition() {
        final Vector2 center = getCenter();
        float xShift = 0;

        for (final Container<RoomWidget> page : containers) {
            page.setPosition(center.x - (page.getWidth() / 2) + xShift,
                    center.y - (page.getHeight() / 2));
            xShift += page.getWidth();
        }
    }

    private void initPageScale() {
        for (final Container<RoomWidget> page : containers) {
            page.setScale(getScaleValue(getPageCenterPosition(page).x));
        }
    }

    private void setInputEventsCallbacks() {
        addCaptureListener(new ActorGestureListener() {

            @Override
            public void touchDown(final InputEvent event,
                                  final float x,
                                  final float y,
                                  final int pointer,
                                  final int button) {
                super.touchDown(event, x, y, pointer, button);

                wasPanning = false;
            }

            @Override
            public void pan(final InputEvent event,
                            final float x,
                            final float y,
                            final float deltaX,
                            final float deltaY) {
                super.pan(event, x, y, deltaX, deltaY);

                dragPages(deltaX);

                wasPanning = true;
            }

            @Override
            public void touchUp(final InputEvent event,
                                final float x,
                                final float y,
                                final int pointer,
                                final int button) {
                super.touchUp(event, x, y, pointer, button);

                centerToClosestPages();
            }
        });

        for (final Container<RoomWidget> page : containers) {
            page.getActor().addCaptureListener(new ClickListener() {
                @Override
                public void clicked(final InputEvent event,
                                    final float x,
                                    final float y) {
                    if (wasPanning) {
                        page.getActor().setProcessClick(false);
                        return;
                    }

                    if (!isPageAtCenter(page)) {
                        page.getActor().setProcessClick(false);
                        centerToPage(page);
                    } else {
                        page.getActor().setProcessClick(true);
                    }
                }
            });
        }
    }

    private void dragPages(final float x) {
        if (getPageCenterPosition(containers.getFirst()).x > getCenter().x
                || getPageCenterPosition(containers.getLast()).x < getCenter().x) {
            return;
        }

        for (final Container<RoomWidget> page : containers) {
            final MoveToAction moveToAction = createMoveToAction(page.getX() + x,
                    page.getY());
            final ScaleToAction scaleToAction = createScaleAction(
                    getPageCenterPosition(page).x + x);

            final Action action = Actions.parallel(moveToAction, scaleToAction);
            page.addAction(action);
        }
    }

    private void movePages(final float x) {
        for (final Container<RoomWidget> page : containers) {
            final MoveToAction moveToAction = createMoveToAction(page.getX() + x,
                    page.getY());
            final ScaleToAction scaleToAction = createScaleAction(
                    getPageCenterPosition(page).x + x);

            moveToAction.setDuration(BALANCE_ANIMATION_DURATION);
            moveToAction.setInterpolation(Interpolation.circleOut);
            scaleToAction.setDuration(BALANCE_ANIMATION_DURATION);
            scaleToAction.setInterpolation(Interpolation.circleOut);

            final Action action = Actions.parallel(
                    moveToAction,
                    scaleToAction);

            page.addAction(action);
        }
    }

    private void centerToPage(final Container<RoomWidget> page) {
        final float centerShift = getCenter().x -
                getPageCenterPosition(page).x;

        movePages(centerShift);
    }

    private MoveToAction createMoveToAction(final float x,
                                            final float y) {
        return Actions.moveTo(x, y);
    }

    private ScaleToAction createScaleAction(final float pageCenterX) {
        final float scaleValue = getScaleValue(pageCenterX);
        return Actions.scaleTo(scaleValue, scaleValue);
    }

    private float getScaleValue(final float pageCenterX) {
        final float distanceToCenter = Math.abs(getCenter().x - pageCenterX);
        final float max = width;

        return 1 - (distanceToCenter / max);
    }

    private void centerToClosestPages() {
        final Vector2 center = getCenter();
        float balanceShift = width;

        for (final Container<RoomWidget> page : containers) {
            final Vector2 pageCenter = getPageCenterPosition(page);
            if (Math.abs(center.x - pageCenter.x) < balanceShift) {
                balanceShift = (center.x - pageCenter.x);
            }
        }

        movePages(balanceShift);
    }

    private Vector2 getCenter() {
        return new Vector2(width / 2, height / 2);
    }

    private Vector2 getPageCenterPosition(final Container<RoomWidget> page) {
        return new Vector2(page.getX() + (page.getWidth() / 2),
                page.getY() + (page.getHeight() / 2));
    }

    private boolean isPageAtCenter(final Container<RoomWidget> page) {
        return Math.abs(getCenter().x - getPageCenterPosition(page).x) < 20;
    }
}