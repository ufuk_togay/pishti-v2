package com.isbirbilisim.pisti.view;

import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;

/**
 * View that represents main menu item.
 */
public class MenuItem extends ImageTextButton {

    private final MenuEntry menuEntry;

    public MenuItem(final MenuEntry menuEntry,
                    final Skin skin,
                    final float textTopPadding) {
        super(menuEntry.getName(skin.get(Res.properties.STRINGS, Strings.class)),
                skin,
                Res.buttonStyles.MENU_HORIZONTAL_ITEM_STYLE);

        this.menuEntry = menuEntry;

        init(textTopPadding);
    }

    private void init(final float textTopPadding) {
        top();
        padTop(textTopPadding);
    }

    public MenuEntry getMenuEntry() {
        return menuEntry;
    }
}