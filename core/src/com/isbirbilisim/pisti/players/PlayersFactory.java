package com.isbirbilisim.pisti.players;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PlayersFactory {
    /**
     * Creates list of {@link com.isbirbilisim.pisti.players.Player} players,
     * that will participate in match.
     *
     * @param playerInfo - user's info.
     * @return - list of players.
     */
    public static List<Player> createSingleGamePlayers(
            final UserInfo playerInfo,
            final int numberOfPlayers,
            final Skin skin) {
        if (numberOfPlayers != 2 && numberOfPlayers != 4) {
            throw new IllegalArgumentException("Only 2 or 4 players allowed");
        }

        final List<Player> players = new ArrayList<Player>();
        final List<String> randomNames = getRandomNames(skin, numberOfPlayers);

        final Player currentPlayer = new Player(Role.PLAYER, playerInfo,
                PlayerState.READY, String.valueOf(0));
        currentPlayer.setPlaceOnTable(PlayerPlace.BOTTOM);
        players.add(currentPlayer);

        final Random rand = new Random();

        for (int i = 1; i < numberOfPlayers; i++) {
            final UserInfo aiInfo = new UserInfo();
            aiInfo.setFacebookId(String.valueOf(i));
            aiInfo.setWarpId(String.valueOf(i));
            aiInfo.setName(randomNames.get(i - 1));
            aiInfo.setMale(rand.nextBoolean());
            aiInfo.setLevel(Level.values()[rand.nextInt(Level.values().length)]);
            aiInfo.setPlayerPhotoURL(null);

            final Player player = new Player(Role.AI,
                    aiInfo,
                    PlayerState.READY,
                    String.valueOf(i));

            final PlayerPlace place = PlayerPlace.values()
                    [(PlayerPlace.values().length / numberOfPlayers) * i];

            player.setPlaceOnTable(place);
            players.add(player);
        }

        return players;
    }

    private static List<String> getRandomNames(final Skin skin,
                                               final int numberOfPlayers) {
        final List<String> namesList = skin.get(Res.properties.STRINGS, Strings.class).names;
        Collections.shuffle(namesList);

        final List<String> names = new ArrayList<String>();
        for (int i = 0; i < numberOfPlayers - 1; i++) {
            names.add(namesList.get(i));
        }

        return names;
    }
}