package com.isbirbilisim.pisti.players;

/**
 * Describes role of some player in particular match.
 */
public enum Role {
    PLAYER,
    OPPONENT,
    AI
}