package com.isbirbilisim.pisti.players;

/**
 * Describes player's state in particular match.
 */
public enum PlayerState {
    PREPARING(0),
    READY_TO_SYNC(2),
    READY(1),
    SYNC_ERROR(-1);

    private final int code;

    PlayerState(final int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static PlayerState fromCodeValue(final int code) {
        switch (code) {
            case 0:
                return PREPARING;
            case 1:
                return READY;
            case -1:
                return SYNC_ERROR;
            case 2:
                return READY_TO_SYNC;
            default:
                throw new IllegalArgumentException("Unknown code");
        }
    }
}