package com.isbirbilisim.pisti.players;

import java.util.List;

public class PlayerUtils {

    /**
     * Returns current player from player list.
     *
     * @param players - list of players.
     * @return - current player.
     */
    public static Player getCurrentPlayer(final List<Player> players) {
        for (final Player player : players) {
            if (player.getRole() == Role.PLAYER) {
                return player;
            }
        }

        return null;
    }

    /**
     * Returns position of current player in list.
     *
     * @param players - list of players.
     * @return - player position.
     */
    public static int getCurrentPlayerPosition(final List<Player> players) {
        for (int pos = 0; pos < players.size(); pos++) {
            if (players.get(pos).getRole() == Role.PLAYER) {
                return pos;
            }
        }

        return 0;
    }

    /**
     * Returns position of given player in list
     *
     * @param players - list of players.
     * @return - player position.
     */
    public static int getPlayerPosition(final List<Player> players,
                                        final Player player) {
        for (int pos = 0; pos < players.size(); pos++) {
            if (players.get(pos).getPlaceOnTable() == player.getPlaceOnTable()) {
                return pos;
            }
        }

        return 0;
    }
}