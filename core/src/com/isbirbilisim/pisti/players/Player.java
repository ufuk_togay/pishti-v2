package com.isbirbilisim.pisti.players;

import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.scores.OnSuperScoresCollectedListener;
import com.isbirbilisim.pisti.scores.ScoreCounter;
import com.isbirbilisim.pisti.scores.Scores;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Contains info about player (current match data). Also has reference to object with
 * player data that is not related to match (account info, overall scores, etc.).
 */
public class Player {

    private final UserInfo userInfo;
    private final ArrayList<CardModel> cardsInHand = new ArrayList<CardModel>();
    private final ArrayList<CardModel> collectedCards = new ArrayList<CardModel>();
    private final Scores scores;
    private final String participantId;

    private Role role;
    private PlayerState state;
    private OnSuperScoresCollectedListener listener;

    private PlayerPlace placeOnTable;

    public Player(final Role role,
                  final UserInfo userInfo,
                  final PlayerState state,
                  final String participantId) {
        this.role = role;
        this.userInfo = userInfo;
        this.state = state;
        this.participantId = participantId;
        this.scores = new Scores();
    }

    /**
     * Sets player's position on table (bottom, left, top, right).
     *
     * @param placeOnTable - player's place.
     */
    public void setPlaceOnTable(final PlayerPlace placeOnTable) {
        this.placeOnTable = placeOnTable;
    }

    /**
     * @return - player's place.
     */
    public PlayerPlace getPlaceOnTable() {
        return placeOnTable;
    }

    public void setOnSuperScoresCollectedListener(
            final OnSuperScoresCollectedListener listener) {
        this.listener = listener;
    }

    /**
     * Calculates scores earned by collecting given cards and adds this scores
     * to player total scores.
     *
     * @param cards - collected cards.
     * @return - scores, earned by collecting given cards.
     */
    public Scores collectCards(final Stack<CardModel> cards) {
        collectedCards.addAll(cards);

        final Scores scores = ScoreCounter.getScore(cards);

        this.scores.sumScores(scores);

        if (listener != null) {
            if (scores.getPistiScore() != 0) {
                listener.onPishtiScoresCollected();
            }
            if (scores.getSuperPistiScore() != 0) {
                listener.onSuperPishtiScoresCollected();
            }
        }

        return scores;
    }

    public void addBonusScore(final int bonusScore) {
        scores.addTotalScores(bonusScore);
    }

    /**
     * Reseting scores after clicking on "Play again" button.
     */
    public void resetGameScores() {
        scores.resetMultiroundScores();

        scores.resetTotalScores();
        scores.resetPistiScores();
        scores.resetSuperPistiScore();
        scores.resetCardsCount();
    }

    /**
     * Reseting scores after round end in multiround game.
     */
    public void resetRoundScores() {
        scores.resetTotalScores();
        scores.resetPistiScores();
        scores.resetSuperPistiScore();
        scores.resetCardsCount();
    }

    public void resetCollectedCards() {
        collectedCards.clear();
    }

    public void resetCards() {
        cardsInHand.clear();
        collectedCards.clear();
    }

    public void updateCardsInHand(final List<CardModel> cards) {
        if (!cardsInHand.isEmpty()) {
            throw new IllegalStateException();
        }

        cardsInHand.addAll(cards);
    }

    public int collectedCardsCount() {
        return collectedCards.size();
    }

    public List<CardModel> getCardsInHand() {
        return cardsInHand;
    }

    public Scores getScores() {
        return scores;
    }

    public boolean outOfCards() {
        return cardsInHand.isEmpty();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public String getParticipantId() {
        return participantId;
    }

    public PlayerState getState() {
        return state;
    }

    public void setState(final PlayerState state) {
        this.state = state;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Player)) {
            throw new IllegalArgumentException("Argument should be instance of Player "
                    + "class");
        }

        return this.participantId
                .equals(((Player) obj).getParticipantId());
    }

    /**
     * Gets auto move card.
     *
     * @param lastCard - last placed card on table
     * @return - auto move card.
     */
    public CardModel getAutoMove(final CardModel lastCard) {
        final CardModel pickedCard = pickSameCard(lastCard);

        cardsInHand.remove(pickedCard);

        return pickedCard;
    }

    private CardModel pickSameCard(final CardModel lastCard) {
        if (lastCard == null) {
            return pickAnyCard();
        }

        for (final CardModel card : cardsInHand) {
            if (card.isSameCard(lastCard)) {
                return card;
            }
        }

        return pickJack();
    }

    private CardModel pickJack() {
        for (final CardModel card : cardsInHand) {
            if (card.isJack()) {
                return card;
            }
        }

        return pickAnyCard();
    }

    private CardModel pickAnyCard() {
        for (final CardModel card : cardsInHand) {
            if (!card.isJack()) {
                return card;
            }
        }

        return cardsInHand.get(0);
    }
}