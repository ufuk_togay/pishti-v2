package com.isbirbilisim.pisti;

/**
 * Info about platform that app running on.
 */
public interface PlatformInfo {

    /**
     * @return true if vibration is supported on current running platform.
     */
    boolean isVibrationSupported();
}