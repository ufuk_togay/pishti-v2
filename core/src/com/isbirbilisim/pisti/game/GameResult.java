package com.isbirbilisim.pisti.game;

/**
 * Defines possible game results.
 */
public enum GameResult {
    WIN1,
    WIN2,
    DRAW,
    LOSE
}