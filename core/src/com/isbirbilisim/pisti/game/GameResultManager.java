package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.players.Player;

import java.util.List;

/**
 * Class that calculates match result for player.
 */
public interface GameResultManager {

    /**
     * Calculates game result for given player.
     *
     * @param players - list of game participants.
     * @return - match result.
     */
    MatchResult calculateMatchResult(final List<Player> players, final Player player);
}