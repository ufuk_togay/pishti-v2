package com.isbirbilisim.pisti.game;

public class GameSettings {

    public PlayersNumber numberOfPlayers = PlayersNumber.TWO_PLAYERS;
    public GameLength gameLength = GameLength.ONE_ROUND;

    public static enum PlayersNumber {
        TWO_PLAYERS(2),
        FOUR_PLAYERS(4);

        private final int numberOfPlayers;

        PlayersNumber(final int numberOfPlayers) {
            this.numberOfPlayers = numberOfPlayers;
        }

        public int getNumberOfPlayers() {
            return numberOfPlayers;
        }
    }

    public static enum GameLength {
        ONE_ROUND(1),
        SHORT_GAME(51),
        LONG_GAME(101);

        private final int gameLength;

        GameLength(final int gameLength) {
            this.gameLength = gameLength;
        }

        public int getGameLength() {
            return gameLength;
        }
    }
}