package com.isbirbilisim.pisti.game;

/**
 * Defines gameplay types.
 */
public enum GameplayType {
    SINGLE,
    MULTIPLAYER
}