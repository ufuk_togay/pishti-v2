package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.DeckFactory;
import com.isbirbilisim.pisti.cards.animations.OnAnimationFinishedListener;
import com.isbirbilisim.pisti.cards.table.CardTable;
import com.isbirbilisim.pisti.cards.table.OnCardSelectedListener;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.match.GameLifecycle;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerUtils;
import com.isbirbilisim.pisti.players.Role;
import com.isbirbilisim.pisti.scores.ScoreCounter;
import com.isbirbilisim.pisti.scores.Scores;
import com.isbirbilisim.pisti.view.CardView;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Processes players moves according to game rules.
 */
public class PistiMatchProcessor extends GameProcessor implements OnCardSelectedListener {

    private final Stack<CardModel> cardsOnTable;

    private Stack<CardModel> deck;

    // Player that collected cards recently.
    private Player lastCardCollector;

    public PistiMatchProcessor(final List<Player> players,
                               final int numberOfCardInHands,
                               final int numberOfInitialCards,
                               final GameLifecycle gameLifecycleDelegate,
                               final GameResultManager gameResultManager) {
        super(numberOfCardInHands,
                numberOfInitialCards,
                players,
                gameLifecycleDelegate,
                gameResultManager);

        cardsOnTable = new Stack<CardModel>();
    }

    @Override
    public void setTable(final CardTable cardTable) {
        super.setTable(cardTable);

        deck = cardTable.getCards();
        initCardTable();
    }

    private void initCardTable() {
        getCardTable().resetTable();
        getCardTable().setOnCardSelectedListener(this);

        getCardTable().addCardsToCardTable(new OnAnimationFinishedListener() {
            @Override
            public void onAnimationFinished() {
                addInitialPileCards();
            }
        });
    }

    private void addInitialPileCards() {
        updateInitialPile();

        getCardTable().addInitialPileCards(
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        updatePlayersCards();
                        giveCards(null);
                    }
                }
        );
    }

    private void giveCards(final Player pendingMovePlayer) {
        getCardTable().giveCards(
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        getGameLifecycleDelegate().onCardsGiven();
                        setPlayerTurnToMove(pendingMovePlayer);
                    }
                }
        );
    }

    private void setPlayerTurnToMove(final Player lastMovePlayer) {
        if (lastMovePlayer != null) {
            setPendingMovePlayer(getNextPlayer(lastMovePlayer));
        }
        final CardView lastCard = getCardTable().getLastPlacedCard();

        getGameLifecycleDelegate().onPlayerTurnToMove(getPendingMovePlayer());

        if (getPendingMovePlayer().getRole() == Role.AI) {
            // If player is AI - make move for him.

            final CardModel card;
            if (lastCard == null) {
                card = getPendingMovePlayer().getAutoMove(null);
            } else {
                card = getPendingMovePlayer().getAutoMove(lastCard.getCard());
            }

            makePlayerMove(getPendingMovePlayer(), card);
        }
    }

    /**
     * Call this when some player makes move.
     *
     * @param player    - player that made a move.
     * @param cardModel - card, moved by player.
     */
    public void makePlayerMove(final Player player,
                               final CardModel cardModel) {
        // performing animation.
        getCardTable().movePlayerCardToCenter(player, cardModel,
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        getGameLifecycleDelegate()
                                .onPlayerMove(player, cardModel);

                        playerMove(player, cardModel);
                    }
                }
        );
    }

    private void playerMove(final Player player, final CardModel cardModel) {
        final boolean shouldCollect = shouldCollectCards(cardModel);

        player.getCardsInHand().remove(cardModel);
        cardsOnTable.push(cardModel);

        if (shouldCollect) {
            final CardTable cardTable = getCardTable();
            final Scores scores = collectCards(player);

            if (scores.getPistiScore() != 0) {
                cardTable.showPistiBonusAnimation(new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        showCollectCardsAnimation(player);
                    }
                });
            } else if (scores.getSuperPistiScore() != 0) {
                cardTable.showSuperPistiBonusAnimation(new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        showCollectCardsAnimation(player);
                    }
                });
            } else {
                showCollectCardsAnimation(player);
            }
        } else {
            checkIfEnd(player);
        }
    }

    private void showCollectCardsAnimation(final Player player) {
        getCardTable().collectCards(player,
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        getGameLifecycleDelegate().onCardsRemovedFromTable();
                        checkIfEnd(player);
                    }
                });
    }

    private Scores collectCards(final Player player) {
        lastCardCollector = player;

        final Scores scores = player.collectCards(cardsOnTable);
        cardsOnTable.clear();

        return scores;
    }

    private void checkIfEnd(final Player player) {
        if (isGameEnd()) {
            processGameEnd(player);
        } else {
            if (isOutOfCards()) {
                if (deck.isEmpty()) {
                    if (cardsOnTable.size() != 0) {
                        collectCards(player);
                        getCardTable().collectCards(player,
                                new OnAnimationFinishedListener() {
                                    @Override
                                    public void onAnimationFinished() {
                                        getCardTable().resetRound();

                                        deck = DeckFactory.createDeck();
                                        getCardTable().setDeck(deck);

                                        addInitialPileCards();
                                    }
                                }
                        );
                    } else {
                        deck = DeckFactory.createDeck();
                        getCardTable().setDeck(deck);

                        addInitialPileCards();
                    }
                } else {
                    updatePlayersCards();
                    giveCards(player);
                }
            } else {
                setPlayerTurnToMove(player);
            }
        }
    }

    private Player getNextPlayer(final Player player) {
        final int pos = getPlayerPosition(player);

        if (pos == getPlayers().size() - 1) {
            return getPlayers().get(0);
        }

        return getPlayers().get(pos + 1);
    }

    private void updateInitialPile() {
        for (int i = 0; i < getNumberOfCardInHands(); i++) {
            cardsOnTable.add(deck.pop());
        }
    }

    private void updatePlayersCards() {
        final List<List<CardModel>> cards = new ArrayList<List<CardModel>>();

        for (int i = 0; i < getPlayers().size(); i++) {
            cards.add(new ArrayList<CardModel>());
        }

        for (int i = 0; i < getNumberOfCardInHands(); i++) {
            for (final List<CardModel> playerCards : cards) {
                playerCards.add(deck.pop());
            }
        }

        for (int i = 0; i < cards.size(); i++) {
            getPlayers().get(i).updateCardsInHand(cards.get(i));
        }
    }

    private boolean isGameEnd() {
        final GameSettings.GameLength gameLength = getGameSettings().gameLength;
        if (gameLength == GameSettings.GameLength.ONE_ROUND) {
            return isOutOfCards() && deck.isEmpty();
        }

        for (final Player player : getPlayers()) {
            if (player.getScores().getMultiroundScores() >= gameLength.getGameLength()
                    && deck.isEmpty()
                    && isOutOfCards()) {
                return true;
            }
        }

        return false;
    }

    private boolean isOutOfCards() {
        for (final Player player : getPlayers()) {
            if (!player.outOfCards()) {
                return false;
            }
        }

        return true;
    }

    private boolean shouldCollectCards(final CardModel newCard) {
        if (cardsOnTable.isEmpty()) {
            return false;
        }

        if (newCard.isJack()) {
            return true;
        }

        final CardModel topCard = cardsOnTable.peek();

        return topCard.isSameCard(newCard);
    }

    private void processGameEnd(final Player player) {
        final Player topPlayer = getPlayerWithMoreScores();
        topPlayer.addBonusScore(ScoreCounter.BONUS_SCORE);

        if (cardsOnTable.isEmpty()) {
            /*
             * Case when last cards on table was collected by actual game rules
             * (by placing same card as top card or by placing Jack).
             */
            getGameLifecycleDelegate().onMatchEnd(getMatchResult(),
                    PlayerUtils.getCurrentPlayer(getPlayers()));
            return;
        }

        /*
         * Case when only one players has card and he places it on table, but
         * according to game rules he can't grab cards.
         */
        collectCards(lastCardCollector);
        getCardTable().collectCards(lastCardCollector,
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        getGameLifecycleDelegate().onMatchEnd(getMatchResult(),
                                PlayerUtils.getCurrentPlayer(getPlayers()));
                    }
                }
        );
    }

    @Override
    public void onCardClicked(final CardView cardView) {
        if (getPendingMovePlayer().getPlaceOnTable() == PlayerPlace.BOTTOM) {
            makePlayerMove(getPendingMovePlayer(), cardView.getCard());
        }
    }
}