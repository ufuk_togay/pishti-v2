package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.match.GameLifecycle;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Pisti card game.
 */
public class PistiCardGame implements CardGame {

    private static final int PISTI_GAME_VARIANT_BASE_VALUE = 100;

    @Override
    public GameProcessor getGameProcessor(final List<Player> players,
                                          final GameLifecycle gameLifecycleDelegate,
                                          final long bidValue) {
        return new PistiMatchProcessor(players,
                getMaxCardsInHands(),
                getInitialCardsNumber(),
                gameLifecycleDelegate,
                new PistiGameResultManager(bidValue));
    }


    @Override
    public int getMaxCardsInHands() {
        return 4;
    }

    @Override
    public int getInitialCardsNumber() {
        return 4;
    }

    private static class PistiGameResultManager implements GameResultManager {

        private final long bidValue;

        private PistiGameResultManager(final long bidValue) {
            this.bidValue = bidValue;
        }

        @Override
        public MatchResult calculateMatchResult(final List<Player> players,
                                                final Player player) {
            final GameResult gameResult;
            final long reward;

            if (players.size() == 2) {
                gameResult = getTwoPlayersGameResult(players, player);
                if (gameResult == GameResult.WIN1) {
                    reward = bidValue * 2;
                } else if (gameResult == GameResult.DRAW) {
                    reward = bidValue;
                } else {
                    reward = 0;
                }
            } else {
                final int playerPosition = getPlayerPosition(players, player);
                final double dReward;

                switch (playerPosition) {
                    case 0:
                    case 1:
                        gameResult = GameResult.LOSE;
                        dReward = 0;
                        break;
                    case 2:
                        gameResult = GameResult.WIN2;
                        dReward = bidValue * 4 * 0.4;
                        break;
                    case 3:
                        gameResult = GameResult.WIN1;
                        dReward = bidValue * 4 * 0.6;
                        break;
                    default:
                        gameResult = GameResult.LOSE;
                        dReward = 0;

                }

                reward = (long) dReward;
            }

            return new MatchResult(gameResult, reward);
        }

        private GameResult getTwoPlayersGameResult(final List<Player> players,
                                                   final Player player) {
            if ((players.get(0).getScores().getTotalScore()
                    == players.get(1).getScores().getTotalScore())
                    && (players.get(0).collectedCardsCount()
                    == players.get(1).collectedCardsCount())) {
                return GameResult.DRAW;
            }

            return getTopPlayer(players).getPlaceOnTable() == player.getPlaceOnTable()
                    ? GameResult.WIN1
                    : GameResult.LOSE;
        }

        private Player getTopPlayer(final List<Player> players) {
            Collections.sort(players, new Comparator<Player>() {
                @Override
                public int compare(final Player o1, final Player o2) {
                    if (o1.getScores().getTotalScore()
                            == o2.getScores().getTotalScore()) {
                        if (o1.collectedCardsCount() < o2.collectedCardsCount()) {
                            return -1;
                        }
                        return 1;
                    }

                    if (o1.getScores().getTotalScore()
                            < o2.getScores().getTotalScore()) {
                        return -1;
                    }

                    return 1;
                }
            });

            return players.get(players.size() - 1);
        }

        private int getPlayerPosition(final List<Player> players,
                                      final Player player) {
            Collections.sort(players, new Comparator<Player>() {
                @Override
                public int compare(final Player o1, final Player o2) {
                    if (o1.getScores().getTotalScore()
                            == o2.getScores().getTotalScore()) {
                        if (o1.collectedCardsCount() < o2.collectedCardsCount()) {
                            return -1;
                        }
                        return 1;
                    }

                    if (o1.getScores().getTotalScore()
                            < o2.getScores().getTotalScore()) {
                        return -1;
                    }

                    return 1;
                }
            });

            return PlayerUtils.getPlayerPosition(players, player);
        }
    }
}