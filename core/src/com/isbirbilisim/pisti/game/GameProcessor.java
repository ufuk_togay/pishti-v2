package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.table.CardTable;
import com.isbirbilisim.pisti.match.GameLifecycle;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Base class for classes that should define card game rules and process
 * player's moves.
 */
public abstract class GameProcessor {

    private final int numberOfCardInHands;
    private final int numberOfInitialCards;
    private final List<Player> players;
    private final GameLifecycle gameLifecycleDelegate;
    private final GameResultManager gameResultManager;

    private Player pendingMovePlayer;

    private CardTable cardTable;
    private GameSettings gameSettings;

    public GameProcessor(final int numberOfCardInHands,
                         final int numberOfInitialCards,
                         final List<Player> players,
                         final GameLifecycle gameLifecycleDelegate,
                         final GameResultManager gameResultManager) {
        this.numberOfCardInHands = numberOfCardInHands;
        this.numberOfInitialCards = numberOfInitialCards;
        this.players = players;
        this.gameLifecycleDelegate = gameLifecycleDelegate;
        this.gameResultManager = gameResultManager;
    }

    public void setFirstTurnPlayer(final Player firstTurnPlayer) {
        this.pendingMovePlayer = firstTurnPlayer;
    }

    public void setTable(final CardTable cardTable) {
        this.cardTable = cardTable;
    }

    public void setGameSettings(final GameSettings gameSettings) {
        this.gameSettings = gameSettings;
    }

    /**
     * Processes player's move.
     *
     * @param player    - player which made move.
     * @param cardModel - placed card.
     */
    public abstract void makePlayerMove(final Player player,
                                        final CardModel cardModel);

    public MatchResult getMatchResult() {
        return gameResultManager.calculateMatchResult(new ArrayList<Player>(players), PlayerUtils.getCurrentPlayer(players));
    }

    public GameResultManager getGameResultManager() {
        return gameResultManager;
    }

    protected int getNumberOfCardInHands() {
        return numberOfCardInHands;
    }

    protected int getNumberOfInitialCards() {
        return numberOfInitialCards;
    }

    protected List<Player> getPlayers() {
        return players;
    }

    protected GameLifecycle getGameLifecycleDelegate() {
        return gameLifecycleDelegate;
    }

    protected Player getPendingMovePlayer() {
        return pendingMovePlayer;
    }

    protected void setPendingMovePlayer(final Player player) {
        this.pendingMovePlayer = player;
    }

    protected CardTable getCardTable() {
        return cardTable;
    }

    protected GameSettings getGameSettings() {
        return gameSettings;
    }

    protected Player getPlayerWithMoreScores() {
        Collections.sort(new ArrayList<Player>(getPlayers()), new Comparator<Player>() {
            @Override
            public int compare(final Player o1, final Player o2) {
                if (o1.getScores().getTotalScore()
                        == o2.getScores().getTotalScore()) {
                    if (o1.collectedCardsCount() < o2.collectedCardsCount()) {
                        return -1;
                    }
                    return 1;
                }

                if (o1.getScores().getTotalScore()
                        < o2.getScores().getTotalScore()) {
                    return -1;
                }

                return 1;
            }
        });

        return getPlayers().get(getPlayers().size() - 1);
    }

    protected int getPlayerPosition(final Player player) {
        for (int i = 0; i < getPlayers().size(); i++) {
            if (getPlayers().get(i).equals(player)) {
                return i;
            }
        }

        throw new IllegalArgumentException("Invalid Player instance");
    }
}