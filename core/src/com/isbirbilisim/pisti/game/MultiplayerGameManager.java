package com.isbirbilisim.pisti.game;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.OnUserInfoSyncListener;
import com.isbirbilisim.pisti.account.SyncResult;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.match.MatchLifecycle;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClientImpl;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.players.PlayerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Game manager for multiplayer game. Used by {@link com.isbirbilisim.pisti.multiplayer.MultiplayerClient}
 * implementation and notifies game about connections, moves of players.
 */
public class MultiplayerGameManager extends GameManager implements OnUserInfoSyncListener {

    private final Queue<Runnable> queue;

    private MatchLifecycle lifecycleDelegate;
    private List<Player> players;
    private GameSettings gameSettings;

    private long bidValue;

    private boolean gameIsStarted;

    public MultiplayerGameManager(final Pisti pisti) {
        super(pisti);

        queue = new ConcurrentLinkedQueue<Runnable>();

        getPisti().getMultiplayerClient().registerGameManagerDelegate(this);
    }

    @Override
    public void initGame(final GameSettings gameSettings,
                         final UserInfo userInfo) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                initGame(gameSettings);
            }
        });
    }

    @Override
    public void startGame() {
        MultiplayerGameManager.this.players = new ArrayList<Player>();
        MultiplayerGameManager.this.lifecycleDelegate = getPisti().getMatchLifecycleDelegate();
        MultiplayerGameManager.this.lifecycleDelegate.onGameCreated(
                MultiplayerGameManager.this,
                CardGame.Type.Pisti,
                GameplayType.MULTIPLAYER,
                gameSettings);

        gameIsStarted = true;

        runQueue();
    }

    private void runQueue() {
        while (!queue.isEmpty()) {
            queue.poll().run();
        }
    }

    private void initGame(final GameSettings gameSettings) {
        MultiplayerGameManager.this.gameSettings = gameSettings;

        gameIsStarted = false;

        if (readyToSetGameScreenListener != null) {
            readyToSetGameScreenListener.onReadyToSetGameScreen(this);
        }
    }

    @Override
    public void connectPlayer(final Player player) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        players.add(player);

                        lifecycleDelegate.onPlayerConnected(player);

                        if (players.size() == gameSettings.numberOfPlayers.getNumberOfPlayers()) {
                            initPlayerPlaces();
                            lifecycleDelegate.onPlayersConnected();
                        }
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    private void initPlayerPlaces() {
        final int playerPosition = PlayerUtils.getCurrentPlayerPosition(players);

        int count = 0;
        for (int i = playerPosition; i < playerPosition + gameSettings.numberOfPlayers.getNumberOfPlayers(); i++) {
            final Player player;
            if (i <= gameSettings.numberOfPlayers.getNumberOfPlayers() - 1) {
                player = players.get(i);
            } else {
                player = players.get(i - gameSettings.numberOfPlayers.getNumberOfPlayers());
            }

            player.setPlaceOnTable(PlayerPlace.values()[(PlayerPlace.values().length / gameSettings.numberOfPlayers.getNumberOfPlayers()) * count++]);
        }
    }

    @Override
    public void setFirstTurnPlayer(final Player player) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        lifecycleDelegate.onFirstTurnPlayerDetermined(player);
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    @Override
    public void setDeck(final Stack<CardModel> initialDeck) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        lifecycleDelegate.onDeckCreated(initialDeck);

                        bidValue = calculateBidValue();
                        removeBidGoldFromPlayer(bidValue);
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    private long calculateBidValue() {
        long lowestBidAmount = players.get(0).getUserInfo().getLevel()
                .getBidAmount();

        for (final Player player : players) {
            if (player.getUserInfo().getLevel().getBidAmount()
                    < lowestBidAmount) {
                lowestBidAmount = player.getUserInfo().getLevel().getBidAmount();
            }
        }

        return lowestBidAmount;
    }

    private void removeBidGoldFromPlayer(final long bidValue) {
        final UserInfo userInfo = PlayerUtils.getCurrentPlayer(players).getUserInfo();
        userInfo.removeBidGold(bidValue);

        getPisti().getMultiplayerClient().syncUserInfo(userInfo, this);
    }

    @Override
    public void setPlayerState(final Player player, final PlayerState state) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (state == PlayerState.READY_TO_SYNC) {
                    ((MultiplayerClientImpl) getPisti().getMultiplayerClient())
                            .processPlayerReadyToSyncMessage(player.getUserInfo().getWarpId());
                    return;
                }

                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        if (!players.contains(player)) {
                            throw new IllegalArgumentException("Unknown player!");
                        }

                        player.setState(state);

                        if (state != PlayerState.READY) {
                            lifecycleDelegate.onPlayerSyncError(player);
                        } else {
                            if (playersReady()) {
                                lifecycleDelegate.onHandshakeCompleted(bidValue);
                            }
                        }
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }

            }
        });
    }

    private boolean playersReady() {
        for (final Player player : players) {
            if (player.getState() == PlayerState.PREPARING
                    || player.getState() == PlayerState.SYNC_ERROR
                    || player.getState() == PlayerState.READY_TO_SYNC) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void opponentMove(final Player player, final CardModel card) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        lifecycleDelegate.onOpponentMove(player, card);
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    @Override
    public void chatMessage(final Player player, final String message) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        lifecycleDelegate.onChatMessageReceived(player, message);
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    @Override
    public void disconnectPlayer(final Player player) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                queue.add(new Runnable() {
                    @Override
                    public void run() {
                        lifecycleDelegate.onPlayerDisconnected(player);
                    }
                });

                if (gameIsStarted) {
                    runQueue();
                }
            }
        });
    }

    @Override
    public void sendMoveNotification(final Player player,
                                     final CardModel card) {
        getPisti().getMultiplayerClient().sentMoveMessage(card);
    }

    @Override
    public void sendChatMessage(final String message) {
        getPisti().getMultiplayerClient().sendChatMessage(message);
    }

    @Override
    public void restartGame() {
        // Not implemented in multiplayer mode.
    }

    @Override
    public void onSyncResult(final UserInfo userInfo,
                             final SyncResult result) {
        // Result of sync of current user.

        final Player currentPlayer = PlayerUtils.getCurrentPlayer(players);
        if (currentPlayer.getUserInfo() != userInfo) {
            throw new IllegalStateException("Should be info on current user");
        }

        final PlayerState currentPlayerState = result == SyncResult.SUCCESS
                ? PlayerState.READY
                : PlayerState.SYNC_ERROR;

        setPlayerState(currentPlayer, currentPlayerState);
        getPisti().getMultiplayerClient().sendPlayerStateMessage(currentPlayerState);
    }
}