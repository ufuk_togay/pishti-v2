package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;

import java.util.Stack;

/**
 * Base class for classes that should initialize game.
 */
public abstract class GameManager {
    public static final int SMALL_PARTY = 2;
    public static final int LARGE_PARTY = 4;
    private static final int[] participantCombinations = new int[]
            {SMALL_PARTY, LARGE_PARTY};

    private final Pisti pisti;

    protected OnReadyToSetGameScreenListener readyToSetGameScreenListener;

    public GameManager(final Pisti pisti) {
        this.pisti = pisti;
    }

    /**
     * Initializes game.
     *
     * @param gameSettings - game settings.
     * @param userInfo     - current player info.
     */
    public abstract void initGame(final GameSettings gameSettings,
                                  final UserInfo userInfo);

    /**
     * Starts game.
     */
    public abstract void startGame();

    /**
     * Connects player to game.
     *
     * @param player - player, that should be connected to game.
     */
    public abstract void connectPlayer(final Player player);

    /**
     * Sets player that should move first.
     *
     * @param player - player, that should move first.
     */
    public abstract void setFirstTurnPlayer(final Player player);

    /**
     * Sets initial deck. Deck should be created by client of player that
     * will move first.
     *
     * @param initialDeck - initial deck.
     */
    public abstract void setDeck(final Stack<CardModel> initialDeck);

    /**
     * Sets player state.
     *
     * @param player - player.
     * @param state  - received player's state.
     */
    public abstract void setPlayerState(final Player player,
                                        final PlayerState state);

    /**
     * Notifies game about opponent move.
     *
     * @param player - player, that made move.
     * @param card   - card, moved by player.
     */
    public abstract void opponentMove(final Player player,
                                      final CardModel card);

    /**
     * Notifies about incoming chat message.
     *
     * @param player  - message sender.
     * @param message - incoming message.
     */
    public abstract void chatMessage(final Player player, final String message);

    /**
     * Notifies game about game abandoning by some player.
     *
     * @param player - player, which left game.
     */
    public abstract void disconnectPlayer(final Player player);

    /**
     * Sends notification to game participants about player's move. Should be
     * used only by GameScreen.
     *
     * @param player - player that made move.
     * @param card   - moved card.
     */
    public abstract void sendMoveNotification(final Player player,
                                              final CardModel card);

    /**
     * Sends chat message to other players.
     *
     * @param message - message to send.
     */
    public abstract void sendChatMessage(final String message);

    /**
     * Restarts game.
     */
    public abstract void restartGame();

    /**
     * @return - {@link com.isbirbilisim.pisti.Pisti} instance.
     */
    public Pisti getPisti() {
        return pisti;
    }

    /**
     * @return - possible numbers of game participants.
     */
    public static int[] getParticipantCombinations() {
        return participantCombinations;
    }

    /**
     * Sets callback that will be triggered when its time to set game screen.
     */
    public void setOnReadyToSetGameScreenListener(
            final OnReadyToSetGameScreenListener listener) {
        this.readyToSetGameScreenListener = listener;
    }

    public interface OnReadyToSetGameScreenListener {

        /**
         * Called when its time to set game screen.
         *
         * @param gameManager - game manager instance.
         */
        void onReadyToSetGameScreen(final GameManager gameManager);
    }
}