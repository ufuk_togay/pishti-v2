package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.DeckFactory;
import com.isbirbilisim.pisti.match.FirstTurnChooser;
import com.isbirbilisim.pisti.match.MatchLifecycle;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.players.PlayersFactory;

import java.util.List;
import java.util.Stack;

/**
 * Game manager for single game.
 */
public class SingleGameManager extends GameManager {

    public SingleGameManager(final Pisti pisti) {
        super(pisti);
    }

    private GameSettings gameSettings;
    private UserInfo userInfo;

    private List<Player> players;

    @Override
    public void initGame(final GameSettings gameSettings,
                         final UserInfo userInfo) {
        this.gameSettings = gameSettings;
        this.userInfo = userInfo;

        if (readyToSetGameScreenListener != null) {
            readyToSetGameScreenListener.onReadyToSetGameScreen(this);
        }
    }

    @Override
    public void startGame() {
        final MatchLifecycle lifecycleDelegate = getPisti()
                .getMatchLifecycleDelegate();

        players = PlayersFactory.createSingleGamePlayers(userInfo,
                gameSettings.numberOfPlayers.getNumberOfPlayers(),
                getPisti().getAssets().getDialogSkin());

        connectPlayers(lifecycleDelegate, players);

        lifecycleDelegate.onGameCreated(this,
                CardGame.Type.Pisti,
                GameplayType.SINGLE,
                gameSettings);
        lifecycleDelegate.onDeckCreated(DeckFactory.createDeck());
        lifecycleDelegate.onFirstTurnPlayerDetermined(FirstTurnChooser
                .getSingleGameFirstTurnPlayer(players));
        lifecycleDelegate.onHandshakeCompleted(0);
    }

    @Override
    public void connectPlayer(final Player player) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void setFirstTurnPlayer(final Player player) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void setDeck(final Stack<CardModel> initialDeck) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void setPlayerState(final Player player, final PlayerState state) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void opponentMove(final Player player, final CardModel card) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void chatMessage(final Player player, final String message) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void disconnectPlayer(final Player player) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void sendMoveNotification(final Player player,
                                     final CardModel card) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void sendChatMessage(final String message) {
        // Doing nothing, should be implemented only in multiplayer
        // mode.
    }

    @Override
    public void restartGame() {
        final MatchLifecycle lifecycleDelegate = getPisti()
                .getMatchLifecycleDelegate();

        lifecycleDelegate.onDeckCreated(DeckFactory.createDeck());
        lifecycleDelegate.onFirstTurnPlayerDetermined(FirstTurnChooser
                .getSingleGameFirstTurnPlayer(players));
        lifecycleDelegate.onHandshakeCompleted(0);
    }

    private void connectPlayers(final MatchLifecycle matchLifecycleDelegate,
                                final List<Player> players) {
        for (final Player player : players) {
            matchLifecycleDelegate.onPlayerConnected(player);
        }

        matchLifecycleDelegate.onPlayersConnected();
    }
}