package com.isbirbilisim.pisti.game;

import com.isbirbilisim.pisti.match.GameLifecycle;
import com.isbirbilisim.pisti.players.Player;

import java.util.List;

/**
 * Class that describes kind of card game, used in application.
 */
public interface CardGame {

    /**
     * Return class that ca process players' moves.
     *
     * @return - GameProcessor implementation.
     */
    GameProcessor getGameProcessor(final List<Player> players,
                                   final GameLifecycle gameLifecycleDelegate,
                                   final long bidValue);

    /**
     * @return - number of cards that player may have.
     */
    int getMaxCardsInHands();

    /**
     * @return - number of cards in initial deck.
     */
    int getInitialCardsNumber();

    /**
     * Defines types of game.
     */
    public enum Type {
        Pisti
    }
}