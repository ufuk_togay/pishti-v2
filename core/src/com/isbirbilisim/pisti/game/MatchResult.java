package com.isbirbilisim.pisti.game;

/**
 * Class that contains match result.
 */
public class MatchResult {

    private final GameResult gameResult;
    private final long reward;

    public MatchResult(final GameResult gameResult, final long reward) {
        this.gameResult = gameResult;
        this.reward = reward;
    }

    public GameResult getGameResult() {
        return gameResult;
    }

    public long getReward() {
        return reward;
    }
}