package com.isbirbilisim.pisti.game;

/**
 * Factory that creates CardGame implementations.
 */
public class CardGameFactory {

    /**
     * Returns CardGame implementation by game type.
     *
     * @param type - game type.
     * @return - CardGame implementation.
     */
    public static CardGame getCardGame(final CardGame.Type type) {
        switch (type) {
            case Pisti:
                return new PistiCardGame();
            default:
                return null;
        }
    }
}