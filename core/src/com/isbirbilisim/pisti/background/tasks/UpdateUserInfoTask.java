package com.isbirbilisim.pisti.background.tasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.isbirbilisim.pisti.background.Header;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;

import java.util.List;

/**
 * Task for updating user info.
 */
public class UpdateUserInfoTask extends NetworkTask {

    private final List<Header> headers;
    private final String content;

    public UpdateUserInfoTask(final OnNetworkTaskExecutedListener listener,
                              final List<Header> headers,
                              final String content) {
        super(listener);
        this.headers = headers;
        this.content = content;
    }

    @Override
    public void execute() {
        final Net.HttpRequest httpPost = new Net.HttpRequest(Net.HttpMethods.POST);
        httpPost.setUrl(BASE_URL + "service/playerData/");
        for (final Header header : headers) {
            httpPost.setHeader(header.getName(), header.getValue());
        }
        httpPost.setContent(content);

        Gdx.net.sendHttpRequest(httpPost, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(final Net.HttpResponse httpResponse) {
                notifyExecutionSuccess(httpResponse.getResultAsString());
            }

            /**
             * NOT HTTP ERROR!
             */
            @Override
            public void failed(final Throwable t) {
                notifyExecutionFailure();
            }

            @Override
            public void cancelled() {

            }
        });
    }
}