package com.isbirbilisim.pisti.background.tasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.isbirbilisim.pisti.background.Header;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;

import java.util.List;

/**
 * Task for getting user info from remote server.
 */
public class GetUserInfoTask extends NetworkTask {

    private final String userId;
    private final boolean isMale;
    private final List<Header> headers;

    public GetUserInfoTask(final String userId,
                           final boolean isMale,
                           final List<Header> headers,
                           final OnNetworkTaskExecutedListener listener) {
        super(listener);
        this.userId = userId;
        this.isMale = isMale;
        this.headers = headers;
    }

    @Override
    public void execute() {
        final Net.HttpRequest httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(BASE_URL + "service/playerData/" + userId + "?isMale=" + String.valueOf(isMale));
        for (final Header header : headers) {
            httpGet.setHeader(header.getName(), header.getValue());
        }

        Gdx.net.sendHttpRequest(httpGet, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(final Net.HttpResponse httpResponse) {
                notifyExecutionSuccess(httpResponse.getResultAsString());
            }

            /**
             * NOT HTTP ERROR!
             */
            @Override
            public void failed(final Throwable t) {
                notifyExecutionFailure();
            }

            @Override
            public void cancelled() {

            }
        });
    }
}