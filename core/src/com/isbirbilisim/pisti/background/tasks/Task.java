package com.isbirbilisim.pisti.background.tasks;

/**
 * Interface for classes which can be executed in background thread.
 */
public interface Task {

    /**
     * Executes task in background thread.
     */
    void execute();
}