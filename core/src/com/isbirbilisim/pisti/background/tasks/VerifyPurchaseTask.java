package com.isbirbilisim.pisti.background.tasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;

/**
 * Task for verifying user purchase on server.
 */
public class VerifyPurchaseTask extends NetworkTask {

    private final String itemPurchase;
    private final String userId;

    public VerifyPurchaseTask(final String purchase,
                              final String userId,
                              final OnNetworkTaskExecutedListener listener) {
        super(listener);
        this.itemPurchase = purchase;
        this.userId = userId;
    }

    @Override
    public void execute() {
        final Net.HttpRequest httpPost = new Net.HttpRequest(Net.HttpMethods.POST);
        httpPost.setUrl(BASE_URL + "service/verifyPurchase" + userId);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setContent(itemPurchase);

        Gdx.net.sendHttpRequest(httpPost, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(final Net.HttpResponse httpResponse) {
                notifyExecutionSuccess(httpResponse.getResultAsString());
            }

            /**
             * NOT HTTP ERROR!
             */
            @Override
            public void failed(final Throwable t) {
                notifyExecutionFailure();
            }

            @Override
            public void cancelled() {

            }
        });
    }
}