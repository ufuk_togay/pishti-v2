package com.isbirbilisim.pisti.background.tasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.net.URL;

/**
 * Task for setting user photo asynchronously.
 */
public class SetUserPhotoTask implements Task, Net.HttpResponseListener {

    private final Image image;
    private final URL url;
    private final Drawable failureDrawable;

    public SetUserPhotoTask(final Image image,
                            final URL url,
                            final Drawable failureDrawable) {
        this.image = image;
        this.url = url;
        this.failureDrawable = failureDrawable;
    }

    @Override
    public void execute() {
        final Net.HttpRequest httpRequest = new Net.HttpRequest(Net.HttpMethods.GET);
        httpRequest.setUrl(url.toString());
        Gdx.net.sendHttpRequest(httpRequest, this);
    }

    @Override
    public void handleHttpResponse(final Net.HttpResponse httpResponse) {
        final byte[] rawImageBytes = httpResponse.getResult();
        Gdx.app.postRunnable(new Runnable() {
            public void run() {
                image.setVisible(true);
                try {
                    final Pixmap pixmap = new Pixmap(rawImageBytes, 0, rawImageBytes.length);
                    final Texture texture = new Texture(pixmap);
                    image.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
                } catch (final GdxRuntimeException exception) {
                    image.setDrawable(failureDrawable);
                }
            }
        });
    }

    @Override
    public void failed(final Throwable t) {
        image.setDrawable(failureDrawable);
    }

    @Override
    public void cancelled() {
        image.setDrawable(failureDrawable);
    }
}