package com.isbirbilisim.pisti.background.tasks;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;

/**
 * Base class for network tasks.
 */
public abstract class NetworkTask implements Task {

    /**
     * Server url.
     */
    public static final String BASE_URL = "https://bumenu-gc.appspot.com/";

    private final OnNetworkTaskExecutedListener listener;

    public NetworkTask(final OnNetworkTaskExecutedListener listener) {
        this.listener = listener;
    }

    protected OnNetworkTaskExecutedListener getListener() {
        return listener;
    }

    protected void notifyExecutionFailure() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                listener.onTaskFailed();
            }
        });
    }

    protected void notifyExecutionSuccess(final String response) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                listener.onTaskCompleted(response);
            }
        });
    }
}