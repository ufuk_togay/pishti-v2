package com.isbirbilisim.pisti.background;

import com.isbirbilisim.pisti.background.tasks.Task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class that manages background tasks.
 */
public class BackgroundTasksManager {

    private static BackgroundTasksManager instance;

    private final ExecutorService executorService;

    private BackgroundTasksManager() {
        executorService = Executors.newSingleThreadExecutor();
    }

    public static BackgroundTasksManager getInstance() {
        synchronized (BackgroundTasksManager.class) {
            if (instance == null) {
                instance = new BackgroundTasksManager();
            }
        }
        return instance;
    }

    /**
     * Executes task in background thread.
     *
     * @param task - task that should be executed.
     */
    public void postTask(final Task task) {
        executorService.submit(new TaskExecutor(task));
    }

    private static class TaskExecutor implements Runnable {

        private final Task task;

        private TaskExecutor(final Task task) {
            this.task = task;
        }

        @Override
        public void run() {
            task.execute();
        }
    }
}