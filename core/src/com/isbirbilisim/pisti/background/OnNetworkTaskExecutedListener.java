package com.isbirbilisim.pisti.background;

/**
 * Callback for receiving notification about task execution result.
 */
public interface OnNetworkTaskExecutedListener {

    /**
     * Called when task is executed.
     */
    void onTaskCompleted(final String response);

    /**
     * Called when task execution failed.
     */
    void onTaskFailed();
}