package com.isbirbilisim.pisti.analytics;

import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;

/**
 * Interface fo Analytics manager.
 */
public interface AnalyticsManager {

    void sendMenuScreenShownEvent();

    void sendGameScreenShownEvent();

    void sendSingleGameCompletedEvent();

    void sendMultiplayerGameCompletedEvent();
}