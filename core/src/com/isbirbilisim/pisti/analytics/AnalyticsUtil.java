package com.isbirbilisim.pisti.analytics;

import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;

/**
 * Created by picarex on 09/11/14.
 */
public class AnalyticsUtil {
    private final AnalyticsManager analyticsManager;

    public AnalyticsUtil(final AnalyticsManager analyticsManager) {
        this.analyticsManager = analyticsManager;
    }

    public void sendMenuScreenShownEvent() {
        analyticsManager.sendMenuScreenShownEvent();
    }

    public void sendGameScreenShownEvent() {
        analyticsManager.sendGameScreenShownEvent();
    }

    public void sendSingleGameCompletedEvent() {
        analyticsManager.sendSingleGameCompletedEvent();
    }

    public void sendMultiplayerGameCompletedEvent() {
        analyticsManager.sendMultiplayerGameCompletedEvent();
    }
}