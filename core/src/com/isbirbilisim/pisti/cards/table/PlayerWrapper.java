package com.isbirbilisim.pisti.cards.table;

import com.isbirbilisim.pisti.view.CardView;
import com.isbirbilisim.pisti.players.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains info about player and cards.
 */
public class PlayerWrapper {
    private final Player player;
    private final List<CardView> cards;

    public PlayerWrapper(final Player player) {
        this.player = player;
        this.cards = new ArrayList<CardView>();
    }

    public Player getPlayer() {
        return player;
    }

    public List<CardView> getCardsViews() {
        return cards;
    }
}