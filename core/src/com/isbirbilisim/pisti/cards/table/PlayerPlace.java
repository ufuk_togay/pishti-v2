package com.isbirbilisim.pisti.cards.table;

/**
 * Possible places of players.
 */
public enum PlayerPlace {
    BOTTOM,
    LEFT,
    TOP,
    RIGHT
}