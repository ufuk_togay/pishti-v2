package com.isbirbilisim.pisti.cards.table;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.CardsSettings;
import com.isbirbilisim.pisti.cards.animations.CardAnimationManager;
import com.isbirbilisim.pisti.cards.animations.CardGroupAnimation;
import com.isbirbilisim.pisti.cards.animations.OnAnimationFinishedListener;
import com.isbirbilisim.pisti.cards.animations.SingleCardAnimation;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.Role;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.CardView;
import com.isbirbilisim.pisti.view.CardViewFactory;
import com.isbirbilisim.pisti.view.CardViewSettings;
import com.isbirbilisim.pisti.view.GameLogoView;
import com.isbirbilisim.pisti.view.PlayerWidget;
import com.isbirbilisim.pisti.view.PlayerWidgetGroup;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Contains all CardViews that currently are on table and performs animations.
 */
public class CardTable extends WidgetGroup {
    public static final int MOTION_X_LIMIT = 200;
    public static final int MOTION_Y_LIMIT = 200;
    private final MultimediaManager multimediaManager;
    private final List<CardView> cardsOnCenter;
    private final List<CardView> initialPileCards;
    private final CardAnimationManager cardAnimationManager;
    private final CardsSettings cardsSettings;
    private final List<PlayerWrapper> playerWrappers;
    private final PlayerWidgetGroup playerWidgetGroup;
    private final GameLogoView gameLogoView;
    private final Skin skin;
    private final boolean multiround;
    private final boolean networkAvailable;

    private final int maxCardsInHands;
    private final int initialCardsNumber;

    private Stack<CardView> deck;

    private OnCardSelectedListener listener;
    private EventListener cardClickListener;

    public CardTable(final Skin skin,
                     final ScreenUtils screenUtils,
                     final MultimediaManager multimediaManager,
                     final GameLogoView gameLogoView,
                     final List<Player> players,
                     final int maxCardsInHands,
                     final int initialCardsNumber,
                     final boolean multiround,
                     final float tableWidth,
                     final float tableHeight,
                     final boolean showingAd,
                     final boolean networkAvailable) {
        if (players.size() != 2 && players.size() != 4) {
            throw new IllegalArgumentException("Only 2 or 4 players allowed");
        }
        this.multimediaManager = multimediaManager;
        this.maxCardsInHands = maxCardsInHands;
        this.initialCardsNumber = initialCardsNumber;
        this.skin = skin;
        this.gameLogoView = gameLogoView;
        this.multiround = multiround;
        this.networkAvailable = networkAvailable;
        setSize(tableWidth, tableHeight);

        playerWrappers = initPlayers(players);

        initialPileCards = new ArrayList<CardView>();
        cardsOnCenter = new ArrayList<CardView>();

        cardAnimationManager = new CardAnimationManager(tableWidth, tableHeight, multimediaManager);
        cardsSettings = new CardsSettings(
                screenUtils,
                tableWidth,
                tableHeight,
                maxCardsInHands,
                showingAd);

        playerWidgetGroup = new PlayerWidgetGroup();
        initPlayerViews(screenUtils, skin);
    }

    /**
     * Sets card deck.
     *
     * @param initialDeck - deck of cards.
     */
    public void setDeck(final Stack<CardModel> initialDeck) {
        final CardViewFactory cardViewFactory = new CardViewFactory(skin);
        this.deck = cardViewFactory.createCardViewList(initialDeck);

        initCardCallbacks();
    }

    public List<PlayerWidget> getPlayerWidgets() {
        return playerWidgetGroup.getPlayerWidgets();
    }

    public PlayerWidget getCurrentPlayerWidget() {
        for (final PlayerWidget widget : playerWidgetGroup.getPlayerWidgets()) {
            if (widget.getPlayer().getRole() == Role.PLAYER) {
                return widget;
            }
        }

        return null;
    }

    public PlayerWidget getPlayerWidget(final Player player) {
        for (final PlayerWidget widget : playerWidgetGroup.getPlayerWidgets()) {
            if (widget.getPlayer().equals(player)) {
                return widget;
            }
        }

        return null;
    }

    private List<PlayerWrapper> initPlayers(final List<Player> players) {
        final List<PlayerWrapper> wrappers = new ArrayList<PlayerWrapper>();

        for (final Player player : players) {
            wrappers.add(new PlayerWrapper(player));
        }

        return wrappers;
    }

    private void initPlayerViews(final ScreenUtils screenUtils,
                                 final Skin skin) {
        for (final PlayerWrapper wrapper : playerWrappers) {
            final PlayerWidget.PlayerWidgetSettings settings =
                    getPlayerWidgetSettings(
                            wrapper.getPlayer().getPlaceOnTable());

            final Image turnBackground = createPlayerTurnBackground(settings);
            addActor(turnBackground);

            final PlayerWidget widget = new PlayerWidget(
                    screenUtils,
                    settings.getWidth(),
                    settings.getHeight(),
                    wrapper.getPlayer(),
                    skin,
                    multiround,
                    networkAvailable,
                    turnBackground);

            playerWidgetGroup.addWidget(widget);
            addActor(widget);
            widget.setPosition(settings.getX(), settings.getY());
        }
    }

    private Image createPlayerTurnBackground(final PlayerWidget.PlayerWidgetSettings settings) {
        final Image turnBackground = new Image(
                skin.newDrawable(Res.textures.PHOTO_TURN,
                        settings.getColor()));
        turnBackground.addAction(Actions.alpha(0));
        final float turnBackgroundWidth = settings.getWidth() * 1.8F;
        final float turnBackgroundHeight = settings.getHeight() * 1.8F;

        turnBackground.setSize(turnBackgroundWidth, turnBackgroundHeight);
        turnBackground.setPosition(settings.getX()
                        + (settings.getWidth() / 2 * 1.25F)
                        - (turnBackgroundWidth / 2),
                settings.getY()
                        + (settings.getHeight() / 2)
                        - (turnBackgroundHeight / 2));

        return turnBackground;
    }

    private PlayerWidget.PlayerWidgetSettings getPlayerWidgetSettings(
            final PlayerPlace playerPlace) {
        switch (playerPlace) {
            case LEFT:
                return cardsSettings.getLeftPlayerSettings();
            case RIGHT:
                return cardsSettings.getRightPlayerSettings();
            case TOP:
                return cardsSettings.getTopPlayerSettings();
            case BOTTOM:
                return cardsSettings.getBottomPlayerSettings();
            default:
                return null;
        }
    }

    /**
     * Shows deck shuffle animation.
     *
     * @param listener - animation end listener.
     */
    public void addCardsToCardTable(final OnAnimationFinishedListener listener) {
        final List<CardViewSettings> settings = cardsSettings.getPileSettings();
        for (int i = 0; i < deck.size(); i++) {
            final CardView card = deck.get(i);
            final CardViewSettings cardSettings = settings.get(i);

            addActor(card);
            card.setPosition(-cardSettings.width * 1.5F, cardSettings.y);
            card.setSize(cardSettings.width, cardSettings.height);
        }

        final CardGroupAnimation animation = cardAnimationManager.obtainShuffleDeckAnimation(
                cardsSettings,
                deck,
                listener
        );

        animation.perform();
        multimediaManager.playDeckShuffle();
    }

    private void initCardCallbacks() {
        cardClickListener = getPlayerCardListener();
    }

    private PlayerWrapper getPlayerWrapper(final Player player) {
        for (final PlayerWrapper playerWrapper : playerWrappers) {
            if (playerWrapper.getPlayer().equals(player)) {
                return playerWrapper;
            }
        }

        throw new IllegalArgumentException("This player does not participates in match.");
    }

    private void disableMotionEvents() {
        setTouchable(Touchable.disabled);

        for (final Actor actor : getChildren()) {
            actor.setTouchable(Touchable.disabled);
        }
    }

    private void enableMotionEvent() {
        setTouchable(Touchable.enabled);

        for (final Actor actor : getChildren()) {
            actor.setTouchable(Touchable.enabled);
        }
    }

    /**
     * Resets table state. Should be called before new game begins (or after
     * clicking on "Play again" button.
     */
    public void resetTable() {
        for (final PlayerWrapper playerWrapper : playerWrappers) {
            playerWrapper.getCardsViews().clear();
        }

        playerWidgetGroup.resetGameEnd();

        cardAnimationManager.reset();

        initialPileCards.clear();
        cardsOnCenter.clear();
        cardsSettings.reset();
    }

    /**
     * Resets players after round end. Should be called before starting new round
     * in multiround game.
     */
    public void resetRound() {
        playerWidgetGroup.resetRoundEnd();
    }

    /**
     * Sets {@link OnCardSelectedListener} listener.
     *
     * @param listener - card click listener.
     */
    public void setOnCardSelectedListener(
            final OnCardSelectedListener listener) {
        this.listener = listener;
    }

    /**
     * Returns list of {@link CardModel} that are on table.
     *
     * @return - list of cards.
     */
    public Stack<CardModel> getCards() {
        final Stack<CardModel> cardModels = new Stack<CardModel>();

        for (final CardView cardView : deck) {
            cardModels.add(cardView.getCard());
        }

        return cardModels;
    }

    /**
     * @return - last placed card on center. Returns null if there are no cards
     * on center.
     */
    public CardView getLastPlacedCard() {
        if (!cardsOnCenter.isEmpty()) {
            return cardsOnCenter.get(cardsOnCenter.size() - 1);
        }

        if (initialPileCards.isEmpty()) {
            return null;
        }

        return initialPileCards.get(initialPileCards.size() - 1);
    }

    /**
     * Shows that player should move and starts timer.
     */
    public void showPlayerTurnTimer(final Player player, final Timer.Task task) {
        playerWidgetGroup.showPlayerTurnTimer(player, task);
    }

    /**
     * Shows that player should move.
     */
    public void showPlayerTurnToMove(final Player player) {
        playerWidgetGroup.showPlayerTurnToMove(player);
    }

    /**
     * Hides turn to move image.
     */
    public void hidePlayerTurnToMove(final Player player) {
        playerWidgetGroup.hidePlayerTurnToMove(player);
    }

    /**
     * Stops current countdown.
     */
    public void stopCountDown() {
        playerWidgetGroup.cancelCountDown();
    }

    /**
     * Refreshes current players' scores.
     *
     * @param player - player that made move.
     */
    public void refreshScores(final Player player) {
        playerWidgetGroup.refreshScores(player);
    }

    /**
     * Performs initial pile animation.
     *
     * @param listener - animation end listener.
     */
    public void addInitialPileCards(final OnAnimationFinishedListener listener) {
        disableMotionEvents();

        for (int i = 0; i < initialCardsNumber; i++) {
            initialPileCards.add(deck.pop());
        }

        final CardGroupAnimation animation = cardAnimationManager
                .obtainInitialPileAnimation(initialPileCards,
                        cardsSettings.getInitialPileCardSettings(),
                        new OnAnimationFinishedListener() {
                            @Override
                            public void onAnimationFinished() {
                                enableMotionEvent();
                                listener.onAnimationFinished();
                            }
                        }
                );
        animation.perform();
    }

    /**
     * Gives cards to all players.
     *
     * @param listener - animation end listener.
     */
    public void giveCards(final OnAnimationFinishedListener listener) {
        disableMotionEvents();

        List<List<CardView>> cardsToAdd = new ArrayList<List<CardView>>();

        for (int i = 0; i < playerWrappers.size(); i++) {
            cardsToAdd.add(new LinkedList<CardView>());
        }

        for (int i = 0; i < maxCardsInHands; i++) {
            for (final List<CardView> playerCards : cardsToAdd) {
                playerCards.add(deck.pop());
            }
        }

        addCardsToPlayerWrappers(cardsToAdd);

        final CardGroupAnimation animation = cardAnimationManager
                .obtainGiveCardsAnimation(
                        maxCardsInHands,
                        playerWrappers,
                        cardsSettings,
                        new OnAnimationFinishedListener() {
                            @Override
                            public void onAnimationFinished() {
                                enableMotionEvent();
                                listener.onAnimationFinished();
                            }
                        }
                );
        animation.perform();
    }

    private void addCardsToPlayerWrappers(final List<List<CardView>> cardsToAdd) {
        for (int i = 0; i < playerWrappers.size(); i++) {
            if (cardsToAdd.get(i).size() != maxCardsInHands) {
                throw new IllegalArgumentException("Invalid number of cards.");
            }

            final PlayerWrapper playerWrapper = playerWrappers.get(i);

            if (!playerWrapper.getCardsViews().isEmpty()) {
                throw new IllegalStateException("Player has cards in hands.");
            }

            playerWrapper.getCardsViews().addAll(cardsToAdd.get(i));

            if (playerWrapper.getPlayer().getPlaceOnTable()
                    == PlayerPlace.BOTTOM) {
                // Adds cards click listener to player's cards.
                updatePlayerCardListener(playerWrapper, cardClickListener);
            }
        }
    }

    /**
     * Animates player's card to center of table.
     *
     * @param player   - player.
     * @param card     - card to be moved to center.
     * @param listener - animation end listener.
     */
    public void movePlayerCardToCenter(final Player player,
                                       final CardModel card,
                                       final OnAnimationFinishedListener listener) {
        for (final CardView cardView : getPlayerWrapper(player).getCardsViews()) {
            if (cardView.getCard().equals(card)) {
                moveCard(getPlayerWrapper(player).getCardsViews(), cardView);
                performMoveCardAnimation(player, cardView, listener);

                if (player.getPlaceOnTable() == PlayerPlace.BOTTOM) {
                    // Removes click callback from card.
                    removeCardListener(cardView);
                }
                return;
            }
        }

        throw new IllegalArgumentException("Requested player does not have given card");
    }

    /**
     * Shows Pisti animation.
     *
     * @param listener - animation end listener.
     */
    public void showPistiBonusAnimation(final OnAnimationFinishedListener listener) {
        cardAnimationManager.obtainMoveCardsAnimation(cardsOnCenter,
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        gameLogoView.showPistiAnimation(listener);
                    }
                }).perform();
    }

    /**
     * Shows Super Pisti animation.
     *
     * @param listener - animation end listener.
     */
    public void showSuperPistiBonusAnimation(final OnAnimationFinishedListener listener) {
        cardAnimationManager.obtainMoveCardsAnimation(cardsOnCenter,
                new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        gameLogoView.showSuperPistiAnimation(listener);
                    }
                }).perform();
    }

    /**
     * Performs animation of removing cards from table.
     *
     * @param player   - player.
     * @param listener - animation end listener.
     */
    public void collectCards(final Player player,
                             final OnAnimationFinishedListener listener) {
        if (!initialPileCards.isEmpty() && player.getRole() == Role.PLAYER) {
            /*
             * Showing cards in initial pile before collecting cards.
             */
            final CardGroupAnimation animation = cardAnimationManager
                    .obtainsShowInitialCardsAnimation(initialPileCards,
                            cardsOnCenter,
                            cardsSettings,
                            new OnAnimationFinishedListener() {
                                @Override
                                public void onAnimationFinished() {
                                    performCollectCardsAnimation(player, listener);
                                }
                            });
            animation.perform();
        } else {
            performCollectCardsAnimation(player, listener);
        }
    }

    private void performCollectCardsAnimation(
            final Player player,
            final OnAnimationFinishedListener listener) {
        final List<CardView> collectedCards = new ArrayList<CardView>();
        collectedCards.addAll(initialPileCards);
        collectedCards.addAll(cardsOnCenter);

        final CardGroupAnimation animation = cardAnimationManager
                .obtainCollectCardAnimation(collectedCards,
                        player.getPlaceOnTable(),
                        cardsSettings,
                        new OnAnimationFinishedListener() {
                            @Override
                            public void onAnimationFinished() {
                                removeCardsFromCenter();
                                listener.onAnimationFinished();
                            }
                        }
                );
        animation.perform();
    }

    private void performMoveCardAnimation(final Player player,
                                          final CardView card,
                                          final OnAnimationFinishedListener listener) {
        disableMotionEvents();

        final CardGroupAnimation animation = cardAnimationManager
                .obtainPlayerMoveAnimation(
                        player.getPlaceOnTable(),
                        card,
                        new OnAnimationFinishedListener() {
                            @Override
                            public void onAnimationFinished() {
                                enableMotionEvent();
                                listener.onAnimationFinished();
                            }
                        },
                        cardsSettings.getInitialPileCardSettings().get(0),
                        cardsSettings.getCenterCardsSettings()
                );

        animation.perform();
    }

    private void moveCard(final List<CardView> playersCards,
                          final CardView cardToMove) {
        cardsOnCenter.add(cardToMove);
        playersCards.remove(cardToMove);
    }

    private void removeCardsFromCenter() {
        for (final CardView cardView : cardsOnCenter) {
            removeActor(cardView);
        }

        for (final CardView cardView : initialPileCards) {
            removeActor(cardView);
        }

        cardsOnCenter.clear();
        initialPileCards.clear();
    }

    private EventListener getPlayerCardListener() {
        return new ClickListener() {

            float clickedX = 0;
            float clickedY = 0;
            float startCardPositionX = 0;
            float startCardPositionY = 0;
            float prevPosX = 0;
            float prevPosY = 0;
            float pilePositionX = 0;
            float pilePositionY = 0;
            float mx = 0;
            float my = 0;
            boolean isCanceled = false;

            @Override
            public boolean touchDown(final InputEvent event,
                                     final float x,
                                     final float y,
                                     final int pointer,
                                     final int button) {
                final CardView card = (CardView) event.getListenerActor();
                final SingleCardAnimation animation = cardAnimationManager
                        .obtainTouchDownAnimation(card);
                animation.perform();

                isCanceled = false;

                clickedX = x;
                clickedY = y;
                startCardPositionX = card.getX();
                startCardPositionY = card.getY();

                pilePositionX = CardTable.this.cardsSettings.getRightPlayerSettings().getX();
                pilePositionY = CardTable.this.cardsSettings.getRightPlayerSettings().getY();

                mx = pilePositionX + ((startCardPositionX - pilePositionX) / 2);
                my = pilePositionY + ((startCardPositionY - pilePositionY) / 2);

                System.out.println(String.format("Pile position:[x=%f,y=%f]",pilePositionX,pilePositionY));

                System.out.println(String.format("Card position:[x=%f,y=%f]",card.getX(),card.getY()));

                System.out.println(String.format("middle pile position:[x=%f,y=%f]",mx,my));

                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(final InputEvent event,
                                final float x,
                                final float y,
                                final int pointer,
                                final int button) {
                super.touchUp(event, x, y, pointer, button);

                final CardView card = (CardView) event.getListenerActor();
                final SingleCardAnimation animation = cardAnimationManager
                        .obtainTouchUpAnimation(card);
                animation.perform();
                if (listener != null && !isCanceled) {
                    listener.onCardClicked(card);
                }
            }

            @Override
            public void clicked(final InputEvent event,
                                final float x,
                                final float y) { {
                super.clicked(event, x, y);
            }
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                final CardView card = (CardView) event.getListenerActor();
                float deltaX = card.getX() + x - clickedX;
                float deltaY = card.getY() + y - clickedY;
                float nextX = card.getX() + x;
                float nextY = card.getY() + y;

                card.toFront();
                float shiftX = Math.abs(startCardPositionX - deltaX);
                float shiftY = Math.abs(startCardPositionY - deltaY);

//                if (shiftX > MOTION_X_LIMIT) {
//                    deltaX = prevPosX;
//                } else {
//                    prevPosX = deltaX;
//                }
//                if (shiftY > MOTION_Y_LIMIT) {
//                    deltaY = prevPosY;
//                } else {
//                    prevPosY = deltaY;
//                }
//                if (nextX > startCardPositionX && nextY < startCardPositionY) {
//                    deltaX = deltaX - x;
//                }
//                if (nextY < startCardPositionY) {
//                    deltaY = deltaY - y;
//                }
//
//                if (!isCanceled) {
//                    final CardGroupAnimation animation = cardAnimationManager
//                            .obtainTouchMoveAnimation(card, deltaX, deltaY);
//                    animation.perform();
//                } else {
//                    return;
//                }

//                if (card.getX() > mx && card.getY() > my) {
//                    final CardGroupAnimation animation = cardAnimationManager
//                            .obtainReturnCardToPileAnimation(card, startCardPositionX, startCardPositionY);
//                    animation.perform();
//
//                    isCanceled = true;
//                    return;
//                }
//
//                if (card.getX() + card.getWidth() <= mx && card.getY() + card.getHeight() <= my) {
//                    final CardGroupAnimation animation = cardAnimationManager
//                            .obtainReturnCardToPileAnimation(card, pilePositionX, pilePositionY);
//                    animation.perform();
//
//                    //isCanceled = true;
//                    return;
//                }

                if (deltaY <= startCardPositionY) {
                    return;
                }

                if ((card.getX() > mx && card.getY() > my)
                        ||
                        (shiftX > MOTION_X_LIMIT || shiftY > MOTION_Y_LIMIT)) {
                    final CardGroupAnimation animation = cardAnimationManager
                            .obtainReturnCardToPileAnimation(card, startCardPositionX, startCardPositionY);
                    animation.perform();

                    isCanceled = true;
                    return;
                }

                if (card.getX() + card.getWidth() <= mx && card.getY() + card.getHeight() <= my) {
                    final CardGroupAnimation animation = cardAnimationManager
                            .obtainReturnCardToPileAnimation(card, pilePositionX, pilePositionY);
                    animation.perform();

                    //isCanceled = true;
                    return;
                }

                if (!isCanceled) {
                    final CardGroupAnimation animation = cardAnimationManager
                            .obtainTouchMoveAnimation(card, deltaX, deltaY);
                    animation.perform();
                }
            }
        };
    }

    private void updatePlayerCardListener(final PlayerWrapper wrapper,
                                          final EventListener listener) {
        for (final CardView card : wrapper.getCardsViews()) {
            card.addListener(listener);
        }
    }

    private void removeCardListener(final CardView cardView) {
        cardView.removeListener(cardClickListener);
    }
}