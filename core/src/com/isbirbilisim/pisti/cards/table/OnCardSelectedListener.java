package com.isbirbilisim.pisti.cards.table;

import com.isbirbilisim.pisti.view.CardView;

public interface OnCardSelectedListener {
    /**
     * Called when card was clicked by user.
     *
     * @param cardView - clicked card.
     */
    void onCardClicked(final CardView cardView);
}