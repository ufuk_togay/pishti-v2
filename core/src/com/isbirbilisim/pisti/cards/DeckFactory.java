package com.isbirbilisim.pisti.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Factory that creates initial deck.
 */
public class DeckFactory {
    public static final String CARD_HIDDEN = "image/cards/card_back_default.png";

    private static final String CARD_SPADES_ACE = "image/cards/spades_ace.png";
    private static final String CARD_SPADES_2 = "image/cards/spades_two.png";
    private static final String CARD_SPADES_3 = "image/cards/spades_three.png";
    private static final String CARD_SPADES_4 = "image/cards/spades_four.png";
    private static final String CARD_SPADES_5 = "image/cards/spades_five.png";
    private static final String CARD_SPADES_6 = "image/cards/spades_six.png";
    private static final String CARD_SPADES_7 = "image/cards/spades_seven.png";
    private static final String CARD_SPADES_8 = "image/cards/spades_eight.png";
    private static final String CARD_SPADES_9 = "image/cards/spades_nine.png";
    private static final String CARD_SPADES_10 = "image/cards/spades_ten.png";
    private static final String CARD_SPADES_JACK = "image/cards/spades_jack.png";
    private static final String CARD_SPADES_QUEEN = "image/cards/spades_queen.png";
    private static final String CARD_SPADES_KING = "image/cards/spades_king.png";

    private static final String CARD_HEARTS_ACE = "image/cards/hearts_ace.png";
    private static final String CARD_HEARTS_2 = "image/cards/hearts_two.png";
    private static final String CARD_HEARTS_3 = "image/cards/hearts_three.png";
    private static final String CARD_HEARTS_4 = "image/cards/hearts_four.png";
    private static final String CARD_HEARTS_5 = "image/cards/hearts_five.png";
    private static final String CARD_HEARTS_6 = "image/cards/hearts_six.png";
    private static final String CARD_HEARTS_7 = "image/cards/hearts_seven.png";
    private static final String CARD_HEARTS_8 = "image/cards/hearts_eight.png";
    private static final String CARD_HEARTS_9 = "image/cards/hearts_nine.png";
    private static final String CARD_HEARTS_10 = "image/cards/hearts_ten.png";
    private static final String CARD_HEARTS_JACK = "image/cards/hearts_jack.png";
    private static final String CARD_HEARTS_QUEEN = "image/cards/hearts_queen.png";
    private static final String CARD_HEARTS_KING = "image/cards/hearts_king.png";

    private static final String CARD_DIAMONDS_ACE = "image/cards/diamonds_ace.png";
    private static final String CARD_DIAMONDS_2 = "image/cards/diamonds_two.png";
    private static final String CARD_DIAMONDS_3 = "image/cards/diamonds_three.png";
    private static final String CARD_DIAMONDS_4 = "image/cards/diamonds_four.png";
    private static final String CARD_DIAMONDS_5 = "image/cards/diamonds_five.png";
    private static final String CARD_DIAMONDS_6 = "image/cards/diamonds_six.png";
    private static final String CARD_DIAMONDS_7 = "image/cards/diamonds_seven.png";
    private static final String CARD_DIAMONDS_8 = "image/cards/diamonds_eight.png";
    private static final String CARD_DIAMONDS_9 = "image/cards/diamonds_nine.png";
    private static final String CARD_DIAMONDS_10 = "image/cards/diamonds_ten.png";
    private static final String CARD_DIAMONDS_JACK = "image/cards/diamonds_jack.png";
    private static final String CARD_DIAMONDS_QUEEN = "image/cards/diamonds_queen.png";
    private static final String CARD_DIAMONDS_KING = "image/cards/diamonds_king.png";

    private static final String CARD_CLUBS_ACE = "image/cards/clubs_ace.png";
    private static final String CARD_CLUBS_2 = "image/cards/clubs_two.png";
    private static final String CARD_CLUBS_3 = "image/cards/clubs_three.png";
    private static final String CARD_CLUBS_4 = "image/cards/clubs_four.png";
    private static final String CARD_CLUBS_5 = "image/cards/clubs_five.png";
    private static final String CARD_CLUBS_6 = "image/cards/clubs_six.png";
    private static final String CARD_CLUBS_7 = "image/cards/clubs_seven.png";
    private static final String CARD_CLUBS_8 = "image/cards/clubs_eight.png";
    private static final String CARD_CLUBS_9 = "image/cards/clubs_nine.png";
    private static final String CARD_CLUBS_10 = "image/cards/clubs_ten.png";
    private static final String CARD_CLUBS_JACK = "image/cards/clubs_jack.png";
    private static final String CARD_CLUBS_QUEEN = "image/cards/clubs_queen.png";
    private static final String CARD_CLUBS_KING = "image/cards/clubs_king.png";

    /**
     * Creates THREE-TIMES-SHUFFLED deck.
     *
     * @return - created deck.
     */
    public static Stack<CardModel> createDeck() {
        final Stack<CardModel> deck = createDefaultDeck();

        do {
            Collections.shuffle(deck);
            Collections.shuffle(deck);
            Collections.shuffle(deck);
        } while (deck.get(deck.size() - 4).isJack());

        return deck;
    }

    /**
     * Creates default deck (deck with defined card order).
     *
     * @return - created deck.
     */
    public static Stack<CardModel> createDefaultDeck() {
        final Stack<CardModel> deck = new Stack<CardModel>();

        addClubsToDeck(deck);
        addDiamondsToDeck(deck);
        addSpadesToDeck(deck);
        addHeartsToDeck(deck);
        return deck;
    }

    public static List<String> getCardResourcesList() {
        final List<String> resources = new ArrayList<String>();
        resources.add(CARD_SPADES_ACE);
        resources.add(CARD_SPADES_10);
        resources.add(CARD_SPADES_9);
        resources.add(CARD_SPADES_8);
        resources.add(CARD_SPADES_7);
        resources.add(CARD_SPADES_6);
        resources.add(CARD_SPADES_5);
        resources.add(CARD_SPADES_4);
        resources.add(CARD_SPADES_3);
        resources.add(CARD_SPADES_2);
        resources.add(CARD_SPADES_JACK);
        resources.add(CARD_SPADES_QUEEN);
        resources.add(CARD_SPADES_KING);

        resources.add(CARD_HEARTS_ACE);
        resources.add(CARD_HEARTS_10);
        resources.add(CARD_HEARTS_9);
        resources.add(CARD_HEARTS_8);
        resources.add(CARD_HEARTS_7);
        resources.add(CARD_HEARTS_6);
        resources.add(CARD_HEARTS_5);
        resources.add(CARD_HEARTS_4);
        resources.add(CARD_HEARTS_3);
        resources.add(CARD_HEARTS_2);
        resources.add(CARD_HEARTS_JACK);
        resources.add(CARD_HEARTS_QUEEN);
        resources.add(CARD_HEARTS_KING);

        resources.add(CARD_DIAMONDS_ACE);
        resources.add(CARD_DIAMONDS_10);
        resources.add(CARD_DIAMONDS_9);
        resources.add(CARD_DIAMONDS_8);
        resources.add(CARD_DIAMONDS_7);
        resources.add(CARD_DIAMONDS_6);
        resources.add(CARD_DIAMONDS_5);
        resources.add(CARD_DIAMONDS_4);
        resources.add(CARD_DIAMONDS_3);
        resources.add(CARD_DIAMONDS_2);
        resources.add(CARD_DIAMONDS_JACK);
        resources.add(CARD_DIAMONDS_QUEEN);
        resources.add(CARD_DIAMONDS_KING);

        resources.add(CARD_CLUBS_ACE);
        resources.add(CARD_CLUBS_10);
        resources.add(CARD_CLUBS_9);
        resources.add(CARD_CLUBS_8);
        resources.add(CARD_CLUBS_7);
        resources.add(CARD_CLUBS_6);
        resources.add(CARD_CLUBS_5);
        resources.add(CARD_CLUBS_4);
        resources.add(CARD_CLUBS_3);
        resources.add(CARD_CLUBS_2);
        resources.add(CARD_CLUBS_JACK);
        resources.add(CARD_CLUBS_QUEEN);
        resources.add(CARD_CLUBS_KING);

        resources.add(CARD_HIDDEN);

        return resources;
    }

    private static void addClubsToDeck(final Stack<CardModel> deck) {

        deck.push(new CardModel(2, CardModel.CLUBS,
                CARD_CLUBS_2,
                CARD_HIDDEN));
        deck.push(new CardModel(3, CardModel.CLUBS,
                CARD_CLUBS_3,
                CARD_HIDDEN));
        deck.push(new CardModel(4, CardModel.CLUBS,
                CARD_CLUBS_4,
                CARD_HIDDEN));
        deck.push(new CardModel(5, CardModel.CLUBS,
                CARD_CLUBS_5,
                CARD_HIDDEN));
        deck.push(new CardModel(6, CardModel.CLUBS,
                CARD_CLUBS_6,
                CARD_HIDDEN));
        deck.push(new CardModel(7, CardModel.CLUBS,
                CARD_CLUBS_7,
                CARD_HIDDEN));
        deck.push(new CardModel(8, CardModel.CLUBS,
                CARD_CLUBS_8,
                CARD_HIDDEN));
        deck.push(new CardModel(9, CardModel.CLUBS,
                CARD_CLUBS_9,
                CARD_HIDDEN));
        deck.push(new CardModel(10, CardModel.CLUBS,
                CARD_CLUBS_10,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.JACK, CardModel.CLUBS,
                CARD_CLUBS_JACK,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.QUEEN, CardModel.CLUBS,
                CARD_CLUBS_QUEEN,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.KING, CardModel.CLUBS,
                CARD_CLUBS_KING,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.ACE, CardModel.CLUBS,
                CARD_CLUBS_ACE,
                CARD_HIDDEN));
    }

    private static void addDiamondsToDeck(final Stack<CardModel> deck) {
        deck.push(new CardModel(2, CardModel.DIAMONDS,
                CARD_DIAMONDS_2,
                CARD_HIDDEN));
        deck.push(new CardModel(3, CardModel.DIAMONDS,
                CARD_DIAMONDS_3,
                CARD_HIDDEN));
        deck.push(new CardModel(4, CardModel.DIAMONDS,
                CARD_DIAMONDS_4,
                CARD_HIDDEN));
        deck.push(new CardModel(5, CardModel.DIAMONDS,
                CARD_DIAMONDS_5,
                CARD_HIDDEN));
        deck.push(new CardModel(6, CardModel.DIAMONDS,
                CARD_DIAMONDS_6,
                CARD_HIDDEN));
        deck.push(new CardModel(7, CardModel.DIAMONDS,
                CARD_DIAMONDS_7,
                CARD_HIDDEN));
        deck.push(new CardModel(8, CardModel.DIAMONDS,
                CARD_DIAMONDS_8,
                CARD_HIDDEN));
        deck.push(new CardModel(9, CardModel.DIAMONDS,
                CARD_DIAMONDS_9,
                CARD_HIDDEN));
        deck.push(new CardModel(10, CardModel.DIAMONDS,
                CARD_DIAMONDS_10,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.JACK, CardModel.DIAMONDS,
                CARD_DIAMONDS_JACK,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.QUEEN, CardModel.DIAMONDS,
                CARD_DIAMONDS_QUEEN,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.KING, CardModel.DIAMONDS,
                CARD_DIAMONDS_KING,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.ACE, CardModel.DIAMONDS,
                CARD_DIAMONDS_ACE,
                CARD_HIDDEN));
    }

    private static void addSpadesToDeck(final Stack<CardModel> deck) {
        deck.push(new CardModel(2, CardModel.SPADES,
                CARD_SPADES_2,
                CARD_HIDDEN));
        deck.push(new CardModel(3, CardModel.SPADES,
                CARD_SPADES_3,
                CARD_HIDDEN));
        deck.push(new CardModel(4, CardModel.SPADES,
                CARD_SPADES_4,
                CARD_HIDDEN));
        deck.push(new CardModel(5, CardModel.SPADES,
                CARD_SPADES_5,
                CARD_HIDDEN));
        deck.push(new CardModel(6, CardModel.SPADES,
                CARD_SPADES_6,
                CARD_HIDDEN));
        deck.push(new CardModel(7, CardModel.SPADES,
                CARD_SPADES_7,
                CARD_HIDDEN));
        deck.push(new CardModel(8, CardModel.SPADES,
                CARD_SPADES_8,
                CARD_HIDDEN));
        deck.push(new CardModel(9, CardModel.SPADES,
                CARD_SPADES_9,
                CARD_HIDDEN));
        deck.push(new CardModel(10, CardModel.SPADES,
                CARD_SPADES_10,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.JACK, CardModel.SPADES,
                CARD_SPADES_JACK,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.QUEEN, CardModel.SPADES,
                CARD_SPADES_QUEEN,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.KING, CardModel.SPADES,
                CARD_SPADES_KING,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.ACE, CardModel.SPADES,
                CARD_SPADES_ACE,
                CARD_HIDDEN));
    }

    private static void addHeartsToDeck(final Stack<CardModel> deck) {
        deck.push(new CardModel(2, CardModel.HEARTS,
                CARD_HEARTS_2,
                CARD_HIDDEN));
        deck.push(new CardModel(3, CardModel.HEARTS,
                CARD_HEARTS_3,
                CARD_HIDDEN));
        deck.push(new CardModel(4, CardModel.HEARTS,
                CARD_HEARTS_4,
                CARD_HIDDEN));
        deck.push(new CardModel(5, CardModel.HEARTS,
                CARD_HEARTS_5,
                CARD_HIDDEN));
        deck.push(new CardModel(6, CardModel.HEARTS,
                CARD_HEARTS_6,
                CARD_HIDDEN));
        deck.push(new CardModel(7, CardModel.HEARTS,
                CARD_HEARTS_7,
                CARD_HIDDEN));
        deck.push(new CardModel(8, CardModel.HEARTS,
                CARD_HEARTS_8,
                CARD_HIDDEN));
        deck.push(new CardModel(9, CardModel.HEARTS,
                CARD_HEARTS_9,
                CARD_HIDDEN));
        deck.push(new CardModel(10, CardModel.HEARTS,
                CARD_HEARTS_10,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.JACK, CardModel.HEARTS,
                CARD_HEARTS_JACK,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.QUEEN, CardModel.HEARTS,
                CARD_HEARTS_QUEEN,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.KING, CardModel.HEARTS,
                CARD_HEARTS_KING,
                CARD_HIDDEN));
        deck.push(new CardModel(CardModel.ACE, CardModel.HEARTS,
                CARD_HEARTS_ACE,
                CARD_HIDDEN));
    }
}