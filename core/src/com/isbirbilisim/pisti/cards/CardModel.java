package com.isbirbilisim.pisti.cards;

public class CardModel {
    public final static int SPADES = 0; // Codes for the 4 suits.
    public final static int HEARTS = 1;
    public final static int DIAMONDS = 2;
    public final static int CLUBS = 3;

    public final static int ACE = 1; // Codes for the non-numeric cards.
    public final static int JACK = 11; // Cards 2 through 10 have their
    public final static int QUEEN = 12; // numerical values for their codes.
    public final static int KING = 13;

    private final String visibleDrawableName;
    private final String hiddenDrawableName;

    /**
     * This card's suit, one of the constants SPADES, HEARTS, DIAMONDS, CLUBS
     * The suit cannot be changed after the card is constructed.
     */
    private final int suit;

    /**
     * The card's value. For a normal card, this is one of the values 1 through
     * 13, with 1 representing ACE. The
     * value cannot be changed after the card is constructed.
     */
    private final int value;

    /**
     * The card's score points. Points are;
     * <p/>
     * Ace = 1 point
     * Jack = 1 point
     * 2 of Clubs = 2 points
     * 10 of Diamonds = 3 points
     * Pişti = 10 points
     * Super Pişti (pişti with Jack) = 20 points
     */
    private int scoreValue;

    /**
     * Creates a card with a specified suit and value.
     *
     * @param value               the value of the new card. For a regular card the
     *                            value must be in the range 1 through 13, with 1 representing
     *                            an Ace. You can use the constants CardModel.ACE, CardModel.JACK,
     *                            CardModel.QUEEN, and CardModel.KING.
     * @param suit                the suit of the new card. This must be one of the values
     *                            CardModel.SPADES, CardModel.HEARTS, CardModel.DIAMONDS, CardModel.CLUBS
     * @param visibleDrawableName path to file with card resource.
     * @param hiddenDrawableName  path to file with hidden card resource.
     * @throws IllegalArgumentException if the parameter values are not in the permissible ranges
     */
    public CardModel(final int value,
                     final int suit,
                     final String visibleDrawableName,
                     final String hiddenDrawableName) {
        if (suit != SPADES
                && suit != HEARTS
                && suit != DIAMONDS
                && suit != CLUBS) {
            throw new IllegalArgumentException("Illegal playing card suit");
        }
        if (value < 1 || value > 13) {
            throw new IllegalArgumentException("Illegal playing card value");
        }

        this.value = value;
        this.suit = suit;

        if (value == ACE || value == JACK) {
            scoreValue = 1;
        } else if (value == 2 && suit == CLUBS) {
            scoreValue = 2;
        } else if (value == 10 && suit == DIAMONDS) {
            scoreValue = 3;
        }

        this.visibleDrawableName = visibleDrawableName;
        this.hiddenDrawableName = hiddenDrawableName;
    }

    /**
     * Returns the suit of this card.
     *
     * @returns the suit, which is one of the constants CardModel.SPADES,
     * CardModel.HEARTS, CardModel.DIAMONDS, CardModel.CLUBS.
     */
    public int getSuit() {
        return suit;
    }

    /**
     * Returns the value of this card.
     *
     * @return the value, which is one of the numbers 1 through 13
     */
    public int getValue() {
        return value;
    }

    public int getScoreValue() {
        return scoreValue;
    }

    /**
     * @return - path to file with image of card in hidden state.
     */
    public String getHiddenDrawableName() {
        return hiddenDrawableName;
    }

    /**
     * @return - path to file with card image.
     */
    public String getVisibleDrawableName() {
        return visibleDrawableName;
    }

    /**
     * Returns a String representation of the card's suit.
     *
     * @return one of the strings "Spades", "Hearts", "Diamonds", "Clubs".
     */
    public String getSuitAsString() {
        switch (suit) {
            case SPADES:
                return "Spades";
            case HEARTS:
                return "Hearts";
            case DIAMONDS:
                return "Diamonds";
            default:
                return "Clubs";
        }
    }

    /**
     * Returns a String representation of the card's value.
     *
     * @return for a regular card, one of the strings "Ace", "2", "3", ...,
     * "10", "Jack", "Queen", or "King".
     */
    public String getValueAsString() {
        switch (value) {
            case 1:
                return "Ace";
            case 2:
                return "2";
            case 3:
                return "3";
            case 4:
                return "4";
            case 5:
                return "5";
            case 6:
                return "6";
            case 7:
                return "7";
            case 8:
                return "8";
            case 9:
                return "9";
            case 10:
                return "10";
            case 11:
                return "Jack";
            case 12:
                return "Queen";
            default:
                return "King";
        }
    }

    public boolean isJack() {
        return value == JACK;
    }

    public boolean isSameCard(final CardModel card) {
        return value == card.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardModel)) return false;

        CardModel cardModel = (CardModel) o;

        if (suit != cardModel.suit) return false;
        if (value != cardModel.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = suit;
        result = 31 * result + value;
        return result;
    }

    /**
     * Returns a string representation of this card, including both its suit and
     * its value . Sample return values are: "Queen of Hearts", "10 of Diamonds",
     * "Ace of Spades"
     */
    public String toString() {
        return getValueAsString() + " of " + getSuitAsString();
    }
}