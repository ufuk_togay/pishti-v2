package com.isbirbilisim.pisti.cards;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.view.CardViewSettings;
import com.isbirbilisim.pisti.view.PlayerWidget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Calculates and stores cards positions (Cards which are in players).
 */
public class CardsSettings {
    private static final float DECK_CAPACITY = 52;
    private static final float CARD_RATIO = 1.36F;

    /* Relative size of cards assuming that table height is 1000. Table
    width depends on real screen sides ratio. V_ means virtual. */
    private static final float V_PILE_CARD_WIDTH = 70;
    private static final float V_PILE_CARD_HEIGHT =
            V_PILE_CARD_WIDTH * CARD_RATIO;

    private static final float V_OPPONENT_CARD_WIDTH = 35;
    private static final float V_OPPONENT_CARD_HEIGHT =
            V_OPPONENT_CARD_WIDTH * CARD_RATIO;

    private static final float V_PLAYER_CARD_WIDTH = 150;
    private static final float V_PLAYER_CARD_HEIGHT =
            V_PLAYER_CARD_WIDTH * CARD_RATIO;

    private static final float V_CENTER_CARD_WIDTH = 110;
    private static final float V_CENTER_CARD_HEIGHT =
            V_CENTER_CARD_WIDTH * CARD_RATIO;

    private static final float V_COLLECT_CARD_SHIFT = 300;

    private static final float V_OPPONENT_INFO_WIDTH_NO_AD = 130;
    private static final float V_OPPONENT_INFO_HEIGHT_NO_AD = 130;
    private static final float V_OPPONENT_INFO_WIDTH_AD = 100;
    private static final float V_OPPONENT_INFO_HEIGHT_AD = 100;
    private static final float V_PLAYER_INFO_WIDTH = 190;
    private static final float V_PLAYER_INFO_HEIGHT = 190;

    /* Sum angle of initial cards at player. */
    private static final float CARD_IN_HANDS_ANGLE = 70;
    private static final float OPPONENT_CARDS_ANGLE = 40;
    /* Angle of card at center. */
    private static final float CENTER_CARD_ANGLE = 20;

    private static final float BORDER_MARGIN = 20;
    private static final float OPPONENT_CARD_ZONE_X_MARGIN = 20;
    private static final float OPPONENT_CARD_ZONE_Y_MARGIN = 20;

    private static final float OPPONENTS_WIDGET_MARGIN = 10;

    /* Real table size. */
    private final float tableWidth;
    private final float tableHeight;
    private final int initialCardNumber;

    /* Values that help convert virtual card size to real. */
    private final float mul_x;
    private final float mul_y;

    /* Real sizes and positions of cards, depend on table size. */
    private final List<CardViewSettings> leftPlayerCardSettings;
    private final List<CardViewSettings> topPlayerCardSettings;
    private final List<CardViewSettings> rightPlayerCardSettings;
    private final List<CardViewSettings> bottomPlayerCardSettings;
    private final List<CardViewSettings> initialPileCardSettings;
    private final List<CardViewSettings> pileSettings;
    private final List<CardViewSettings> revealCardsSettings;

    /* Real sizes and positions of PlayerWidgets. */
    private final PlayerWidget.PlayerWidgetSettings leftPlayerSettings;
    private final PlayerWidget.PlayerWidgetSettings topPlayerSettings;
    private final PlayerWidget.PlayerWidgetSettings rightPlayerSettings;
    private final PlayerWidget.PlayerWidgetSettings bottomPlayerSettings;

    private final List<Color> playerColors = Arrays.asList(Res.colors.playersColors);
    private int next;

    private Vector2 leftPlayerCollectCardPoint;
    private Vector2 topPlayerCollectCardPoint;
    private Vector2 rightPlayerCollectCardPoint;
    private Vector2 bottomPlayerCollectCardPoint;

    private Vector2 centerCardsSettings;

    private boolean showingAd;

    public CardsSettings(final ScreenUtils screenUtils,
                         final float tableWidth,
                         final float tableHeight,
                         final int initialCardNumber,
                         final boolean showingAd) {
        this.tableWidth = tableWidth;
        this.tableHeight = tableHeight;
        this.initialCardNumber = initialCardNumber;
        this.showingAd = showingAd;

        Collections.shuffle(playerColors);

        this.mul_x = tableWidth / screenUtils.getVirtualWidth();
        this.mul_y = tableHeight / screenUtils.getVirtualHeight();

        this.pileSettings = initPileSettings();

        this.centerCardsSettings = initCenterCardsSettings();

        this.leftPlayerCardSettings = initLeftPlayerCardSettings();
        this.rightPlayerCardSettings = initRightPlayerCardSettings();
        this.topPlayerCardSettings = initTopPlayerCardSettings();
        this.bottomPlayerCardSettings = initBottomPlayerCardSettings();
        this.initialPileCardSettings = initInitialPileCardSettings();

        this.leftPlayerSettings = initLeftPlayerSettings();
        this.topPlayerSettings = initTopPlayerSettings();
        this.rightPlayerSettings = initRightPlayerSettings();
        this.bottomPlayerSettings = initBottomPlayerSettings();

        this.revealCardsSettings = initRevealCardsSettings();
    }

    private Vector2 calculatePosition(final float cardWidth,
                                      final Vector2 center,
                                      final float radius,
                                      final float angle) {
        final Vector2 circlePoint = getCirclePoint(center,
                radius,
                angle);

        return calculateFixedPosition(circlePoint, cardWidth);
    }

    private Vector2 calculateFixedPosition(final Vector2 position,
                                           final float itemWidth) {
        final Vector2 vector2 = new Vector2();
        vector2.x = position.x - (itemWidth / 2);
        vector2.y = position.y;

        return vector2;
    }

    final Vector2 getCirclePoint(final Vector2 center,
                                 final float radius,
                                 final float angle) {
        final float x;
        final float y;

        if (angle > 0) {
            x = center.x
                    - radius * MathUtils.cosDeg(-angle + 90);
            y = center.y
                    + radius * MathUtils.sinDeg(-angle + 90);
        } else {
            x = center.x
                    + radius * MathUtils.cosDeg(angle + 90);
            y = center.y
                    + radius * MathUtils.sinDeg(-angle + 90);
        }

        return new Vector2(x, y);
    }

    private Vector2 initCenterCardsSettings() {
        final float x = tableWidth / 2;

        final float topPad = showingAd ? ((Res.values.AD_TOP_PAD * mul_y) / 2) : 0;
        final float y = tableHeight / 2 - topPad;

        return new Vector2(x, y);
    }

    private List<CardViewSettings> initLeftPlayerCardSettings() {
        final List<CardViewSettings> cardSettings =
                new ArrayList<CardViewSettings>();
        final CardZoneSettings settings = getOpponentZoneSettings();
        final Vector2 opponentWidgetSize = getOpponentWidgetSize();

        final float centerX = settings.zoneWidth / 2
                + opponentWidgetSize.x
                + (OPPONENTS_WIDGET_MARGIN * mul_x)
                + (BORDER_MARGIN * mul_x)
                + (Res.values.GAME_SCREEN_SETTINGS_ICON_WIDTH * mul_x * 2);

        final float topBorder = showingAd ? (Res.values.AD_TOP_PAD * mul_y) : 0;
        final float centerY = tableHeight
                - settings.cardHeight
                - settings.circleRadius
                - (getOpponentInfoHeight() / 2)
                - topBorder;

        final Vector2 center = new Vector2(centerX, centerY);
        leftPlayerCollectCardPoint = new Vector2(
                -(V_COLLECT_CARD_SHIFT * mul_x),
                centerY + (V_COLLECT_CARD_SHIFT * mul_y));

        final float topCardAngle = -(settings.sumAngle / 2);
        final Vector2 topCardPos = calculatePosition(
                settings.cardWidth,
                center,
                settings.circleRadius,
                topCardAngle);

        cardSettings.add(new CardViewSettings(settings.cardWidth,
                settings.cardHeight,
                topCardPos.x,
                topCardPos.y,
                topCardAngle));

        for (int i = 1; i < initialCardNumber; i++) {
            final float angle = topCardAngle
                    + ((settings.sumAngle / (initialCardNumber - 1)) * i);
            final Vector2 cardPos = calculatePosition(
                    settings.cardWidth,
                    center,
                    settings.circleRadius,
                    angle);

            cardSettings.add(new CardViewSettings(settings.cardWidth,
                    settings.cardHeight,
                    cardPos.x,
                    cardPos.y,
                    angle));
        }

        Collections.reverse(cardSettings);

        return cardSettings;
    }

    private List<CardViewSettings> initRightPlayerCardSettings() {
        final List<CardViewSettings> cardSettings =
                new ArrayList<CardViewSettings>();
        final CardZoneSettings settings = getOpponentZoneSettings();

        final float centerX = tableWidth - (OPPONENT_CARD_ZONE_X_MARGIN * mul_x)
                - (settings.zoneWidth / 2);

        final float topBorder = showingAd ? (Res.values.AD_TOP_PAD * mul_y) : 0;
        final float centerY = tableHeight
                - settings.cardHeight
                - settings.circleRadius
                - (getOpponentInfoHeight() / 2)
                - topBorder;

        final Vector2 center = new Vector2(centerX, centerY);
        rightPlayerCollectCardPoint = new Vector2(
                centerX + (V_COLLECT_CARD_SHIFT * mul_x),
                centerY + (V_COLLECT_CARD_SHIFT * mul_y));

        final float topCardAngle = -(settings.sumAngle / 2);
        final Vector2 topCardPos = calculatePosition(
                settings.cardWidth,
                center,
                settings.circleRadius,
                topCardAngle);

        cardSettings.add(new CardViewSettings(settings.cardWidth,
                settings.cardHeight,
                topCardPos.x,
                topCardPos.y,
                topCardAngle));

        for (int i = 1; i < initialCardNumber; i++) {
            final float angle = topCardAngle
                    + ((settings.sumAngle / (initialCardNumber - 1)) * i);
            final Vector2 cardPos = calculatePosition(
                    settings.cardWidth,
                    center,
                    settings.circleRadius,
                    angle);

            cardSettings.add(new CardViewSettings(settings.cardWidth,
                    settings.cardHeight,
                    cardPos.x,
                    cardPos.y,
                    angle));
        }

        Collections.reverse(cardSettings);

        return cardSettings;
    }

    private List<CardViewSettings> initTopPlayerCardSettings() {
        final List<CardViewSettings> cardSettings =
                new ArrayList<CardViewSettings>();
        final CardZoneSettings settings = getOpponentZoneSettings();

        final float centerX = (rightPlayerCardSettings.get(2).x
                - leftPlayerCardSettings.get(2).x) / 2
                + leftPlayerCardSettings.get(2).x;

        final float topBorder = showingAd ? (Res.values.AD_TOP_PAD * mul_y) : 0;
        final float centerY = tableHeight
                - settings.cardHeight
                - settings.circleRadius
                - (getOpponentInfoHeight() / 2)
                - topBorder;
        final Vector2 center = new Vector2(centerX, centerY);
        topPlayerCollectCardPoint = new Vector2(
                centerX - (V_COLLECT_CARD_SHIFT * mul_x),
                centerY + (V_COLLECT_CARD_SHIFT * mul_y));

        final float topCardAngle = -(settings.sumAngle / 2);
        final Vector2 topCardPos = calculatePosition(
                settings.cardWidth,
                center,
                settings.circleRadius,
                topCardAngle);

        cardSettings.add(new CardViewSettings(settings.cardWidth,
                settings.cardHeight,
                topCardPos.x,
                topCardPos.y,
                topCardAngle));

        for (int i = 1; i < initialCardNumber; i++) {
            final float angle = topCardAngle
                    + ((settings.sumAngle / (initialCardNumber - 1)) * i);
            final Vector2 cardPos = calculatePosition(
                    settings.cardWidth,
                    center,
                    settings.circleRadius,
                    angle);

            cardSettings.add(new CardViewSettings(settings.cardWidth,
                    settings.cardHeight,
                    cardPos.x,
                    cardPos.y,
                    angle));
        }

        Collections.reverse(cardSettings);

        return cardSettings;
    }

    private List<CardViewSettings> initBottomPlayerCardSettings() {
        final List<CardViewSettings> cardSettings =
                new ArrayList<CardViewSettings>();
        final CardZoneSettings settings = getPlayerZoneSettings();

        final float centerX = tableWidth
                - (settings.cardWidth / 2)
                - BORDER_MARGIN * mul_x;
        final float centerY = -settings.circleRadius + (settings.cardWidth / 2);
        final Vector2 center = new Vector2(centerX, centerY);
        bottomPlayerCollectCardPoint = new Vector2(
                centerX + (V_COLLECT_CARD_SHIFT * mul_x),
                centerY - (V_COLLECT_CARD_SHIFT * mul_y));

        final float topCardAngle = 0;
        final Vector2 topCardPos = calculatePosition(
                settings.cardWidth,
                center,
                settings.circleRadius,
                topCardAngle);

        cardSettings.add(new CardViewSettings(settings.cardWidth,
                settings.cardHeight,
                topCardPos.x,
                topCardPos.y,
                topCardAngle));

        for (int i = 1; i < initialCardNumber; i++) {
            final float angle = topCardAngle
                    + ((settings.sumAngle / (initialCardNumber - 1)) * i);
            final Vector2 cardPos = calculatePosition(
                    settings.cardWidth,
                    center,
                    settings.circleRadius,
                    angle);

            cardSettings.add(new CardViewSettings(settings.cardWidth,
                    settings.cardHeight,
                    cardPos.x,
                    cardPos.y,
                    angle));
        }

        Collections.reverse(cardSettings);

        return cardSettings;
    }

    private PlayerWidget.PlayerWidgetSettings initLeftPlayerSettings() {
        final Vector2 size = getOpponentWidgetSize();

        final float x = getLeftPlayerCardSettings().get(0).x - (size.x)
                - (OPPONENTS_WIDGET_MARGIN * mul_x);

        final CardViewSettings cardViewSettings = getLeftPlayerCardSettings().get(2);
        final float y = cardViewSettings.y + (cardViewSettings.height / 2F)
                - (size.y / 2F);

        return new PlayerWidget.PlayerWidgetSettings(size.x, size.y, x, y, getPlayerColor(PlayerPlace.LEFT));
    }

    private PlayerWidget.PlayerWidgetSettings initTopPlayerSettings() {
        final Vector2 size = getOpponentWidgetSize();

        final float x = getTopPlayerCardSettings().get(0).x - (size.x)
                - (OPPONENTS_WIDGET_MARGIN * mul_x);

        final CardViewSettings cardViewSettings = getTopPlayerCardSettings().get(2);
        final float y = cardViewSettings.y + (cardViewSettings.height / 2F)
                - (size.y / 2F);

        return new PlayerWidget.PlayerWidgetSettings(size.x, size.y, x, y, getPlayerColor(PlayerPlace.TOP));
    }

    private PlayerWidget.PlayerWidgetSettings initRightPlayerSettings() {
        final Vector2 size = getOpponentWidgetSize();

        final float x = getRightPlayerCardSettings().get(0).x - (size.x)
                - (OPPONENTS_WIDGET_MARGIN * mul_x);

        final CardViewSettings cardViewSettings = getRightPlayerCardSettings().get(2);
        final float y = cardViewSettings.y + (cardViewSettings.height / 2F)
                - (size.y / 2F);

        return new PlayerWidget.PlayerWidgetSettings(size.x, size.y, x, y, getPlayerColor(PlayerPlace.RIGHT));
    }

    private PlayerWidget.PlayerWidgetSettings initBottomPlayerSettings() {
        final Vector2 size = getPlayerWidgetSize();

        final float x = BORDER_MARGIN * mul_x;
        final float y = BORDER_MARGIN * mul_y;

        return new PlayerWidget.PlayerWidgetSettings(size.x, size.y, x, y, getPlayerColor(PlayerPlace.BOTTOM));
    }

    private Color getPlayerColor(final PlayerPlace playerPlace) {
        if (playerPlace == PlayerPlace.BOTTOM) {
            return Color.GREEN;
        }

        return Color.RED;
    }

    private Vector2 getPlayerWidgetSize() {
        return new Vector2(V_PLAYER_INFO_WIDTH * mul_x,
                V_PLAYER_INFO_HEIGHT * mul_y);
    }

    private Vector2 getOpponentWidgetSize() {
        return new Vector2(getOpponentInfoWidth(),
                getOpponentInfoHeight());
    }

    private float getOpponentInfoWidth() {
        return showingAd
                ? V_OPPONENT_INFO_WIDTH_AD * mul_x
                : V_OPPONENT_INFO_WIDTH_NO_AD * mul_x;
    }

    private float getOpponentInfoHeight() {
        return showingAd
                ? V_OPPONENT_INFO_HEIGHT_AD * mul_y
                : V_OPPONENT_INFO_HEIGHT_NO_AD * mul_y;
    }

    private List<CardViewSettings> initInitialPileCardSettings() {
        final List<CardViewSettings> cardPositions =
                new ArrayList<CardViewSettings>();
        final CardZoneSettings settings = getCenterZoneSettings();

        // y coordinate is always the same.
        final float topPad = showingAd ? ((Res.values.AD_TOP_PAD * mul_y) / 2) : 0;
        final float y = centerCardsSettings.y - (settings.cardHeight / 2) - topPad;
        // x coordinate of card that will be added last.
        final float topCardX = tableWidth / 2 + settings.zoneWidth / 2
                - settings.cardWidth;
        cardPositions.add(new CardViewSettings(
                settings.cardWidth,
                settings.cardHeight,
                topCardX,
                y,
                settings.sumAngle));

        // Calculates positions from second last card to first card.
        for (int i = 1; i < initialCardNumber; i++) {
            final float x = topCardX - i * settings.cardVisiblePartWidth;
            cardPositions.add(new CardViewSettings(
                    settings.cardWidth,
                    settings.cardHeight,
                    x,
                    y,
                    settings.sumAngle));
        }

        Collections.reverse(cardPositions);

        return cardPositions;
    }

    private CardZoneSettings getOpponentZoneSettings() {
        final float cardWidth = V_OPPONENT_CARD_WIDTH * mul_x;
        final float cardHeight = V_OPPONENT_CARD_HEIGHT * mul_y;
        final float cardZoneWidth = cardWidth * 3;
        final float cardVisiblePartWidth = (cardZoneWidth - cardWidth) / 3;
        final float circleRadius = cardHeight * 1.4F;

        return new CardZoneSettings(
                cardWidth,
                cardHeight,
                cardVisiblePartWidth,
                cardZoneWidth,
                circleRadius,
                OPPONENT_CARDS_ANGLE);
    }

    private CardZoneSettings getPlayerZoneSettings() {
        final float cardWidth = V_PLAYER_CARD_WIDTH * mul_x;
        final float cardHeight = V_PLAYER_CARD_HEIGHT * mul_y;
        final float cardZoneWidth = cardWidth * 3;
        final float cardVisiblePartWidth = (cardZoneWidth - cardWidth) / 3;
        final float circleRadius = cardHeight * 1.5F;

        return new CardZoneSettings(
                cardWidth,
                cardHeight,
                cardVisiblePartWidth,
                cardZoneWidth,
                circleRadius,
                CARD_IN_HANDS_ANGLE * 0.8F);
    }

    private CardZoneSettings getCenterZoneSettings() {
        final float cardWidth = V_CENTER_CARD_WIDTH * mul_x;
        final float cardHeight = V_CENTER_CARD_HEIGHT * mul_y;
        final float cardZoneWidth = cardWidth * 1.2F;
        final float cardVisiblePartWidth = (cardZoneWidth - cardWidth) / 3;

        return new CardZoneSettings(
                cardWidth,
                cardHeight,
                cardVisiblePartWidth,
                cardZoneWidth,
                0,
                CENTER_CARD_ANGLE);
    }

    private List<CardViewSettings> initPileSettings() {
        final List<CardViewSettings> settings = new
                ArrayList<CardViewSettings>();

        final float width = V_PILE_CARD_WIDTH * mul_x;
        final float height = V_PILE_CARD_HEIGHT * mul_y;

        final float x = BORDER_MARGIN * mul_x;
        final float y = tableHeight / 2F - (height / 2);

        final Random random = new Random();

        int rotation;
        for (int i = 0; i < DECK_CAPACITY; i++) {
            rotation = random.nextInt((5 - (-5)) + 1) + (-5);

            settings.add(new CardViewSettings(width, height, x, y, rotation));
        }

        return settings;
    }

    private List<CardViewSettings> initRevealCardsSettings() {
        final List<CardViewSettings> settings = new ArrayList<CardViewSettings>();

        float rotation;
        final Vector2 circleCenter = new Vector2(tableWidth / 2,
                V_CENTER_CARD_HEIGHT * mul_y / 2);
        for (int i = 0; i < 4; i++) {
            rotation = -(-30 + (20 * (i)));
            final Vector2 point = getCirclePoint(circleCenter,
                    tableHeight / 2,
                    rotation);

            final CardViewSettings cardViewSettings = new CardViewSettings(
                    0, 0, point.x - (V_CENTER_CARD_WIDTH * mul_x / 2), point.y, rotation);
            settings.add(cardViewSettings);
        }

        return settings;
    }

    public List<CardViewSettings> getRevealCardsSettings() {
        return revealCardsSettings;
    }

    /**
     * Returns list of left player's cards settings (cards order - from left to
     * right).
     *
     * @return - cards settings list.
     */
    public List<CardViewSettings> getLeftPlayerCardSettings() {
        return leftPlayerCardSettings;
    }

    /**
     * Returns list of top player's cards settings (cards order - from left to right).
     *
     * @return - cards settings list.
     */
    public List<CardViewSettings> getTopPlayerCardSettings() {
        return topPlayerCardSettings;
    }

    /**
     * Returns list of right player's cards settings (cards order - from left to right).
     *
     * @return - cards settings list.
     */
    public List<CardViewSettings> getRightPlayerCardSettings() {
        return rightPlayerCardSettings;
    }

    /**
     * Returns list of bottom player's cards settings (cards order - from left to right).
     *
     * @return - cards settings list.
     */
    public List<CardViewSettings> getBottomPlayerCardSettings() {
        return bottomPlayerCardSettings;
    }

    /**
     * Returns list of initial cards settings (cards order - from left to right).
     *
     * @return - cards settings list.
     */
    public List<CardViewSettings> getInitialPileCardSettings() {
        return initialPileCardSettings;
    }

    /**
     * Returns settings of pile.
     *
     * @return - pile settings;
     */
    public List<CardViewSettings> getPileSettings() {
        return pileSettings;
    }

    /**
     * @return - destination point of collect card animation for left player.
     */
    public Vector2 getLeftPlayerCollectCardPoint() {
        return leftPlayerCollectCardPoint;
    }

    /**
     * @return - destination point of collect card animation for left player.
     */
    public Vector2 getTopPlayerCollectCardPoint() {
        return topPlayerCollectCardPoint;
    }

    /**
     * @return - destination point of collect card animation for right player.
     */
    public Vector2 getRightPlayerCollectCardPoint() {
        return rightPlayerCollectCardPoint;
    }

    /**
     * @return - destination point of collect card animation for bottom player.
     */
    public Vector2 getBottomPlayerCollectCardPoint() {
        return bottomPlayerCollectCardPoint;
    }

    public PlayerWidget.PlayerWidgetSettings getBottomPlayerSettings() {
        return bottomPlayerSettings;
    }

    public PlayerWidget.PlayerWidgetSettings getRightPlayerSettings() {
        return rightPlayerSettings;
    }

    public PlayerWidget.PlayerWidgetSettings getTopPlayerSettings() {
        return topPlayerSettings;
    }

    public PlayerWidget.PlayerWidgetSettings getLeftPlayerSettings() {
        return leftPlayerSettings;
    }

    /**
     * Resets settings of all cards to initial settings. Should be called before
     * giving cards to players.
     */
    public void reset() {
        leftPlayerCardSettings.clear();
        leftPlayerCardSettings.addAll(initLeftPlayerCardSettings());

        rightPlayerCardSettings.clear();
        rightPlayerCardSettings.addAll(initRightPlayerCardSettings());

        topPlayerCardSettings.clear();
        topPlayerCardSettings.addAll(initTopPlayerCardSettings());

        bottomPlayerCardSettings.clear();
        bottomPlayerCardSettings.addAll(initBottomPlayerCardSettings());
    }

    /**
     * Removes card settings from left player and updates settings of the rest of the
     * cards.
     *
     * @return - list of updated settings.
     */
    public List<CardViewSettings> removeCardFromLeftPlayer() {
        leftPlayerCardSettings.remove(0);
        // TODO update positions (shift rest of cards positions to center).

        return leftPlayerCardSettings;
    }

    /**
     * Removes card settings from right player and updates settings of the rest of the
     * cards.
     *
     * @return - list of updated settings.
     */
    public List<CardViewSettings> removeCardFromRightPlayer() {
        rightPlayerCardSettings.remove(0);
        // TODO update positions (shift rest of cards positions to center).

        return rightPlayerCardSettings;
    }

    /**
     * @return - point on screen, where cards should move during "Move animation".
     */
    public Vector2 getCenterCardsSettings() {
        return centerCardsSettings;
    }

    /**
     * Removes card settings from top player and updates settings of the rest of the
     * cards.
     *
     * @return - list of updated settings.
     */
    public List<CardViewSettings> removeCardFromTopPlayer() {
        topPlayerCardSettings.remove(0);

        // TODO update positions (shift rest of cards positions to center).

        return topPlayerCardSettings;
    }

    /**
     * Removes card settings from bottom player and updates settings of the rest of the
     * cards.
     *
     * @return - list of updated settings.
     */
    public List<CardViewSettings> removeCardFromBottomPlayer() {
        bottomPlayerCardSettings.remove(0);
        // TODO update positions (shift rest of cards positions to center).

        return bottomPlayerCardSettings;
    }

    private static class CardZoneSettings {
        private final float cardWidth;
        private final float cardHeight;
        private final float cardVisiblePartWidth;
        private final float zoneWidth;
        private final float circleRadius;
        private final float sumAngle;

        private CardZoneSettings(final float cardWidth,
                                 final float cardHeight,
                                 final float cardVisiblePartWidth,
                                 final float zoneWidth,
                                 final float circleRadius,
                                 final float sumAngle) {
            this.cardHeight = cardHeight;
            this.cardWidth = cardWidth;
            this.cardVisiblePartWidth = cardVisiblePartWidth;
            this.zoneWidth = zoneWidth;
            this.circleRadius = circleRadius;
            this.sumAngle = sumAngle;
        }
    }
}