package com.isbirbilisim.pisti.cards.animations;

/**
 * Animation end listener.
 */
public interface OnAnimationFinishedListener {
    /**
     * Called when animation is finished.
     */
    void onAnimationFinished();
}