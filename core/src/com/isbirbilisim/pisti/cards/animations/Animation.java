package com.isbirbilisim.pisti.cards.animations;

import com.badlogic.gdx.scenes.scene2d.Action;

/**
 * Base interface for animations.
 */
public interface Animation {

    /**
     * Performs animation.
     */
    void perform();

    /**
     * Performs animation and runs postAction after that.
     *
     * @param postAction - additional action.
     * @param delayIndex - value in [0..1] range. Set to 0 if postAction
     *                   should be started at the same time as main animation.
     *                   Set to 1 if postAction should be performed when
     *                   animation ends.
     */
    void perform(final Action postAction, final float delayIndex);
}