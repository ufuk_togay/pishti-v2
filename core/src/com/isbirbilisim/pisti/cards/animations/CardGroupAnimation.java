package com.isbirbilisim.pisti.cards.animations;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;

import java.util.Queue;

/**
 * Card group animation. Use {@link Builder} Builder for creating new CardGroupAnimation.
 */
public class CardGroupAnimation implements Animation {
    private OnAnimationFinishedListener listener;
    private Queue<Animation> cardAnimationQueue;
    private Type type = Type.PARALLEL;
    private float delayIndex = 1;

    private Action postAction;

    private CardGroupAnimation() {
    }

    /**
     * Performs group card animation.
     */
    @Override
    public void perform() {
        switch (type) {
            case PARALLEL:
                performAnimationsParallel();
                break;
            case SEQUENCE:
                performAnimationsSequence();
                break;
        }
    }

    /**
     * Performs group card animation.
     */
    @Override
    public void perform(final Action postAction, final float delayIndex) {
        this.postAction = postAction;

        switch (type) {
            case PARALLEL:
                performAnimationsParallel();
                break;
            case SEQUENCE:
                performAnimationsSequence();
                break;
        }
    }


    private void performAnimationsParallel() {
        while (!cardAnimationQueue.isEmpty()) {
            if (cardAnimationQueue.size() == 1) {
                cardAnimationQueue.poll()
                        .perform(createAnimationFinishedAction(), 1);
            } else {
                cardAnimationQueue.poll().perform();
            }
        }
    }

    private Action createAnimationFinishedAction() {
        if (postAction != null) {
            return postAction;
        }

        return Actions.sequence(Actions.delay(0.1F),
                new RunnableAction() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onAnimationFinished();
                        }
                    }
                }
        );
    }

    private void performAnimationsSequence() {
        performNextAnimation(cardAnimationQueue.poll());
    }

    private void performNextAnimation(final Animation animation) {
        if (cardAnimationQueue.isEmpty()) {
            animation.perform(createAnimationFinishedAction(), 1);
        } else {
            animation.perform(
                    new RunnableAction() {
                        @Override
                        public void run() {
                            performNextAnimation(cardAnimationQueue.poll());
                        }
                    },
                    delayIndex
            );
        }
    }

    /**
     * Provides methods for building {@link CardGroupAnimation} group animation.
     */
    public static class Builder {
        private final CardGroupAnimation cardGroupAnimation;

        public Builder() {
            cardGroupAnimation = new CardGroupAnimation();
        }

        /**
         * Sets callback that will be triggered when group animation will be finished.
         *
         * @param listener - animation end listener.
         * @return - Builder instance.
         */
        public Builder setAnimationEndListener(
                final OnAnimationFinishedListener listener) {
            cardGroupAnimation.listener = listener;
            return this;
        }

        /**
         * Sets animation queue.
         *
         * @param cardAnimationQueue - queue with animations.
         * @return - Builder instance.
         */
        public Builder setAnimations(final Queue<Animation> cardAnimationQueue) {
            cardGroupAnimation.cardAnimationQueue = cardAnimationQueue;
            return this;
        }

        /**
         * Sets group animation type.
         *
         * @param type - type of order of animation performing.
         * @return - Builder instance.
         */
        public Builder setType(final Type type) {
            cardGroupAnimation.type = type;
            return this;
        }

        /**
         * Sets sequence animation delay index. If animation type is PARALLEL
         * value will be ignored. Default value = 1.
         *
         * @param delayIndex - sequence animation delay index.
         * @return - Builder instance.
         */
        public Builder setDelayIndex(final float delayIndex) {
            cardGroupAnimation.delayIndex = delayIndex;
            return this;
        }

        /**
         * Builds group animation with previously described params.
         *
         * @return - CardGroupAnimation instance.
         */
        public CardGroupAnimation build() {
            if (cardGroupAnimation.cardAnimationQueue == null
                    || cardGroupAnimation.cardAnimationQueue.isEmpty()) {
                throw new IllegalStateException("Call " +
                        "CardParallelAnimation.Builder#setAnimations() " +
                        "before calling CardParallelAnimation.Builder#build()");
            }

            return cardGroupAnimation;
        }
    }

    /**
     * Types of group animation.
     */
    public enum Type {
        PARALLEL,
        SEQUENCE
    }
}