package com.isbirbilisim.pisti.cards.animations;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.scenes.scene2d.actions.SizeToAction;
import com.badlogic.gdx.utils.Pool;
import com.isbirbilisim.pisti.cards.CardsSettings;
import com.isbirbilisim.pisti.cards.table.PlayerPlace;
import com.isbirbilisim.pisti.cards.table.PlayerWrapper;
import com.isbirbilisim.pisti.multimedia.CardSound;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.view.CardView;
import com.isbirbilisim.pisti.view.CardViewSettings;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Class that provides functionality for creating card animations.
 */
public class CardAnimationManager {
    private static final float CENTER_MAX_X_SHIFT = 50F;
    private static final float CENTER_MAX_Y_SHIFT = 25F;

    private static final float TOUCH_SCALE_ANIMATION_DURATION = 0.1F;
    private static final float MOVE_TO_CENTER_ANIMATION_DURATION = 0.7F;
    private static final float TOUCH_DRAGGED_ANIMATION_DURATION = 0.1F;
    private static final float COLLECT_CARD_ANIMATION_DURATION = 0.3F;
    private static final float INITIAL_PILE_CARD_ANIMATION_DURATION = 0.4F;
    private static final float GIVE_CARD_ANIMATION_DURATION = 0.9F;
    private static final float GIVE_CARD_ANIMATION_DELAY_INDEX = 0.35F;
    private static final float LOWER_TOP_CARDS_ANIMATION_DURATION = 0.2F;
    private static final float REVEAL_INITIAL_CARDS_ANIMATION_DURATION = 1.5F;
    private static final float MOVE_CARDS_ANIMATION_DURATION = 0.2F;
    private static final float SHUFFLE_DECK_ANIMATION_DURATION = 0.25F;
    private static final Interpolation MOVE_TO_CENTER_INTERPOLATION =
            Interpolation.pow2Out;
    private static final Interpolation TOUCH_DRAGGED_INTERPOLATION =
            Interpolation.pow2Out;
    private static final Interpolation COLLECT_CARD_INTERPOLATION =
            Interpolation.circle;
    private static final Interpolation INITIAL_PILE_INTERPOLATION =
            Interpolation.circle;
    private static final Interpolation GIVE_CARDS_INTERPOLATION =
            Interpolation.fade;
    private static final Interpolation LOWER_TOP_CARDS_INTERPOLATION =
            Interpolation.circle;
    private static final Interpolation REVEAL_INITIAL_CARDS_INTERPOLATION =
            Interpolation.exp10Out;
    private static final Interpolation MOVE_CARDS_ANIMATION_INTERPOLATION =
            Interpolation.circle;
    private static final Interpolation SHUFFLE_DECK_ANIMATION_INTERPOLATION =
            Interpolation.fade;

    private final float cardTableWidth;
    private final float cardTableHeight;
    private final MultimediaManager multimediaManager;

    private final Pool<MoveToAction> moveToActionPool = new Pool<MoveToAction>() {
        @Override
        protected MoveToAction newObject() {
            return new MoveToAction();
        }
    };

    private final Pool<RotateToAction> rotateToActionPool = new Pool<RotateToAction>() {
        @Override
        protected RotateToAction newObject() {
            return new RotateToAction();
        }
    };

    private final Pool<SizeToAction> sizeToActionPool = new Pool<SizeToAction>() {
        @Override
        protected SizeToAction newObject() {
            return new SizeToAction();
        }
    };

    private final Pool<ScaleByAction> scaleByActionPool = new Pool<ScaleByAction>() {
        @Override
        protected ScaleByAction newObject() {
            return new ScaleByAction();
        }
    };

    private int highestZIndex;

    public CardAnimationManager(final float cardTableWidth,
                                final float cardTableHeight,
                                final MultimediaManager multimediaManager) {
        this.cardTableWidth = cardTableWidth;
        this.cardTableHeight = cardTableHeight;
        this.multimediaManager = multimediaManager;

        highestZIndex = 100000;
    }

    public void reset() {
        highestZIndex = 100000;
    }

    private void increaseZIndex() {
        highestZIndex += 10000;
    }

    /**
     * Creates animation that shuffles deck.
     */
    public CardGroupAnimation obtainShuffleDeckAnimation(
            final CardsSettings cardsSettings,
            final List<CardView> cards,
            final OnAnimationFinishedListener listener) {
        final List<CardViewSettings> cardViewSettings = cardsSettings.getPileSettings();

        CardViewSettings settings;
        for (int i = 0; i < 30; i++) {
            final CardView cardView = cards.get(i);
            cardView.setVisible(false);

            settings = cardViewSettings.get(i);
            cardView.setPosition(settings.x, settings.y);
            cardView.setRotation(settings.rotation);
        }

        final Queue<Animation> cardSequenceAnimations = new LinkedList<Animation>();
        for (int i = 31; i < cards.size(); i++) {
            final MoveToAction moveToAction = moveToActionPool.obtain();
            final RotateToAction rotateToAction = rotateToActionPool.obtain();

            settings = cardViewSettings.get(i);
            moveToAction.setPosition(settings.x, settings.y);
            moveToAction.setDuration(SHUFFLE_DECK_ANIMATION_DURATION);
            moveToAction.setInterpolation(SHUFFLE_DECK_ANIMATION_INTERPOLATION);
            rotateToAction.setRotation(settings.rotation);
            rotateToAction.setDuration(SHUFFLE_DECK_ANIMATION_DURATION);
            rotateToAction.setInterpolation(SHUFFLE_DECK_ANIMATION_INTERPOLATION);

            cardSequenceAnimations.add(new SingleCardAnimation(cards.get(i),
                    Actions.parallel(moveToAction, rotateToAction),
                    SHUFFLE_DECK_ANIMATION_DURATION,
                    false,
                    multimediaManager,
                    CardSound.NONE));
        }

        final CardGroupAnimation cardSequenceAnimation = new CardGroupAnimation.Builder()
                .setAnimations(cardSequenceAnimations)
                .setAnimationEndListener(new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        for (int i = 0; i < 30; i++) {
                            cards.get(i).setVisible(true);
                        }
                        listener.onAnimationFinished();
                    }
                })
                .setType(CardGroupAnimation.Type.SEQUENCE)
                .setDelayIndex(0.2F)
                .build();

        return cardSequenceAnimation;
    }

    /**
     * Returns animation that performs card touch up animation.
     *
     * @param card - animated card.
     * @return - animation.
     */
    public SingleCardAnimation obtainTouchUpAnimation(final CardView card) {
        final ScaleByAction scale = scaleByActionPool.obtain();
        scale.setAmount(-0.2F, -0.2F);
        scale.setDuration(TOUCH_SCALE_ANIMATION_DURATION);

        return new SingleCardAnimation(card,
                scale,
                TOUCH_SCALE_ANIMATION_DURATION,
                false,
                multimediaManager,
                CardSound.NONE
        );
    }

    /**
     * Returns animation that performs card touch down animation.
     *
     * @param card - animated card.
     * @return - animation.
     */
    public SingleCardAnimation obtainTouchDownAnimation(final CardView card) {
        final ScaleByAction scale = scaleByActionPool.obtain();
        scale.setAmount(0.2F, 0.2F);
        scale.setDuration(TOUCH_SCALE_ANIMATION_DURATION);

        return new SingleCardAnimation(card,
                scale,
                TOUCH_SCALE_ANIMATION_DURATION,
                false,
                multimediaManager,
                CardSound.NONE
        );
    }

    public CardGroupAnimation obtainTouchMoveAnimation(final CardView cardView, float x, float y) {
        final MoveToAction move = moveToActionPool.obtain();
        move.setInterpolation(TOUCH_DRAGGED_INTERPOLATION);
        move.setPosition(x,y);
        final Queue<Animation> animations = new LinkedList<Animation>();
        final SingleCardAnimation singleCardAnimation = new SingleCardAnimation(cardView,
                Actions.parallel(move),
                TOUCH_DRAGGED_ANIMATION_DURATION,
                false,
                multimediaManager,
                CardSound.NONE);
        animations.add(singleCardAnimation);
        return new CardGroupAnimation.Builder()
                .setType(CardGroupAnimation.Type.PARALLEL)
                .setAnimations(animations)
                .build();
    }

    public CardGroupAnimation obtainReturnCardToPileAnimation(final CardView cardView, float x, float y) {
        final MoveToAction move = moveToActionPool.obtain();
        move.setInterpolation(MOVE_CARDS_ANIMATION_INTERPOLATION);
        move.setPosition(x,y);
        final Queue<Animation> animations = new LinkedList<Animation>();
        final SingleCardAnimation singleCardAnimation = new SingleCardAnimation(cardView,
                Actions.parallel(move),
                5f,
                false,
                multimediaManager,
                CardSound.NONE);
        animations.add(singleCardAnimation);
        return new CardGroupAnimation.Builder()
                .setType(CardGroupAnimation.Type.PARALLEL)
                .setAnimations(animations)
                .build();
    }

    /**
     * Returns Action that performs player move animation with random card rotation and
     * coordinates near to card table center.
     *
     * @return - player move animation Action.
     */
    public CardGroupAnimation obtainPlayerMoveAnimation(
            final PlayerPlace place,
            final CardView cardView,
            final OnAnimationFinishedListener listener,
            final CardViewSettings settings,
            final Vector2 center) {
        final MoveToAction move = moveToActionPool.obtain();
        final SizeToAction size = sizeToActionPool.obtain();
        final RotateToAction rotate = rotateToActionPool.obtain();
        final DelayAction delayAction = Actions.delay(
                MOVE_TO_CENTER_ANIMATION_DURATION / 4,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        increaseZIndex();
                        cardView.setZIndex(highestZIndex);
                    }
                })
        );

        move.setDuration(MOVE_TO_CENTER_ANIMATION_DURATION);
        size.setDuration(MOVE_TO_CENTER_ANIMATION_DURATION);
        rotate.setDuration(MOVE_TO_CENTER_ANIMATION_DURATION);
        move.setInterpolation(MOVE_TO_CENTER_INTERPOLATION);
        size.setInterpolation(MOVE_TO_CENTER_INTERPOLATION);
        rotate.setInterpolation(MOVE_TO_CENTER_INTERPOLATION);

        final Vector2 position = getRandomCenterPoint(
                center,
                settings.width,
                settings.height);
        move.setPosition(position.x, position.y);
        size.setSize(settings.width, settings.height);
        rotate.setRotation(getRandomRotation());

        final SingleCardAnimation singleCardAnimation = new SingleCardAnimation(cardView,
                Actions.parallel(move, size, rotate, delayAction),
                MOVE_TO_CENTER_ANIMATION_DURATION,
                place != PlayerPlace.BOTTOM,
                multimediaManager,
                CardSound.MOVE);
        final Queue<Animation> animations = new LinkedList<Animation>();
        animations.add(singleCardAnimation);

        return new CardGroupAnimation.Builder()
                .setType(CardGroupAnimation.Type.PARALLEL)
                .setAnimations(animations)
                .setAnimationEndListener(listener)
                .build();
    }

    private Vector2 getRandomCenterPoint(final Vector2 center,
                                         final float cardWidth,
                                         final float cardHeight) {
        final float centerX = center.x;
        final float centerY = center.y;

        final float shiftX = -CENTER_MAX_X_SHIFT + (float) (Math.random()
                * (2 * CENTER_MAX_X_SHIFT));
        final float shiftY = -CENTER_MAX_Y_SHIFT + (float) (Math.random()
                * (2 * CENTER_MAX_Y_SHIFT));

        return new Vector2(centerX + shiftX - (cardWidth / 2),
                centerY + shiftY - (cardHeight / 2));
    }

    private float getRandomRotation() {
        return -40 + (float) (Math.random()
                * (81));
    }

    /**
     * Returns animation that shows cards moving from center. Should be
     * performed before Pisti and Super Pisti animations.
     *
     * @param cards    - cards that are on table.
     * @param listener - animation end listener.
     * @return - animation instance.
     */
    public CardGroupAnimation obtainMoveCardsAnimation(final List<CardView> cards,
                                                       final OnAnimationFinishedListener listener) {
        final MoveToAction firstCardAnimation = moveToActionPool.obtain();
        firstCardAnimation.setDuration(MOVE_CARDS_ANIMATION_DURATION);
        firstCardAnimation.setInterpolation(MOVE_CARDS_ANIMATION_INTERPOLATION);
        firstCardAnimation.setPosition(cardTableWidth / 2 + (cardTableWidth / 4),
                cardTableHeight / 2 + (cardTableHeight / 4));

        final MoveToAction secondCardAnimation = moveToActionPool.obtain();
        secondCardAnimation.setDuration(MOVE_CARDS_ANIMATION_DURATION);
        secondCardAnimation.setInterpolation(MOVE_CARDS_ANIMATION_INTERPOLATION);
        secondCardAnimation.setPosition(
                cardTableWidth / 2 - (cardTableWidth / 4) - cards.get(1).getWidth(),
                cardTableHeight / 2 - (cardTableHeight / 4) - cards.get(1).getHeight());

        final Queue<Animation> animationQueue = new LinkedList<Animation>();
        animationQueue.add(new SingleCardAnimation(cards.get(0),
                firstCardAnimation,
                MOVE_CARDS_ANIMATION_DURATION,
                false,
                multimediaManager,
                CardSound.CARDS_BONUS));
        animationQueue.add(new SingleCardAnimation(cards.get(1),
                secondCardAnimation,
                MOVE_CARDS_ANIMATION_DURATION,
                false,
                multimediaManager,
                CardSound.CARDS_BONUS));

        return new CardGroupAnimation.Builder()
                .setAnimationEndListener(listener)
                .setType(CardGroupAnimation.Type.PARALLEL)
                .setAnimations(animationQueue)
                .build();
    }

    /**
     * Returns Action that performs collect card animation.
     *
     * @param place - card move place during collect cards animation.
     * @return - collect cards animation action.
     */
    public CardGroupAnimation obtainCollectCardAnimation(
            final List<CardView> cards,
            final PlayerPlace place,
            final CardsSettings settings,
            final OnAnimationFinishedListener listener) {
        final Vector2 position;

        switch (place) {
            case TOP:
                position = settings.getTopPlayerCollectCardPoint();
                break;
            case BOTTOM:
                position = settings.getBottomPlayerCollectCardPoint();
                break;
            case LEFT:
                position = settings.getLeftPlayerCollectCardPoint();
                break;
            case RIGHT:
                position = settings.getRightPlayerCollectCardPoint();
                break;
            default:
                position = null;
                break;
        }

        final Queue<Animation> collectCardsAnimations =
                new LinkedList<Animation>();
        for (final CardView card : cards) {
            final MoveToAction move = moveToActionPool.obtain();
            move.setDuration(COLLECT_CARD_ANIMATION_DURATION);
            move.setInterpolation(COLLECT_CARD_INTERPOLATION);
            move.setPosition(position.x, position.y);

            collectCardsAnimations.add(new SingleCardAnimation(card,
                    move,
                    COLLECT_CARD_ANIMATION_DURATION,
                    false,
                    multimediaManager,
                    CardSound.COLLECT_CARDS));
        }

        final CardGroupAnimation.Builder builder =
                new CardGroupAnimation.Builder()
                        .setAnimations(collectCardsAnimations);

        return builder
                .setAnimationEndListener(listener)
                .setType(CardGroupAnimation.Type.PARALLEL)
                .build();
    }

    public CardGroupAnimation obtainsShowInitialCardsAnimation(
            final List<CardView> cardsToShow,
            final List<CardView> topCards,
            final CardsSettings settings,
            final OnAnimationFinishedListener listener) {
        final Queue<Animation> lowerTopCardsAnimations =
                new LinkedList<Animation>();
        for (final CardView card : topCards) {
            final MoveByAction moveByAction = new MoveByAction();
            moveByAction.setDuration(LOWER_TOP_CARDS_ANIMATION_DURATION);
            moveByAction.setInterpolation(LOWER_TOP_CARDS_INTERPOLATION);
            moveByAction.setAmountY(-cardTableHeight / 4);

            lowerTopCardsAnimations.add(new SingleCardAnimation(card,
                    moveByAction,
                    LOWER_TOP_CARDS_ANIMATION_DURATION,
                    false,
                    multimediaManager,
                    CardSound.LOWER_CARDS));
        }

        final Queue<Animation> revealInitialCardsAnimations =
                new LinkedList<Animation>();

        final List<CardViewSettings> cardViewSettings = settings.getRevealCardsSettings();
        int i = 0;
        for (final CardView card : cardsToShow) {
            final CardViewSettings viewSettings = cardViewSettings.get(i++);
            final MoveToAction moveToAction = new MoveToAction();
            moveToAction.setDuration(REVEAL_INITIAL_CARDS_ANIMATION_DURATION);
            moveToAction.setInterpolation(REVEAL_INITIAL_CARDS_INTERPOLATION);
            moveToAction.setPosition(viewSettings.x, viewSettings.y);

            final RotateToAction rotateToAction = new RotateToAction();
            rotateToAction.setInterpolation(REVEAL_INITIAL_CARDS_INTERPOLATION);
            rotateToAction.setDuration(REVEAL_INITIAL_CARDS_ANIMATION_DURATION);
            rotateToAction.setRotation(viewSettings.rotation);

            revealInitialCardsAnimations.add(new SingleCardAnimation(card,
                    Actions.parallel(moveToAction, rotateToAction),
                    REVEAL_INITIAL_CARDS_ANIMATION_DURATION,
                    card.isHidden(),
                    multimediaManager,
                    CardSound.SHOW_INITIAL_PILE));
        }

        final CardGroupAnimation.Builder builder =
                new CardGroupAnimation.Builder()
                        .setAnimations(revealInitialCardsAnimations)
                        .setAnimationEndListener(listener)
                        .setType(CardGroupAnimation.Type.PARALLEL);
        final CardGroupAnimation revealInitialCardsAnimation = builder.build();

        return new CardGroupAnimation.Builder()
                .setAnimations(lowerTopCardsAnimations)
                .setType(CardGroupAnimation.Type.PARALLEL)
                .setAnimationEndListener(new OnAnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        increaseZIndex();
                        for (final CardView cardView : cardsToShow) {
                            cardView.setZIndex(highestZIndex);
                            cardView.setOrigin(cardView.getWidth() / 2,
                                    cardView.getHeight() / 2);
                        }
                        revealInitialCardsAnimation.perform();
                    }
                })
                .build();
    }

    /**
     * Returns CardGroupAnimation that animates initial pile.
     *
     * @param cards             - initial pile cards.
     * @param cardFinalSettings - cards final settings.
     * @param listener          - animation end listener.
     * @return - initial pile animation.
     */
    public CardGroupAnimation obtainInitialPileAnimation(
            final List<CardView> cards,
            final List<CardViewSettings> cardFinalSettings,
            final OnAnimationFinishedListener listener) {
        final Queue<Animation> initialPileAnimations =
                new LinkedList<Animation>();

        for (int i = 0; i < cards.size() - 1; i++) {
            final CardView card = cards.get(i);

            final RunnableAction runnableAction = new RunnableAction();
            runnableAction.setRunnable(new Runnable() {
                @Override
                public void run() {
                    increaseZIndex();
                    card.setZIndex(highestZIndex);
                }
            });

            initialPileAnimations.add(new SingleCardAnimation(card,
                    createInitialPileAnimation(cardFinalSettings.get(i),
                            runnableAction),
                    INITIAL_PILE_CARD_ANIMATION_DURATION,
                    false,
                    multimediaManager,
                    CardSound.INITIAL_PILE
            ));
        }

        final CardView cardView = cards.get(cards.size() - 1);
        final RunnableAction runnableAction = new RunnableAction();
        runnableAction.setRunnable(new Runnable() {
            @Override
            public void run() {
                increaseZIndex();
                cardView.setZIndex(highestZIndex);
            }
        });

        initialPileAnimations.add(new SingleCardAnimation(
                cards.get(cards.size() - 1),
                createInitialPileAnimation(
                        cardFinalSettings.get(cards.size() - 1),
                        runnableAction),
                INITIAL_PILE_CARD_ANIMATION_DURATION,
                true,
                multimediaManager,
                CardSound.INITIAL_PILE
        ));

        return new CardGroupAnimation.Builder()
                .setAnimationEndListener(listener)
                .setAnimations(initialPileAnimations)
                .setType(CardGroupAnimation.Type.SEQUENCE)
                .build();
    }

    private Action createInitialPileAnimation(
            final CardViewSettings cardFinalSettings,
            final RunnableAction updateZIndex) {
        final MoveToAction move = moveToActionPool.obtain();
        move.setDuration(INITIAL_PILE_CARD_ANIMATION_DURATION);
        move.setPosition(cardFinalSettings.x, cardFinalSettings.y);
        move.setInterpolation(INITIAL_PILE_INTERPOLATION);

        final SizeToAction size = sizeToActionPool.obtain();
        size.setDuration(INITIAL_PILE_CARD_ANIMATION_DURATION);
        size.setSize(cardFinalSettings.width, cardFinalSettings.height);
        size.setInterpolation(INITIAL_PILE_INTERPOLATION);

        final RotateToAction rotate = rotateToActionPool.obtain();
        rotate.setDuration(INITIAL_PILE_CARD_ANIMATION_DURATION);
        rotate.setRotation(cardFinalSettings.rotation);
        rotate.setInterpolation(INITIAL_PILE_INTERPOLATION);

        return Actions.parallel(move, size, rotate, updateZIndex);
    }

    /**
     * Returns group animation that gives cards to players.
     *
     * @param playerWrappers - list of players and their cards.
     * @param settings       - cards final settings.
     * @param listener       - animation end listener.
     * @return - give cards animation.
     */
    public CardGroupAnimation obtainGiveCardsAnimation(
            final int maxCardsInHands,
            final List<PlayerWrapper> playerWrappers,
            final CardsSettings settings,
            final OnAnimationFinishedListener listener) {
        final Queue<Animation> cardAnimations = initPlayersAnimations(
                maxCardsInHands,
                playerWrappers,
                settings);

        return new CardGroupAnimation.Builder()
                .setType(CardGroupAnimation.Type.SEQUENCE)
                .setDelayIndex(GIVE_CARD_ANIMATION_DELAY_INDEX)
                .setAnimations(cardAnimations)
                .setAnimationEndListener(listener)
                .build();
    }

    private Queue<Animation> initPlayersAnimations(
            final int maxCardsInHands,
            final List<PlayerWrapper> playerWrappers,
            final CardsSettings settings) {
        final Queue<Animation> animations = new LinkedList<Animation>();

        for (int i = 0; i < maxCardsInHands; i++) {
            for (final PlayerWrapper playerWrapper : playerWrappers) {
                final MoveToAction move = moveToActionPool.obtain();
                final SizeToAction size = sizeToActionPool.obtain();
                final RotateToAction rotate = rotateToActionPool.obtain();
                move.setDuration(GIVE_CARD_ANIMATION_DURATION);
                size.setDuration(GIVE_CARD_ANIMATION_DURATION);
                rotate.setDuration(GIVE_CARD_ANIMATION_DURATION);
                move.setInterpolation(GIVE_CARDS_INTERPOLATION);
                size.setInterpolation(GIVE_CARDS_INTERPOLATION);
                rotate.setInterpolation(GIVE_CARDS_INTERPOLATION);

                final CardViewSettings cardViewSettings;

                switch (playerWrapper.getPlayer().getPlaceOnTable()) {
                    case BOTTOM:
                        cardViewSettings = settings.getBottomPlayerCardSettings()
                                .get(i);
                        break;
                    case LEFT:
                        cardViewSettings = settings.getLeftPlayerCardSettings()
                                .get(i);
                        break;
                    case TOP:
                        cardViewSettings = settings.getTopPlayerCardSettings()
                                .get(i);
                        break;
                    case RIGHT:
                        cardViewSettings = settings.getRightPlayerCardSettings()
                                .get(i);
                        break;
                    default:
                        cardViewSettings = null;
                        break;
                }

                move.setPosition(cardViewSettings.x, cardViewSettings.y);
                size.setSize(cardViewSettings.width, cardViewSettings.height);
                rotate.setRotation(cardViewSettings.rotation);

                final CardView cardView = playerWrapper.getCardsViews().get(i);

                final RunnableAction updateZIndex = Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        increaseZIndex();
                        cardView.setZIndex(highestZIndex);
                    }
                });

                animations.add(new SingleCardAnimation(
                        cardView,
                        Actions.parallel(move, size, rotate, updateZIndex),
                        GIVE_CARD_ANIMATION_DURATION,
                        playerWrapper.getPlayer().getPlaceOnTable()
                                == PlayerPlace.BOTTOM,
                        multimediaManager,
                        CardSound.DEALING
                ));
            }
        }

        return animations;
    }
}