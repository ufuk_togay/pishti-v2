package com.isbirbilisim.pisti.cards.animations;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.isbirbilisim.pisti.multimedia.CardSound;
import com.isbirbilisim.pisti.multimedia.MultimediaManager;
import com.isbirbilisim.pisti.view.CardView;

/**
 * Contains Actor and Action that should be applied to this Actor.
 */
public class SingleCardAnimation implements Animation {
    private final CardView cardView;
    private final Action action;
    private final float duration;
    private final boolean flip;
    private final MultimediaManager multimediaManager;
    private final CardSound cardSound;

    public SingleCardAnimation(final CardView cardView,
                               final Action action,
                               final float duration,
                               final boolean flip,
                               final MultimediaManager multimediaManager,
                               final CardSound sound) {
        this.cardView = cardView;
        this.action = action;
        this.duration = duration;
        this.flip = flip;
        this.multimediaManager = multimediaManager;
        this.cardSound = sound;
    }

    /**
     * Performs Action.
     */
    @Override
    public void perform() {
        if (flip) {
            performFlipping();
        } else {
            cardView.addAction(this.action);
        }
        playSound();
    }

    /**
     * Performs additional action after performing of
     * {@link SingleCardAnimation#action}.
     *
     * @param action     - additional action (post animation action).
     * @param delayIndex - post animation action delay index.
     */
    @Override
    public void perform(final Action action, final float delayIndex) {
        if (flip) {
            performFlipping(action, delayIndex);
        } else {
            cardView.addAction(Actions.parallel(this.action,
                    Actions.delay(duration * delayIndex, action)));
        }
        playSound();
    }

    private void performFlipping() {
        cardView.flipCard(duration, action);
    }

    private void performFlipping(final Action action, final float delayIndex) {
        cardView.flipCard(duration, Actions.parallel(this.action,
                Actions.delay(duration * delayIndex, action)));
    }

    private void playSound() {
        multimediaManager.playCardSound(cardSound);
    }
}