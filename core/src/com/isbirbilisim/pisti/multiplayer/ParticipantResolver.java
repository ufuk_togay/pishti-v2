package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.players.Role;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates {@link com.isbirbilisim.pisti.players.Player} list, loads names,
 * scores.
 */
public class ParticipantResolver {

    private final LiveRoomInfoEvent liveRoomInfoEvent;
    private final UserInfo currentPlayer;
    private final List<Player> players;

    public ParticipantResolver(final LiveRoomInfoEvent liveRoomInfoEvent,
                               final UserInfo currentPlayer) {
        this.liveRoomInfoEvent = liveRoomInfoEvent;
        this.currentPlayer = currentPlayer;

        players = new ArrayList<>();

        initPlayers();
    }

    private void initPlayers() {
        for (final String userId : liveRoomInfoEvent.getJoinedUsers()) {
            final Player player;
            if (userId.contains(currentPlayer.getWarpId())) {
                player = new Player(Role.PLAYER, currentPlayer, PlayerState.PREPARING, userId);
            } else {
                player = new Player(Role.OPPONENT, parseUserInfo(userId), PlayerState.PREPARING, userId);
            }

            players.add(player);
        }
    }

    private UserInfo parseUserInfo(final String warpId) {
        final String[] data = warpId.split("###");
        final UserInfo userInfo = new UserInfo();
        userInfo.setFacebookId(data[0]);
        userInfo.setName(data[1]);
        userInfo.setWarpId(warpId);
        userInfo.setLevel(Level.LEVEL1);
        userInfo.setPlayerPhotoURL(ParticipantUtils.getUserPhotoURL(data[0]));
        // TODO get real level.

        return userInfo;
    }

    /**
     * Returns list of {@link com.isbirbilisim.pisti.players.Player} objects.
     *
     * @return - player list.
     */
    public List<Player> getPlayers() {
        return players;
    }
}