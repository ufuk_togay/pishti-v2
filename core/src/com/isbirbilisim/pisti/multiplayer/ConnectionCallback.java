package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.purchases.Item;

import java.net.URL;

/**
 * Class that should be implemented and registered using
 * {@link com.isbirbilisim.pisti.multiplayer.MultiplayerClient#registerConnectionCallbacks(ConnectionCallback)}
 * in order to receive callback about player's connection status.
 */
public interface ConnectionCallback {

    /**
     * Called when user successfully signed into game with his account.
     *
     * @param userInfo - current user info.
     */
    void onUserInfoUpdated(final UserInfo userInfo);

    /**
     * Called after processing purchase result.
     *
     * @param item    - purchased item.
     * @param success - true if purchase was successful.
     */
    void onUserPurchaseResult(final Item item, boolean success);

    /**
     * Called when user info request failed.
     */
    void onUserInfoRequestFailed(final UserRequestFail cause);

    /**
     * Called when user disconnected from game.
     */
    void onDisconnected();

    /**
     * Called when user was unable to join room.
     */
    void onJoinRoomError();

    /**
     * Called when user was unable to retrieve room info
     */
    void onGetRoomInfoError();

    /**
     * Called when user successfully joiner room and and started waiting for other players.
     */
    void onWaitingForPlayers();

    /**
     * Called when other player joins room.
     */
    void onPlayerConnected(final String userId,
                           final String playerName,
                           final Level level,
                           final URL photoUrl,
                           final boolean male);

    /**
     * Called when user is disconnected from waiting room.
     */
    void onPlayerDisconnected(final String userId);

    /**
     * Called when all players including current user are connected to room.
     */
    void onPlayersReady();

    enum UserRequestFail {
        CONNECTION_ERROR,
        OTHER_USER_SIGNED,
        STATISTIC_CONNECTION_ERROR,

        SUCCESS,
        AUTH_ERROR,
        RESOURCE_NOT_FOUND,
        RESOURCE_MOVED,
        BAD_REQUEST,
        CONNECTION_ERR,
        UNKNOWN_ERROR,
        RESULT_SIZE_ERROR,
        SUCCESS_RECOVERED,
        CONNECTION_ERR_RECOVERABLE,
        USER_PAUSED_ERROR
    }
}