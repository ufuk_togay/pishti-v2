package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.UserInfo;

/**
 * Interface for receiving user info.
 */
public interface OnUserInfoLoadedListener {

    /**
     * Called when user info gets loaded.
     *
     * @param userInfo - loaded user info.
     */
    void onUserInfoLoaded(final UserInfo userInfo);

    /**
     * Called when user info request failed.
     */
    void onUserInfoRequestFailed();
}