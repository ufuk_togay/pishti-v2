package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.game.GameSettings;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;

import java.util.HashMap;

class RoomPropertiesHelper {

    private static final String LEVELS_KEY = "level";
    private static final String PLAYERS_KEY = "players";
    private static final String GAME_LENGTH_KEY = "game_length";
    private static final String GAME_STATUS = "game_status";
    private static final String GAME_STARTED = "game_started";
    private static final String GAME_SEARCHING = "game_searching";

    private final HashMap<String, Object> roomProperties;
    private final GameSettings gameSettings;
    private final int level;

    RoomPropertiesHelper(final GameSettings gameSettings,
                         final int level) {
        roomProperties = new HashMap<>();
        roomProperties.put(LEVELS_KEY, level);
        roomProperties.put(PLAYERS_KEY, gameSettings.numberOfPlayers.getNumberOfPlayers());
        roomProperties.put(GAME_LENGTH_KEY, gameSettings.gameLength.getGameLength());
        roomProperties.put(GAME_STATUS, GAME_SEARCHING);

        this.gameSettings = gameSettings;
        this.level = level;
    }

    public static boolean isRoomReady(final HashMap<String, Object> properties) {
        return properties.get(GAME_STATUS).equals(GAME_STARTED);
    }

    public HashMap<String, Object> getRoomProperties() {
        return roomProperties;
    }

    public HashMap<String, Object> getRoomPropertiesWithStartedGame() {
        roomProperties.put(GAME_STATUS, GAME_STARTED);
        return roomProperties;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public int getLevel() {
        return level;
    }

    public static boolean allPlayersConnected(final LiveRoomInfoEvent liveRoomInfoEvent) {
        return liveRoomInfoEvent.getJoinedUsers().length
                == (Integer) liveRoomInfoEvent.getProperties().get(PLAYERS_KEY);
    }
}