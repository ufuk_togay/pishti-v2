package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.players.Player;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Class that helps to work with game participants.
 */
public class ParticipantUtils {

    /**
     * Randomizes player from given list.
     *
     * @param players - current players.
     * @return - random player from list.
     */
    public static String randomFirstTurnPlayerParticipantId(
            final List<Player> players) {
        return players.get(new Random().nextInt(players.size())).getUserInfo().getFacebookId();
    }

    /**
     * Returns player, that will decide which player will move first.
     *
     * @param participants - current players.
     * @return - player that should take move first.
     */
    public static Player getFirstTurnDecider(final List<Player> participants) {
        Collections.sort(participants, new Comparator<Player>() {
            @Override
            public int compare(final Player left,
                               final Player right) {
                return left.getUserInfo().getFacebookId()
                        .compareTo(right.getUserInfo().getFacebookId());
            }
        });

        return participants.get(0);
    }

    /**
     * Returns player from given list with matching facebookId.
     *
     * @param players    - current players.
     * @param facebookId - facebookId.
     * @return - matching player.
     */
    public static Player getPlayerByFacebookId(final List<Player> players,
                                               final String facebookId) {
        for (final Player player : players) {
            if (player.getUserInfo().getFacebookId().equals(facebookId)) {
                return player;
            }
        }

        throw new IllegalArgumentException("Unknown facebookId");
    }

    /**
     * Returns player from given list with matching warpId.
     *
     * @param players - current players.
     * @param warpId  - warpId.
     * @return - matching player.
     */
    public static Player getPlayerByWarpId(final List<Player> players,
                                           final String warpId) {
        for (final Player player : players) {
            if (player.getUserInfo().getWarpId().equals(warpId)) {
                return player;
            }
        }

        throw new IllegalArgumentException("Unknown warpId");
    }

    public static URL getUserPhotoURL(final String userId) {
        try {
            return new URL("https://graph.facebook.com/" + userId + "/picture/?width=128&height=128");
        } catch (final MalformedURLException exception) {
            return null;
        }
    }
}