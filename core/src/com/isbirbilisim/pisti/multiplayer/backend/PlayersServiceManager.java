package com.isbirbilisim.pisti.multiplayer.backend;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.OnUserInfoSyncListener;
import com.isbirbilisim.pisti.account.SyncResult;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.background.BackgroundTasksManager;
import com.isbirbilisim.pisti.background.Header;
import com.isbirbilisim.pisti.background.OnNetworkTaskExecutedListener;
import com.isbirbilisim.pisti.background.tasks.GetUserInfoTask;
import com.isbirbilisim.pisti.background.tasks.UpdateUserInfoTask;
import com.isbirbilisim.pisti.multiplayer.OnUserInfoLoadedListener;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookProfile;
import com.isbirbilisim.pisti.security.EncodingUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Provides methods for loading user info.
 */
public class PlayersServiceManager {

    private static final String CONTENT_TYPE_HEADER_NAME = "Content-Type";
    private static final String CONTENT_TYPE_HEADER_VALUE = "application/json";

    private static final String SIGNATURE_HEADER = "My-Authorization";

    private static final String DATE_FIELD = "Date";
    private static final String AGENT_TYPE_FIELD = "Agent-Type";
    private static final String AGENT_TYPE_ANDROID = "Android";

    private static final String TAG =
            PlayersServiceManager.class.getSimpleName();

    private final SignatureUtils signatureUtils;

    private UserInfo info;

    public PlayersServiceManager(final SignatureUtils utils) {
        this.signatureUtils = utils;
    }

    /**
     * Returns last loaded user info or null if info wasn't loaded.
     *
     * @return - {@link com.isbirbilisim.pisti.account.UserInfo} instance.
     */
    public UserInfo getLastUserInfo() {
        return info;
    }

    /**
     * Loads current user's info from backend by its player ID. If user connects
     * first time new account will be created for him at backend.
     *
     * @param player   - current player.
     * @param isMale   - player.
     * @param listener - operation completion listener.
     */
    public void loadUserInfo(final FacebookProfile player,
                             final boolean isMale,
                             final OnUserInfoLoadedListener listener) {
        final List<Header> headers = new ArrayList<Header>();

        headers.add(new Header(AGENT_TYPE_FIELD, AGENT_TYPE_ANDROID));
        headers.add(new Header(DATE_FIELD, getCurrentDate()));
        headers.add(new Header(CONTENT_TYPE_HEADER_NAME, CONTENT_TYPE_HEADER_VALUE));

        BackgroundTasksManager.getInstance().postTask(new GetUserInfoTask(
                player.getId(),
                isMale,
                headers,
                new OnNetworkTaskExecutedListener() {
                    @Override
                    public void onTaskCompleted(final String response) {
                        if (!response.contains("ERROR")) {
                            final UserInfo userInfo = new Json()
                                    .fromJson(UserInfo.class, response);
                            refreshLevel(userInfo);

                            setUserFacebookInfo(player, userInfo);
                            info = userInfo;
                            listener.onUserInfoLoaded(userInfo);
                        } else {
                            listener.onUserInfoRequestFailed();
                        }
                    }

                    @Override
                    public void onTaskFailed() {
                        listener.onUserInfoRequestFailed();
                    }
                }
        ));
    }

    private String getCurrentDate() {
        final Date date = Calendar.getInstance().getTime();
        final DateFormat format = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
        return format.format(date);
    }

    private void refreshLevel(final UserInfo info) {
        info.setLevel(Level.fromGold(info.getGold()));
    }

    private void setUserFacebookInfo(final FacebookProfile player,
                                     final UserInfo userInfo) {
        userInfo.setFacebookId(player.getId());
        userInfo.setName(player.getName());
    }

    /**
     * Synchronizes user info with remote server.
     *
     * @param info     - info that will be pushed to remote server.
     * @param listener - sync listener.
     */
    public void syncUserInfo(final UserInfo info,
                             final OnUserInfoSyncListener listener) {
        refreshLevel(info);

        final String stringDate = getCurrentDate();

        final Json json = new Json();
        json.setOutputType(JsonWriter.OutputType.json);
        final String userInfoJsonString = json.toJson(info, UserInfo.class);
        final Header signatureHeader;
        try {
            signatureHeader = new Header(SIGNATURE_HEADER,
                    EncodingUtils.encodeContent(
                            info.getFacebookId(),
                            userInfoJsonString,
                            stringDate,
                            CONTENT_TYPE_HEADER_VALUE,
                            "TopSecretAndroid"));
            signatureUtils.getAppSignature();

        } catch (final EncodingUtils.EncodingException exception) {
            listener.onSyncResult(info, SyncResult.ENCODING_ERROR);
            return;
        }
        final List<Header> headers = new ArrayList<Header>();
        headers.add(signatureHeader);
        headers.add(new Header(CONTENT_TYPE_HEADER_NAME, CONTENT_TYPE_HEADER_VALUE));
        headers.add(new Header(AGENT_TYPE_FIELD, AGENT_TYPE_ANDROID));
        headers.add(new Header(DATE_FIELD, stringDate));

        BackgroundTasksManager.getInstance().postTask(new UpdateUserInfoTask(
                new OnNetworkTaskExecutedListener() {
                    @Override
                    public void onTaskCompleted(final String response) {
                        if (listener == null) {
                            return;
                        }

                        if (response.contains("OK")) {
                            listener.onSyncResult(info, SyncResult.SUCCESS);
                        } else if (response.contains("ERROR")) {
                            listener.onSyncResult(info, SyncResult.SERVER_NOT_AVAILABLE);
                        } else {
                            listener.onSyncResult(info, SyncResult.SERVER_NOT_AVAILABLE);
                        }
                    }

                    @Override
                    public void onTaskFailed() {
                        if (listener == null) {
                            return;
                        }

                        listener.onSyncResult(info,
                                SyncResult.SERVER_NOT_AVAILABLE);
                    }
                },
                headers,
                userInfoJsonString
        ));
    }

}