package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.multiplayer.messages.Message;
import com.isbirbilisim.pisti.multiplayer.messages.MessagingHelper;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.players.PlayerUtils;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;

import java.util.HashMap;
import java.util.List;

/**
 * Class that contains data about particular match.
 */
public class MultiplayerMatch {

    private final RoomPropertiesHelper roomPropertiesHelper;
    private final UserInfo currentuserInfo;
    private final GameManager gameManager;

    private RoomData roomData;
    private GameInitializationHelper gameInitializationHelper;
    private MessagingHelper messagingHelper;

    private boolean createdByCurrentUser;

    private boolean started;

    public MultiplayerMatch(final GameSettings gameSettings,
                            final int gameVariant,
                            final MessagingHelper messagingHelper,
                            final UserInfo currentUserInfo,
                            final GameManager gameManager) {
        roomPropertiesHelper = new RoomPropertiesHelper(gameSettings, gameVariant);
        this.messagingHelper = messagingHelper;
        this.currentuserInfo = currentUserInfo;
        this.gameManager = gameManager;
    }

    public static boolean isRoomReady(final HashMap<String, Object> properties) {
        return RoomPropertiesHelper.isRoomReady(properties);
    }

    public HashMap<String, Object> getRoomProperties() {
        return roomPropertiesHelper.getRoomProperties();
    }

    public HashMap<String, Object> getRoomPropertiesWithStartedGame() {
        return roomPropertiesHelper.getRoomPropertiesWithStartedGame();
    }

    public String getRoomId() {
        return roomData.getId();
    }

    public GameSettings getGameSettings() {
        return roomPropertiesHelper.getGameSettings();
    }

    public List<Player> getPlayers() {
        return gameInitializationHelper.getPlayers();
    }

    public void setRoomData(final RoomData roomData) {
        this.roomData = roomData;
    }

    public void setLiveRoomInfo(final LiveRoomInfoEvent liveRoomInfoEvent) {
        gameInitializationHelper = new GameInitializationHelper(messagingHelper,
                gameManager,
                liveRoomInfoEvent,
                currentuserInfo);
        roomData = liveRoomInfoEvent.getData();
    }

    public void initGame() {
        started = true;

        gameInitializationHelper.initGame(roomPropertiesHelper.getGameSettings());
    }

    public void sendMessageFromPlayer(final Message message) {
        messagingHelper.sendMessageFromPlayer(message);
    }

    public void setPlayerLeftGame(final String warpId) {
        final Player player = ParticipantUtils.getPlayerByWarpId(gameInitializationHelper.getPlayers(),
                warpId);

        gameManager.disconnectPlayer(player);
    }

    public boolean isGameStarted() {
        return started;
    }

    public void setCurrentPlayerReadyToSync() {
        PlayerUtils.getCurrentPlayer(getPlayers()).setState(PlayerState.READY_TO_SYNC);
    }

    public void setPlayerReadyToSync(final String userId) {
        ParticipantUtils.getPlayerByWarpId(getPlayers(), userId).setState(PlayerState.READY_TO_SYNC);
    }

    public boolean isPlayersReadyToSync() {
        for (final Player player : getPlayers()) {
            if (player.getState() != PlayerState.READY_TO_SYNC) {
                return false;
            }
        }

        return true;
    }

    public boolean isCreatedByCurrentUser() {
        return createdByCurrentUser;
    }

    public void setCreatedByCurrentUser() {
        this.createdByCurrentUser = true;
    }
}