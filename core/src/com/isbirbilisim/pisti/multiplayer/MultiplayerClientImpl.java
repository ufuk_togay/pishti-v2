package com.isbirbilisim.pisti.multiplayer;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.OnUserInfoSyncListener;
import com.isbirbilisim.pisti.account.SyncResult;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.backend.CoinsManager;
import com.isbirbilisim.pisti.backend.StatisticManager;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookCallbacks;
import com.isbirbilisim.pisti.multiplayer.facebook.FacebookProfile;
import com.isbirbilisim.pisti.multiplayer.messages.InputMessageProcessor;
import com.isbirbilisim.pisti.multiplayer.messages.MessageCreator;
import com.isbirbilisim.pisti.multiplayer.messages.MessagingHelper;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.scores.Achievement;
import com.isbirbilisim.pisti.scores.Leaderboard;
import com.isbirbilisim.pisti.scores.Scores;
import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyData;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ChatRequestListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.LobbyRequestListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener;
import com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener;
import com.shephertz.app42.paas.sdk.java.App42API;
import com.shephertz.app42.paas.sdk.java.social.Social;
import com.shephertz.app42.paas.sdk.java.social.SocialService;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MultiplayerClientImpl implements MultiplayerClient {

    private static final String TAG = MultiplayerClientImpl.class.getSimpleName();

    private static final String API_KEY = "92f3294abd1c60f5b8a801b9cfbf2378a2378f160c1311203558ee1c32fa10e9";
    private static final String SECRET_KEY = "688d2528b7ae2788d950174210e0b74b18d00af5cf9ac21cc8c7514d1b4ab917";

    private static final String FACEBOOK_APP_ID = "505127956262208";
    private static final String FACEBOOK_APP_SECRET = "f33b49a8b0ebb3b73771d1c36f44af14";

    private static final String ROOM_NAME = "Pisti";
    public static final int FIRST_RUN_COINS = 1000;

    private final Pisti pisti;
    private final SocialService socialService;

    private final ChatRequestListenerImpl chatRequestListener;
    private final ConnectionRequestListenerImpl connectionRequestListener;
    private final ZoneListenerImpl zoneListener;
    private final LobbyRequestListenerImpl lobbyRequestListener;
    private final RoomRequestListenerImpl roomRequestListener;
    private final NotifyListenerImpl notifyListener;
    private final FacebookCallbacksImpl facebookCallbacks;

    private final List<ConnectionCallback> connectionListeners = new LinkedList<>();

    private GameManager gameManager;
    private Social social;
    private WarpClient warpClient;

    private MessagingHelper messagingHelper;

    boolean connected = false;

    /*
     * Temporary object.
     */
    private MultiplayerMatch multiplayerMatch;

    private UserInfo currentUserInfo;

    public MultiplayerClientImpl(final Pisti pisti) {
        this.pisti = pisti;
        connectionRequestListener = new ConnectionRequestListenerImpl(this);
        zoneListener = new ZoneListenerImpl(this);
        chatRequestListener = new ChatRequestListenerImpl(this);
        lobbyRequestListener = new LobbyRequestListenerImpl(this);
        roomRequestListener = new RoomRequestListenerImpl(this);
        notifyListener = new NotifyListenerImpl(this);
        facebookCallbacks = new FacebookCallbacksImpl(this, pisti.getConnectivityUtils(), pisti.getFacebookClient());

        pisti.getFacebookClient().addFacebookCallbacks(facebookCallbacks);

        socialService = App42API.buildSocialService();
    }

    @Override
    public void signIn() {
        final Facebook facebook = pisti.getFacebookClient();

        if (facebook.isSignedIn()) {
            facebook.loadUserProfile();
        } else {
            facebook.signIn();
        }
    }

    private void initSocial(final FacebookProfile facebookProfile) {
        social = socialService.linkUserFacebookAccount(
                facebookProfile.getName(),
                facebookProfile.getAccessToken(),
                FACEBOOK_APP_ID,
                FACEBOOK_APP_SECRET);

        final Social.FacebookProfile profile = social.getFacebookProfile();
        log(profile.getFirstName()
                + "\n" + profile.getLastName()
                + "\n" + profile.getGender()
                + "\n" + profile.getLocale()
                + "\n" + profile.getLink()
                + "\n" + profile.getId());
    }

    private void initWarp(final FacebookProfile facebookProfile) {
        WarpClient.initialize(API_KEY, SECRET_KEY);

        try {
            warpClient = WarpClient.getInstance();
            messagingHelper = new MessagingHelper(warpClient);
        } catch (final Exception exception) {
            for (final ConnectionCallback listener : connectionListeners) {
                listener.onDisconnected();
            }
            return;
        }

        warpClient.addConnectionRequestListener(connectionRequestListener);
        warpClient.addZoneRequestListener(zoneListener);
        warpClient.addLobbyRequestListener(lobbyRequestListener);
        warpClient.addRoomRequestListener(roomRequestListener);
        warpClient.addNotificationListener(notifyListener);
        warpClient.addChatRequestListener(chatRequestListener);

        /**
         * Connecting with string which contains facebook user id and name in following format:
         * "userID###user_name", for instance, "1015382424267###Marks Kuspis".
         * That's why we need it:
         * 1. We will be able to identify uniquely each user by using facebook ID.
         * 2. We will be able to retrieve user name easily later on.
         */
        warpClient.connectWithUserName(facebookProfile.getId() + "###" + facebookProfile.getName());
    }

    @Override
    public void signOut() {
        currentUserInfo = null;
        connected = false;

        warpClient.disconnect();
        pisti.getFacebookClient().signOut();
    }

    @Override
    public boolean isSignedIn() {
        return connected;
    }

    @Override
    public UserInfo getCurrentUserInfo() {
        return currentUserInfo;
    }

    @Override
    public void registerConnectionCallbacks(final ConnectionCallback callback) {
        connectionListeners.add(callback);
    }

    @Override
    public void unregisterConnectionCallbacks(final ConnectionCallback callback) {
        connectionListeners.remove(callback);
    }

    @Override
    public void clearConnectionCallbacks() {
        connectionListeners.clear();
    }

    @Override
    public void registerGameManagerDelegate(final GameManager instance) {
        gameManager = instance;
    }

    @Override
    public void unregisterGameManagerDelegate() {
        gameManager = null;
    }

    @Override
    public void syncUserInfo(final UserInfo userInfo, final OnUserInfoSyncListener listener) {
        CoinsManager.getInstance().saveData(userInfo.getWarpId(), userInfo.getGold());
        if (listener != null) {
            listener.onSyncResult(userInfo, SyncResult.SUCCESS);
        }
    }

    @Override
    public void sendPlayerStateMessage(final PlayerState state) {
        multiplayerMatch.sendMessageFromPlayer(MessageCreator.createPlayerStateMessage(state));
    }

    @Override
    public void quickGame(final GameSettings gameSettings, final int gameVariant) {
        log("Quick game");

        multiplayerMatch = new MultiplayerMatch(gameSettings,
                gameVariant,
                messagingHelper,
                getCurrentUserInfo(),
                gameManager);

        /**
         * Tries to join room with specified params. Result will be put in {@link RoomRequestListenerImpl#onJoinRoomDone(RoomEvent)}
         */
        warpClient.joinRoomWithProperties(multiplayerMatch.getRoomProperties());
    }

    @Override
    public void cancelGameSearch() {
        if (multiplayerMatch != null) {
            warpClient.unsubscribeRoom(multiplayerMatch.getRoomId());
            warpClient.leaveRoom(multiplayerMatch.getRoomId());
        }
    }

    @Override
    public void playWithFriends(final GameSettings gameSettings) {

    }

    @Override
    public void inviteFriends() {

    }

    @Override
    public void sentMoveMessage(final CardModel cardModel) {
        multiplayerMatch.sendMessageFromPlayer(MessageCreator.createMoveDescriptionMessage(cardModel));
    }

    @Override
    public void sendChatMessage(final String message) {
        multiplayerMatch.sendMessageFromPlayer(MessageCreator.createChatMessage(message));
    }

    @Override
    public void endGame() {
        warpClient.unsubscribeRoom(multiplayerMatch.getRoomId());
        warpClient.leaveRoom(multiplayerMatch.getRoomId());

        multiplayerMatch = null;
    }

    @Override
    public void unlockAchievement(final Achievement achievement) {

    }

    @Override
    public void showLeaderboard(final Leaderboard leaderboard) {

    }

    private void processWarpInitialized(final boolean success,
                                        final ConnectionCallback.UserRequestFail cause) {
        connected = success;

        for (final ConnectionCallback connectionCallback : connectionListeners) {
            if (success) {
                connectionCallback.onUserInfoUpdated(currentUserInfo);
            } else {
                connectionCallback.onUserInfoRequestFailed(cause);
            }
        }
    }

    private void processFacebookLogOut() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                /**
                 * ConcurrentModificationException sometimes is thrown on desktop version.
                 */
                synchronized (connectionListeners) {
                    for (final ConnectionCallback listener : connectionListeners) {
                        listener.onDisconnected();
                    }
                }
            }
        });
    }

    private void processJoinRoomError() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                for (final ConnectionCallback connectionCallback : connectionListeners) {
                    connectionCallback.onJoinRoomError();
                }
            }
        });
    }

    private void processGetRoomInfoError() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                for (final ConnectionCallback connectionCallback : connectionListeners) {
                    connectionCallback.onGetRoomInfoError();
                }
            }
        });
    }

    private void processWaitingForPlayers() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                for (final ConnectionCallback connectionCallback : connectionListeners) {
                    connectionCallback.onWaitingForPlayers();
                }
            }
        });
    }

    private void processOpponentJoinedRoom(final String userId,
                                           final String userName,
                                           final Level level,
                                           final URL photoUrl) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                for (final ConnectionCallback connectionCallback : connectionListeners) {
                    connectionCallback.onPlayerConnected(userId, userName, level, photoUrl, true);
                }
            }
        });
    }

    private void processPlayersConnected(final LiveRoomInfoEvent event) {
        multiplayerMatch.setLiveRoomInfo(event);
    }

    private void processPlayersInfoRetrieved() {
        log("retrieved info of all players");

        multiplayerMatch.setCurrentPlayerReadyToSync();
        multiplayerMatch.sendMessageFromPlayer(MessageCreator.createPlayerStateMessage(PlayerState.READY_TO_SYNC));

        if (multiplayerMatch.isPlayersReadyToSync()) {
            processAllPlayersReadyToSync();
        }
    }

    /**
     * Called from MultiplayerGameManager.
     */
    public void processPlayerReadyToSyncMessage(final String playerId) {
        log(playerId + " is ready to sync");

        multiplayerMatch.setPlayerReadyToSync(playerId);

        if (multiplayerMatch.isPlayersReadyToSync()) {
            processAllPlayersReadyToSync();
        }
    }

    private void processAllPlayersReadyToSync() {
        if (multiplayerMatch.isCreatedByCurrentUser()) {
            /**
             * Marking room as one with started match. Its needed in case when
             * someone leaves game and other players search room with same criteria.
             *
             * Result will be put in {@link NotifyListenerImpl#onUserChangeRoomProperty}
             */
            warpClient.updateRoomProperties(multiplayerMatch.getRoomId(),
                    multiplayerMatch.getRoomPropertiesWithStartedGame(), null);
        }
    }

    private void processGameIsReady() {
        for (final ConnectionCallback connectionCallback : connectionListeners) {
            connectionCallback.onPlayersReady();
        }
        multiplayerMatch.initGame();
    }

    private void processPlayerLeftGame(final String warpId) {
        if (multiplayerMatch != null) {
            if (multiplayerMatch.isGameStarted()) {
                multiplayerMatch.setPlayerLeftGame(warpId);
            } else {
                for (final ConnectionCallback connectionCallback : connectionListeners) {
                    connectionCallback.onPlayerDisconnected(warpId);
                }
            }
        }
    }

    private void initUserFacebookData(final FacebookProfile facebookProfile) {
        if (currentUserInfo == null) {
            currentUserInfo = new UserInfo();
        }

        currentUserInfo = new UserInfo();
        currentUserInfo.setName(facebookProfile.getName());
        currentUserInfo.setFacebookId(facebookProfile.getId());
        currentUserInfo.setWarpId(facebookProfile.getId() + "###" + facebookProfile.getName());
        currentUserInfo.setPlayerPhotoURL(ParticipantUtils.getUserPhotoURL(facebookProfile.getId()));
        currentUserInfo.setLevel(Level.LEVEL5);
        currentUserInfo.setScores(new Scores());
    }

    private void initUserStatsData(final UserStatisticModel stats) {
        currentUserInfo.setGames(stats.getCompletedGamesCount());
        currentUserInfo.setWins(stats.getWinGamesCount());
        currentUserInfo.setLoses(stats.getLostGameCount());
        currentUserInfo.setDraws(stats.getDrawGamesCount());
        currentUserInfo.setScores(new Scores(
                stats.getTotalScoresCount(),
                stats.getPistiScoresCount(),
                stats.getSuperPistiScoresCount()));
    }

    private void initUserGoldData(final long gold) {
        currentUserInfo.setGold(gold);
    }

    private void requestOpponentGold(final String userId,
                                     final Runnable completitionCallback) {
        CoinsManager.getInstance().loadCoinsData(
                userId,
                new CoinsManager.CoinsManagerCallBack() {
                    @Override
                    public void onComplete(final long coins) {
                        final String opponentName = getUserNameFromWarpId(userId);
                        final URL opponentPhoto = ParticipantUtils.getUserPhotoURL(getFacebookIdFromWarpId(userId));
                        final Level opponentLevel = Level.fromGold(coins);

                        processOpponentJoinedRoom(userId, opponentName, opponentLevel, opponentPhoto);

                        completitionCallback.run();
                    }

                    @Override
                    public void onFirstRun() {
                        log("wtf: opponent joins game without gold");
                    }

                    @Override
                    public void onFail() {
                        log("opponent gold amount request failed.");
                    }
                }
        );

    }

    private void requestConnectedOpponentsInfo(final LiveRoomInfoEvent liveRoomInfoEvent,
                                               final boolean shouldStartGame) {
        final String[] joinedUsers = liveRoomInfoEvent.getJoinedUsers();

        final Queue<String> users = new LinkedList<>(Arrays.asList(joinedUsers));
        requestPlayersInfo(users, new Runnable() {
            @Override
            public void run() {
                if (shouldStartGame) {
                    /**
                     * We can start handshake between players.
                     */
                    processPlayersInfoRetrieved();
                }
            }
        });
    }

    private void requestPlayersInfo(final Queue<String> players,
                                    final Runnable completitionCallback) {
        if (players.isEmpty()) {
            completitionCallback.run();
            return;
        }

        String userId = players.poll();
        if (userId.equals(currentUserInfo.getWarpId())) {
            if (players.isEmpty()) {
                completitionCallback.run();
                return;
            } else {
                userId = players.poll();
            }
        }

        requestOpponentGold(userId, new Runnable() {
            @Override
            public void run() {
                requestPlayersInfo(players, completitionCallback);
            }
        });
    }

    private String getUserNameFromWarpId(final String warpId) {
        return warpId.substring(warpId.indexOf("###") + 3);
    }

    private String getFacebookIdFromWarpId(final String warpId) {
        return warpId.substring(0, warpId.indexOf("###"));
    }

    private void log(final String message) {
        Gdx.app.log(TAG, message);
    }

    private static class FacebookCallbacksImpl implements FacebookCallbacks {

        private final MultiplayerClientImpl multiplayerClient;
        private final ConnectivityUtils connectivityUtils;
        private final Facebook facebook;
        private FacebookProfile lastUserProfile;

        private FacebookCallbacksImpl(final MultiplayerClientImpl multiplayerClient,
                                      final ConnectivityUtils connectivityUtils,
                                      final Facebook facebook) {
            this.multiplayerClient = multiplayerClient;
            this.connectivityUtils = connectivityUtils;
            this.facebook = facebook;
        }

        @Override
        public void onUserSignedIn() {
            if (!connectivityUtils.isNetworkAvailable()) {
                multiplayerClient.processWarpInitialized(false,
                        ConnectionCallback.UserRequestFail.CONNECTION_ERROR);
                return;
            }

            // When user successfully signed into facebook we should get info about him (required for initializing AppWarp)
            facebook.loadUserProfile();
        }

        @Override
        public void onUserSignedOut() {
            lastUserProfile = null;

            multiplayerClient.processFacebookLogOut();
        }

        @Override
        public void onUserProfileReceived(final FacebookProfile profile) {
            lastUserProfile = profile;
            multiplayerClient.initUserFacebookData(profile);

            final String userId = multiplayerClient.currentUserInfo.getWarpId();

            StatisticManager.getInstance().loadMultiPlayerStatistic(
                    userId,
                    new StatisticManager.DataLoadCallback() {
                        @Override
                        public void onDataLoaded(final UserStatisticModel userStatistic) {
                            multiplayerClient.initUserStatsData(userStatistic);
                            CoinsManager.getInstance().loadCoinsData(
                                    userId,
                                    new CoinsManager.CoinsManagerCallBack() {
                                        @Override
                                        public void onComplete(final long coins) {
                                            multiplayerClient.initUserGoldData(coins);
                                            // After receiving user profile we should init AppWarp and SocialClient.
                                            //multiplayerClient.initSocial(profile);
                                            multiplayerClient.initWarp(profile);
                                        }

                                        @Override
                                        public void onFirstRun() {
                                            multiplayerClient.initUserGoldData(FIRST_RUN_COINS);
                                            // After receiving user profile we should init AppWarp and SocialClient.
                                            //multiplayerClient.initSocial(profile);
                                            CoinsManager.getInstance().saveData(userId, FIRST_RUN_COINS);
                                            multiplayerClient.initWarp(profile);
                                        }

                                        @Override
                                        public void onFail() {
                                            System.err.println("Loading player data failed");
                                        }
                                    });
                        }

                        @Override
                        public void onFail() {
                            System.err.println("Loading player data failed");
                        }
                    });
        }

        @Override
        public void onUserProfileRequestFailed() {
            multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.CONNECTION_ERROR);
        }
    }

    private static class ConnectionRequestListenerImpl implements ConnectionRequestListener {

        private final MultiplayerClientImpl multiplayerClient;

        private ConnectionRequestListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onConnectDone(final ConnectEvent connectEvent) {
            switch (connectEvent.getResult()) {
                case WarpResponseResultCode.SUCCESS:
                    multiplayerClient.processWarpInitialized(true, null);
                    break;
                case WarpResponseResultCode.AUTH_ERROR:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.OTHER_USER_SIGNED);
                    break;
                case WarpResponseResultCode.RESOURCE_NOT_FOUND:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.RESOURCE_NOT_FOUND);
                    break;
                case WarpResponseResultCode.RESOURCE_MOVED:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.RESOURCE_MOVED);
                    break;
                case WarpResponseResultCode.BAD_REQUEST:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.BAD_REQUEST);
                    break;
                case WarpResponseResultCode.CONNECTION_ERROR:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.CONNECTION_ERR);
                    break;
                case WarpResponseResultCode.UNKNOWN_ERROR:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.UNKNOWN_ERROR);
                    break;
                case WarpResponseResultCode.RESULT_SIZE_ERROR:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.RESULT_SIZE_ERROR);
                    break;
                case WarpResponseResultCode.SUCCESS_RECOVERED:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.SUCCESS_RECOVERED);
                    break;
                case WarpResponseResultCode.CONNECTION_ERROR_RECOVERABLE:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.CONNECTION_ERR_RECOVERABLE);
                    break;
                case WarpResponseResultCode.USER_PAUSED_ERROR:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.USER_PAUSED_ERROR);
                    break;

                default:
                    multiplayerClient.processWarpInitialized(false, ConnectionCallback.UserRequestFail.CONNECTION_ERROR);
            }
        }

        @Override
        public void onDisconnectDone(final ConnectEvent connectEvent) {
            multiplayerClient.connected = false;
        }

        @Override
        public void onInitUDPDone(final byte b) {
            // Intents to stay empty.
        }
    }

    private static class ZoneListenerImpl implements ZoneRequestListener {

        private final MultiplayerClientImpl multiplayerClient;

        private ZoneListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onDeleteRoomDone(final RoomEvent roomEvent) {

        }

        @Override
        public void onGetAllRoomsDone(final AllRoomsEvent allRoomsEvent) {

        }

        @Override
        public void onCreateRoomDone(final RoomEvent roomEvent) {
            if (roomEvent.getResult() == WarpResponseResultCode.SUCCESS) {
                multiplayerClient.log("room successfully created, joining to room");

                /**
                 * Once room is created we try to join to it. Result will put in {@link RoomRequestListenerImpl#onJoinRoomDone(RoomEvent)}
                 */
                multiplayerClient.warpClient.joinRoomWithProperties(multiplayerClient.multiplayerMatch.getRoomProperties());
            } else {
                multiplayerClient.log("room creation failed");

                multiplayerClient.processJoinRoomError();
            }
        }

        @Override
        public void onGetOnlineUsersDone(final AllUsersEvent allUsersEvent) {

        }

        @Override
        public void onGetLiveUserInfoDone(final LiveUserInfoEvent liveUserInfoEvent) {

        }

        @Override
        public void onSetCustomUserDataDone(final LiveUserInfoEvent liveUserInfoEvent) {

        }

        @Override
        public void onGetMatchedRoomsDone(final MatchedRoomsEvent matchedRoomsEvent) {

        }
    }

    private static class RoomRequestListenerImpl implements RoomRequestListener {

        private final MultiplayerClientImpl multiplayerClient;

        private RoomRequestListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onSubscribeRoomDone(final RoomEvent roomEvent) {
            if (roomEvent.getResult() == WarpResponseResultCode.SUCCESS) {
                multiplayerClient.log("successfully subscribed to room, retrieving room info");

                /**
                 * Once we successfully subscribed and joined to room we should
                 * request status of room (number of connected users). Result will be put in
                 * {@link #onGetLiveRoomInfoDone(LiveRoomInfoEvent)}
                 */
                multiplayerClient.warpClient.getLiveRoomInfo(multiplayerClient.multiplayerMatch.getRoomId());

            } else {
                multiplayerClient.log("room subscription failed");

                multiplayerClient.warpClient.leaveRoom(multiplayerClient.multiplayerMatch.getRoomId());
                multiplayerClient.processJoinRoomError();
            }
        }

        @Override
        public void onUnSubscribeRoomDone(final RoomEvent roomEvent) {

        }

        @Override
        public void onJoinRoomDone(final RoomEvent roomEvent) {
            if (roomEvent.getResult() == WarpResponseResultCode.SUCCESS) {
                multiplayerClient.log("joined to existing room, subscribing to room");

                /**
                 * We successfully found and joined to room.
                 * Now we subscribe to it (for receiving notifications from other players)
                 * Result of subscribe will be put in {@link #onSubscribeRoomDone(RoomEvent)}
                 */
                multiplayerClient.warpClient.subscribeRoom(roomEvent.getData().getId());
                multiplayerClient.multiplayerMatch.setRoomData(roomEvent.getData());

            } else if (roomEvent.getResult() == WarpResponseResultCode.RESOURCE_NOT_FOUND) {
                multiplayerClient.log("creating room");

                /**
                 * Room was not found, so we should create it. Once room will be created,
                 * {@link ZoneListenerImpl#onCreateRoomDone(RoomEvent)} will be called
                 */
                multiplayerClient.warpClient.createRoom(ROOM_NAME,
                        multiplayerClient.facebookCallbacks.lastUserProfile.getName(),
                        multiplayerClient.multiplayerMatch.getGameSettings().numberOfPlayers.getNumberOfPlayers(),
                        multiplayerClient.multiplayerMatch.getRoomProperties());
                multiplayerClient.multiplayerMatch.setCreatedByCurrentUser();

            } else {
                multiplayerClient.log("joining to room failed");

                multiplayerClient.processJoinRoomError();
            }
        }

        @Override
        public void onLeaveRoomDone(final RoomEvent roomEvent) {

        }

        @Override
        public void onGetLiveRoomInfoDone(final LiveRoomInfoEvent liveRoomInfoEvent) {
            if (liveRoomInfoEvent.getResult() == WarpResponseResultCode.SUCCESS) {
                if (RoomPropertiesHelper.allPlayersConnected(liveRoomInfoEvent)) {
                    multiplayerClient.log("retrieved info, all players are connected");

                    multiplayerClient.processPlayersConnected(liveRoomInfoEvent);
                    multiplayerClient.requestConnectedOpponentsInfo(liveRoomInfoEvent, true);
                } else {
                    multiplayerClient.log("retrieved info, waiting for other players to connect");

                    if (liveRoomInfoEvent.getJoinedUsers().length == 1) {
                        multiplayerClient.multiplayerMatch.setCreatedByCurrentUser();
                    }

                    multiplayerClient.requestConnectedOpponentsInfo(liveRoomInfoEvent, false);

                    /**
                     * We should wait for players to connect to room. If some user connects
                     * to room {@link NotifyListenerImpl#onUserJoinedRoom(RoomData, String)}
                     * will be called.
                     */
                }
            } else {
                multiplayerClient.log("error retrieving info");

                multiplayerClient.warpClient.unsubscribeRoom(liveRoomInfoEvent.getData().getId());
                multiplayerClient.warpClient.leaveRoom(liveRoomInfoEvent.getData().getId());

                multiplayerClient.processGetRoomInfoError();
            }
        }

        @Override
        public void onSetCustomRoomDataDone(final LiveRoomInfoEvent liveRoomInfoEvent) {

        }

        @Override
        public void onUpdatePropertyDone(final LiveRoomInfoEvent liveRoomInfoEvent) {

        }

        @Override
        public void onLockPropertiesDone(final byte b) {

        }

        @Override
        public void onUnlockPropertiesDone(final byte b) {

        }
    }

    private static class LobbyRequestListenerImpl implements LobbyRequestListener {

        private final MultiplayerClientImpl multiplayerClient;

        private LobbyRequestListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onJoinLobbyDone(final LobbyEvent lobbyEvent) {

        }

        @Override
        public void onLeaveLobbyDone(final LobbyEvent lobbyEvent) {

        }

        @Override
        public void onSubscribeLobbyDone(final LobbyEvent lobbyEvent) {

        }

        @Override
        public void onUnSubscribeLobbyDone(final LobbyEvent lobbyEvent) {

        }

        @Override
        public void onGetLiveLobbyInfoDone(final LiveRoomInfoEvent liveRoomInfoEvent) {

        }
    }

    private static class ChatRequestListenerImpl implements ChatRequestListener {

        private final MultiplayerClientImpl multiplayerClient;

        private ChatRequestListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onSendChatDone(final byte b) {

        }

        @Override
        public void onSendPrivateChatDone(final byte b) {

        }
    }

    private static class NotifyListenerImpl implements NotifyListener {

        private final MultiplayerClientImpl multiplayerClient;

        private NotifyListenerImpl(final MultiplayerClientImpl multiplayerClient) {
            this.multiplayerClient = multiplayerClient;
        }

        @Override
        public void onRoomCreated(final RoomData roomData) {
            multiplayerClient.log("onRoomCreated");
        }

        @Override
        public void onRoomDestroyed(final RoomData roomData) {
            multiplayerClient.log("onRoomDestroyed");
        }

        @Override
        public void onUserLeftRoom(final RoomData roomData, final String username) {
            multiplayerClient.log("onUserLeftRoom");

            multiplayerClient.processPlayerLeftGame(username);
        }

        @Override
        public void onUserJoinedRoom(final RoomData roomData, final String username) {
            multiplayerClient.log("user " + username + " joined room, retrieving room info again");

            /**
             * Requesting room info once again to check if all players are connected.
             */
            multiplayerClient.warpClient.getLiveRoomInfo(roomData.getId());
        }

        @Override
        public void onUserLeftLobby(final LobbyData lobbyData, final String username) {

        }

        @Override
        public void onUserJoinedLobby(final LobbyData lobbyData, final String username) {

        }

        @Override
        public void onChatReceived(final ChatEvent chatEvent) {
            if (!chatEvent.getSender().equals(multiplayerClient.getCurrentUserInfo().getWarpId())) {
                InputMessageProcessor.processMessage(
                        multiplayerClient.gameManager,
                        multiplayerClient.multiplayerMatch.getPlayers(),
                        chatEvent);
            }
        }

        @Override
        public void onPrivateChatReceived(final String sender, final String message) {

        }

        @Override
        public void onPrivateUpdateReceived(final String sender, final byte[] update, final boolean fromUdp) {

        }

        @Override
        public void onUpdatePeersReceived(final UpdateEvent updateEvent) {

        }

        @Override
        public void onUserChangeRoomProperty(final RoomData event, final String username, final HashMap<String, Object> properties, final HashMap<String, String> stringStringHashMap) {
            if (MultiplayerMatch.isRoomReady(properties)) {
                multiplayerClient.processGameIsReady();
            }
        }

        @Override
        public void onMoveCompleted(final MoveEvent moveEvent) {

        }

        @Override
        public void onGameStarted(final String sender, final String roomId, final String nextTurn) {

        }

        @Override
        public void onGameStopped(final String sender, final String roomId) {

        }

        @Override
        public void onUserPaused(final String locid, final boolean isLobby, final String username) {

        }

        @Override
        public void onUserResumed(final String locid, final boolean isLobby, final String username) {

        }

        @Override
        public void onNextTurnRequest(final String s) {

        }
    }
}