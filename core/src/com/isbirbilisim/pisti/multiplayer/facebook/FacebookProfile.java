package com.isbirbilisim.pisti.multiplayer.facebook;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class FacebookProfile implements Json.Serializable {

    private String name;
    private String id;
    private String accessToken;

    public FacebookProfile(final String name, final String id) {
        this.name = name;
        this.id = id;
    }

    public FacebookProfile(final String response) {
        readFromString(response);
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public void write(final Json json) {
        // Not needed since we will only received data from Facebook.
    }

    @Override
    public void read(final Json json, final JsonValue jsonData) {
        // Not needed
    }

    private void readFromString(final String data) {
        final JsonValue jsonValue = new JsonReader().parse(data);

        id = jsonValue.getString("id");
        name = jsonValue.getString("name");
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}