package com.isbirbilisim.pisti.multiplayer.facebook;

public interface FacebookCallbacks {

    /**
     * Called when user successfully signed into facebook.
     */
    void onUserSignedIn();

    /**
     * Called when user signed out from facebook.
     */
    void onUserSignedOut();

    /**
     * Called when received info about current user.
     *
     * @param profile - user profile.
     */
    void onUserProfileReceived(final FacebookProfile profile);

    /**
     * Called when user info request failed.
     */
    void onUserProfileRequestFailed();
}