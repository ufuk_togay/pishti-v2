package com.isbirbilisim.pisti.multiplayer.facebook;

/**
 * interface for client for Facebook. Implementation uses specific SDK depending on platform.
 */
public interface Facebook {

    public boolean isSignedIn();

    public void signIn();

    public void signOut();

    public void addFacebookCallbacks(final FacebookCallbacks callbacks);

    public void removeFacebookCallbacks(final FacebookCallbacks callbacks);

    public void loadUserProfile();
}