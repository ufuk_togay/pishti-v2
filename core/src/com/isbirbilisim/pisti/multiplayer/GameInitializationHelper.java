package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.DeckFactory;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.multiplayer.messages.MessageCreator;
import com.isbirbilisim.pisti.multiplayer.messages.MessagingHelper;
import com.isbirbilisim.pisti.players.Player;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;

import java.util.List;
import java.util.Stack;

/**
 * Initializes game by connecting players to game, decides player, which should
 * move first.
 */
public class GameInitializationHelper {

    private final MessagingHelper messageSender;
    private final GameManager gameManager;
    private final UserInfo currentUserInfo;

    private final List<Player> players;

    public GameInitializationHelper(final MessagingHelper messageSender,
                                    final GameManager gameManager,
                                    final LiveRoomInfoEvent liveRoomInfoEvent,
                                    final UserInfo currentUserInfo) {
        this.messageSender = messageSender;
        this.gameManager = gameManager;
        this.currentUserInfo = currentUserInfo;

        players = new ParticipantResolver(liveRoomInfoEvent, currentUserInfo).getPlayers();
    }

    /**
     * Starts game initialization.
     */
    public void initGame(final GameSettings gameSettings) {
        gameManager.initGame(gameSettings, currentUserInfo);
        connectPlayers();

        if (shouldMakeFirstMoveDecision()) {
            initFirstTurnPlayer();
            initDeck();
        }
        // else app should wait for broadcast from other player.
    }

    /**
     * @return - player list.
     */
    public List<Player> getPlayers() {
        return players;
    }

    private void initFirstTurnPlayer() {
        final String firstTurnPlayerFacebookId =
                ParticipantUtils.randomFirstTurnPlayerParticipantId(players);

        sendFirstTurnPlayerMessage(firstTurnPlayerFacebookId);
        setFirstTurnPlayerLocally(firstTurnPlayerFacebookId);
    }

    private void initDeck() {
        final Stack<CardModel> initialDeck = getShuffledDeck();

        sendInitialDeckMessage(initialDeck);
        setInitialDeckLocally(initialDeck);
    }

    private void setFirstTurnPlayerLocally(final String facebookId) {
        gameManager.setFirstTurnPlayer(ParticipantUtils
                        .getPlayerByFacebookId(players,
                                facebookId)
        );
    }

    private void setInitialDeckLocally(final Stack<CardModel> shuffledDeck) {
        gameManager.setDeck(shuffledDeck);
    }

    private void connectPlayers() {
        for (final Player player : players) {
            gameManager.connectPlayer(player);
        }
    }

    private boolean shouldMakeFirstMoveDecision() {
        return isCurrentPlayer(ParticipantUtils
                .getFirstTurnDecider(players));
    }

    private boolean isCurrentPlayer(final Player player) {
        return (player.getUserInfo().getFacebookId()
                .equals(currentUserInfo.getFacebookId()));
    }


    private Stack<CardModel> getShuffledDeck() {
        return DeckFactory.createDeck();
    }

    private void sendFirstTurnPlayerMessage(final String participantId) {
        messageSender.sendMessageFromPlayer(MessageCreator
                .createFirstTurnMessage(participantId));
    }

    private void sendInitialDeckMessage(final Stack<CardModel> shuffledDeck) {
        messageSender.sendMessageFromPlayer(MessageCreator
                .createInitialDeckMessage(shuffledDeck));
    }
}