package com.isbirbilisim.pisti.multiplayer.messages;

/**
 * Defines interface for messages, that can be sent to game participants.
 */
public interface Message {

    /**
     * Returns byte array representation of message.
     *
     * @return - byte array.
     */
    byte[] toBytes();
}