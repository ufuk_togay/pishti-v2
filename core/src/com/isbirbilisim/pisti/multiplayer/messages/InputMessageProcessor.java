package com.isbirbilisim.pisti.multiplayer.messages;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.multiplayer.ParticipantUtils;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerState;
import com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent;

import java.util.List;
import java.util.Stack;

public class InputMessageProcessor {

    /**
     * Processes message received from other game participants and notifies
     * game client accordingly.
     *
     * @param gameManager - game manager.
     * @param players     - list of current players.
     */
    public static void processMessage(final GameManager gameManager,
                                      final List<Player> players,
                                      final ChatEvent chatEvent) {
        final byte[] messageData = chatEvent.getMessage().getBytes();
        final byte messageHeader = messageData[0];

        switch (messageHeader) {
            case MessageCreator.FIRST_TURN_PLAYER_MESSAGE:
                final String firstTurnParticipantId =
                        MessageCreator.processFirstTurnMessage(messageData);
                final Player player = ParticipantUtils
                        .getPlayerByFacebookId(players,
                                firstTurnParticipantId);
                gameManager.setFirstTurnPlayer(player);
                break;

            case MessageCreator.INITIAL_DECK_MESSAGE:
                final Stack<CardModel> initialDeck =
                        MessageCreator.processInitialDeckMessage(messageData);
                gameManager.setDeck(initialDeck);
                break;

            case MessageCreator.PLAYER_STATE_MESSAGE:
                final PlayerState state =
                        MessageCreator.processPlayerStateMessage(messageData);
                final Player messageSender = ParticipantUtils
                        .getPlayerByWarpId(players,
                                chatEvent.getSender());
                gameManager.setPlayerState(messageSender, state);
                break;

            case MessageCreator.MOVE_MESSAGE:
                final String senderParticipantId = chatEvent.getSender();
                final CardModel moveDescription =
                        MessageCreator
                                .processMoveDescriptionMessage(messageData);
                final Player sender = ParticipantUtils
                        .getPlayerByWarpId(players,
                                senderParticipantId);
                gameManager.opponentMove(sender, moveDescription);
                break;

            case MessageCreator.CHAT_MESSAGE:
                final String senderId = chatEvent.getSender();
                final String message = MessageCreator.processChatMessage(messageData);

                gameManager.chatMessage(ParticipantUtils.getPlayerByWarpId(players, senderId),
                        message);

                break;
        }
    }
}