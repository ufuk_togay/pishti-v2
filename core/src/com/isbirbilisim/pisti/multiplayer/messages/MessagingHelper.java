package com.isbirbilisim.pisti.multiplayer.messages;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;

public class MessagingHelper {

    private final WarpClient warpClient;

    public MessagingHelper(final WarpClient warpClient) {
        this.warpClient = warpClient;
    }

//    /**
//     * Sends broadcast to other game participants.
//     *
//     * @param message - message to be sent
//     */
//    public void sendMessage(final Message message) {
//        warpClient.sendUpdatePeers(message.toBytes());
//    }

    public void sendMessageFromPlayer(final Message message) {
        warpClient.sendChat(new String(message.toBytes()));
    }
}