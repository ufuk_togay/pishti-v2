package com.isbirbilisim.pisti.multiplayer.messages;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.DeckFactory;
import com.isbirbilisim.pisti.players.PlayerState;

import org.apache.commons.lang3.ArrayUtils;

import java.io.UnsupportedEncodingException;
import java.util.Stack;

/**
 * Creates {@link Message} instances from
 * given data.
 */
public class MessageCreator {

    private static final Stack<CardModel> defaultDeck = DeckFactory
            .createDefaultDeck();
    private static final String UTF8_ENCODING = "UTF-8";
    private static final String UTF16_ENCODING = "UTF-16";

    public static final byte FIRST_TURN_PLAYER_MESSAGE = 101;
    public static final byte INITIAL_DECK_MESSAGE = 102;
    public static final byte PLAYER_STATE_MESSAGE = 103;
    public static final byte MOVE_MESSAGE = 104;
    public static final byte CHAT_MESSAGE = 105;

    /**
     * Creates first turn message with
     *
     * @param firstTurnParticipantId - id of participant that should move first.
     * @return - corresponding message.
     */
    public static Message createFirstTurnMessage(
            final String firstTurnParticipantId) {
        return new MessageImpl(ArrayUtils.add(
                stringToByteArray(firstTurnParticipantId),
                0,
                FIRST_TURN_PLAYER_MESSAGE));
    }

    /**
     * Creates message with shuffled deck.
     *
     * @param deck - shuffled deck.
     */
    public static Message createInitialDeckMessage(final Stack<CardModel> deck) {
        final byte[] byteCards = new byte[deck.size()];

        for (byte i = 0; i < deck.size(); i++) {
            for (byte j = 0; j < deck.size(); j++) {
                if (deck.get(j).equals(defaultDeck.get(i))) {
                    byteCards[i] = j;
                }
            }
        }

        return new MessageImpl(ArrayUtils.add(
                byteCards, 0, INITIAL_DECK_MESSAGE));
    }

    /**
     * Creates new message with move description.
     *
     * @param card - card that is moved.
     */
    public static Message createMoveDescriptionMessage(final CardModel card) {
        return new MessageImpl(ArrayUtils
                .add(cardToByteArray(card), 0, MOVE_MESSAGE));
    }

    /**
     * Creates message with player's state description.
     *
     * @param state - player's state.
     * @return - result message.
     */
    public static Message createPlayerStateMessage(final PlayerState state) {
        return new MessageImpl(ArrayUtils.add(
                new byte[]{(byte) state.getCode()},
                0,
                PLAYER_STATE_MESSAGE));
    }

    /**
     * Creates message with chat message.
     *
     * @param message - message to send.
     * @return - multiplayer message.
     */
    public static Message createChatMessage(final String message) {
        return new MessageImpl(ArrayUtils.add(
                stringToByteArray(message),
                0,
                CHAT_MESSAGE
        ));
    }

    /**
     * @param msg - array of bytes, received from other participant.
     * @return - participantId of player, that should move first.
     */
    public static String processFirstTurnMessage(final byte[] msg) {
        final byte[] data = ArrayUtils.remove(msg, 0);

        return byteArrayToString(data);
    }

    /**
     * @param msg - array of bytes, received from other participant.
     * @return - chat message.
     */
    public static String processChatMessage(final byte[] msg) {
        final byte[] data = ArrayUtils.remove(msg, 0);

        return byteArrayToString(data);
    }

    /**
     * Converts byte array to array of CardModel.
     *
     * @param msg - array of bytes, received from other participant.
     * @return - array of {@link com.isbirbilisim.pisti.cards.CardModel}.
     */
    public static Stack<CardModel> processInitialDeckMessage(final byte[] msg) {
        final byte[] data = ArrayUtils.remove(msg, 0);

        final Stack<CardModel> deck = new Stack<CardModel>();

        for (int j = 0; j < defaultDeck.size(); j++) {
            for (byte i = 0; i < defaultDeck.size(); i++) {
                if (data[i] == j) {
                    deck.push(defaultDeck.get(i));
                    break;
                }
            }
        }

        return deck;
    }

    /**
     * Converts byte array to {@link com.isbirbilisim.pisti.cards.CardModel}  with opponent's move description.
     *
     * @param msg - array of bytes, received from other participant.
     * @return - {@link com.isbirbilisim.pisti.cards.CardModel} with opponent's move description.
     */
    public static CardModel processMoveDescriptionMessage(final byte[] msg) {
        final byte[] data = ArrayUtils.remove(msg, 0);

        return byteArrayToCard(data);
    }

    /**
     * Process player status message.
     *
     * @param msg - input message.
     * @return - true if player is ready
     */
    public static PlayerState processPlayerStateMessage(final byte[] msg) {
        final byte[] data = ArrayUtils.remove(msg, 0);

        return PlayerState.fromCodeValue(data[0]);
    }

    private static byte[] stringToByteArray(final String string) {
        try {
            return string.getBytes(UTF8_ENCODING);
        } catch (final UnsupportedEncodingException exception) {
            return null;
        }
    }

    private static byte[] UTF16stringToByteArray(final String string) {
        try {
            return string.getBytes(UTF16_ENCODING);
        } catch (final UnsupportedEncodingException exception) {
            return null;
        }
    }

    private static String byteArrayToString(final byte[] msg) {
        try {
            return new String(msg, UTF8_ENCODING);
        } catch (final UnsupportedEncodingException exception) {
            return null;
        }
    }

    private static String byteArrayToUTF16String(final byte[] msg) {
        try {
            return new String(msg, UTF16_ENCODING);
        } catch (final UnsupportedEncodingException exception) {
            return null;
        }
    }

    private static byte[] cardToByteArray(final CardModel card) {
        return new byte[]{(byte) card.getSuit(), (byte) card.getValue()};
    }

    private static CardModel byteArrayToCard(final byte[] msg) {
        final int suit = msg[0];
        final int value = msg[1];
        for (final CardModel card : defaultDeck) {
            if ((card.getSuit() == suit) && (card.getValue() == value)) {
                return card;
            }
        }
        return null;
    }

    private static class MessageImpl implements Message {

        private final byte[] data;

        public MessageImpl(final byte[] data) {
            this.data = data;
        }

        @Override
        public byte[] toBytes() {
            return data;
        }
    }
}