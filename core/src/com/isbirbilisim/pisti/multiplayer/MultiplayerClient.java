package com.isbirbilisim.pisti.multiplayer;

import com.isbirbilisim.pisti.account.OnUserInfoSyncListener;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.players.PlayerState;
import com.isbirbilisim.pisti.scores.Achievement;
import com.isbirbilisim.pisti.scores.Leaderboard;

/**
 * Interface for multiplayer manager.
 */
public interface MultiplayerClient {

    /**
     * Starts sing in flow.
     */
    void signIn();

    /**
     * Sings out user.
     */
    void signOut();

    /**
     * @return - true if user currently signed in.
     */
    boolean isSignedIn();

    /**
     * If user signed in - returns info about, returns null otherwise.
     */
    UserInfo getCurrentUserInfo();

    /**
     * Registers connection listener.
     *
     * @param callback - callback to be registered.
     */
    void registerConnectionCallbacks(final ConnectionCallback callback);

    /**
     * Removes connection callback.
     *
     * @param callback - callback that needs to be removed.
     */
    void unregisterConnectionCallbacks(final ConnectionCallback callback);

    /**
     * Removes all callback.
     */
    void clearConnectionCallbacks();

    /**
     * Sets {@link com.isbirbilisim.pisti.game.GameManager} delegate,
     * which will receive multiplayer callbacks.
     *
     * @param instance - GameManager instance.
     */
    void registerGameManagerDelegate(final GameManager instance);

    /**
     * Removes previously added {@link com.isbirbilisim.pisti.game.GameManager}
     * instance.
     */
    void unregisterGameManagerDelegate();

    /**
     * Synchronizes user info with remote server.
     *
     * @param userInfo - user info that should be synchronized;
     * @param listener - user info sync listener.
     */
    void syncUserInfo(final UserInfo userInfo,
                      final OnUserInfoSyncListener listener);

    /**
     * Sends message with player's state.
     *
     * @param state - current state.
     */
    void sendPlayerStateMessage(final PlayerState state);

    /**
     * Starts quick game flow.
     *
     * @param gameSettings - game settings.
     * @param gameVariant  - value that uniquely represents game type.
     */
    void quickGame(final GameSettings gameSettings, final int gameVariant);

    /**
     * Cancels game search.
     */
    void cancelGameSearch();

    /**
     * Starts flow for selecting friends to play with.
     *
     * @param gameSettings - game settings.
     */
    void playWithFriends(final GameSettings gameSettings);

    /**
     * Starts flow for inviting your friends to install game.
     */
    void inviteFriends();

    /**
     * Sends message with move description to other players.
     */
    void sentMoveMessage(final CardModel cardModel);

    /**
     * Sends chat message to players.
     */
    void sendChatMessage(final String message);

    /**
     * Ends current game
     */
    void endGame();

    /**
     * Unlocks specific achievement for player.
     *
     * @param achievement - unlocked achievement.
     */
    void unlockAchievement(final Achievement achievement);

    /**
     * Shows leaderboard.
     *
     * @param leaderboard - leaderboard type.
     */
    void showLeaderboard(final Leaderboard leaderboard);
}