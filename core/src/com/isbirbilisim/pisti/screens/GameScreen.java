package com.isbirbilisim.pisti.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.ad.AdUtil;
import com.isbirbilisim.pisti.analytics.AnalyticsUtil;
import com.isbirbilisim.pisti.backend.StatisticManager;
import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.cards.table.CardTable;
import com.isbirbilisim.pisti.dialogs.DialogCallback;
import com.isbirbilisim.pisti.dialogs.GameDialog;
import com.isbirbilisim.pisti.game.CardGame;
import com.isbirbilisim.pisti.game.CardGameFactory;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameProcessor;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.game.GameplayType;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.match.GameLifecycle;
import com.isbirbilisim.pisti.match.MatchLifecycle;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.PlayerUtils;
import com.isbirbilisim.pisti.players.Role;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.resources.Strings;
import com.isbirbilisim.pisti.scores.OnSuperScoresCollectedListener;
import com.isbirbilisim.pisti.scores.Scores;
import com.isbirbilisim.pisti.view.CardView;
import com.isbirbilisim.pisti.view.ChatInput;
import com.isbirbilisim.pisti.view.GameLogoView;
import com.isbirbilisim.pisti.view.GameScreenMenu;
import com.isbirbilisim.pisti.view.PlayerWidget;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class GameScreen extends BaseScreen implements MatchLifecycle,
        GameLifecycle {
    private final List<Player> players;
    private Player firstTurnPlayer;

    private int width;
    private int height;

    private WidgetGroup rootGroup;
    private CardTable cardTable;
    private GameLogoView gameLogoView;
    private ImageButton settingsButton;
    private GameScreenMenu gameScreenMenu;
    private ChatInput chatInput;
    private Skin skin;

    private GameplayType gameplayType;
    private GameSettings gameSettings;
    private CardGame cardGame;
    private GameProcessor gameProcessor;
    private GameManager gameManager;

    private long bidValue;

    private boolean isLeaveGameDialogShown;

    private boolean isPlaying;

    public GameScreen(final Pisti pisti) {
        super(pisti);
        players = new ArrayList<>();
    }

    @Override
    public void show() {
        super.show();

        this.width = getScreenUtils().getScreenWidth();
        this.height = getScreenUtils().getScreenHeight();

        initInput();
        initScreen();

        final AnalyticsUtil analyticsUtil = getPisti().getAnalyticsUtil();
        analyticsUtil.sendGameScreenShownEvent();
    }

    private void initInput() {
        Gdx.input.setCatchBackKey(true);
        Gdx.input.setCatchMenuKey(true);
    }

    @Override
    public void hide() {
        super.hide();

        Gdx.input.setCatchBackKey(false);
        Gdx.input.setCatchMenuKey(false);

        players.clear();

        if (cardTable != null) {
            cardTable.stopCountDown();
        }

        cardTable = null;
        gameProcessor = null;
    }

    @Override
    public void pause() {
        super.pause();

        if (isPlaying) {
            leaveGame();
            isPlaying = false;
            navigateBack();
        }
    }

    private void initScreen() {
        skin = getAssets().getGameScreenSkin();

        final Table table = new Table();
        table.setBackground(skin.getDrawable(Res.textures.GAME_SCREEN_BACKGROUND));
        table.setFillParent(true);
        getStage().addActor(table);

        rootGroup = new WidgetGroup();
        rootGroup.setFillParent(true);

        table.add(rootGroup).expand().fill();

        gameLogoView = createLogoView();
        rootGroup.addActor(gameLogoView);

        settingsButton = createSettingsButton();
        getStage().addActor(settingsButton);

        gameScreenMenu = createMenu();
        rootGroup.addActor(gameScreenMenu);
    }

    private GameLogoView createLogoView() {
        final ScreenUtils screenUtils = getScreenUtils();

        final float logoWidth = screenUtils.toRealWidth(Res.values.GAME_SCREEN_LOGO_WIDTH);
        final float logoHeight = screenUtils.toRealHeight(Res.values.GAME_SCREEN_LOGO_HEIGHT);

        final GameLogoView gameLogoView = new GameLogoView(skin, logoWidth, logoHeight);

        gameLogoView.setPosition(screenUtils.getScreenWidth() / 2F - (logoWidth / 2F),
                screenUtils.getScreenHeight() / 2F - (logoHeight / 2F));

        return gameLogoView;
    }

    private ImageButton createSettingsButton() {
        final ScreenUtils screenUtils = getScreenUtils();

        final float buttonWidth = screenUtils.toRealWidth(Res.values.GAME_SCREEN_SETTINGS_ICON_WIDTH);
        final float buttonHeight = screenUtils.toRealHeight(Res.values.GAME_SCREEN_SETTINGS_ICON_HEIGHT);

        final ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();
        style.up = skin.getDrawable(Res.textures.ICON_SETTINGS);
        style.down = skin.newDrawable(Res.textures.ICON_SETTINGS,
                Res.colors.GAME_SCREEN_SETTINGS_BUTTON_PRESSED_COLOR);

        final ImageButton button = new ImageButton(style);
        button.setSize(buttonWidth, buttonHeight);

        final float padTop = screenUtils.toRealHeight(Res.settings.showingAd
                ? Res.values.GAME_SCREEN_SETTINGS_ICON_PAD_WITH_AD
                : Res.values.GAME_SCREEN_SETTINGS_ICON_PAD_NO_AD);
        button.setPosition(buttonWidth / 2, height - padTop);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                showMenu();
            }
        });

        return button;
    }

    private void initChatIfNeeded() {
        if (gameplayType == GameplayType.SINGLE) {
            return;
        }

        final Button chatButton = new ImageTextButton(
                skin.get(Res.properties.STRINGS, Strings.class).chat,
                skin.get(Res.buttonStyles.CHAT_BUTTON_STYLE,
                        ImageTextButton.ImageTextButtonStyle.class));

        final PlayerWidget widget = cardTable.getCurrentPlayerWidget();
        final float xMargin = widget.getWidth() / 4;
        final float x = widget.getX() + widget.getWidth() + xMargin;
        final float y = widget.getY() + widget.getHeight() / 5;
        chatButton.setPosition(x, y);
        final float buttonWidth = getScreenUtils().toRealWidth(Res.values.CHAT_BUTTON_WIDTH);
        final float buttonHeight = getScreenUtils().toRealHeight(Res.values.CHAT_BUTTON_HEIGHT);
        chatButton.setSize(buttonWidth, buttonHeight);
        rootGroup.addActor(chatButton);

        chatInput = new ChatInput(skin, getScreenUtils(), new ChatInput.OnSendMessageListener() {
            @Override
            public void onSendMessage(final String message) {
                gameManager.sendChatMessage(message);
            }
        });
        final float inputWidth = getScreenUtils().toRealWidth(Res.values.CHAT_INPUT_WIDTH);
        final float inputHeight = getScreenUtils().toRealHeight(Res.values.CHAT_INPUT_HEIGHT);
        chatInput.setSize(inputWidth, inputHeight);
        chatInput.setPosition(width / 2 - (inputWidth / 2), height - (height / 3));
        chatInput.setVisible(false);
        rootGroup.addActor(chatInput);

        chatButton.addListener(new ClickListener() {
            @Override
            public void clicked(final InputEvent event, final float x, final float y) {
                chatInput.showInput(getStage());
            }
        });
    }

    private GameScreenMenu createMenu() {
        final GameScreenMenu screenMenu = new GameScreenMenu(
                getScreenUtils(),
                getPisti().getPlatformInfo(),
                skin,
                new ClickListener() {
                    @Override
                    public void clicked(final InputEvent event,
                                        final float x,
                                        final float y) {
                        hideMenu();
                    }
                },
                new ClickListener() {
                    @Override
                    public void clicked(final InputEvent event,
                                        final float x,
                                        final float y) {
                        hideMenu();
                        showLeaveGameDialog();
                    }
                },
                height);
        screenMenu.setHeight(height);
        screenMenu.setPosition(-screenMenu.getPrefWidth(), 0);

        return screenMenu;
    }

    private void showMenu() {
        gameScreenMenu.toFront();
        final MoveByAction showMenuAction = Actions.moveBy(
                gameScreenMenu.getWidth(),
                0,
                Res.values.SHOW_MENU_ANIMATION_DURATION,
                Interpolation.circleOut);
        gameScreenMenu.addAction(Actions.sequence(
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        hideSettingsButton();
                    }
                }),
                showMenuAction
        ));
    }

    private void hideMenu() {
        final MoveByAction hideMenuAction = Actions.moveBy(
                -gameScreenMenu.getWidth(),
                0,
                Res.values.SHOW_MENU_ANIMATION_DURATION,
                Interpolation.pow3);
        gameScreenMenu.addAction(Actions.sequence(
                hideMenuAction,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        showSettingsButton();
                    }
                })
        ));
    }

    private void hideSettingsButton() {
        settingsButton.addAction(Actions.sequence(
                Actions.touchable(Touchable.disabled),
                Actions.fadeOut(0.05F)));
    }

    private void showSettingsButton() {
        settingsButton.addAction(Actions.sequence(
                Actions.fadeIn(0.2F),
                Actions.touchable(Touchable.enabled)));
    }

    @Override
    public void dispose() {
        getStage().dispose();
        skin.dispose();
    }

    @Override
    public void onGameCreated(final GameManager gameManager,
                              final CardGame.Type type,
                              final GameplayType gameplayType,
                              final GameSettings gameSettings) {
        this.gameManager = gameManager;
        this.cardGame = CardGameFactory.getCardGame(type);
        this.gameplayType = gameplayType;
        this.gameSettings = gameSettings;

        final AdUtil adUtil = getPisti().getAdUtil();
        adUtil.showAds(Res.settings.showingAd);

        if (gameplayType == GameplayType.SINGLE) {
            adUtil.showInterstitialIfNeeded();
        }
    }

    @Override
    public void onPlayerConnected(final Player player) {
        // TODO update UI.
        player.setOnSuperScoresCollectedListener(new OnSuperScoresCollectedListener() {
            @Override
            public void onPishtiScoresCollected() {
                getPisti().getMultimediaManager().notifyPistiScores();
            }

            @Override
            public void onSuperPishtiScoresCollected() {
                getPisti().getMultimediaManager().notifySuperPistiScores();
            }
        });

        players.add(player);
    }

    @Override
    public void onPlayersConnected() {

    }

    @Override
    public void onPlayerSyncError(final Player player) {
        // TODO show message about player pre-game sync error.
        final UserInfo userInfo = PlayerUtils.getCurrentPlayer(players)
                .getUserInfo();
        userInfo.addGold(bidValue);

        getPisti().getMultiplayerClient().syncUserInfo(userInfo, null);
        getPisti().getMultiplayerClient().endGame();
    }

    @Override
    public void onDeckCreated(final Stack<CardModel> initialDeck) {
        if (cardTable == null) {
            cardTable = new CardTable(
                    skin,
                    getScreenUtils(),
                    getPisti().getMultimediaManager(),
                    gameLogoView,
                    players,
                    cardGame.getMaxCardsInHands(),
                    cardGame.getInitialCardsNumber(),
                    gameSettings.gameLength != GameSettings.GameLength.ONE_ROUND,
                    width,
                    height,
                    Res.settings.showingAd,
                    getPisti().getConnectivityUtils().isNetworkAvailable()
            );

            rootGroup.addActor(cardTable);
        }

        cardTable.setDeck(initialDeck);

        for (final Player player : players) {
            cardTable.refreshScores(player);
        }
    }

    @Override
    public void onFirstTurnPlayerDetermined(final Player player) {
        firstTurnPlayer = player;
    }

    @Override
    public void onHandshakeCompleted(final long bidValue) {
        this.bidValue = bidValue;

        if (gameProcessor == null) {
            this.gameProcessor = cardGame.getGameProcessor(players,
                    this,
                    bidValue);
        }

        this.gameProcessor.setFirstTurnPlayer(firstTurnPlayer);
        this.gameProcessor.setTable(cardTable);
        this.gameProcessor.setGameSettings(gameSettings);

        isPlaying = true;

        initChatIfNeeded();
    }

    @Override
    public void onOpponentMove(final Player opponent, final CardModel card) {
        gameProcessor.makePlayerMove(opponent, card);
    }

    @Override
    public void onChatMessageReceived(final Player player, final String message) {
        cardTable.getPlayerWidget(player).showChatMessage(message);
    }

    @Override
    public void onPlayerDisconnected(final Player player) {
        // TODO update view.
        player.setRole(Role.AI);
    }

    @Override
    public void onCardsGiven() {
        // TODO show "Game starts" message.
    }

    @Override
    public void onPlayerTurnToMove(final Player player) {
        if (player.getRole() == Role.PLAYER) {
            getPisti().getMultimediaManager().notifyPlayerTurnToMove();
        }

        if (gameplayType == GameplayType.SINGLE) {
            cardTable.showPlayerTurnToMove(player);
            return;
        }

        cardTable.showPlayerTurnTimer(player, new Timer.Task() {
            @Override
            public void run() {
                /**
                 * We check if player is bot AFTER timer end because real player
                 * can be replaced by bot while timer is ticking for this player
                 * @see com.isbirbilisim.pisti.screens.GameScreen#onPlayerDisconnected(com.isbirbilisim.pisti.players.Player).
                 *
                 * So if player was replaced by bot we will make move for him.
                 * Next move bot make by himself immediately when it will be its
                 * turn to move.
                 */
                if (player.getRole() == Role.PLAYER
                        || player.getRole() == Role.AI) {
                    makeMoveForPlayer(player);
                } else {
                    // waiting for opponent move.
                }
            }
        });
    }

    private void makeMoveForPlayer(final Player player) {
        final CardView cardView = cardTable.getLastPlacedCard();

        gameProcessor.makePlayerMove(player,
                player.getAutoMove(cardView == null ? null : cardView.getCard()));
    }

    @Override
    public void onPlayerMove(final Player player, final CardModel card) {
        if (player.getRole() == Role.PLAYER) {
            gameManager.sendMoveNotification(player, card);
        }

        if (gameplayType == GameplayType.SINGLE) {
            cardTable.hidePlayerTurnToMove(player);
        } else {
            cardTable.stopCountDown();
        }

        cardTable.refreshScores(player);
    }

    @Override
    public void onCardsRemovedFromTable() {
        // TODO
    }

    @Override
    public void onMatchEnd(final MatchResult matchResult,
                           final Player currentPlayer) {
        final UserInfo currentUserInfo = currentPlayer.getUserInfo();
        if (gameplayType == GameplayType.MULTIPLAYER) {
            currentUserInfo.addGame(matchResult);

            final Scores totalScores = currentUserInfo.getScores();
            totalScores.sumScores(currentPlayer.getScores());
            currentUserInfo.setScores(totalScores);
            /*
             * Syncing user info after game end;
             */
            getPisti().getMultiplayerClient().syncUserInfo(currentUserInfo, null);

            // Save multiPlayer statistic
            StatisticManager.getInstance().sendMultiPlayerGameResults(matchResult, currentPlayer);

            getPisti().getAnalyticsUtil().sendMultiplayerGameCompletedEvent();
        } else {
            UserInfo userInfo = currentPlayer.getUserInfo();
            if (userInfo != null && userInfo.getWarpId() != null && !userInfo.getWarpId().isEmpty()) {
                StatisticManager.getInstance().sendSinglePlayerGameResults(matchResult, currentPlayer);
            } else {
                System.err.println("FaceBook ID is not set. Skip storing user statistic...");
            }
            getPisti().getAnalyticsUtil().sendSingleGameCompletedEvent();
        }

        isPlaying = false;

        showFinalScoresDialog();
    }

    private void showFinalScoresDialog() {
        final GameDialog dialog = getDialogManager()
                .getFinalScoresDialog(cardTable.getPlayerWidgets(),
                        gameplayType,
                        gameProcessor.getGameResultManager(),
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                if (gameplayType == GameplayType.MULTIPLAYER) {
                                    getPisti().getMultiplayerClient().endGame();
                                }
                                hideDialog();
                                navigateBack();
                            }
                        },
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();
                                resetGame();
                            }
                        }
                );
        showDialog(dialog);
    }

    private void resetGame() {
        gameManager.restartGame();
    }

    private void showLeaveGameDialog() {
        isLeaveGameDialogShown = true;

        final GameDialog dialog = getDialogManager()
                .getLeaveGameDialog(
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();

                                leaveGame();

                                navigateBack();

                                isLeaveGameDialogShown = false;
                            }
                        },
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();

                                isLeaveGameDialogShown = false;
                            }
                        }
                );
        showDialog(dialog);
    }

    private void leaveGame() {
        // Leaving game before end.
        if (gameplayType == GameplayType.MULTIPLAYER) {
            getPisti().getMultiplayerClient().endGame();
        }
    }

    private void navigateBack() {
        getPisti().getScreenNavigator().setMainMenuScreen();
    }

    @Override
    public boolean keyDown(final int keycode) {
        switch (keycode) {
            case Input.Keys.BACK:
            case Input.Keys.MENU:
                if (!isLeaveGameDialogShown) {
                    showLeaveGameDialog();
                }
                return true;
            case Input.Keys.ENTER:
                if (chatInput != null) {
                    chatInput.enterPressed();
                }
        }

        return false;
    }
}