package com.isbirbilisim.pisti.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.preferences.AppVersionUtils;
import com.isbirbilisim.pisti.view.LogoView;

/**
 * Screen that shows loading progress.
 */
public class SplashScreen extends BaseScreen {

    private Screen loadingScreen;
    private Skin splashScreenSkin;

    public SplashScreen(final Pisti pisti) {
        super(pisti);
    }

    @Override
    public void show() {
        super.show();

        splashScreenSkin = getAssets().getSplashScreenSkin();

        initTable();
    }

    private void initTable() {
        final float screenWidth = getScreenUtils().getScreenWidth();
        final float screenHeight = getScreenUtils().getScreenHeight();

        final Table table = new Table();
        table.setFillParent(true);

        getStage().addActor(table);
        final WidgetGroup rootGroup = new WidgetGroup();
        table.add(rootGroup).expand().fill();

        final LogoView logoView = new LogoView(getPisti(),
                splashScreenSkin,
                screenWidth,
                screenHeight);
        logoView.setPosition(0, 0);
        rootGroup.addActor(logoView);
    }

    /**
     * Sets screen that needs to be set after loading assets.
     */
    public void setLoadingScreen(final Screen loadingScreen) {
        this.loadingScreen = loadingScreen;
    }

    @Override
    public void render(final float delta) {
        super.render(delta);

        if (getAssets().update()) {
            /*
             * When resource loading is done we write current app version to preferences
             */
            AppVersionUtils.updateSavedVersionCode();

            getPisti().setScreen(loadingScreen);
        }
    }
}