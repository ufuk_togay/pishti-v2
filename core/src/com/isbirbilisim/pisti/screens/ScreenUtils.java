package com.isbirbilisim.pisti.screens;

/**
 * Utils for changing screen orientation. Should be implemented on each
 * platform.
 * <p/>
 * Provides methods for converting virtual width and height / coordinates to
 * real size / coordinate. Virtual width equals 1000 points in landscape
 * orientation and virtual height equals 1000 points in portrait.
 */
public interface ScreenUtils {
    public static final int VIRTUAL_SIDE = 1000;

    /**
     * Sets screen orientation.
     *
     * @param orientation - screen orientation.
     */
    void setOrientation(final ScreenOrientation orientation);

    /**
     * @return - current screen orientation.
     */
    ScreenOrientation getOrientation();

    /**
     * @return - screen height.
     */
    int getScreenHeight();

    /**
     * @return - screen width.
     */
    int getScreenWidth();

    /**
     * Returns virtual screen width. If current screen orientation is
     * landscape - returns 1000, if current screen orientation is portrait -
     * returns value that equals 1000 / (realScreenHeight / realScreenWidth).
     *
     * @return - virtual screen width.
     */
    float getVirtualWidth();

    /**
     * Returns virtual screen height. If current screen orientation is
     * portrait - returns 1000, if current screen orientation is landscape -
     * returns value that equals 1000 / (realScreenWidth / realScreenHeight).
     *
     * @return - virtual screen width.
     */
    float getVirtualHeight();

    /**
     * Converts virtual points to real size.
     *
     * @param virtualWidth - width or x coordinate, measured in virtual points.
     * @return - real width.
     */
    float toRealWidth(final float virtualWidth);

    /**
     * Converts virtual points to real size.
     *
     * @param virtualHeight - height or y coordinate, measured in virtual points.
     * @return - real height.
     */
    float toRealHeight(final float virtualHeight);
}