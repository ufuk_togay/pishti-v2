package com.isbirbilisim.pisti.screens;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.account.Level;
import com.isbirbilisim.pisti.account.UserInfo;
import com.isbirbilisim.pisti.backend.StatisticManager;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.dialogs.DialogCallback;
import com.isbirbilisim.pisti.dialogs.GameDialog;
import com.isbirbilisim.pisti.dialogs.GameOptionsDialog;
import com.isbirbilisim.pisti.dialogs.WaitingRoomDialog;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.multiplayer.ConnectionCallback;
import com.isbirbilisim.pisti.multiplayer.MultiplayerClient;
import com.isbirbilisim.pisti.purchases.Item;
import com.isbirbilisim.pisti.resources.Res;
import com.isbirbilisim.pisti.scores.Leaderboard;
import com.isbirbilisim.pisti.screens.mainmenu.MenuEntry;
import com.isbirbilisim.pisti.view.HorizontalMenu;
import com.isbirbilisim.pisti.view.HorizontalMenuCreator;
import com.isbirbilisim.pisti.view.LoadingGameView;
import com.isbirbilisim.pisti.view.LogoView;
import com.isbirbilisim.pisti.view.SlideScreenView;
import com.isbirbilisim.pisti.view.VerticalMenuCreator;
import com.isbirbilisim.pisti.view.VerticalScrollMenu;

import java.net.URL;

/**
 * Screen that shows main menu and logo.
 */
public class MenuScreen extends BaseScreen
        implements VerticalScrollMenu.OnMenuItemSelectedListener,
        HorizontalMenu.OnMenuItemSelectedListener,
        ConnectionCallback,
        GameManager.OnReadyToSetGameScreenListener {
    private final MultiplayerClient multiplayerClient;

    private Table table;
    private Skin splashScreenSkin;
    private Skin mainMenuSkin;

    private LogoView logoView;
    private SlideScreenView slideScreenView;

    private MenuEntry pendingAction;

    public MenuScreen(final Pisti pisti) {
        super(pisti);
        this.multiplayerClient = pisti.getMultiplayerClient();

        pisti.getSingleGameManager().setOnReadyToSetGameScreenListener(this);
        pisti.getMultiplayerGameManager().setOnReadyToSetGameScreenListener(this);
    }

    @Override
    public void show() {
        super.show();

        splashScreenSkin = getAssets().getSplashScreenSkin();
        mainMenuSkin = getAssets().getMainMenuSkin();

        initTable();
        showMenu();

        refreshUserInfo();
        registerCallbacks();

        getPisti().getAdUtil().showAds(Res.settings.showingAd);
        getPisti().getAnalyticsUtil().sendMenuScreenShownEvent();
    }

    private void initTable() {
        final float screenWidth = getScreenUtils().getScreenWidth();
        final float screenHeight = getScreenUtils().getScreenHeight();

        table = new Table();
        table.setFillParent(true);

        getStage().addActor(table);
        final WidgetGroup rootGroup = new WidgetGroup();
        table.add(rootGroup).expand().fill();

        logoView = new LogoView(getPisti(),
                splashScreenSkin,
                screenWidth,
                screenHeight);
        logoView.setPosition(0, 0);
        rootGroup.addActor(logoView);

        slideScreenView = new SlideScreenView(
                screenWidth,
                screenHeight,
                getScreenUtils());
        slideScreenView.setPosition(screenWidth, 0);

        rootGroup.addActor(slideScreenView);
    }

    @Override
    public void dispose() {
        super.dispose();

        getPisti().getMultiplayerClient().unregisterConnectionCallbacks(this);
    }

    private void showMenu() {
        slideScreenView.setSkin(mainMenuSkin);
        slideScreenView.setViews(
                VerticalMenuCreator.create(
                        this,
                        mainMenuSkin,
                        getScreenUtils(),
                        getPisti().getMultimediaManager(),
                        getScreenUtils().getScreenWidth() / 2,
                        getScreenUtils().getScreenHeight() / 4F * 3F
                ),
                HorizontalMenuCreator.create(
                        getPisti().getMultimediaManager(),
                        this,
                        mainMenuSkin,
                        getScreenUtils().getScreenWidth() / 2,
                        getScreenUtils().getScreenHeight() / 4F
                ),
                new LoadingGameView(
                        getScreenUtils(),
                        mainMenuSkin,
                        getScreenUtils().getScreenWidth(),
                        getScreenUtils().getScreenHeight()));
        slideScreenView.addAction(getShowMenuAction());

        logoView.setMainMenuSkin(mainMenuSkin);
        logoView.addAction(Actions.sequence(getShiftLogoViewAction(),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        logoView.showViews();
                    }
                })));
    }

    private Action getShowMenuAction() {
        return Actions.moveBy(-(getScreenUtils().getScreenWidth() / 2F),
                0,
                Res.values.SHOW_MENU_ANIMATION_DURATION,
                Interpolation.circleOut);
    }

    private Action getShiftLogoViewAction() {
        final float x = getScreenUtils().getScreenWidth() / 4F;
        return Actions.moveBy(
                -x,
                0,
                Res.values.SHOW_MENU_ANIMATION_DURATION,
                Interpolation.circleOut);
    }

    private void registerCallbacks() {
        multiplayerClient.registerConnectionCallbacks(this);
    }

    private void initConnection() {
        if (getPisti().getConnectivityUtils().isNetworkAvailable()
                && !multiplayerClient.isSignedIn()) {
            multiplayerClient.signIn();

            showDialog(getDialogManager().getLoadingPlayerDialog());
        }

        refreshUserInfo();
    }

    private void refreshUserInfo() {
        if (multiplayerClient.getCurrentUserInfo() != null
                && multiplayerClient.isSignedIn()) {
            updateUserSignedInUI();
        } else {
            updateUserSignedOutUI();
        }

        slideScreenView.enableConnectionDependentItems(
                getPisti().getConnectivityUtils().isNetworkAvailable());
    }

    private void updateUserSignedInUI() {
        logoView.getGooglePlusButton().setSigned(true);
        logoView.updateGoldAmount(multiplayerClient.getCurrentUserInfo().getGold());
    }

    private void updateUserSignedOutUI() {
        logoView.getGooglePlusButton().setSigned(false);
        logoView.hideGoldLabelAndIcon();
    }

    @Override
    public void onUserInfoUpdated(final UserInfo userInfo) {
        if (isDialogShowing()) {
            hideDialog();
        }
        updateUserSignedInUI();

        if (pendingAction != null) {
            onMenuItemSelected(pendingAction);
            pendingAction = null;
        }
    }

    @Override
    public void onUserPurchaseResult(final Item item, final boolean success) {
        // TODO notify UI about purchase result.
    }

    @Override
    public void onUserInfoRequestFailed(final UserRequestFail cause) {
        if (isDialogShowing()) {
            hideDialog();
        }

        if (pendingAction != null) {
            pendingAction = null;
        }

        showDialog(getDialogManager().getMultiplayerErrorDialog(
                new DialogCallback() {
                    @Override
                    public void onButtonClicked() {
                        hideDialog();

                        if (cause != UserRequestFail.STATISTIC_CONNECTION_ERROR) {
                            multiplayerClient.signOut();
                        }
                    }
                },
                cause));

        updateUserSignedOutUI();
    }

    @Override
    public void onDisconnected() {
        if (isDialogShowing()) {
            hideDialog();
        }

        updateUserSignedOutUI();
    }

    @Override
    public void onJoinRoomError() {
        ((WaitingRoomDialog) getDialog()).setJoinRoomErrorMessage();
    }

    @Override
    public void onGetRoomInfoError() {
        ((WaitingRoomDialog) getDialog()).setJoinRoomErrorMessage();
    }

    @Override
    public void onWaitingForPlayers() {
        ((WaitingRoomDialog) getDialog()).setWaitingForPlayersMessage();
    }

    @Override
    public void onPlayerConnected(final String userId,
                                  final String playerName,
                                  final Level level,
                                  final URL photoUrl,
                                  final boolean male) {
        ((WaitingRoomDialog) getDialog()).connectPlayer(userId, playerName, level, photoUrl, male);
    }

    @Override
    public void onPlayerDisconnected(final String userId) {
        ((WaitingRoomDialog) getDialog()).disconnectPlayer(userId);
    }

    @Override
    public void onPlayersReady() {
        ((WaitingRoomDialog) getDialog()).setPlayersFoundMessage();
    }

    @Override
    public void onMenuItemSelected(final MenuEntry menuEntry) {
        switch (menuEntry) {
            case SINGLE_PLAYER:
                processSinglePlayerMenuSelected();
                break;
            case SINGLE_PLAYER_STATISTIC:
                processSinglePlayerStatisticMenuSelected();
                break;
            case ABOUT:
                showAboutDialog();
                break;
            case SETTINGS:
                processSettingsMenuSelected();
                break;
            case QUICK_GAME:
                processQuickGameMenuSelected();
                break;
            default:
                showComingSoonDialog();
                break;
            /**
             * temporary unavailable.
             */
//            case PLAY_WITH_FRIENDS:
//                processPlayWithFriendsMenuSelected();
//                break;
//            case INVITE_FRIENDS:
//                processInviteFriendsMenuSelected();
//                break;
//            case LEADERBOARD:
//                processLeaderboardMenuSelected();
//                break;
        }
    }

    private void showComingSoonDialog() {
        final GameDialog dialog = getDialogManager()
                .getComingSoonDialog(
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();
                            }
                        }
                );
        showDialog(dialog);
    }

    private void showAboutDialog() {
        final GameDialog dialog = getDialogManager()
                .getAboutDialog(
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();
                            }
                        }
                );
        showDialog(dialog);
    }

    private void showSinglePlayerStatisticDialog() {
        showLoadingDialog();
        StatisticManager.getInstance().loadSinglePlayerStatistic(
                multiplayerClient.getCurrentUserInfo().getWarpId(),
                new StatisticManager.DataLoadCallback() {
                    @Override
                    public void onDataLoaded(final UserStatisticModel stats) {
                        final GameDialog dialog = getDialogManager()
                                .getSinglePlayerStatisticDialog(
                                        new DialogCallback() {
                                            @Override
                                            public void onButtonClicked() {
                                                hideDialog();
                                            }
                                        },
                                        stats
                                );
                        hideDialog();
                        showDialog(dialog);
                    }

                    @Override
                    public void onFail() {
                        //TODO Refactor that.
                        onUserInfoRequestFailed(
                                UserRequestFail.STATISTIC_CONNECTION_ERROR);
                    }
                });
    }

    private void processSinglePlayerMenuSelected() {
        final GameDialog dialog = getDialogManager()
                .getSetGameOptionsDialog(
                        new GameOptionsDialog.OnGameOptionsSetListener() {
                            @Override
                            public void onGameOptionsSet(final GameSettings gameSettings) {
                                hideDialog();

                                getPisti().getSingleGameManager().initGame(
                                        gameSettings, multiplayerClient.getCurrentUserInfo());
                            }

                            @Override
                            public void onDialogCanceled() {
                                hideDialog();
                            }
                        }
                );
        showDialog(dialog);
    }

    private void processSettingsMenuSelected() {
        final GameDialog gameDialog = getDialogManager()
                .getSettingsDialog(
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();
                            }
                        },
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                hideDialog();
                                showLoadingDialog();
                            }
                        });
        showDialog(gameDialog);
    }

    private void processQuickGameMenuSelected() {
        if (!checkUserSignedInStatus(MenuEntry.QUICK_GAME)) {
            return;
        }

        showPlayersNumberDialog(true, Level.LEVEL1);
    }

    private void processSinglePlayerStatisticMenuSelected() {
        if (!checkUserSignedInStatus(MenuEntry.SINGLE_PLAYER_STATISTIC)) {
            return;
        }
        showSinglePlayerStatisticDialog();
    }

    private void showLoadingDialog() {
        showDialog(getDialogManager().getLoadingDialog());
    }

    private void processPlayWithFriendsMenuSelected() {
        if (!checkUserSignedInStatus(MenuEntry.PLAY_WITH_FRIENDS)) {
            return;
        }

        showPlayersNumberDialog(false, null);
    }

    private void processInviteFriendsMenuSelected() {
        if (!checkUserSignedInStatus(MenuEntry.INVITE_FRIENDS)) {
            return;
        }

        multiplayerClient.inviteFriends();
    }

    private void processLeaderboardMenuSelected() {
        if (!checkUserSignedInStatus(MenuEntry.LEADERBOARD)) {
            return;
        }

        multiplayerClient.showLeaderboard(Leaderboard.PISTI_TOP_SCORES);
    }

    private void showPlayersNumberDialog(final boolean quickGame,
                                         final Level playersLevel) {
        final GameDialog dialog = getDialogManager()
                .getSetGameOptionsDialog(
                        new GameOptionsDialog.OnGameOptionsSetListener() {
                            @Override
                            public void onGameOptionsSet(final GameSettings gameSettings) {
                                hideDialog();
                                showWaitingRoom(multiplayerClient.getCurrentUserInfo(),
                                        gameSettings.numberOfPlayers.getNumberOfPlayers());
                                startMultiplayerGame(quickGame,
                                        gameSettings,
                                        playersLevel);
                            }

                            @Override
                            public void onDialogCanceled() {
                                hideDialog();
                            }
                        }
                );
        showDialog(dialog);
    }

    private void showWaitingRoom(final UserInfo user,
                                 final int players) {
        final WaitingRoomDialog dialog = getDialogManager()
                .getWaitingRoomDialog(
                        user,
                        players,
                        new DialogCallback() {
                            @Override
                            public void onButtonClicked() {
                                multiplayerClient.cancelGameSearch();
                                hideDialog();
                            }
                        });
        dialog.setSearchingGameMessage();
        showDialog(dialog);
    }

    private boolean checkUserSignedInStatus(final MenuEntry pendingAction) {
        if (!getPisti().getConnectivityUtils().isNetworkAvailable()) {
            final GameDialog dialog = getDialogManager()
                    .getNetworkUnavailableDialog(new DialogCallback() {
                        @Override
                        public void onButtonClicked() {
                            hideDialog();
                        }
                    });
            showDialog(dialog);
            return false;
        }

        if (!getPisti().getMultiplayerClient().isSignedIn()) {
            this.pendingAction = pendingAction;

            final GameDialog dialog = getDialogManager()
                    .getUserNotSignedDialog(
                            new DialogCallback() {
                                @Override
                                public void onButtonClicked() {
                                    getPisti().getMultiplayerClient().signIn();
                                    hideDialog();
                                    showLoadingDialog();
                                }
                            },
                            new DialogCallback() {
                                @Override
                                public void onButtonClicked() {
                                    hideDialog();
                                }
                            });
            showDialog(dialog);
            return false;
        }

        return true;
    }

    private void startMultiplayerGame(final boolean quickGame,
                                      final GameSettings gameSettings,
                                      final Level playersLevel) {
        if (quickGame) {
            multiplayerClient.quickGame(gameSettings,
                    playersLevel.getLevelValue());
        } else {
            multiplayerClient.playWithFriends(gameSettings);
        }
    }

    @Override
    public void onReadyToSetGameScreen(final GameManager gameManager) {
        final float x = getScreenUtils().getScreenWidth();
        final MoveByAction moveByAction = Actions.moveBy(
                -x,
                0,
                Res.values.SHOW_MENU_ANIMATION_DURATION,
                Interpolation.circleOut);

        slideScreenView.addAction(Actions.sequence(moveByAction,
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        getPisti().getScreenNavigator().setGameScreen();
                        gameManager.startGame();
                    }
                })));
    }

    @Override
    public void onConnectivityChanged(final boolean connected) {
        if (connected) {
            slideScreenView.enableConnectionDependentItems(true);
        } else {
            slideScreenView.enableConnectionDependentItems(false);
        }
    }
}