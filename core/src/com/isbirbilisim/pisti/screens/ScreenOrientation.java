package com.isbirbilisim.pisti.screens;

/**
 * Defines possible screen orientation.
 */
public enum ScreenOrientation {
    LANDSCAPE,
    PORTRAIT
}