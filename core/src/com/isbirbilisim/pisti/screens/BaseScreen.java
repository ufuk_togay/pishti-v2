package com.isbirbilisim.pisti.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.isbirbilisim.pisti.connectivity.ConnectivityListener;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.dialogs.BaseDialog;
import com.isbirbilisim.pisti.dialogs.DialogManager;
import com.isbirbilisim.pisti.dialogs.GameDialog;
import com.isbirbilisim.pisti.resources.Assets;

/**
 * Base screen that implements InputProcessor.
 */
public class BaseScreen extends ScreenAdapter implements InputProcessor, ConnectivityListener {

    private final Pisti pisti;

    private Stage stage;

    private BaseDialog shownDialog;

    public BaseScreen(final Pisti pisti) {
        this.pisti = pisti;
    }

    @Override
    public void show() {
        super.show();

        if (Gdx.app.getType() == Application.ApplicationType.iOS) {
            stage = new Stage(new ScreenViewport());
        } else {
            stage = new Stage();
        }

        final InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(this);
        multiplexer.addProcessor(stage);

        Gdx.input.setInputProcessor(multiplexer);

        pisti.getConnectivityUtils().addConnectivityListener(this);
    }

    @Override
    public void render(final float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void hide() {
        super.hide();

        pisti.getConnectivityUtils().removeConnectivityListener(this);
    }

    @Override
    public void dispose() {
        super.dispose();

        stage.dispose();
    }

    protected Stage getStage() {
        return stage;
    }

    public void showDialog(final BaseDialog baseDialog) {
        shownDialog = baseDialog;

        /*
         * Adding to all actors (except from dialog) listener that will block touch events.
         */
        for (final Actor actor : stage.getActors()) {
            actor.addCaptureListener(ignoreTouchDown);
        }

        shownDialog.show(stage);
    }

    public void hideDialog() {
        shownDialog.hide();

        /*
         * Removing from all actors listener that blocks touch events.
         */
        for (final Actor actor : stage.getActors()) {
            actor.removeCaptureListener(ignoreTouchDown);
        }

        shownDialog = null;
    }

    public BaseDialog getDialog() {
        return shownDialog;
    }

    protected Pisti getPisti() {
        return pisti;
    }

    protected DialogManager getDialogManager() {
        return pisti.getDialogManager();
    }

    protected Assets getAssets() {
        return pisti.getAssets();
    }

    protected ScreenUtils getScreenUtils() {
        return pisti.getScreenUtils();
    }

    public boolean isDialogShowing() {
        return shownDialog != null;
    }

    @Override
    public boolean keyDown(final int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(final int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(final char character) {
        return false;
    }

    @Override
    public boolean touchDown(final int screenX,
                             final int screenY,
                             final int pointer,
                             final int button) {
        return false;
    }

    @Override
    public boolean touchUp(final int screenX,
                           final int screenY,
                           final int pointer,
                           final int button) {
        return false;
    }

    @Override
    public boolean touchDragged(final int screenX,
                                final int screenY,
                                final int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(final int screenX,
                              final int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(final int amount) {
        return false;
    }

    private InputListener ignoreTouchDown = new InputListener() {
        public boolean touchDown(final InputEvent event,
                                 final float x,
                                 final float y,
                                 final int pointer,
                                 final int button) {
            event.cancel();
            return false;
        }
    };

    @Override
    public void onConnectivityChanged(final boolean connected) {
        // default implementation does nothing.
    }
}