package com.isbirbilisim.pisti.screens.mainmenu;

import com.isbirbilisim.pisti.resources.Strings;

public enum MenuEntry {
    SINGLE_PLAYER(false),
    PLAY_WITH_FRIENDS(true),
    INVITE_FRIENDS(true),
    QUICK_GAME(true),
    SINGLE_PLAYER_STATISTIC(true),
    ABOUT(false),
    LEADERBOARD(true),
    BUY_COINS(true),
    SETTINGS(false);

    private final boolean connectionRequired;

    MenuEntry(final boolean connectionRequired) {
        this.connectionRequired = connectionRequired;
    }

    public String getName(final Strings strings) {
        switch (this) {
            case SINGLE_PLAYER:
                return strings.singlePlayer;
            case PLAY_WITH_FRIENDS:
                return strings.playerWithFriends;
            case INVITE_FRIENDS:
                return strings.inviteFriends;
            case QUICK_GAME:
                return strings.quickGame;
            case SINGLE_PLAYER_STATISTIC:
                return strings.singlePlayerStatistic;
            case ABOUT:
                return strings.about;
            case LEADERBOARD:
                return strings.leaderboard;
            case BUY_COINS:
                return strings.buyCoins;
            case SETTINGS:
                return strings.settings;
            default:
                return "";
        }
    }

    public boolean isConnectionRequired() {
        return connectionRequired;
    }
}