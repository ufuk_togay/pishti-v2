package com.isbirbilisim.pisti;

import com.isbirbilisim.pisti.ad.AdManager;
import com.isbirbilisim.pisti.analytics.AnalyticsManager;
import com.isbirbilisim.pisti.backend.IBackendManager;
import com.isbirbilisim.pisti.connectivity.ConnectivityUtils;
import com.isbirbilisim.pisti.multiplayer.facebook.Facebook;
import com.isbirbilisim.pisti.resources.LocaleProvider;
import com.isbirbilisim.pisti.screens.ScreenUtils;
import com.isbirbilisim.pisti.security.SignatureUtils;
import com.isbirbilisim.pisti.view.ViewUtils;

/**
 * Interface for class that returns instances of game components,
 * implementation of which depend on specific platform.
 */
public interface ComponentManager {

    /**
     * @return - ScreenUtils implementation.
     */
    public ScreenUtils getScreenUtils();

    /**
     * @return - ViewUtils implementation.
     */
    public ViewUtils getViewUtils();

    /**
     * @return - implementation of ConnectivityUtils.
     */
    public ConnectivityUtils getConnectivityUtils();

    /**
     * @return - implementation of SignatureUtils.
     */
    public SignatureUtils getSignatureUtils();

    /**
     * @return - implementation of AdManager.
     */
    public AdManager getAdManager();

    /**
     * @return - implementation of analytics.
     */
    public AnalyticsManager getAnalyticsManager();

    /**
     * @return - implementation of LocaleProvider.
     */
    public LocaleProvider getLocaleProvider();

    /**
     * @return - implementation of PlatformInfo.
     */
    public PlatformInfo getPlatformInfo();

    /**
     * @return - implementation of FacebookClient.
     */
    public Facebook getFacebook();

    /**
     * @return - implementation of IBackendManager.
     */
    public IBackendManager getBackendManager();
}