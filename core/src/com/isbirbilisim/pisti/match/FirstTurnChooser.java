package com.isbirbilisim.pisti.match;

import com.isbirbilisim.pisti.players.Player;

import java.util.List;
import java.util.Random;

public class FirstTurnChooser {
    /**
     * Randomizes player that should move first.
     *
     * @param players - game participants.
     * @return - player that should move first.
     */
    public static Player getSingleGameFirstTurnPlayer(final List<Player> players) {
        final int pos = new Random().nextInt(players.size());
        return players.get(pos);
    }
}