package com.isbirbilisim.pisti.match;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;

public interface GameLifecycle {
    /**
     * Called when cards given to all players and appropriate animations are performed.
     * Since that moment players can take turn to move.
     */
    void onCardsGiven();

    /**
     * Called when its turn to move of some player.
     *
     * @param player - player that has turn to move.
     */
    void onPlayerTurnToMove(final Player player);

    /**
     * Called player made move. App should send message with
     * player's move to other game participants if game mode is multiplayer
     * AND player, that made move is not AI. Moves, made by AI players should
     * be predicted (according to Pisti rules, its possible) by all of the
     * client sides, that are participating in match.
     */
    void onPlayerMove(final Player player, final CardModel card);

    /**
     * Called when some player made move and took cards from table,
     * animation is already performed. Game should decide if it is end of game or
     * not (if not - give cards to players).
     */
    void onCardsRemovedFromTable();

    /**
     * According to Pisti rules game is over.
     *
     * @param matchResult - result of match for current player.
     * @param currentPlayer - current user.
     */
    void onMatchEnd(final MatchResult matchResult, final Player currentPlayer);
}
