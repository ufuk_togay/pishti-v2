package com.isbirbilisim.pisti.match;

import com.isbirbilisim.pisti.cards.CardModel;
import com.isbirbilisim.pisti.game.CardGame;
import com.isbirbilisim.pisti.game.GameManager;
import com.isbirbilisim.pisti.game.GameSettings;
import com.isbirbilisim.pisti.game.GameplayType;
import com.isbirbilisim.pisti.players.Player;

import java.util.Stack;

public interface MatchLifecycle {
    /**
     * Called when game is started (players are not connected yet).
     *
     * @param gameManager  - class, that provides methods for notifying
     *                     game participants about players move.
     * @param type         - type of card game.
     * @param gameplayType - type of gameplay.
     */
    void onGameCreated(final GameManager gameManager,
                       final CardGame.Type type,
                       final GameplayType gameplayType,
                       final GameSettings gameSettings);

    /**
     * Called when some player connected to match. View and game data should be updated
     * accordingly here.
     * <p/>
     * If game mode is single player - called when screen created.
     *
     * @param player - connected player.
     */
    void onPlayerConnected(final Player player);

    /**
     * Called when all players are connected to match. At this point players should
     * start performing some handshake mechanisms (sending initial data, etc.).
     * <p/>
     * If game mode is single player - called when screen created.
     */
    void onPlayersConnected();

    /**
     * Called when some player can't complete server sync.
     *
     * @param player - player which has sync issue.
     */
    void onPlayerSyncError(final Player player);

    /**
     * If game mode is single player - called when screen created.
     *
     * @param initialDeck - list of initial cards in deck.
     */
    void onDeckCreated(final Stack<CardModel> initialDeck);

    /**
     * Called when determined which player should move first.
     *
     * @param player - player that should move first.
     */
    void onFirstTurnPlayerDetermined(final Player player);

    /**
     * Called when all players are ready to play and deck is created.
     * <p/>
     * If game mode is single player - called when screen created.
     *
     * @param bidValue - match bid value. Equals 0 if game mod is single game.
     */
    void onHandshakeCompleted(final long bidValue);

    /**
     * Called only in multiplayer mode, when opponent makes move.
     */
    void onOpponentMove(final Player opponent, final CardModel card);

    /**
     * Called only in multiplayer mode.
     */
    void onChatMessageReceived(final Player player, final String message);

    /**
     * Called when some player leaved match. App should end match and update player's
     * scores accordingly.
     * <p/>
     * Will not be called in single game mode.
     */
    void onPlayerDisconnected(final Player player);
}