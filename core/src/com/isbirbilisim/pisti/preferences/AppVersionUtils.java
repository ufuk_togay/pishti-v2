package com.isbirbilisim.pisti.preferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;
import com.isbirbilisim.pisti.resources.Res;

public class AppVersionUtils {

    private static final String APP_VERSION_CODE_PROPERTY = "app_version_code";

    /**
     * Returns true, if application is updated (app version code is increased).
     */
    public static boolean isApplicationUpdated() {
        final int appVersionCode = getAppVersionCode();
        final int lastSavedVersionCode = AppPreferences.getInstance().readAppVersionCode();

        return appVersionCode > lastSavedVersionCode;
    }

    /**
     * Saves current app version code to preferences.
     */
    public static void updateSavedVersionCode() {
        final int appVersionCode = getAppVersionCode();

        AppPreferences.getInstance().writeAppVersionCode(appVersionCode);
    }

    private static int getAppVersionCode() {
        final FileHandle fileHandle = Gdx.files.internal(Res.properties.APP_VERSION);
        final I18NBundle bundle = I18NBundle.createBundle(fileHandle);

        return Integer.valueOf(bundle.get(APP_VERSION_CODE_PROPERTY));
    }
}