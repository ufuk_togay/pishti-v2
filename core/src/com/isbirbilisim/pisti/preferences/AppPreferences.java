package com.isbirbilisim.pisti.preferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Class that provides singleton to application preferences;
 */
public class AppPreferences {

    private static final String PISTI_PREFERENCES = "pisti_preferences";
    private static final String AD_SHOW_TIME_KEY = "ad_show_time";
    private static final String APP_VERSION_CODE_KEY = "version_code";
    private static final String VIBRATION_KEY = "vibration";
    private static final String SOUND_KEY = "sound";

    private static AppPreferences instance;

    private AppPreferences() {
        // Needed to avoid instance creation from outside.
    }

    public static AppPreferences getInstance() {
        if (instance == null) {
            instance = new AppPreferences();
        }

        return instance;
    }

    /**
     * @return - last time when full screen ad was shown.
     */
    public long getLastAdShowTime() {
        final Preferences preferences = getPreferences();
        if (!preferences.contains(AD_SHOW_TIME_KEY)) {
            return 0;
        }

        return preferences.getLong(AD_SHOW_TIME_KEY);
    }

    /**
     * Saves ad show time to preferences file.
     *
     * @param time - show time in milliseconds.
     */
    public void setLastAdShowTime(final long time) {
        final Preferences preferences = getPreferences();
        preferences.putLong(AD_SHOW_TIME_KEY, time);
        preferences.flush();
    }

    private Preferences getPreferences() {
        return Gdx.app.getPreferences(PISTI_PREFERENCES);
    }

    /**
     * Writes version code to preferences file.
     */
    public void writeAppVersionCode(final int versionCode) {
        final Preferences preferences = getPreferences();
        preferences.putInteger(APP_VERSION_CODE_KEY, versionCode);
        preferences.flush();
    }

    /**
     * Returns version code, stored in preferences file.
     * If value does not exist - returns 0.
     */
    public int readAppVersionCode() {
        final Preferences preferences = getPreferences();
        return preferences.getInteger(APP_VERSION_CODE_KEY, 0);
    }

    /**
     * @return true if vibration is enabled by user.
     */
    public boolean isVibrationEnabled() {
        final Preferences preferences = getPreferences();
        return preferences.getBoolean(VIBRATION_KEY, true);
    }

    /**
     * Enables/disabled vibration.
     *
     * @param enabled
     */
    public void setVibrationEnabled(final boolean enabled) {
        final Preferences preferences = getPreferences();
        preferences.putBoolean(VIBRATION_KEY, enabled);
        preferences.flush();
    }

    /**
     * @return true if sound is enabled;
     */
    public boolean isSoundEnabled() {
        final Preferences preferences = getPreferences();
        return preferences.getBoolean(SOUND_KEY, true);
    }

    /**
     * Enables/disables sound.
     *
     * @param enabled
     */
    public void setSoundEnabled(final boolean enabled) {
        final Preferences preferences = getPreferences();
        preferences.putBoolean(SOUND_KEY, enabled);
        preferences.flush();
    }
}