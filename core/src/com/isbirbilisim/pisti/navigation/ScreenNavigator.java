package com.isbirbilisim.pisti.navigation;

/**
 * Provides methods for navigating between app screens. Implementation depends on
 * specific platform. Should be instantiated once at the start of application.
 */
public interface ScreenNavigator {
    /**
     * Sets screen with game main menu.
     */
    void setMainMenuScreen();

    /**
     * Sets screen that provides menu for selecting opponents to play against.
     */
    void setPlayersSelectionScreen();

    /**
     * Sets game screen.
     */
    void setGameScreen();

    /**
     * Sets screen with game leaderboard.
     */
    void setLeaderboardScreen();

    /**
     * Sets screen with menu for managing tournaments.
     */
    void setTournamentsMenuScreen();
}