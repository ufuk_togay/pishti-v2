package com.isbirbilisim.pisti.navigation;

import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.screens.GameScreen;
import com.isbirbilisim.pisti.screens.MenuScreen;
import com.isbirbilisim.pisti.screens.SplashScreen;

/**
 * Screen navigator with default implementation.
 */
public class CoreScreenNavigator implements ScreenNavigator {
    private final Pisti pistiGame;

    private final SplashScreen splashScreen;

    private MenuScreen menuScreen;
    private GameScreen gameScreen;

    public CoreScreenNavigator(final Pisti pistiGame) {
        this.pistiGame = pistiGame;

        splashScreen = new SplashScreen(pistiGame);
    }

    public void showSplashScreen() {
        pistiGame.setScreen(splashScreen);
        splashScreen.setLoadingScreen(getMainMenuScreen());
    }

    @Override
    public void setMainMenuScreen() {
        pistiGame.setScreen(getMainMenuScreen());
    }

    private MenuScreen getMainMenuScreen() {
        if (menuScreen == null) {
            menuScreen = new MenuScreen(pistiGame);
        }

        return menuScreen;
    }

    @Override
    public void setPlayersSelectionScreen() {

    }

    @Override
    public void setGameScreen() {
        pistiGame.setScreen(getGameScreen());
    }

    @Override
    public void setLeaderboardScreen() {

    }

    @Override
    public void setTournamentsMenuScreen() {

    }

    public GameScreen getGameScreen() {
        if (gameScreen == null) {
            gameScreen = new GameScreen(pistiGame);
        }

        return gameScreen;
    }
}