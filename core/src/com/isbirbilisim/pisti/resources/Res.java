package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.graphics.Color;

/**
 * Defines needed app resources.
 */
public class Res {
    public static class fonts {
        public static final String GAME_FONT_PATH = "fonts/13947.ttf";

        public static final String FONT_1 = "FONT_1";
        public static final String FONT_2 = "FONT_2";
        public static final String FONT_3 = "FONT_3";
        public static final String FONT_4 = "FONT_4";
    }

    public static class colors {
        public static final Color[] playersColors = new Color[]{Color.BLUE,
                Color.GREEN, Color.ORANGE, Color.RED, Color.YELLOW};

        public static final Color MENU_VERTICAL_ITEM_LABEL_COLOR = Color.valueOf("fdefbcff");
        public static final Color MENU_VERTICAL_ITEM_LABEL_COLOR_DISABLED = Color.valueOf("8c8154ff");
        public static final Color MENU_VERTICAL_ITEM_LABEL_COLOR_PRESSED = Color.valueOf("ddb310ff");

        public static final Color MENU_HORIZONTAL_ITEM_LABEL_COLOR = Color.valueOf("fdefbcff");
        public static final Color MENU_HORIZONTAL_ITEM_LABEL_COLOR_DISABLED = Color.valueOf("8c8154ff");
        public static final Color MENU_HORIZONTAL_ITEM_LABEL_COLOR_PRESSED = Color.valueOf("ddb310ff");


        public static final Color DIALOG_RADIO_BUTTON_TEXT_COLOR = Color.valueOf("fdefbcff");
        public static final Color DIALOG_CONTENT_COLOR = Color.valueOf("ddb310ff");
        public static final Color DIALOG_CONTENT_LINK_COLOR = Color.valueOf("3280ED");
        public static final Color DIALOG_CONTENT_DISABLED_COLOR = Color.valueOf("7e6810ff");
        public static final Color FINAL_SCORES_COUNTERS_COLOR = Color.valueOf("ffffffff");

        public static final Color GAME_SCREEN_LABEL_COLOR = Color.valueOf("fdefbcff");

        public static final Color GAME_SCREEN_SETTINGS_BUTTON_PRESSED_COLOR = Color.valueOf("ddb310ff");

        public static final Color MENU_GOLD_LABEL_COLOR =
                new Color(0.55F, 0.42F, 0.14F, 1);

        public static final Color TOTAL_SCORE_COLOR = Color.CYAN;
        public static final Color CARD_COUNT_COLOR = Color.valueOf("00B0B0ff");
        public static final Color MULTIROUND_COLOR = Color.valueOf("008080ff");
        public static final Color CHAT_BUTTON_TEXT_COLOR = Color.valueOf("34282aff");
    }

    public static class textures {
        /**
         * Dialogs textures.
         */
        public static final String DIALOG_BACKGROUND_PATH = "frame_uncut.png";
        public static final String DIALOG_BACKGROUND = "DIALOG_BACKGROUND";

        public static final String DIALOG_OK_PATH = "main_menu/dialog_ok.png";
        public static final String DIALOG_OK_PRESSED_PATH = "main_menu/dialog_ok_pressed.png";

        public static final String DIALOG_BACK_PATH = "main_menu/dialog_back.png";
        public static final String DIALOG_BACK_PRESSED_PATH = "main_menu/dialog_back_pressed.png";

        public static final String RADIO_BUTTON_PATH = "main_menu/radio_button_unchecked.png";
        public static final String RADIO_BUTTON_CHECKED_PATH = "main_menu/radio_button_checked.png";

        public static final String CHECKBOX_PATH = "main_menu/checkbox.png";
        public static final String CHECKBOX_CHECKED_PATH = "main_menu/checkbox_checked.png";

        public static final String LEAVE_GAME_ICON_PATH = "game_screen/leave_game_icon.png";
        public static final String LEAVE_GAME_ICON = "LEAVE_GAME_ICON";

        public static final String STUDIO_ICON = "STUDIO_ICON";
        public static final String STUDIO_LOGO_PATH = "main_menu/logo_picarex.png";

        /**
         * Splash screen textures
         */
        public static final String SPLASH_BACKGROUND = "SPLASH_BACKGROUND";
        public static final String GAME_LOGO = "GAME_LOGO";

        public static final String SPLASH_BACKGROUND_PATH = "main_menu/splash_screen_background.png";
        public static final String GAME_LOGO_PATH = "main_menu/logo.png";

        /**
         * Main menu resources
         */
        public static final String SPINNER = "main_menu/spinner.png";
        public static final String MENU_BACKGROUND_PATH = "main_menu/main_menu_background.png";
        public static final String FACEBOOK_ICON_PATH = "main_menu/facebook.png";
        public static final String GOOGLE_PLUS_ICON_PATH = "main_menu/google_plus.png";
        public static final String ICON_BUY_COINS_PATH = "main_menu/icon_buy_coins.png";
        public static final String ICON_COIN_PATH = "main_menu/icon_coin.png";
        public static final String ICON_INVITE_FRIENDS_PATH = "main_menu/icon_invite_friends.png";
        public static final String ICON_LEADERBOARD_PATH = "main_menu/icon_leaderboard.png";
        public static final String ICON_PLAY_WITH_FRIENDS_PATH = "main_menu/icon_play_with_friends.png";
        public static final String ICON_SETTINGS_PATH = "main_menu/icon_settings.png";
        public static final String ICON_SINGLE_GAME_PATH = "main_menu/icon_single_game.png";
        public static final String ICON_QUICK_GAME_PATH = "main_menu/quick_game_icon.png";
        public static final String ICON_ABOUT_PATH = "main_menu/icon_about.png";
        public static final String BIG_VERTICAL_LINE_PATH = "main_menu/big_vertical_line.png";
        public static final String HORIZONTAL_LINE_PATH = "main_menu/horizontal_line.png";
        public static final String VERTICAL_LINE_1_PATH = "main_menu/vertical_line_1.png";
        public static final String VERTICAL_LINE_2_PATH = "main_menu/vertical_line_2.png";

        public static final String MENU_BACKGROUND = "MENU_BACKGROUND";
        public static final String FACEBOOK_ICON = "FACEBOOK_ICON";
        public static final String GOOGLE_PLUS_ICON = "GOOGLE_PLUS_ICON";
        public static final String ICON_BUY_COINS = "ICON_BUY_COINS";
        public static final String ICON_COIN = "ICON_COIN";
        public static final String ICON_INVITE_FRIENDS = "ICON_INVITE_FRIENDS";
        public static final String ICON_LEADERBOARD = "ICON_LEADERBOARD";
        public static final String ICON_PLAY_WITH_FRIENDS = "ICON_PLAY_WITH_FRIENDS";
        public static final String ICON_SETTINGS = "ICON_SETTINGS";
        public static final String ICON_SINGLE_GAME = "ICON_SINGLE_GAME";
        public static final String ICON_QUICK_GAME = "ICON_QUICK_GAME";
        public static final String ICON_ABOUT = "ABOUT_ICON";
        public static final String BIG_VERTICAL_LINE = "BIG_VERTICAL_LINE";
        public static final String HORIZONTAL_LINE = "HORIZONTAL_LINE";
        public static final String VERTICAL_LINE_1 = "VERTICAL_LINE_1";
        public static final String VERTICAL_LINE_2 = "VERTICAL_LINE_2";

        public static final String VIBRATION_ICON_PATH = "main_menu/vibration.png";
        public static final String SOUND_ICON_PATH = "main_menu/sound.png";
        public static final String VIBRATION_ICON = "VIBRATION_ICON";
        public static final String SOUND_ICON = "SOUND_ICON";

        /**
         * Game screen resources
         */
        public static final String CHAT_BUTTON_PATH = "game_screen/chat_button.png";
        public static final String CHAT_BUTTON_PRESSED_PATH = "game_screen/chat_button_pressed.png";
        public static final String CHAT_NOTIFICATION_PATH = "game_screen/chat_notification.png";
        public static final String CHAT_WINDOW_BACKGROUND_PATH = "game_screen/chat_window_background.png";
        public static final String CHAT_WINDOW_FRAME_PATH = "game_screen/chat_window_frame.png";
        public static final String CHAT_OUTPUT_BACKGROUND_PATH = "game_screen/chat-bubble.png";
        public static final String CLOSE_CHAT_BUTTON_PATH = "game_screen/close_chat_button.png";
        public static final String CLOSE_CHAT_BUTTON_PRESSED_PATH = "game_screen/close_chat_button_pressed.png";
        public static final String CURSOR_PATH = "game_screen/cursor.png";
        public static final String TEXT_SELECTION_PATH = "game_screen/text_selection.png";
        public static final String DEALER_ICON_PATH = "game_screen/dealer_icon.png";
        public static final String GAME_SCREEN_BACKGROUND_PATH = "game_screen/game_screen_background.png";
        public static final String GAME_SCREEN_LOGO_MAIN_PATH = "game_screen/logo_main.png";
        public static final String GAME_SCREEN_LOGO_PISTI_PATH = "game_screen/logo_pisti.png";
        public static final String GAME_SCREEN_LOGO_PISTI_LIGHT_PATH = "game_screen/logo_pisti_light.png";
        public static final String GAME_SCREEN_LOGO_LEFT_PATH = "game_screen/logo_left.png";
        public static final String GAME_SCREEN_LOGO_RIGHT_PATH = "game_screen/logo_right.png";
        public static final String GAME_SCREEN_LOGO_TOP_PATH = "game_screen/logo_top.png";
        public static final String GAME_SCREEN_LOGO_BOTTOM_PATH = "game_screen/logo_bottom.png";
        public static final String GAME_SCREEN_LOGO_LEFT_BACK_PATH = "game_screen/logo_left_back.png";
        public static final String GAME_SCREEN_LOGO_RIGHT_BACK_PATH = "game_screen/logo_right_back.png";
        public static final String GAME_SCREEN_LOGO_TOP_BACK_PATH = "game_screen/logo_top_back.png";
        public static final String GAME_SCREEN_LOGO_BOTTOM_BACK_PATH = "game_screen/logo_bottom_back.png";
        public static final String GAME_SCREEN_LOGO_SUPER_PATH = "game_screen/super_colorful.png";
        public static final String MUTE_DEALER_BUTTON_PATH = "game_screen/mute_dealer_button.png";
        public static final String MUTE_DEALER_BUTTON_PRESSED_PATH = "game_screen/mute_dealer_button_pressed.png";
        public static final String MUTE_PLAYER_BUTTON_PATH = "game_screen/mute_player_button.png";
        public static final String MUTE_PLAYER_BUTTON_PRESSED_PATH = "game_screen/mute_player_button_pressed.png";
        public static final String PLAYER_PHOTO_FRAME_PATH = "game_screen/player_photo_frame.png";
        public static final String SLIDER_PATH = "game_screen/slider.png";
        public static final String GAME_MENU_BACKGROUND_PATH = "game_screen/game_menu_background.png";
        public static final String MUTE_PLAYERS_ICON_PATH = "game_screen/mute_players_button.png";

        public static final String CHAT_BUTTON = "CHAT_BUTTON";
        public static final String CHAT_BUTTON_PRESSED = "CHAT_BUTTON_PRESSED";
        public static final String CHAT_NOTIFICATION = "CHAT_NOTIFICATION";
        public static final String CHAT_WINDOW_BACKGROUND = "CHAT_WINDOW_BACKGROUND";
        public static final String CHAT_WINDOW_FRAME = "CHAT_WINDOW_FRAME";
        public static final String CHAT_OUTPUT_BACKGROUND = "CHAT_OUTPUT_BACKGROUND";
        public static final String CLOSE_CHAT_BUTTON = "CLOSE_CHAT_BUTTON";
        public static final String CLOSE_CHAT_BUTTON_PRESSED = "CLOSE_CHAT_BUTTON_PRESSED";
        public static final String CURSOR = "CURSOR";
        public static final String TEXT_SELECTION = "TEXT_SELECTION";
        public static final String DEALER_ICON = "DEALER_ICON";
        public static final String GAME_SCREEN_BACKGROUND = "GAME_SCREEN_BACKGROUND";
        public static final String GAME_SCREEN_LOGO_MAIN = "GAME_SCREEN_LOGO_MAIN";
        public static final String GAME_SCREEN_LOGO_PISTI = "GAME_SCREEN_LOGO_PISTI";
        public static final String GAME_SCREEN_LOGO_PISTI_LIGHT = "GAME_SCREEN_LOGO_PISTI_LIGHT";
        public static final String GAME_SCREEN_LOGO_LEFT = "GAME_SCREEN_LOGO_LEFT";
        public static final String GAME_SCREEN_LOGO_RIGHT = "GAME_SCREEN_LOGO_RIGHT";
        public static final String GAME_SCREEN_LOGO_TOP = "GAME_SCREEN_LOGO_TOP";
        public static final String GAME_SCREEN_LOGO_BOTTOM = "GAME_SCREEN_LOGO_BOTTOM";
        public static final String GAME_SCREEN_LOGO_LEFT_BACK = "GAME_SCREEN_LOGO_LEFT_BACK";
        public static final String GAME_SCREEN_LOGO_RIGHT_BACK = "GAME_SCREEN_LOGO_RIGHT_BACK";
        public static final String GAME_SCREEN_LOGO_TOP_BACK = "GAME_SCREEN_LOGO_TOP_BACK";
        public static final String GAME_SCREEN_LOGO_BOTTOM_BACK = "GAME_SCREEN_LOGO_BOTTOM_BACK";
        public static final String GAME_SCREEN_LOGO_SUPER = "GAME_SCREEN_LOGO_SUPER";
        public static final String MUTE_DEALER_BUTTON = "MUTE_DEALER_BUTTON";
        public static final String MUTE_DEALER_BUTTON_PRESSED = "MUTE_DEALER_BUTTON_PRESSED";
        public static final String MUTE_PLAYER_BUTTON = "MUTE_PLAYER_BUTTON";
        public static final String MUTE_PLAYER_BUTTON_PRESSED = "MUTE_PLAYER_BUTTON_PRESSED";
        public static final String PLAYER_PHOTO_FRAME = "PLAYER_PHOTO_FRAME";
        public static final String SLIDER = "SLIDER";
        public static final String GAME_MENU_BACKGROUND = "GAME_MENU_BACKGROUND";
        public static final String MUTE_PLAYERS_ICON = "MUTE_PLAYERS_ICON";

        public static final String PHOTO_TIMER_PATH = "photo_timer.png";
        public static final String PHOTO_TIMER = "PHOTO_TIMER";
        public static final String PHOTO_TURN_PATH = "photo_turn.png";
        public static final String PHOTO_TURN = "PHOTO_TURN";

        public static final String AVATAR_MALE_PATH = "avatar_male.png";
        public static final String AVATAR_MALE = "AVATAR_MALE";

        public static final String AVATAR_FEMALE_PATH = "avatar_female.png";
        public static final String AVATAR_FEMALE = "AVATAR_FEMALE";

        public static final String AVATAR_BOT_PATH = "avatar_bot.png";
        public static final String AVATAR_BOT = "AVATAR_BOT";

        public static final String MAIN_MENU_COIN = "menu/coin.png";
    }

    public static class values {
        public static final int FONT_1_SIZE = 60;
        public static final int FONT_2_SIZE = 45;
        public static final int FONT_3_SIZE = 35;
        public static final int FONT_4_SIZE = 30;
        //----------------------------------------------------------------------

        public static final int LINE_THICKNESS = 5;
        public static final int PHOTO_FRAME_THICKNESS = 4;

        public static final int SOCIAL_BUTTON_SIZE = 75;

        public static final int COIN_SIZE = 75;

        public static final int GAME_SCREEN_LOGO_WIDTH = 300;
        public static final int GAME_SCREEN_LOGO_HEIGHT = 230;

        public static final int AD_TOP_PAD = 65;

        public static final int GAME_SCREEN_SETTINGS_ICON_WIDTH = 80;
        public static final int GAME_SCREEN_SETTINGS_ICON_HEIGHT = 80;
        public static final int GAME_SCREEN_SETTINGS_ICON_PAD_NO_AD = 120;
        public static final int GAME_SCREEN_SETTINGS_ICON_PAD_WITH_AD = 155;

        public static final int GAME_SCREEN_MENU_WIDTH = 400;
        public static final int GAME_SCREEN_MENU_OPTIONS_PAD_TOP_NO_AD = 120;
        public static final int GAME_SCREEN_MENU_OPTIONS_PAD_TOP_WITH_AD = 145;

        public static final int MOVE_TURN_VIBRATION_DURATION = 200;
        public static final int PISTI_VIBRATION_DURATION = 150;
        public static final int SUPER_PISTI_VIBRATION_DURATION = 150;

        public static final int GAME_ROOM_DIALOG_WIDTH = 850;
        public static final int GAME_ROOM_DIALOG_HEIGHT = 450;
        public static final int LEVELS_PADDING = 10;

        public static final float SHOW_MENU_ANIMATION_DURATION = 0.6F;

        public static final int HIDE_MENU_BUTTON_SIZE = GAME_SCREEN_SETTINGS_ICON_WIDTH;

        public static final int FINAL_SCORES_DIALOG_WIDTH = 850;
        public static final int FINAL_SCORES_2_DIALOG_HEIGHT = 400;
        public static final int FINAL_SCORES_4_DIALOG_HEIGHT = 500;
        public static final int COMING_SOON_DIALOG_WIDTH = 400;
        public static final int COMING_SOON_DIALOG_HEIGHT = 200;
        public static final int ABOUT_DIALOG_WIDTH = 500;
        public static final int ABOUT_DIALOG_HEIGHT = 450;
        public static final int LEAVE_GAME_DIALOG_WIDTH = 500;
        public static final int LEAVE_GAME_DIALOG_HEIGHT = 200;
        public static final int GAME_SETTINGS_DIALOG_WIDTH = 650;
        public static final int GAME_SETTINGS_DIALOG_HEIGHT = 280;
        public static final int SETTINGS_DIALOG_WIDTH = 600;
        public static final int SETTINGS_DIALOG_HEIGHT = 500;
        public static final int LOADING_PLAYER_DIALOG_WIDTH = 400;
        public static final int LOADING_PLAYER_DIALOG_HEIGHT = 300;
        public static final int GAME_SEARCH_DIALOG_WIDTH = 500;
        public static final int GAME_SEARCH_DIALOG_HEIGHT = 300;
        public static final int NETWORK_REQUIRED_DIALOG_WIDTH = 550;
        public static final int NETWORK_REQUIRED_DIALOG_HEIGHT = 300;
        public static final int USER_NOT_SIGNED_DIALOG_WIDTH = 600;
        public static final int USER_NOT_SIGNED_DIALOG_HEIGHT = 300;
        public static final int USER_STATS_DIALOG_WIDTH = 520;
        public static final int USER_STATS_DIALOG_HEIGHT = 400;
        public static final int MULTIPLAYER_ERROR_DIALOG_WIDTH = 600;
        public static final int MULTIPLAYER_ERROR_DIALOG_HEIGHT = 300;
        public static final int SPINNER_SIZE = 150;
        public static final int WAITING_ROOM_WIDTH = 700;
        public static final int WAITING_ROOM_HEIGHT_2 = 400;
        public static final int WAITING_ROOM_HEIGHT_4 = 500;

        public static final int DIALOG_FRAME_PADDING = 50;
        public static final int DIALOG_TITLE_PAD_TOP = 100;
        public static final int DIALOG_MESSAGE_PAD = 25;

        public static final int RADIO_BUTTON_SIZE = 40;

        public static final int FINAL_SCORES_2_PHOTO_SIZE = 80;
        public static final int FINAL_SCORES_4_PHOTO_SIZE = 60;

        public static final int CHAT_BUTTON_WIDTH = 80;
        public static final int CHAT_BUTTON_HEIGHT = 45;
        public static final int CHAT_INPUT_WIDTH = 550;
        public static final int CHAT_INPUT_HEIGHT = 60;
        public static final int CHAT_CANCEL_SIZE = 40;
        public static final int CHAT_TEXT_MARGIN = 23;
    }

    public static class settings {
        public static final boolean showingAd = true;
    }

    public static class buttonStyles {
        public static final String SMALL_CHECKBOX_STYLE = "CHECKBOX_STYLE";
        public static final String LARGE_CHECKBOX_STYLE = "CHECKBOX_STYLE";
        public static final String RADIO_BUTTON_STYLE = "RADIO_BUTTON_STYLE";
        public static final String DIALOG_OK_BUTTON_STYLE = "DIALOG_OK_BUTTON_STYLE";
        public static final String DIALOG_BACK_BUTTON_STYLE = "DIALOG_BACK_BUTTON_STYLE";
        public static final String MENU_VERTICAL_ITEM_STYLE = "MENU_VERTICAL_ITEM_STYLE";
        public static final String MENU_HORIZONTAL_ITEM_STYLE = "MENU_HORIZONTAL_ITEM_STYLE";
        public static final String CHAT_BUTTON_STYLE = "CHAT_BUTTON_STYLE";
    }

    public static class labelStyles {
        public static final String GAME_SCREEN_SMALL_LABEL_STYLE = "GAME_SCREEN_SMALL_LABEL_STYLE";
        public static final String GAME_SCREEN_LARGE_LABEL_STYLE = "GAME_SCREEN_LARGE_LABEL_STYLE";

        public static final String LEVEL_LABEL_STYLE = "LEVEL_LABEL_STYLE";
        public static final String GOLD_AMOUNT_LABEL_STYLE = "GOLD_AMOUNT_LABEL_STYLE";

        public static final String DIALOG_CONTENT_LARGE_LABEL_STYLE = "DIALOG_CONTENT_LARGE_LABEL_STYLE";
        public static final String DIALOG_CONTENT_SMALL_LABEL_STYLE = "DIALOG_CONTENT_SMALL_LABEL_STYLE";
        public static final String DIALOG_CONTENT_LINK_LABEL_STYLE = "DIALOG_CONTENT_LINK_LABEL_STYLE";
        public static final String CHAT_INPUT_STYLE = "CHAT_INPUT_STYLE";
        public static final String CHAT_OUTPUT_STYLE = "CHAT_OUTPUT_STYLE";
    }

    public static class properties {
        public static final String STRINGS = "STRINGS";
        public static final String PROPERTIES_DEFAULT = "bundles/Pisti";
        public static final String APP_VERSION = "bundles/version";
    }

    public static class sounds {
        public static final String CARD_SHUFFLE_SOUND = "sounds/shuffling-cards-4.mp3";
        public static final String BUTTON_SOUND = "sounds/button-29.mp3";
        public static final String CARD_BONUS = "sounds/cardShove1.wav";
    }
}