package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.isbirbilisim.pisti.Pisti;
import com.isbirbilisim.pisti.cards.DeckFactory;

/**
 * Class that manages application assets.
 */
public class Assets {

    private final Pisti pisti;
    private final AssetManager assetManager;

    /**
     * Path to folder with resources that corresponds to screen size.
     */
    private final String resourcePrefix;


    public Assets(final Pisti pisti) {
        this.pisti = pisti;

        resourcePrefix = getResourceFolder();

        assetManager = new AssetManager();
        assetManager.setLoader(BitmapFont.class, new FontLoader());
        assetManager.setLoader(Strings.class, new StringsLoader());
    }

    private String getResourceFolder() {
        return "1080/";
    }

    /**
     * Loads localized strings.
     */
    public void loadLocalizedStrings() {
        final StringsParams stringsParams = new StringsParams(
                pisti.getLocaleProvider().getLocale(),
                pisti.getLocaleProvider().getLocaleIdentifier());

        assetManager.load(Res.properties.PROPERTIES_DEFAULT, Strings.class,
                stringsParams);
    }

    /**
     * Loads skin for splash screen.
     */
    public void loadSplashScreenSkin() {
        assetManager.load(resourcePrefix + Res.textures.SPLASH_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_LOGO_PATH, Texture.class);
        assetManager.finishLoading();
    }

    /**
     * Starts loading assets used in main menu screen.
     */
    public void loadMainMenuScreenAssets() {
        assetManager.load(resourcePrefix + Res.textures.MENU_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.FACEBOOK_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GOOGLE_PLUS_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_BUY_COINS_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_COIN_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_INVITE_FRIENDS_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_LEADERBOARD_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_PLAY_WITH_FRIENDS_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_SETTINGS_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_SINGLE_GAME_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_QUICK_GAME_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_ABOUT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.BIG_VERTICAL_LINE_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.HORIZONTAL_LINE_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.VERTICAL_LINE_1_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.VERTICAL_LINE_2_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_MAIN_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_SUPER_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.SPINNER, Texture.class);
    }

    /**
     * Starts loading assets used in dialog.
     */
    public void loadDialogAssets() {
        assetManager.load(Res.textures.DIALOG_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.DIALOG_OK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.DIALOG_OK_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.RADIO_BUTTON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.RADIO_BUTTON_CHECKED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.DIALOG_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.DIALOG_BACK_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHECKBOX_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHECKBOX_CHECKED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.VIBRATION_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.SOUND_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.LEAVE_GAME_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.PLAYER_PHOTO_FRAME_PATH, Texture.class);
        assetManager.load(Res.textures.AVATAR_MALE_PATH, Texture.class);
        assetManager.load(Res.textures.AVATAR_FEMALE_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.STUDIO_LOGO_PATH, Texture.class);
    }

    /**
     * Starts loading screen used in game screen.
     */
    public void loadGameScreenAssets() {
        for (final String resourceFile : DeckFactory.getCardResourcesList()) {
            assetManager.load(resourceFile, Texture.class);
        }
        assetManager.load(resourcePrefix + Res.textures.CHAT_BUTTON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHAT_BUTTON_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHAT_NOTIFICATION_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHAT_WINDOW_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHAT_WINDOW_FRAME_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CLOSE_CHAT_BUTTON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CLOSE_CHAT_BUTTON_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.DEALER_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.ICON_SETTINGS_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_MAIN_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_SUPER_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.MUTE_DEALER_BUTTON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.MUTE_DEALER_BUTTON_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.MUTE_PLAYER_BUTTON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.MUTE_PLAYER_BUTTON_PRESSED_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.PLAYER_PHOTO_FRAME_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.SLIDER_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.GAME_MENU_BACKGROUND_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.BIG_VERTICAL_LINE_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.LEAVE_GAME_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.MUTE_PLAYERS_ICON_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CURSOR_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.TEXT_SELECTION_PATH, Texture.class);
        assetManager.load(resourcePrefix + Res.textures.CHAT_OUTPUT_BACKGROUND_PATH, Texture.class);
        assetManager.load(Res.textures.PHOTO_TIMER_PATH, Texture.class);
        assetManager.load(Res.textures.PHOTO_TURN_PATH, Texture.class);
    }

    /**
     * Loads game fonts.
     */
    public void loadFonts() {
        assetManager.load(
                Res.fonts.FONT_1,
                BitmapFont.class,
                new BitmapFontParams(Res.values.FONT_1_SIZE));
        assetManager.load(
                Res.fonts.FONT_2,
                BitmapFont.class,
                new BitmapFontParams(Res.values.FONT_2_SIZE));
        assetManager.load(
                Res.fonts.FONT_3,
                BitmapFont.class,
                new BitmapFontParams(Res.values.FONT_3_SIZE));
        assetManager.load(
                Res.fonts.FONT_4,
                BitmapFont.class,
                new BitmapFontParams(Res.values.FONT_4_SIZE));
    }

    /**
     * @see {@link com.badlogic.gdx.assets.AssetManager#update()}
     */
    public boolean update() {
        return assetManager.update();
    }

    /**
     * @see {@link com.badlogic.gdx.assets.AssetManager#getProgress()}
     */
    public float getProgress() {
        return assetManager.getProgress();
    }

    /**
     * @see {@link com.badlogic.gdx.assets.AssetManager#dispose()}
     */
    public void dispose() {
        assetManager.dispose();
    }

    /**
     * @see {@link com.badlogic.gdx.assets.AssetManager#finishLoading()}
     */
    public void finishLoading() {
        assetManager.finishLoading();
    }

    /**
     * @return - skin with assets for SplashScreen. Call {@link #loadSplashScreenSkin()}
     * before calling this method.
     */
    public Skin getSplashScreenSkin() {
        final Skin skin = new Skin();
        skin.add(Res.textures.SPLASH_BACKGROUND,
                assetManager.get(resourcePrefix + Res.textures.SPLASH_BACKGROUND_PATH,
                        Texture.class));
        skin.add(Res.textures.GAME_LOGO,
                assetManager.get(resourcePrefix + Res.textures.GAME_LOGO_PATH,
                        Texture.class));

        return skin;
    }

    /**
     * @return - skin with main menu assets. Call {@link #loadMainMenuScreenAssets()}
     * and wait until {@link #update()} will return true before using this
     * method.
     */
    public Skin getMainMenuSkin() {
        final Skin mainMenuSkin = new Skin();

        mainMenuSkin.add(Res.textures.ICON_SETTINGS,
                get(resourcePrefix + Res.textures.ICON_SETTINGS_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.MENU_BACKGROUND,
                get(resourcePrefix + Res.textures.MENU_BACKGROUND_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.FACEBOOK_ICON,
                get(resourcePrefix + Res.textures.FACEBOOK_ICON_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GOOGLE_PLUS_ICON,
                get(resourcePrefix + Res.textures.GOOGLE_PLUS_ICON_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_BUY_COINS,
                get(resourcePrefix + Res.textures.ICON_BUY_COINS_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_COIN,
                get(resourcePrefix + Res.textures.ICON_COIN_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_INVITE_FRIENDS,
                get(resourcePrefix + Res.textures.ICON_INVITE_FRIENDS_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_LEADERBOARD,
                get(resourcePrefix + Res.textures.ICON_LEADERBOARD_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_PLAY_WITH_FRIENDS,
                get(resourcePrefix + Res.textures.ICON_PLAY_WITH_FRIENDS_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_SETTINGS,
                get(resourcePrefix + Res.textures.ICON_SETTINGS_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_SINGLE_GAME,
                get(resourcePrefix + Res.textures.ICON_SINGLE_GAME_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_QUICK_GAME,
                get(resourcePrefix + Res.textures.ICON_QUICK_GAME_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.ICON_ABOUT,
                get(resourcePrefix + Res.textures.ICON_ABOUT_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.BIG_VERTICAL_LINE,
                get(resourcePrefix + Res.textures.BIG_VERTICAL_LINE_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.HORIZONTAL_LINE,
                get(resourcePrefix + Res.textures.HORIZONTAL_LINE_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.VERTICAL_LINE_1,
                get(resourcePrefix + Res.textures.VERTICAL_LINE_1_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.VERTICAL_LINE_2,
                get(resourcePrefix + Res.textures.VERTICAL_LINE_2_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_MAIN,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_MAIN_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_PISTI,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_LEFT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_RIGHT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_TOP,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_BOTTOM,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_LEFT_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_BACK_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_TOP_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_BACK_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.GAME_SCREEN_LOGO_SUPER,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_SUPER_PATH, Texture.class));
        mainMenuSkin.add(Res.textures.SPINNER,
                get(resourcePrefix + Res.textures.SPINNER, Texture.class));
        mainMenuSkin.add(Res.buttonStyles.MENU_VERTICAL_ITEM_STYLE,
                getVerticalMenuItemStyle(),
                ImageTextButton.ImageTextButtonStyle.class);
        mainMenuSkin.add(Res.buttonStyles.MENU_HORIZONTAL_ITEM_STYLE,
                getHorizontalMenuItemStyle(),
                ImageTextButton.ImageTextButtonStyle.class);
        mainMenuSkin.add(Res.labelStyles.GOLD_AMOUNT_LABEL_STYLE,
                getGoldAmountLabelStyle(),
                Label.LabelStyle.class);

        mainMenuSkin.add(Res.properties.STRINGS,
                get(Res.properties.PROPERTIES_DEFAULT, Strings.class));

        return mainMenuSkin;
    }

    private ImageTextButton.ImageTextButtonStyle getVerticalMenuItemStyle() {
        final ImageTextButton.ImageTextButtonStyle style =
                new ImageTextButton.ImageTextButtonStyle();

        style.font = get(Res.fonts.FONT_1, BitmapFont.class);
        style.fontColor = Res.colors.MENU_VERTICAL_ITEM_LABEL_COLOR;
        style.disabledFontColor = Res.colors.MENU_VERTICAL_ITEM_LABEL_COLOR_DISABLED;
        style.downFontColor = Res.colors.MENU_VERTICAL_ITEM_LABEL_COLOR_PRESSED;

        return style;
    }

    private ImageTextButton.ImageTextButtonStyle getHorizontalMenuItemStyle() {
        final ImageTextButton.ImageTextButtonStyle style =
                new ImageTextButton.ImageTextButtonStyle();

        style.font = get(Res.fonts.FONT_3, BitmapFont.class);
        style.fontColor = Res.colors.MENU_HORIZONTAL_ITEM_LABEL_COLOR;
        style.disabledFontColor = Res.colors.MENU_HORIZONTAL_ITEM_LABEL_COLOR_DISABLED;
        style.downFontColor = Res.colors.MENU_HORIZONTAL_ITEM_LABEL_COLOR_PRESSED;

        return style;
    }

    private Label.LabelStyle getGoldAmountLabelStyle() {
        final Label.LabelStyle style = new Label.LabelStyle();

        style.font = get(Res.fonts.FONT_1, BitmapFont.class);
        style.fontColor = Res.colors.MENU_GOLD_LABEL_COLOR;

        return style;
    }

    /**
     * @return - skin with game screen assets. Call {@link #loadGameScreenAssets()}
     * and wait until {@link #update()} will return true before using this
     * method.
     */
    public Skin getGameScreenSkin() {
        final Skin gameScreenSkin = new Skin();

        for (final String resourceFile : DeckFactory.getCardResourcesList()) {
            gameScreenSkin.add(resourceFile, get(resourceFile, Texture.class));
        }
        gameScreenSkin.add(Res.textures.CHAT_BUTTON,
                get(resourcePrefix + Res.textures.CHAT_BUTTON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CHAT_BUTTON_PRESSED,
                get(resourcePrefix + Res.textures.CHAT_BUTTON_PRESSED_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CHAT_NOTIFICATION,
                get(resourcePrefix + Res.textures.CHAT_NOTIFICATION_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CHAT_WINDOW_BACKGROUND,
                get(resourcePrefix + Res.textures.CHAT_WINDOW_BACKGROUND_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CHAT_WINDOW_FRAME,
                get(resourcePrefix + Res.textures.CHAT_WINDOW_FRAME_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CLOSE_CHAT_BUTTON,
                get(resourcePrefix + Res.textures.CLOSE_CHAT_BUTTON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CLOSE_CHAT_BUTTON_PRESSED,
                get(resourcePrefix + Res.textures.CLOSE_CHAT_BUTTON_PRESSED_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.CHAT_OUTPUT_BACKGROUND,
                get(resourcePrefix + Res.textures.CHAT_OUTPUT_BACKGROUND_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.DEALER_ICON,
                get(resourcePrefix + Res.textures.DEALER_ICON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_BACKGROUND,
                get(resourcePrefix + Res.textures.GAME_SCREEN_BACKGROUND_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_MAIN,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_MAIN_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_PISTI,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_PISTI_LIGHT_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_LEFT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_RIGHT,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_TOP,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_BOTTOM,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_LEFT_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_LEFT_BACK_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_RIGHT_BACK_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_TOP_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_TOP_BACK_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_BOTTOM_BACK_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_SCREEN_LOGO_SUPER,
                get(resourcePrefix + Res.textures.GAME_SCREEN_LOGO_SUPER_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.MUTE_DEALER_BUTTON,
                get(resourcePrefix + Res.textures.MUTE_DEALER_BUTTON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.MUTE_DEALER_BUTTON_PRESSED,
                get(resourcePrefix + Res.textures.MUTE_DEALER_BUTTON_PRESSED_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.MUTE_PLAYER_BUTTON,
                get(resourcePrefix + Res.textures.MUTE_PLAYER_BUTTON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.MUTE_PLAYER_BUTTON_PRESSED,
                get(resourcePrefix + Res.textures.MUTE_PLAYER_BUTTON_PRESSED_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.PLAYER_PHOTO_FRAME,
                get(resourcePrefix + Res.textures.PLAYER_PHOTO_FRAME_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.SLIDER,
                get(resourcePrefix + Res.textures.SLIDER_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.ICON_SETTINGS,
                get(resourcePrefix + Res.textures.ICON_SETTINGS_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.VIBRATION_ICON,
                get(resourcePrefix + Res.textures.VIBRATION_ICON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.SOUND_ICON,
                get(resourcePrefix + Res.textures.SOUND_ICON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.GAME_MENU_BACKGROUND,
                get(resourcePrefix + Res.textures.GAME_MENU_BACKGROUND_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.BIG_VERTICAL_LINE,
                get(resourcePrefix + Res.textures.BIG_VERTICAL_LINE_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.LEAVE_GAME_ICON,
                get(resourcePrefix + Res.textures.LEAVE_GAME_ICON_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.MUTE_PLAYERS_ICON,
                get(resourcePrefix + Res.textures.MUTE_PLAYERS_ICON_PATH, Texture.class));
        gameScreenSkin.add(Res.buttonStyles.SMALL_CHECKBOX_STYLE,
                getSmallCheckboxStyle(),
                ImageTextButton.ImageTextButtonStyle.class);
        gameScreenSkin.add(Res.buttonStyles.DIALOG_BACK_BUTTON_STYLE,
                getDialogBackButtonStyle(),
                ImageButton.ImageButtonStyle.class);

        gameScreenSkin.add(Res.textures.PHOTO_TIMER,
                get(Res.textures.PHOTO_TIMER_PATH, Texture.class));
        gameScreenSkin.add(Res.textures.PHOTO_TURN,
                get(Res.textures.PHOTO_TURN_PATH, Texture.class));

        gameScreenSkin.add(Res.labelStyles.GAME_SCREEN_SMALL_LABEL_STYLE,
                getGameScreenSmallLabelStyle());
        gameScreenSkin.add(Res.labelStyles.GAME_SCREEN_LARGE_LABEL_STYLE,
                getGameScreenLargeLabelStyle());
        gameScreenSkin.add(Res.buttonStyles.CHAT_BUTTON_STYLE,
                getChatButtonStyle());
        gameScreenSkin.add(Res.labelStyles.CHAT_INPUT_STYLE,
                getChatTextFieldStyle());
        gameScreenSkin.add(Res.labelStyles.CHAT_OUTPUT_STYLE,
                getChatOutputStyle());

        gameScreenSkin.add(Res.properties.STRINGS,
                get(Res.properties.PROPERTIES_DEFAULT, Strings.class));

        return gameScreenSkin;
    }

    /**
     * @return - skin with dialog assets. Call {@link #loadDialogAssets()}
     * and wait until {@link #update()} will return true before using this
     * method.
     */
    public Skin getDialogSkin() {
        final Skin dialogSkin = new Skin();

        dialogSkin.add(Res.buttonStyles.DIALOG_OK_BUTTON_STYLE,
                getDialogOkButtonStyle(),
                ImageButton.ImageButtonStyle.class);
        dialogSkin.add(Res.buttonStyles.DIALOG_BACK_BUTTON_STYLE,
                getDialogBackButtonStyle(),
                ImageButton.ImageButtonStyle.class);
        dialogSkin.add(Res.buttonStyles.RADIO_BUTTON_STYLE,
                getRadioButtonStyle(),
                ImageTextButton.ImageTextButtonStyle.class);
        dialogSkin.add(Res.buttonStyles.LARGE_CHECKBOX_STYLE,
                getLargeCheckboxStyle(),
                ImageTextButton.ImageTextButtonStyle.class);
        dialogSkin.add(Res.labelStyles.DIALOG_CONTENT_LARGE_LABEL_STYLE,
                getDialogLargeLabelStyle(),
                Label.LabelStyle.class);
        dialogSkin.add(Res.labelStyles.DIALOG_CONTENT_SMALL_LABEL_STYLE,
                getDialogSmallLabelStyle(),
                Label.LabelStyle.class);
        dialogSkin.add(Res.labelStyles.DIALOG_CONTENT_LINK_LABEL_STYLE,
                getDialogLinkLabelStyle(),
                Label.LabelStyle.class);
        dialogSkin.add(Res.textures.DIALOG_BACKGROUND,
                get(Res.textures.DIALOG_BACKGROUND_PATH, Texture.class));
        dialogSkin.add(Res.textures.VIBRATION_ICON,
                get(resourcePrefix + Res.textures.VIBRATION_ICON_PATH, Texture.class));
        dialogSkin.add(Res.textures.SOUND_ICON,
                get(resourcePrefix + Res.textures.SOUND_ICON_PATH, Texture.class));
        dialogSkin.add(Res.textures.FACEBOOK_ICON,
                get(resourcePrefix + Res.textures.FACEBOOK_ICON_PATH, Texture.class));
        dialogSkin.add(Res.textures.GOOGLE_PLUS_ICON,
                get(resourcePrefix + Res.textures.GOOGLE_PLUS_ICON_PATH, Texture.class));
        dialogSkin.add(Res.textures.LEAVE_GAME_ICON,
                get(resourcePrefix + Res.textures.LEAVE_GAME_ICON_PATH, Texture.class));
        dialogSkin.add(Res.textures.PLAYER_PHOTO_FRAME,
                get(resourcePrefix + Res.textures.PLAYER_PHOTO_FRAME_PATH, Texture.class));
        dialogSkin.add(Res.textures.SPINNER,
                get(resourcePrefix + Res.textures.SPINNER, Texture.class));

        dialogSkin.add(Res.textures.AVATAR_MALE,
                get(Res.textures.AVATAR_MALE_PATH, Texture.class));
        dialogSkin.add(Res.textures.AVATAR_FEMALE,
                get(Res.textures.AVATAR_FEMALE_PATH, Texture.class));

        dialogSkin.add(Res.properties.STRINGS,
                get(Res.properties.PROPERTIES_DEFAULT, Strings.class));

        dialogSkin.add(Res.textures.STUDIO_ICON,
                get(resourcePrefix + Res.textures.STUDIO_LOGO_PATH, Texture.class));

        return dialogSkin;
    }

    /**
     * @return - skin with assets for gaming room dialog.
     */
    public Skin getGamingRoomDialogSkin() {
        final Skin skin = new Skin();

        skin.add(Res.properties.STRINGS,
                get(Res.properties.PROPERTIES_DEFAULT, Strings.class));

        return skin;
    }

    private Label.LabelStyle getDialogLargeLabelStyle() {
        final Label.LabelStyle style = new Label
                .LabelStyle();

        style.font = get(Res.fonts.FONT_2, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_CONTENT_COLOR;


        return style;
    }

    private Label.LabelStyle getDialogSmallLabelStyle() {
        final Label.LabelStyle style = new Label
                .LabelStyle();

        style.font = get(Res.fonts.FONT_3, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_CONTENT_COLOR;


        return style;
    }

    private Label.LabelStyle getDialogLinkLabelStyle() {
        final Label.LabelStyle style = new Label
                .LabelStyle();

        style.font = get(Res.fonts.FONT_4, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_CONTENT_LINK_COLOR;


        return style;
    }

    private ImageTextButton.ImageTextButtonStyle getRadioButtonStyle() {
        final ImageTextButton.ImageTextButtonStyle style = new ImageTextButton
                .ImageTextButtonStyle();
        final Drawable checkedDrawable = new TextureRegionDrawable(new TextureRegion(
                get(resourcePrefix + Res.textures.RADIO_BUTTON_CHECKED_PATH, Texture.class)));
        final Drawable uncheckedDrawable = new TextureRegionDrawable(new TextureRegion(
                get(resourcePrefix + Res.textures.RADIO_BUTTON_PATH, Texture.class)));

        style.imageChecked = checkedDrawable;
        style.imageUp = uncheckedDrawable;

        style.font = get(Res.fonts.FONT_3, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_RADIO_BUTTON_TEXT_COLOR;

        return style;
    }

    private ImageButton.ImageButtonStyle getDialogOkButtonStyle() {
        final ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();

        final TextureRegionDrawable drawableUp = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.DIALOG_OK_PATH, Texture.class)));
        final TextureRegionDrawable drawableDown = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.DIALOG_OK_PRESSED_PATH, Texture.class)));
        style.up = drawableUp;
        style.down = drawableDown;

        return style;
    }

    private ImageTextButton.ImageTextButtonStyle getLargeCheckboxStyle() {
        final ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle();

        final TextureRegionDrawable uncheckedDrawable = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHECKBOX_PATH, Texture.class)));
        final TextureRegionDrawable checkedDrawable = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHECKBOX_CHECKED_PATH, Texture.class)));
        style.imageUp = uncheckedDrawable;
        style.imageChecked = checkedDrawable;
        style.imageDisabled = getDisabledCheckboxDrawable();

        style.font = get(Res.fonts.FONT_1, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_CONTENT_COLOR;
        style.disabledFontColor = Res.colors.DIALOG_CONTENT_DISABLED_COLOR;

        return style;
    }

    private ImageTextButton.ImageTextButtonStyle getSmallCheckboxStyle() {
        final ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle();

        final TextureRegionDrawable uncheckedDrawable = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHECKBOX_PATH, Texture.class)));
        final TextureRegionDrawable checkedDrawable = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHECKBOX_CHECKED_PATH, Texture.class)));
        style.imageUp = uncheckedDrawable;
        style.imageChecked = checkedDrawable;
        style.imageDisabled = getDisabledCheckboxDrawable();

        style.font = get(Res.fonts.FONT_3, BitmapFont.class);
        style.fontColor = Res.colors.DIALOG_CONTENT_COLOR;
        style.disabledFontColor = Res.colors.DIALOG_CONTENT_DISABLED_COLOR;

        return style;
    }

    private Drawable getDisabledCheckboxDrawable() {
        final TextureRegion uncheckedTextureRegion = new TextureRegion(get(resourcePrefix + Res.textures.CHECKBOX_PATH, Texture.class));
        final Sprite sprite = new Sprite(uncheckedTextureRegion);
        sprite.setColor(Color.GRAY);

        return new SpriteDrawable(sprite);
    }

    private ImageButton.ImageButtonStyle getDialogBackButtonStyle() {
        final ImageButton.ImageButtonStyle style = new ImageButton.ImageButtonStyle();

        final TextureRegionDrawable drawableUp = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.DIALOG_BACK_PATH, Texture.class)));
        final TextureRegionDrawable drawableDown = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.DIALOG_BACK_PRESSED_PATH, Texture.class)));
        style.up = drawableUp;
        style.down = drawableDown;

        return style;
    }

    private ImageTextButton.ImageTextButtonStyle getChatButtonStyle() {
        final ImageTextButton.ImageTextButtonStyle style = new ImageTextButton.ImageTextButtonStyle();

        final TextureRegionDrawable drawableUp = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHAT_BUTTON_PATH, Texture.class)));
        final TextureRegionDrawable drawableDown = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CHAT_BUTTON_PRESSED_PATH, Texture.class)));
        style.up = drawableUp;
        style.down = drawableDown;

        style.font = get(Res.fonts.FONT_3, BitmapFont.class);
        style.fontColor = Res.colors.CHAT_BUTTON_TEXT_COLOR;

        return style;
    }

    private Label.LabelStyle getGameScreenSmallLabelStyle() {
        final Label.LabelStyle labelStyle = new Label.LabelStyle();

        labelStyle.font = get(Res.fonts.FONT_3, BitmapFont.class);
        labelStyle.fontColor = Res.colors.GAME_SCREEN_LABEL_COLOR;

        return labelStyle;
    }

    private Label.LabelStyle getGameScreenLargeLabelStyle() {
        final Label.LabelStyle labelStyle = new Label.LabelStyle();

        labelStyle.font = get(Res.fonts.FONT_2, BitmapFont.class);
        labelStyle.fontColor = Res.colors.GAME_SCREEN_LABEL_COLOR;

        return labelStyle;
    }

    private TextField.TextFieldStyle getChatTextFieldStyle() {
        final TextField.TextFieldStyle style = new TextField.TextFieldStyle();
        style.font = get(Res.fonts.FONT_2, BitmapFont.class);
        style.fontColor = Res.colors.GAME_SCREEN_LABEL_COLOR;

        final TextureRegionDrawable cursor = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.CURSOR_PATH, Texture.class)));
        style.cursor = cursor;

        final TextureRegionDrawable selection = new TextureRegionDrawable(
                new TextureRegion(get(resourcePrefix + Res.textures.TEXT_SELECTION_PATH, Texture.class)));
        style.selection = selection;

        return style;
    }

    private Label.LabelStyle getChatOutputStyle() {
        final Label.LabelStyle style = new Label.LabelStyle();
        style.font = get(Res.fonts.FONT_2, BitmapFont.class);
        style.fontColor = Res.colors.CHAT_BUTTON_TEXT_COLOR;

        return style;
    }

    private <T> T get(final String fileName, final Class<T> clazz) {
        return assetManager.get(fileName, clazz);
    }
}