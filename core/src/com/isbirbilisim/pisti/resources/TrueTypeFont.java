package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Wrapper for {@link com.badlogic.gdx.graphics.g2d.BitmapFont}.
 */
public class TrueTypeFont {

    private final BitmapFont bitmapFont;

    public TrueTypeFont(final BitmapFont bitmapFont) {
        this.bitmapFont = bitmapFont;
    }

    public BitmapFont getBitmapFont() {
        return bitmapFont;
    }

    public static class TrueTypeFontParams
            extends AssetLoaderParameters<TrueTypeFont> {
        /**
         * Font size in pixels.
         */
        public int size;
    }
}