package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

/**
 * Util for retrieving localized strings from .properties file.
 */
class StringsPropertiesUtil {

    /**
     * Returns localized strings.
     */
    static Strings loadStrings(final FileHandle fileHandle,
                               final StringsParams stringsParams) {
        final Locale locale = stringsParams.locale != null
                ? stringsParams.locale
                : Locale.forLanguageTag(stringsParams.localeIdentifier);

        final I18NBundle bundle = I18NBundle.createBundle(fileHandle, locale);

        return Strings.fromBundle(bundle);
    }
}