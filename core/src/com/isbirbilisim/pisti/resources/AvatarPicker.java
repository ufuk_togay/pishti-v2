package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.isbirbilisim.pisti.background.BackgroundTasksManager;
import com.isbirbilisim.pisti.background.tasks.SetUserPhotoTask;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.players.Role;

import java.net.URL;

/**
 * Class that helps to get players avatars.
 */
public class AvatarPicker {

    public static final String STUB_PLAYER_ID = "desktop_gamer_id";

    public static void setAvatar(final Image image,
                                 final Player player,
                                 final boolean networkAvailable) {
        if (player.getRole() == Role.AI) {
            setImageDrawable(image, new Texture(Res.textures.AVATAR_BOT_PATH));
            return;
        }

        if (player.getUserInfo() != null) {
            if (player.getUserInfo().getPlayerPhotoURL() == null) {
                if (player.getUserInfo().isMale()) {
                    setImageDrawable(image, new Texture(Res.textures.AVATAR_FEMALE_PATH));
                } else {
                    setImageDrawable(image, new Texture(Res.textures.AVATAR_FEMALE_PATH));
                }
            } else {
                if (player.getUserInfo().getFacebookId().equals(STUB_PLAYER_ID)) {
                    setImageDrawable(image, new Texture(Res.textures.AVATAR_MALE_PATH));
                    return;
                }

                if (networkAvailable) {
                    setImageAsync(image, player.getUserInfo().getPlayerPhotoURL());
                } else {
                    setImageDrawable(image, new Texture(Res.textures.AVATAR_MALE_PATH));
                }
            }
        } else {
            setImageDrawable(image, new Texture(Res.textures.AVATAR_MALE_PATH));
        }
    }

    public static void setImageAsync(final Image image, final URL url) {
        BackgroundTasksManager.getInstance().postTask(new SetUserPhotoTask(image, url, new TextureRegionDrawable(new TextureRegion(new Texture(Res.textures.AVATAR_MALE_PATH)))));
    }

    private static void setImageDrawable(final Image image, final Texture texture) {
        image.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
    }
}