package com.isbirbilisim.pisti.resources;

import java.util.Locale;

/**
 * Interface for retrieving current locale. Implementation is platform depended.
 */
public interface LocaleProvider {

    /**
     * Returns identifier for current locale.
     */
    public String getLocaleIdentifier();

    /**
     * Returns current locale.
     */
    public Locale getLocale();
}