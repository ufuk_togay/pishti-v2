package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Array;
import com.isbirbilisim.pisti.preferences.AppVersionUtils;

import org.jrenner.smartfont.SmartFontGenerator;

/**
 * Loader for bitmap fonts that uses {@link org.jrenner.smartfont.SmartFontGenerator}.
 */
public class FontLoader extends SynchronousAssetLoader<BitmapFont, BitmapFontParams> {

    private final SmartFontGenerator fontGenerator;

    public FontLoader() {
        super(new InternalFileHandleResolver());

        fontGenerator = new SmartFontGenerator();
    }

    @Override
    public BitmapFont load(final AssetManager assetManager,
                           final String fileName,
                           final FileHandle file,
                           final BitmapFontParams parameter) {
        if (AppVersionUtils.isApplicationUpdated()) {
            fontGenerator.setForceGeneration(true);
        } else {
            fontGenerator.setForceGeneration(false);
        }

        return fontGenerator.createFont(
                file,
                fileName,
                parameter.size);
    }

    @Override
    public FileHandle resolve(final String fileName) {
        return Gdx.files.internal(Res.fonts.GAME_FONT_PATH);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(
            final String fileName,
            final FileHandle file,
            final BitmapFontParams parameter) {
        return null;
    }
}