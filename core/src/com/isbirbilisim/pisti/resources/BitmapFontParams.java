package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * Params for bitmap font.
 */
public class BitmapFontParams extends AssetLoaderParameters<BitmapFont> {

    /**
     * Font size in pixels.
     */
    public int size;

    public BitmapFontParams(final int size) {
        this.size = size;
    }
}