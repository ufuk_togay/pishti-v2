package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

/**
 * Loader for {@link com.isbirbilisim.pisti.resources.Strings} localized strings.
 */
public class StringsLoader extends SynchronousAssetLoader<Strings, StringsParams> {

    public StringsLoader() {
        super(new InternalFileHandleResolver());
    }

    @Override
    public Array<AssetDescriptor> getDependencies(final String fileName,
                                                  final FileHandle file,
                                                  final StringsParams parameter) {
        return null;
    }

    @Override
    public FileHandle resolve(final String fileName) {
        return Gdx.files.internal(fileName);
    }

    @Override
    public Strings load(final AssetManager assetManager,
                        final String fileName,
                        final FileHandle file,
                        final StringsParams parameter) {
        return StringsPropertiesUtil.loadStrings(file, parameter);
    }
}