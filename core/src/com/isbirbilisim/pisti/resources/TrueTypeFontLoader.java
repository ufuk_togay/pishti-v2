package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;

/**
 * Assets loader for {@link com.isbirbilisim.pisti.resources.TrueTypeFont}.
 */
public class TrueTypeFontLoader
        extends SynchronousAssetLoader<TrueTypeFont, TrueTypeFont.TrueTypeFontParams> {

    public TrueTypeFontLoader() {
        super(new InternalFileHandleResolver());
    }

    @Override
    public TrueTypeFont load(final AssetManager assetManager,
                             final String fileName,
                             final FileHandle file,
                             final TrueTypeFont.TrueTypeFontParams parameter) {
        final FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file);
        final BitmapFont font = generator.generateFont(parameter.size);
        generator.dispose();

        return new TrueTypeFont(font);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(
            final String fileName,
            final FileHandle file,
            final TrueTypeFont.TrueTypeFontParams parameter) {
        return null;
    }
}