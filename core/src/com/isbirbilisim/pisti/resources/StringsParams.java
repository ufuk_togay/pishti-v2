package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.assets.AssetLoaderParameters;

import java.util.Locale;

/**
 * Parameters for loading localized strings.
 */
public class StringsParams extends AssetLoaderParameters<Strings> {

    public final Locale locale;
    public final String localeIdentifier;

    public StringsParams(final Locale locale, final String localeIdentifier) {
        this.locale = locale;
        this.localeIdentifier = localeIdentifier;
    }
}