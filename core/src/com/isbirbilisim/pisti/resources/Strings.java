package com.isbirbilisim.pisti.resources;

import com.badlogic.gdx.utils.I18NBundle;

import java.util.ArrayList;
import java.util.List;

/**
 * Class with localized strings.
 */
public class Strings {
    public final String singlePlayer;
    public final String playerWithFriends;
    public final String inviteFriends;
    public final String quickGame;
    public final String about;
    public final String singlePlayerStatistic;
    public final String leaderboard;
    public final String buyCoins;
    public final String settings;

    public final String ok;
    public final String cancel;

    public final String muteDealer;
    public final String mutePlayers;
    public final String vibration;
    public final String sound;
    public final String loginToFacebook;
    public final String logOutFacebook;
    public final String changeGoogle;

    public final String finalScoresDialogTitle;
    public final String columnPlayer;
    public final String columnCollectedCards;
    public final String columnTotalScores;
    public final String columnPlace;
    public final String columnPistiScores;
    public final String columnSuperPistiScores;

    public final String comingSoonMessage;

    public final String aboutDialogMessage;
    public final String aboutDialogLink;
    public final String aboutDialogCopyright;

    public final String singlePlayerDlgCompletedGamesCount;
    public final String singlePlayerDlgWinGamesCount;
    public final String singlePlayerDlgLostGameCount;
    public final String singlePlayerDlgPistiScoresCount;
    public final String singlePlayerDlgSuperPistiScoresCount;
    public final String singlePlayerDlgTotalScoresCount;
    public final String singlePlayerDlgTableTitle;


    public final String returnButton;
    public final String playAgainMessage;

    public final String leaveGameDialogMessage;

    public final String leaveGameOption;

    public final String gameSettingsDialogTitle;
    public final String gamingRoomDialogTitle;

    public final String chat;

    public final String players2;
    public final String players4;

    public final String oneRound;
    public final String shortGame;
    public final String longGame;

    public final String player;
    public final String winner;

    public final String searchingGame;
    public final String cantJoinRoom;
    public final String waitingForOtherPlayers;
    public final String playersFound;

    public final String networkUnavailable;
    public final String signInWithFacebook;

    public final List<String> names;

    public Strings(final String singlePlayer,
                   final String playerWithFriends,
                   final String inviteFriends,
                   final String quickGame,
                   final String about,
                   final String singlePlayerStatistic,
                   final String leaderboard,
                   final String buyCoins,
                   final String settings,
                   final String ok,
                   final String cancel,
                   final String muteDealer,
                   final String mutePlayers,
                   final String vibration,
                   final String sound,
                   final String loginToFacebook,
                   final String logOutFacebook,
                   final String changeGoogle,
                   final String finalScoresDialogTitle,
                   final String columnPlayer,
                   final String columnCollectedCards,
                   final String columnTotalScores,
                   final String columnPlace,
                   final String columnPistiScores,
                   final String columnSuperPistiScores,
                   final String comingSoonMessage,
                   final String aboutDialogMessage,
                   final String aboutDialogLink,
                   final String aboutDialogCopyright,
                   final String singlePlayerDlgCompletedGamesCount,
                   final String singlePlayerDlgWinGamesCount,
                   final String singlePlayerDlgLostGameCount,
                   final String singlePlayerDlgPistiScoresCount,
                   final String singlePlayerDlgSuperPistiScoresCount,
                   final String singlePlayerDlgTotalScoresCount,
                   final String singlePlayerDlgTableTitle,
                   final String returnButton,
                   final String playAgainMessage,
                   final String leaveGameDialogMessage,
                   final String leaveGameOption,
                   final String gameSettingsDialogTitle,
                   final String gamingRoomDialogTitle,
                   final String chat,
                   final String players2,
                   final String players4,
                   final String oneRound,
                   final String shortGame,
                   final String longGame,
                   final String player,
                   final String winner,
                   final String searchingGame,
                   final String cantJoinRoom,
                   final String waitingForOtherPlayers,
                   final String playersFound,
                   final String networkUnavailable,
                   final String signInWithFacebook,
                   final List<String> names) {
        this.singlePlayer = singlePlayer;
        this.playerWithFriends = playerWithFriends;
        this.inviteFriends = inviteFriends;
        this.quickGame = quickGame;
        this.about = about;
        this.singlePlayerStatistic = singlePlayerStatistic;
        this.leaderboard = leaderboard;
        this.buyCoins = buyCoins;
        this.settings = settings;
        this.ok = ok;
        this.cancel = cancel;
        this.muteDealer = muteDealer;
        this.mutePlayers = mutePlayers;
        this.vibration = vibration;
        this.sound = sound;
        this.loginToFacebook = loginToFacebook;
        this.logOutFacebook = logOutFacebook;
        this.changeGoogle = changeGoogle;
        this.finalScoresDialogTitle = finalScoresDialogTitle;
        this.columnPlayer = columnPlayer;
        this.columnCollectedCards = columnCollectedCards;
        this.columnTotalScores = columnTotalScores;
        this.columnPlace = columnPlace;
        this.columnPistiScores = columnPistiScores;
        this.columnSuperPistiScores = columnSuperPistiScores;
        this.comingSoonMessage = comingSoonMessage;
        this.aboutDialogMessage = aboutDialogMessage;
        this.aboutDialogLink = aboutDialogLink;
        this.aboutDialogCopyright = aboutDialogCopyright;
        this.singlePlayerDlgCompletedGamesCount = singlePlayerDlgCompletedGamesCount;
        this.singlePlayerDlgWinGamesCount = singlePlayerDlgWinGamesCount;
        this.singlePlayerDlgLostGameCount = singlePlayerDlgLostGameCount;
        this.singlePlayerDlgPistiScoresCount = singlePlayerDlgPistiScoresCount;
        this.singlePlayerDlgSuperPistiScoresCount = singlePlayerDlgSuperPistiScoresCount;
        this.singlePlayerDlgTotalScoresCount = singlePlayerDlgTotalScoresCount;
        this.singlePlayerDlgTableTitle = singlePlayerDlgTableTitle;
        this.returnButton = returnButton;
        this.playAgainMessage = playAgainMessage;
        this.leaveGameDialogMessage = leaveGameDialogMessage;
        this.leaveGameOption = leaveGameOption;
        this.gameSettingsDialogTitle = gameSettingsDialogTitle;
        this.gamingRoomDialogTitle = gamingRoomDialogTitle;
        this.chat = chat;
        this.players2 = players2;
        this.players4 = players4;
        this.oneRound = oneRound;
        this.shortGame = shortGame;
        this.longGame = longGame;
        this.player = player;
        this.winner = winner;
        this.searchingGame = searchingGame;
        this.cantJoinRoom = cantJoinRoom;
        this.waitingForOtherPlayers = waitingForOtherPlayers;
        this.playersFound = playersFound;
        this.networkUnavailable = networkUnavailable;
        this.signInWithFacebook = signInWithFacebook;
        this.names = names;
    }

    /**
     * Creates Strings from given properties bundle.
     *
     * @param bundle - properties.
     * @return - Strings instance.
     */
    public static Strings fromBundle(final I18NBundle bundle) {
        final List<String> names = new ArrayList<String>();
        for (int i = 1; i <= 50; i++) {
            names.add(bundle.get("name" + i));
        }

        return new Strings(
                bundle.get("singlePlayer"),
                bundle.get("playerWithFriends"),
                bundle.get("inviteFriends"),
                bundle.get("quickGame"),
                bundle.get("about"),
                bundle.get("singlePlayerStatistic"),
                bundle.get("leaderboard"),
                bundle.get("buyCoins"),
                bundle.get("settings"),
                bundle.get("ok"),
                bundle.get("cancel"),
                bundle.get("muteDealer"),
                bundle.get("mutePlayers"),
                bundle.get("vibration"),
                bundle.get("sound"),
                bundle.get("loginToFacebook"),
                bundle.get("logOutFacebook"),
                bundle.get("changeGoogle"),
                bundle.get("finalScoresDialogTitle"),
                bundle.get("columnPlayer"),
                bundle.get("columnCollectedCards"),
                bundle.get("columnTotalScores"),
                bundle.get("columnPlace"),
                bundle.get("columnPistiScores"),
                bundle.get("columnSuperPistiScores"),
                bundle.get("comingSoonMessage"),
                bundle.get("aboutDialogMessage"),
                bundle.get("aboutDialogLink"),
                bundle.get("aboutDialogCopyright"),
                bundle.get("singlePlayerDlgCompletedGamesCount"),
                bundle.get("singlePlayerDlgWinGamesCount"),
                bundle.get("singlePlayerDlgLostGameCount"),
                bundle.get("singlePlayerDlgPistiScoresCount"),
                bundle.get("singlePlayerDlgSuperPistiScoresCount"),
                bundle.get("singlePlayerDlgTotalScoresCount"),
                bundle.get("singlePlayerDlgTableTitle"),
                bundle.get("returnButton"),
                bundle.get("playAgainMessage"),
                bundle.get("leaveGameDialogMessage"),
                bundle.get("leaveGameOption"),
                bundle.get("gameSettingsDialogTitle"),
                bundle.get("gamingRoomDialogTitle"),
                bundle.get("chat"),
                bundle.get("players2"),
                bundle.get("players4"),
                bundle.get("oneRound"),
                bundle.get("shortGame"),
                bundle.get("longGame"),
                bundle.get("player"),
                bundle.get("winner"),
                bundle.get("searchingGame"),
                bundle.get("joinRoomError"),
                bundle.get("waitingForPlayers"),
                bundle.get("playersFound"),
                bundle.get("networkRequired"),
                bundle.get("signInWithFacebook"),
                names
        );
    }
}