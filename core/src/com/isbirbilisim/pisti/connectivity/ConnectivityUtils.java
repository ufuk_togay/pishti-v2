package com.isbirbilisim.pisti.connectivity;

/**
 * Network utils.
 */
public interface ConnectivityUtils {

    /**
     * @return - true if device has internet access;
     */
    boolean isNetworkAvailable();

    void addConnectivityListener(final ConnectivityListener listener);

    void removeConnectivityListener(final ConnectivityListener listener);
}