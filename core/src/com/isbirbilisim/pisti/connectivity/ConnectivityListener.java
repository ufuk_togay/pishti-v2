package com.isbirbilisim.pisti.connectivity;

public interface ConnectivityListener {

    void onConnectivityChanged(final boolean connected);
}