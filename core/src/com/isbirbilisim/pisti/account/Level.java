package com.isbirbilisim.pisti.account;

import java.util.Arrays;
import java.util.List;

public enum Level {
    LEVEL1(1, Room.BEGINNERS, 50),
    LEVEL2(2, Room.BEGINNERS, 100),
    LEVEL3(3, Room.BEGINNERS, 250),
    LEVEL4(4, Room.BEGINNERS, 500),
    LEVEL5(5, Room.BEGINNERS, 1000),
    LEVEL6(6, Room.EXPERTS, 5000),
    LEVEL7(7, Room.EXPERTS, 10000),
    LEVEL8(8, Room.EXPERTS, 25000),
    LEVEL9(9, Room.EXPERTS, 50000),
    LEVEL10(10, Room.EXPERTS, 100000),
    LEVEL11(11, Room.EXPERTS, 250000),
    LEVEL12(12, Room.PROFESIONALS, 500000),
    LEVEL13(13, Room.PROFESIONALS, 1000000),
    LEVEL14(14, Room.PROFESIONALS, 5000000);

    private final int levelValue;
    private final Room room;
    private final long bidAmount;

    Level(final int levelValue, final Room room, final long bidAmount) {
        this.levelValue = levelValue;
        this.room = room;
        this.bidAmount = bidAmount;
    }

    /**
     * Returns Level instance byt level value ( [1..14] ).
     *
     * @param levelValue - level value.
     * @return - Level instance.
     */
    public static Level fromLevelValue(final int levelValue) {
        if (levelValue < 1 || levelValue > 14) {
            throw new IllegalArgumentException("Level value "
                    + levelValue + " is invalid");
        }

        return values()[levelValue - 1];
    }

    /**
     * Return Level instance from gold amount.
     *
     * @param gold - current player's gold.
     * @return - Level.
     */
    public static Level fromGold(final long gold) {
        for (int i = 1; i < values().length - 1; i++) {
            if (values()[i].getBidAmount() > gold
                    && values()[i - 1].getBidAmount() <= gold) {
                return values()[i - 1];
            }
        }

        return LEVEL14;
    }

    /**
     * Returns list of levels in room.
     */
    public static List<Level> getLevelsFromRoom(final Room room) {
        switch (room) {
            case BEGINNERS:
                return Arrays.asList(Arrays.copyOfRange(values(), 0, 4));
            case EXPERTS:
                return Arrays.asList(Arrays.copyOfRange(values(), 5, 10));
            case PROFESIONALS:
                return Arrays.asList(Arrays.copyOfRange(values(), 11, 13));
            default:
                return null;
        }
    }

    public int getLevelValue() {
        return levelValue;
    }

    public String getName(final boolean isMale) {
        return "Level "+Integer.toString(levelValue);
        /*switch (levelValue) {
            case 1:
                return "Level";
            case 2:
                return isMale ? "Serfdom" : "Serfdom";
            case 3:
                return isMale ? "Urbanite" : "Urbanite";
            case 4:
                return isMale ? "Sipahi" : "Sipahi";
            case 5:
                return isMale ? "Subasi" : "Subasi";
            case 6:
                return isMale ? "Qadi" : "Qadi";
            case 7:
                return isMale ? "Sanjak Bey" : "Flag Officer";
            case 8:
                return isMale ? "Belger Beg" : "Lady";
            case 9:
                return isMale ? "Pasha" : "Grand Lady";
            case 10:
                return isMale ? "Governor" : "Governor";
            case 11:
                return isMale ? "Kaymakam" : "Kaymakam";
            case 12:
                return isMale ? "Captain Pasha" : "Princess";
            case 13:
                return isMale ? "Grand Vizier" : "Sultan";
            case 14:
                return isMale ? "Padishah" : "Valide Sultan";
            default:
                return "Player";
        }*/
    }

    public Room getRoom() {
        return room;
    }

    public long getBidAmount() {
        return bidAmount;
    }
}