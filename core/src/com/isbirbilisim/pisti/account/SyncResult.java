package com.isbirbilisim.pisti.account;

/**
 * Defines possible user info sync result.
 */
public enum SyncResult {
    SUCCESS,
    SERVER_NOT_AVAILABLE,
    ENCODING_ERROR
}