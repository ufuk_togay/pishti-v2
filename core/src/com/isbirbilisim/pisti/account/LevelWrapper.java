package com.isbirbilisim.pisti.account;

/**
 * Contains reference to {@link com.isbirbilisim.pisti.account.Level} and info
 * about its availability to player.
 */
public class LevelWrapper {

    private final Level level;

    private boolean enabled;

    public LevelWrapper(final Level level,
                        final boolean enabled) {
        this.level = level;
        this.enabled = enabled;
    }

    /**
     * @return - level info.
     */
    public Level getLevel() {
        return level;
    }

    /**
     * @return - true if level is available to player.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled - pass true if level should be available to player.
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}