package com.isbirbilisim.pisti.account;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.scores.Scores;

import java.net.URL;

/**
 * Contains info about player like Google+/Facebook account.
 */
public class UserInfo implements Json.Serializable {

    private String warpId;
    private String facebookId;

    private String name;
    private URL playerPhotoURI;
    private boolean isMale;
    private Level level;
    /* Overall player scores. */
    private Scores scores;
    private long games;
    private long wins;
    private long loses;
    private long draws;
    private long gold;

    public UserInfo() {
    }

    public String getWarpId() {
        return warpId;
    }

    public void setWarpId(final String warpId) {
        this.warpId = warpId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(final String facebookId) {
        this.facebookId = facebookId;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(final boolean isMale) {
        this.isMale = isMale;
    }

    public URL getPlayerPhotoURL() {
        return playerPhotoURI;
    }

    public void setPlayerPhotoURL(final URL playerPhotoURI) {
        this.playerPhotoURI = playerPhotoURI;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(final Level level) {
        this.level = level;
    }

    public Scores getScores() {
        return scores;
    }

    public void setScores(final Scores scores) {
        this.scores = scores;
    }

    public long getGames() {
        return games;
    }

    public void setGames(final long games) {
        this.games = games;
    }

    public void addGame(final MatchResult matchResult) {
        switch (matchResult.getGameResult()) {
            case WIN1:
            case WIN2:
                wins += 1;
                break;
            case DRAW:
                draws += 1;
                break;
            case LOSE:
                loses += 1;
        }

        games += 1;

        addGold(matchResult.getReward());
    }

    public long getWins() {
        return wins;
    }

    public void setWins(final long wins) {
        this.wins = wins;
    }

    public long getLoses() {
        return loses;
    }

    public void setLoses(final long loses) {
        this.loses = loses;
    }

    public long getDraws() {
        return draws;
    }

    public void setDraws(final long draws) {
        this.draws = draws;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(final long gold) {
        this.gold = gold;
    }

    public void removeBidGold(final long bidValue) {
        this.gold -= bidValue;
    }

    public void addGold(final long goldAmount) {
        this.gold += goldAmount;
    }

    @Override
    public void write(final Json json) {
        json.writeValue("warpId", warpId);
        json.writeValue("facebookId", facebookId);
        json.writeValue("draws", draws);
        json.writeValue("gamesPlayed", games);
        json.writeValue("gold", gold);
        json.writeValue("isMale", isMale);
        json.writeValue("loses", loses);
        json.writeValue("wins", wins);
        json.writeValue("totalScore", scores.getTotalScore());
        json.writeValue("pistiScore", scores.getPistiScore());
        json.writeValue("superPistiScore", scores.getSuperPistiScore());
    }

    @Override
    public void read(final Json json, final JsonValue jsonData) {
        warpId = jsonData.getString("playerId");
        facebookId = jsonData.getString("facebookId");
        draws = jsonData.getInt("draws");
        games = jsonData.getInt("gamesPlayed");
        gold = jsonData.getLong("gold");
        isMale = jsonData.getBoolean("isMale");
        loses = jsonData.getInt("loses");
        wins = jsonData.getInt("wins");

        scores = new Scores(jsonData.getLong("totalScore"),
                jsonData.getLong("pistiScore"),
                jsonData.getLong("superPistiScore"));
    }
}