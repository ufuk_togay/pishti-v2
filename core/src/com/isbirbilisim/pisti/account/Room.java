package com.isbirbilisim.pisti.account;

/**
 * Defines all possible types of game rooms.
 */
public enum Room {

    BEGINNERS("BEGINNERS", 0L, 4999L),
    EXPERTS("EXPERTS", 5000L, 499999L),
    PROFESIONALS("PROFESIONALS", 500000L, 99999999999L);

    private final String name;
    private final long minGold;
    private final long maxGold;

    Room(final String name, final long minGold, final long maxGold) {
        this.name = name;
        this.minGold = minGold;
        this.maxGold = maxGold;
    }

    public String getName() {
        return name;
    }

    public long getMinGold() {
        return minGold;
    }

    public long getMaxGold() {
        return maxGold;
    }
}