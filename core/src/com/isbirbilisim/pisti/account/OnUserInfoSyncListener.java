package com.isbirbilisim.pisti.account;

/**
 * Interface for receiving info about user info sync result.
 */
public interface OnUserInfoSyncListener {

    /**
     * Called when sync is finished.
     *
     * @param userInfo - user info that was synchronizing.
     * @param result - sync result.
     */
    void onSyncResult(final UserInfo userInfo, final SyncResult result);
}