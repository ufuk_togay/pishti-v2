package com.isbirbilisim.pisti.multimedia;

import com.badlogic.gdx.Gdx;
import com.isbirbilisim.pisti.resources.Res;

/**
 * Class that provides methods for vibration.
 */
public class VibrationUtils {

    public void vibratePlayerTurnToMove() {
        Gdx.input.vibrate(Res.values.MOVE_TURN_VIBRATION_DURATION);
    }

    public void vibratePistiScores() {
        Gdx.input.vibrate(new long[]{0,
                Res.values.PISTI_VIBRATION_DURATION,
                Res.values.PISTI_VIBRATION_DURATION,
                Res.values.PISTI_VIBRATION_DURATION}, -1);
    }

    public void vibrateSuperPistiScores() {
        Gdx.input.vibrate(new long[]{0,
                Res.values.SUPER_PISTI_VIBRATION_DURATION,
                Res.values.SUPER_PISTI_VIBRATION_DURATION,
                Res.values.SUPER_PISTI_VIBRATION_DURATION,
                Res.values.SUPER_PISTI_VIBRATION_DURATION}, -1);
    }
}