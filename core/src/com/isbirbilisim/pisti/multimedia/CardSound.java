package com.isbirbilisim.pisti.multimedia;

public enum CardSound {
    DEALING,
    INITIAL_PILE,
    LOWER_CARDS,
    SHOW_INITIAL_PILE,
    COLLECT_CARDS,
    CARDS_BONUS,
    MOVE,
    NONE
}