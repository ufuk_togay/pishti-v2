package com.isbirbilisim.pisti.multimedia;

import com.isbirbilisim.pisti.PlatformInfo;
import com.isbirbilisim.pisti.preferences.AppPreferences;

/**
 * Class for managing in-game sounds and vibration
 */
public class MultimediaManager {

    private final PlatformInfo platformInfo;
    private final VibrationUtils vibrationUtils;
    private final SoundsUtils soundsUtils;

    public MultimediaManager(final PlatformInfo platformInfo) {
        this.platformInfo = platformInfo;
        vibrationUtils = new VibrationUtils();
        soundsUtils = new SoundsUtils();
    }

    public void notifyPlayerTurnToMove() {
        if (vibrationEnabled()) {
            vibrationUtils.vibratePlayerTurnToMove();
        }
    }

    public void notifyPistiScores() {
        if (vibrationEnabled()) {
            vibrationUtils.vibratePistiScores();
        }
    }

    public void notifySuperPistiScores() {
        if (vibrationEnabled()) {
            vibrationUtils.vibrateSuperPistiScores();
        }
    }

    private boolean vibrationEnabled() {
        return platformInfo.isVibrationSupported()
                && AppPreferences.getInstance().isVibrationEnabled();
    }

    private boolean isSoundEnabled() {
        return AppPreferences.getInstance().isSoundEnabled();
    }

    public void playDeckShuffle() {
        if (isSoundEnabled()) {
            soundsUtils.playShuffleCardsSound();
        }
    }

    public void playClickSound() {
        if (isSoundEnabled()) {
            soundsUtils.playClickSound();
        }
    }

    public void playCardSound(final CardSound sound) {
        switch (sound) {
            case DEALING:
                playCardMoveSound();
                break;
            case CARDS_BONUS:
                playCardMoveSound();
                break;
            case INITIAL_PILE:
                playCardMoveSound();
                break;
            case LOWER_CARDS:
                playCardMoveSound();
                break;
            case COLLECT_CARDS:
                playCardMoveSound();
                break;
            case SHOW_INITIAL_PILE:
                playCardMoveSound();
                break;
            case MOVE:
                playCardMoveSound();
                break;
            case NONE:
                break;
        }
    }

    private void playCardMoveSound() {
        if (isSoundEnabled()) {
            soundsUtils.playCardMoveSound();
        }
    }

    public void loadSounds() {
        soundsUtils.prepareMainMenuSounds();
        soundsUtils.prepareGameScreenSounds();
    }

    public void disposeSounds() {
        soundsUtils.disposeGameScreenSounds();
        soundsUtils.disposeMainMenuSounds();
    }
}