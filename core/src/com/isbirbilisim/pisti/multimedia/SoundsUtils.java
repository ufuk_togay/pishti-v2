package com.isbirbilisim.pisti.multimedia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.isbirbilisim.pisti.resources.Res;

/**
 * Class for playing sounds.
 */
public class SoundsUtils {

    private Sound shuffleCardSound;
    private Sound buttonSound;
    private Sound cardMoveSound;

    public void prepareMainMenuSounds() {
        if (buttonSound == null) {
            buttonSound = Gdx.audio.newSound(Gdx.files.internal(Res.sounds.BUTTON_SOUND));
        }
    }

    public void disposeMainMenuSounds() {
        if (buttonSound != null) {
            buttonSound.dispose();
            buttonSound = null;
        }
    }

    public void prepareGameScreenSounds() {
        if (shuffleCardSound == null) {
            shuffleCardSound = Gdx.audio.newSound(Gdx.files.internal(Res.sounds.CARD_SHUFFLE_SOUND));
        }

        if (cardMoveSound == null) {
            cardMoveSound = Gdx.audio.newSound(Gdx.files.internal(Res.sounds.CARD_BONUS));
        }
    }

    public void disposeGameScreenSounds() {
        if (shuffleCardSound != null) {
            shuffleCardSound.dispose();
            shuffleCardSound = null;
        }

        if (cardMoveSound != null) {
            cardMoveSound.dispose();
            cardMoveSound = null;
        }
    }

    public void playShuffleCardsSound() {
        shuffleCardSound.play();
    }

    public void playClickSound() {
        buttonSound.play(0.3F);
    }

    public void playCardMoveSound() {
        /**
         * This sound is performed by multiple cards at the same time, so we
         * are canceling sound and sound will be performed only once.
         */
        cardMoveSound.stop();
        cardMoveSound.play(0.3F);
    }
}