package com.isbirbilisim.pisti.purchases;

import java.util.List;

public interface OnAvailableItemsUpdatesListener {

    /**
     * Called when items available for purchases have been updated.
     *
     * @param items - available for purchase items.
     */
    void onAvailableItemsUpdated(final List<Item> items);

    /**
     * Called when service is unavailable;
     */
    void onServiceUnavailable();
}