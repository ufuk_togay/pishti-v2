package com.isbirbilisim.pisti.purchases;

/**
 * Item that can be purchased in game.
 */
public class Item {
    private final String productId;
    private final String type;
    private final String price;
    private final String priceAmountMicros;
    private final String priceCurrencyCode;
    private final String title;
    private final String description;

    public Item(final String productId,
                final String type,
                final String price,
                final String priceAmountMicros,
                final String priceCurrencyCode,
                final String title,
                final String description) {
        this.productId = productId;
        this.type = type;
        this.price = price;
        this.priceAmountMicros = priceAmountMicros;
        this.priceCurrencyCode = priceCurrencyCode;
        this.title = title;
        this.description = description;
    }

    public String getProductId() {
        return productId;
    }

    public String getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPriceAmountMicros() {
        return priceAmountMicros;
    }

    public String getPriceCurrencyCode() {
        return priceCurrencyCode;
    }
}