package com.isbirbilisim.pisti.purchases;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Class that describes response about item purchase. This purchase should be verified on server.
 */
public class ItemPurchase implements Json.Serializable {

    private String dataSignature;
    private String orderId;
    private String packageName;
    private String productId;
    private String purchaseTime;
    private String purchaseState;
    private String developerPayload;
    private String purchaseToken;

    public ItemPurchase(final String dataSignature,
                        final String orderId,
                        final String packageName,
                        final String productId,
                        final String purchaseTime,
                        final String purchaseState,
                        final String developerPayload,
                        final String purchaseToken) {
        this.dataSignature = dataSignature;
        this.orderId = orderId;
        this.packageName = packageName;
        this.productId = productId;
        this.purchaseTime = purchaseTime;
        this.purchaseState = purchaseState;
        this.developerPayload = developerPayload;
        this.purchaseToken = purchaseToken;
    }

    public String getDataSignature() {
        return dataSignature;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getProductId() {
        return productId;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public String getPurchaseState() {
        return purchaseState;
    }

    public String getDeveloperPayload() {
        return developerPayload;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    @Override
    public void write(final Json json) {
        json.writeValue("dataSignature", dataSignature);
        json.writeValue("orderId", orderId);
        json.writeValue("packageName", packageName);
        json.writeValue("productId", productId);
        json.writeValue("purchaseTime", purchaseTime);
        json.writeValue("purchaseState", purchaseState);
        json.writeValue("developerPayload", developerPayload);
        json.writeValue("purchaseToken", purchaseToken);
    }

    @Override
    public void read(final Json json, final JsonValue jsonData) {
        dataSignature = jsonData.getString("dataSignature");
        orderId = jsonData.getString("orderId");
        packageName = jsonData.getString("packageName");
        productId = jsonData.getString("productId");
        purchaseTime = jsonData.getString("purchaseTime");
        purchaseState = jsonData.getString("purchaseState");
        developerPayload = jsonData.getString("developerPayload");
        purchaseToken = jsonData.getString("purchaseToken");
    }
}