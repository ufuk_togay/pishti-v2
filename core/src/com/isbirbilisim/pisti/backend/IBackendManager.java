package com.isbirbilisim.pisti.backend;

/**
 * Created by igor on 13.12.14.
 */
public interface IBackendManager {

    String API_KEY = "82898adf55abc7c156a0c0029382cd431cb43ec707f61e4fafaa8efdf21beb45";
    String SECRET_KEY = "32f810cc483a634ae200d1ecccc64dda901f58d21d66eb31c8a9c4f7e4f41f25";
    String DBName = "PistiDB";
    String USER_ID_KEY = "userID";

    StatisticCollector getStatisticCollector();

    GameStorageService getGameStorageService();
}
