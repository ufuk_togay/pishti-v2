package com.isbirbilisim.pisti.backend;

/**
 * Created by igor on 13.12.14.
 */
public interface GameStorageService {

    static final int STORAGE_SERVICE_DATA_NOT_FOUND = 2601;
    static final int STORAGE_SERVICE_BAD_REQUEST = 2606;
    static final int STORAGE_SERVICE_NOT_AUTHORIZED = 1401;
    static final int STORAGE_SERVICE_SERVER_ERROR = 1500;

    void loadByKeyValue(final String collectionName,
                        final String key,
                        final String value,
                        final loadDataCallback callback);

    void saveDocument(final String collectionName,
                      final String jsonStr,
                      final String userID);

    interface loadDataCallback {
        void onDataLoaded(String jsonString);
        void onDataNotFound();
        void onFailed();
    }
}
