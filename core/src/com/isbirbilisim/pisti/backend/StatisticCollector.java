package com.isbirbilisim.pisti.backend;

import com.isbirbilisim.pisti.backend.datamodels.JsonSerializable;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;

/**
 * Created by igor on 13.12.14.
 */
public interface StatisticCollector {
    String SINGLE_PLAYER_STATISTIC = "SINGLE_PLAYER_STATISTIC";
    String MULTI_PLAYER_STATISTIC = "MULTI_PLAYER_STATISTIC";

    void saveSinglePlayerStatistic(final UserStatisticModel statisticData);

    void loadSinglePlayerStatisticData(final String userID,
                                       final StatisticLoadCallBack callBack);

    void saveMultiPlayerStatistic(final UserStatisticModel statisticData);

    void loadMultiPlayerStatisticData(final String userID,
                                      final StatisticLoadCallBack callBack);

    interface StatisticLoadCallBack {

        void onStatisticLoaded(final JsonSerializable modelObject);

        void onNoStatistic();

        void onFailed();
    }
}
