package com.isbirbilisim.pisti.backend;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.isbirbilisim.pisti.backend.datamodels.CoinsDataModel;

/**
 * Created by igor on 07.01.15.
 */
public class CoinsManager {

    private static final String PLAYER_COINS = "PLAYER_COINS";

    private static CoinsManager instance;
    protected IBackendManager mBackendManager;
    private Json jsonProcessor;

    private long mCoins;

    public static void createInstance(final IBackendManager backendManager) {
        if (instance == null) {
            instance = new CoinsManager(backendManager);
        }
    }

    public static CoinsManager getInstance() {
        if (instance == null) {
            throw new RuntimeException("The coins manager is not properly initialized!");
        }
        return instance;
    }

    private CoinsManager(final IBackendManager backendManager) {
        mBackendManager = backendManager;
        jsonProcessor = new Json(JsonWriter.OutputType.json);
    }

    public void loadCoinsData(final String userId,
                              final CoinsManagerCallBack callback) {
        mBackendManager.getGameStorageService().loadByKeyValue(PLAYER_COINS,
                IBackendManager.USER_ID_KEY,
                userId,
                new GameStorageService.loadDataCallback() {
                    @Override
                    public void onDataLoaded(final String jsonString) {
                        CoinsDataModel data = jsonProcessor.fromJson(
                                CoinsDataModel.class, jsonString);
                        mCoins = data.coins;
                        callback.onComplete(mCoins);
                    }

                    @Override
                    public void onDataNotFound() {
                        mCoins = 0;
                        callback.onFirstRun();
                    }

                    @Override
                    public void onFailed() {
                        callback.onFail();
                    }
                });
    }

    public void saveData(final String userId, final long coins) {
        CoinsDataModel data = new CoinsDataModel();
        data.coins = coins;
        data.userID = userId;
        mBackendManager.getGameStorageService().saveDocument(PLAYER_COINS,
                jsonProcessor.toJson(data), userId);
    }

    public long getCoins() {
        return mCoins;
    }

    public interface CoinsManagerCallBack {
        void onComplete(long coins);

        void onFirstRun();

        void onFail();
    }
}
