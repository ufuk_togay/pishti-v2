package com.isbirbilisim.pisti.backend.datamodels;


/**
 * Created by igor on 13.12.14.
 */
public class UserStatisticModel implements JsonSerializable {
    private String userID;
    private long completedGamesCount;
    private long abandonedGamesCount;
    private long WinGamesCount;
    private long lostGameCount;
    private long drawGamesCount;
    private long pistiScoresCount;
    private long superPistiScoresCount;
    private long totalScoresCount;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getCompletedGamesCount() {
        return completedGamesCount;
    }

    public void setCompletedGamesCount(long completedGamesCount) {
        this.completedGamesCount = completedGamesCount;
    }

    public long getAbandonedGamesCount() {
        return abandonedGamesCount;
    }

    public void setAbandonedGamesCount(long abandonedGamesCount) {
        this.abandonedGamesCount = abandonedGamesCount;
    }

    public long getWinGamesCount() {
        return WinGamesCount;
    }

    public void setWinGamesCount(long winGamesCount) {
        WinGamesCount = winGamesCount;
    }

    public long getLostGameCount() {
        return lostGameCount;
    }

    public void setLostGameCount(long lostGameCount) {
        this.lostGameCount = lostGameCount;
    }

    public long getDrawGamesCount() {
        return drawGamesCount;
    }

    public void setDrawGamesCount(long drawGamesCount) {
        this.drawGamesCount = drawGamesCount;
    }

    public long getPistiScoresCount() {
        return pistiScoresCount;
    }

    public void setPistiScoresCount(long pistiScoresCount) {
        this.pistiScoresCount = pistiScoresCount;
    }

    public long getSuperPistiScoresCount() {
        return superPistiScoresCount;
    }

    public void setSuperPistiScoresCount(long superPistiScoresCount) {
        this.superPistiScoresCount = superPistiScoresCount;
    }

    public long getTotalScoresCount() {
        return totalScoresCount;
    }

    public void setTotalScoresCount(long totalScoresCount) {
        this.totalScoresCount = totalScoresCount;
    }

    @Override
    public String toString() {
        return "UserStatisticModel{" +
                "userID='" + userID + '\'' +
                ", completedGamesCount=" + completedGamesCount +
                ", abandonedGamesCount=" + abandonedGamesCount +
                ", WinGamesCount=" + WinGamesCount +
                ", lostGameCount=" + lostGameCount +
                ", drawGamesCount=" + drawGamesCount +
                ", pistiScoresCount=" + pistiScoresCount +
                ", superPistiScoresCount=" + superPistiScoresCount +
                ", totalScoresCount=" + totalScoresCount +
                '}';
    }
}
