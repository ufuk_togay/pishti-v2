package com.isbirbilisim.pisti.backend;

import com.isbirbilisim.pisti.backend.datamodels.JsonSerializable;
import com.isbirbilisim.pisti.backend.datamodels.UserStatisticModel;
import com.isbirbilisim.pisti.game.GameResult;
import com.isbirbilisim.pisti.game.MatchResult;
import com.isbirbilisim.pisti.players.Player;
import com.isbirbilisim.pisti.scores.Scores;

/**
 * Created by igor on 24.12.14.
 */
public class StatisticManager {

    private static StatisticManager instance;

    protected IBackendManager mBackendManager;
    protected StatisticCollector mStatisticCollector;

    private UserStatisticModel lastLoadedStats;

    public static void createInstance(final IBackendManager backendManager) {
        if (instance == null) {
            instance = new StatisticManager(backendManager);
        }
    }

    public static StatisticManager getInstance() {
        if (instance == null) {
            throw new RuntimeException("The statistic manager is not properly initialized!");
        }
        return instance;
    }

    private StatisticManager(final IBackendManager backendManager) {
        mBackendManager = backendManager;
        mStatisticCollector = mBackendManager.getStatisticCollector();
    }

    public void loadSinglePlayerStatistic(final String userId,
                                          final DataLoadCallback callback) {
        if (lastLoadedStats != null && lastLoadedStats.getUserID().equals(userId)) {
            callback.onDataLoaded(lastLoadedStats);
        }

        mStatisticCollector.loadSinglePlayerStatisticData(
                userId,
                new StatisticCollector.StatisticLoadCallBack() {
                    @Override
                    public void onStatisticLoaded(final JsonSerializable modelObject) {
                        lastLoadedStats = (UserStatisticModel) modelObject;

                        callback.onDataLoaded(lastLoadedStats);
                    }

                    @Override
                    public void onNoStatistic() {
                        callback.onDataLoaded(new UserStatisticModel());
                    }

                    @Override
                    public void onFailed() {
                        callback.onFail();
                    }
                });
    }

    public void sendSinglePlayerGameResults(final MatchResult gameResult,
                                            final Player currentPlayer) {
        lastLoadedStats = null;

        final boolean isWin = (gameResult.getGameResult() == GameResult.WIN1);
        final Scores scores = currentPlayer.getScores();
        final String userId = currentPlayer.getUserInfo().getWarpId();

        mStatisticCollector.loadSinglePlayerStatisticData(
                userId,
                new StatisticCollector.StatisticLoadCallBack() {
                    @Override
                    public void onStatisticLoaded(final JsonSerializable modelObject) {
                        UserStatisticModel userStatistic = (UserStatisticModel) modelObject;
                        userStatistic.setPistiScoresCount(userStatistic.getPistiScoresCount() + scores.getPistiScore());
                        userStatistic.setSuperPistiScoresCount(userStatistic.getSuperPistiScoresCount() + scores.getSuperPistiScore());
                        userStatistic.setTotalScoresCount(userStatistic.getTotalScoresCount() + scores.getTotalScore());
                        if (isWin) {
                            userStatistic.setWinGamesCount(userStatistic.getWinGamesCount() + 1);
                        } else {
                            userStatistic.setLostGameCount(userStatistic.getLostGameCount() + 1);
                        }
                        userStatistic.setCompletedGamesCount(userStatistic.getCompletedGamesCount() + 1);
                        userStatistic.setUserID(userId);
                        mStatisticCollector.saveSinglePlayerStatistic(userStatistic);
                    }

                    @Override
                    public void onNoStatistic() {
                        UserStatisticModel userStatistic = new UserStatisticModel();
                        userStatistic.setPistiScoresCount(scores.getPistiScore());
                        userStatistic.setSuperPistiScoresCount(scores.getSuperPistiScore());
                        userStatistic.setTotalScoresCount(scores.getTotalScore());
                        if (isWin) {
                            userStatistic.setWinGamesCount(1);
                        } else {
                            userStatistic.setLostGameCount(1);
                        }
                        userStatistic.setCompletedGamesCount(1);
                        userStatistic.setUserID(userId);
                        mStatisticCollector.saveSinglePlayerStatistic(userStatistic);
                    }

                    @Override
                    public void onFailed() {
                        System.err.println("sendSinglePlayerGameResults(): Statistic loading failed.");
                    }
                });
    }

    public void loadMultiPlayerStatistic(final String userId,
                                         final DataLoadCallback callback) {
        mStatisticCollector.loadMultiPlayerStatisticData(
                userId,
                new StatisticCollector.StatisticLoadCallBack() {
                    @Override
                    public void onStatisticLoaded(final JsonSerializable modelObject) {
                        callback.onDataLoaded((UserStatisticModel) modelObject);
                    }

                    @Override
                    public void onNoStatistic() {
                        callback.onDataLoaded(new UserStatisticModel());
                    }

                    @Override
                    public void onFailed() {
                        callback.onFail();
                    }
                });
    }

    public void sendMultiPlayerGameResults(final MatchResult gameResult,
                                           final Player currentPlayer) {
        final boolean isWin = (gameResult.getGameResult() == GameResult.WIN1);
        final Scores scores = currentPlayer.getScores();
        final String userId = currentPlayer.getUserInfo().getWarpId();

        mStatisticCollector.loadMultiPlayerStatisticData(
                userId,
                new StatisticCollector.StatisticLoadCallBack() {
                    @Override
                    public void onStatisticLoaded(final JsonSerializable modelObject) {
                        UserStatisticModel userStatistic = (UserStatisticModel) modelObject;
                        userStatistic.setPistiScoresCount(userStatistic.getPistiScoresCount() + scores.getPistiScore());
                        userStatistic.setSuperPistiScoresCount(userStatistic.getSuperPistiScoresCount() + scores.getSuperPistiScore());
                        userStatistic.setTotalScoresCount(userStatistic.getTotalScoresCount() + scores.getTotalScore());
                        if (isWin) {
                            userStatistic.setWinGamesCount(userStatistic.getWinGamesCount() + 1);
                        } else {
                            userStatistic.setLostGameCount(userStatistic.getLostGameCount() + 1);
                        }
                        userStatistic.setCompletedGamesCount(userStatistic.getCompletedGamesCount() + 1);
                        userStatistic.setUserID(userId);
                        mStatisticCollector.saveMultiPlayerStatistic(userStatistic);
                    }

                    @Override
                    public void onNoStatistic() {
                        UserStatisticModel userStatistic = new UserStatisticModel();
                        userStatistic.setPistiScoresCount(scores.getPistiScore());
                        userStatistic.setSuperPistiScoresCount(scores.getSuperPistiScore());
                        userStatistic.setTotalScoresCount(scores.getTotalScore());
                        if (isWin) {
                            userStatistic.setWinGamesCount(1);
                        } else {
                            userStatistic.setLostGameCount(1);
                        }
                        userStatistic.setCompletedGamesCount(1);
                        userStatistic.setUserID(userId);
                        mStatisticCollector.saveMultiPlayerStatistic(userStatistic);
                    }

                    @Override
                    public void onFailed() {
                        System.err.println("sendMultiPlayerGameResults(): Statistic loading failed.");
                    }
                });
    }

    public IBackendManager getBackendManager() {
        return mBackendManager;
    }

    public interface DataLoadCallback {

        void onDataLoaded(final UserStatisticModel userStatistic);

        void onFail();
    }
}